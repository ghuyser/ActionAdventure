// AAServer.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <stdio.h>
#include <stdarg.h>

#include <chrono>

#include <Server.h>

int main()
{
	Net::Server *server = new Net::Server();
	server->Start();

	while( server->IsRunning() ) {
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
}


#if defined _WIN32 && defined _DEBUG
void __winlog(const char *file, int line, const char *fmt, ...)
{
	static char buf[1024];

	va_list args;
	va_start(args, fmt);
	vsprintf(buf, fmt, args);
	va_end(args);

	if(file != NULL) {
		printf("[%s:%d] %s\n", file, line, buf);
	} else {
		printf("%s\n", buf);
	}
}
#endif

#if (defined __linux && !defined ANDROID) && defined _DEBUG
void __linuxlog(FILE *stream, const char *file, int line, const char *fmt, ...)
{
	static char buf[1024];

	va_list args;
	va_start(args, fmt);
	vsprintf(buf, fmt, args);
	va_end(args);

	if(file != NULL) {
		fprintf(stream, "[%s:%d] %s\n", file, line, buf);
	} else {
		fprintf(stream, "%s\n", buf);
	}
}
#endif
