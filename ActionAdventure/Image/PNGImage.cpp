#include "PNGImage.h"

#include "lodepng.h"

PNGImage::PNGImage()
{
	image = NULL;
	imagesize = 0;
	ignore_alpha = 0;
}

PNGImage::PNGImage(const char *filename)
{
	image = NULL;
	imagesize = 0;
	ignore_alpha = 0;
	LoadFromFile(filename);
}

PNGImage::~PNGImage()
{
	if( image ) {
		free(image);
	}
}

int PNGImage::LoadFromFile(const char *fileName)
{
	FILE *png = fopen(fileName, "rb");
	if( !png )
		return -1;

	fseek(png, 0, SEEK_END);
	size_t size_bytes = ftell(png);
	fseek(png, 0, SEEK_SET);
	unsigned char *PNGdata = (unsigned char*)malloc(size_bytes);
	if( !PNGdata ) {
		fclose(png);
		return -2;
	}
	fread(PNGdata, 1, size_bytes, png);
	fclose(png);

	LoadFromMemory(PNGdata, size_bytes);
	free(PNGdata);

	return 0;
}

int PNGImage::LoadFromMemory(const void *buffer, long bufferSize)
{
	LodePNG_Decoder decoder;

	LodePNG_Decoder_init(&decoder);
	LodePNG_Decoder_decode(&decoder, &image, &imagesize, (const unsigned char*)buffer, bufferSize);

	size_t rawbpp = decoder.infoRaw.color.bitDepth;
	if( decoder.infoRaw.color.colorType == 2 || decoder.infoRaw.color.colorType == 6 )
		rawbpp *= 3;

	ignore_alpha = 0;
	if( decoder.infoRaw.color.colorType > 3 ) {
		ignore_alpha = 1;
		rawbpp += decoder.infoRaw.color.bitDepth;
	}

	depth = rawbpp / 8;
	if( rawbpp % 8 > 0 ) {
		// break, cause i dont want to handle odd bits D:
		free(image);
		image = NULL;
		LodePNG_Decoder_cleanup(&decoder);
		return -3;
	}

	width = decoder.infoPng.width;
	height = decoder.infoPng.height;

	LodePNG_Decoder_cleanup(&decoder);

	return 0;
}

int PNGImage::InitGL()
{
	glGenTextures(1, &glTexture);
	glBindTexture(GL_TEXTURE_2D, glTexture);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);

	return 0;
}

int PNGImage::IgnoreAlpha() const
{
	return ignore_alpha;
}
