#pragma once

#include "opengl.h"

#include <string>

class Image {
public:
	Image() {
		glName = 0;
		glTexture = 0;
		width = 0;
		height = 0;
		depth = 0;
	};

	Image(const char *filename) {
		glName = 0;
		glTexture = 0;
		width = 0;
		height = 0;
		depth = 0;
	};

	virtual ~Image() {
		if( glTexture ) {
			glDeleteBuffers(1, &glTexture);
		}
	};

	virtual const unsigned char operator[](const int& index) const = 0;

	virtual int LoadFromFile(const char *fileName) = 0;

	int LoadFromFile(const std::string fileName) {
		return LoadFromFile(fileName.c_str());
	};

	virtual int LoadFromMemory(const void *buffer, long bufferSize) = 0;

	virtual int InitGL() = 0;

	inline void SetUniform(GLuint uniform) {
		glUniform1i(uniform, glName - GL_TEXTURE0);
	};

	inline void Bind() {
		glBindTexture(GL_TEXTURE_2D, glTexture);
	};

	GLuint GLName() {
		return glName;
	};

	void GLName(GLuint name) {
		glName = name;
	};

	GLuint GLTexture() const {
		return glTexture;
	};

	int Width() const {
		return width;
	};

	int Height() const {
		return height;
	};

	int Depth() const {
		return depth;
	};

	virtual int IgnoreAlpha() const {
		return 0;
	};

protected:
	GLuint glName, glTexture;
	int width, height, depth;
};
