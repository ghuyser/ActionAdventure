#include "Image.h"

#include <string>
#include <unordered_map>

class ImageManager
{
public:
	enum class ImageType {
		Auto,
		PNG,
	};

	static ImageManager* Instance();

	static Image* LoadImageFromFile(const char *image_name, ImageType type = ImageType::Auto);
	static Image* LoadImageFromFile(const std::string &image_name, ImageType type = ImageType::Auto);

private:
	ImageManager();
	~ImageManager();

	std::unordered_map<std::string, Image*> mImages;
};
