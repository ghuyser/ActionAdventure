#include "ImageManager.h"

#include "PNGImage.h"
#include "resources.h"

#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>

static ImageManager *instance = nullptr;

ImageManager::ImageManager()
{
	instance = this;
}

ImageManager::~ImageManager()
{
	for( auto i : mImages ) {
		delete i.second;
	}
}

ImageManager* ImageManager::Instance()
{
	if( !instance ) {
		instance = new ImageManager();
	}

	return instance;
}

Image* ImageManager::LoadImageFromFile(const char *image_name, ImageType type)
{
	return LoadImageFromFile( std::string(image_name), type );
}

Image* ImageManager::LoadImageFromFile(const std::string &image_name, ImageType type)
{
	ImageManager *im = Instance();

	std::string full_path = std::string(RESOURCE_DIR).append(image_name);

	if( im->mImages.count(full_path) > 0 ) {
		return im->mImages[full_path];
	}

	// Find the extension of the file type so we can automatically determine the image type.
	std::string extension; 
	size_t slash_idx_fw = full_path.find_last_of('/');
	size_t slash_idx_bw = full_path.find_last_of('\\');
	size_t slash_idx;
	
	// Have to do this ugly thing since we cant directly convert size_t to some int value.
	// Win   32: size_t = uint32_t    int = int32_t
	// Win   64: size_t = uint64_t    int = int32_t  <-\
	// Linux 32: size_t = uint32_t    int = int32_t     +-- Yay Windows!
	// Linux 64: size_t = uint64_t    int = int64_t  <-/
	if( slash_idx_fw == std::string::npos ) {
		slash_idx = slash_idx_bw;
	} else {
		if( slash_idx_bw == std::string::npos ) {
			slash_idx = slash_idx_fw;
		} else {
			slash_idx = slash_idx_fw > slash_idx_bw ? slash_idx_fw : slash_idx_bw;
		}
	}
	size_t extension_idx = slash_idx == std::string::npos ? full_path.find_last_of('.') : full_path.substr(slash_idx).find_last_of('.');
	if( extension_idx != std::string::npos ) {
		// A file extension exists! Get the extention string.
		extension = full_path.substr( extension_idx + 1 );
		std::transform( extension.begin(), extension.end(), extension.begin(),
			[](unsigned char c) { return std::tolower(c); } );
	}

	ImageType derived_type = type;
	if( type == ImageType::Auto ) {
		if( !extension.compare("png") ) {
			derived_type = ImageType::PNG;
		}
	}

	Image *img;

	switch( derived_type ) {
	default: // Currently only support PNG
	case ImageType::PNG:
		img = new PNGImage();
		break;
	}

	img->LoadFromFile(full_path.c_str());
	img->InitGL();

	im->mImages[full_path] = img;

	return img;
}
