#pragma once

#include "Image.h"

class PNGImage : public Image {
public:
	PNGImage();
	PNGImage(const char *filename);
	~PNGImage();

	inline virtual const unsigned char operator[](const int& index) const {
		if( index < 0 || index >= (int)imagesize ) return image[0];
		return image[index];
	};

	virtual int LoadFromFile(const char *fileName);
	virtual int LoadFromMemory(const void *buffer, long bufferSize);

	virtual int InitGL();

	virtual int IgnoreAlpha() const;

protected:
	unsigned char* image;
	size_t imagesize;
	int ignore_alpha;
};
