#pragma once

#include "Enemy.h"

#include "G3DMath.h"
#include "LuaObject.h"
#include "System/TimeProfiler.h"

class Blob : public Enemy
{
	LUA_OBJECT

public:
	Blob();
	~Blob();

	static void InitLua();

	virtual void Update(double time) override;

protected:
	virtual const char* luaHandleToString() const override;

private:

	System::TimeProfiler mLastAction;
	G3DMath::Vec2d mMoveTo;
};
