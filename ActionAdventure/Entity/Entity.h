#pragma once

#include "Renderer/RenderObject.h"

#include "G3DMath.h"
#include "LuaObject.h"
#include "Signal.h"
#include "types.h"

#define ENTITY_PROPERTY_GET(t, x) public: t x() const { return m##x; }
#define ENTITY_PROPERTY_SET(t, x) public: void x(t v) { m##x = v; }
#define ENTITY_PROPERTY_VAL(t, x) private: t m##x;

#define ENTITY_PROPERTY(t,x) \
	ENTITY_PROPERTY_GET(t, x) \
	ENTITY_PROPERTY_SET(t, x) \
	ENTITY_PROPERTY_VAL(t, x)

#define ENTITY_PROPERTY_READ_ONLY(t, x) \
	ENTITY_PROPERTY_GET(t, x) \
	ENTITY_PROPERTY_VAL(t, x)

class EntityManager;
class EntityRegister;

enum class EntityState {
	Standing,
	Walking,
	Attacking,
	Incapacitated,
	None
};

class Entity : public LuaObject {
	friend EntityManager;
	friend EntityRegister;

	LUA_OBJECT

	// Vital stats
	ENTITY_PROPERTY(uint32_t, HP)
	ENTITY_PROPERTY(uint32_t, MP)
	ENTITY_PROPERTY(uint32_t, SP)

	// Calculated stats
	ENTITY_PROPERTY_READ_ONLY(uint16_t, Attack)
	ENTITY_PROPERTY_READ_ONLY(uint16_t, Defense)

	// Attributes
	ENTITY_PROPERTY(uint8_t, Str) // Strength      | Fire
	ENTITY_PROPERTY(uint8_t, Frt) // Fortitude     | Ice
	ENTITY_PROPERTY(uint8_t, Agi) // Agility       | Wind
	ENTITY_PROPERTY(uint8_t, Dex) // Dexterity     | Water
	ENTITY_PROPERTY(uint8_t, Int) // Intelligence  | Darkness
	ENTITY_PROPERTY(uint8_t, Mnd) // Mind          | Light
	ENTITY_PROPERTY(uint8_t, Chr) // Charisma      | Earth
	ENTITY_PROPERTY(uint8_t, Luk) // Luck          | Lightning

	// Elemental affinities
	ENTITY_PROPERTY(int8_t, Fire)
	ENTITY_PROPERTY(int8_t, Ice)
	ENTITY_PROPERTY(int8_t, Wind)
	ENTITY_PROPERTY(int8_t, Water)
	ENTITY_PROPERTY(int8_t, Earth)
	ENTITY_PROPERTY(int8_t, Lightning)
	ENTITY_PROPERTY(int8_t, Light)
	ENTITY_PROPERTY(int8_t, Darkness)

	// Other attributes
	ENTITY_PROPERTY(double, MaxSpeed)

	ENTITY_PROPERTY_READ_ONLY(EntityState, State);

public:
	Entity();
	virtual ~Entity();

	Signal<G3DMath::Vec2d> AttackStart;
	Signal<> AttackFinish;
	Signal<> FacingChanged;
	Signal<> Incapacitated;
	Signal<double, double, double> Moved;
	Signal<G3DMath::Vec3d> PositionUpdated;
	Signal<EntityState> StateChanged;

	// Render object
	void Renderer(RenderObject *r);
	RenderObject* Renderer();

	// Positioning
	void Move(double dx, double dy);
	void Move(double dx, double dy, double dz);
	void Position(double x, double y);
	void Position(double x, double y, double z);
	void Position(G3DMath::Vec2d &pos);
	void Position(G3DMath::Vec3d &pos);
	G3DMath::Vec3d Position();
	double X();
	double Y();
	double Z();

	// Facing
	void Facing(G3DMath::Vec2d &direction);
	G3DMath::Vec2d Facing();

	// Entity state
	void State(EntityState s);

	virtual bool Collides(const Entity &e) const;
	virtual bool Contains(const G3DMath::Vec2d point) const;

	virtual G3DMath::Shape GetHitBox() const;

	virtual void CalcDamage(Entity *attacker);

	virtual void Update(double elapsed) {};

	//virtual bool Contains(G3DMath::Vec2d &point) = 0;
	//virtual bool Contains(G3DMath::Vec3d &point) { G3DMath::Vec2d p(point); return Contains(p); }
	//virtual bool Contains(G3DMath::Vec4d &point) { G3DMath::Vec2d p(point); return Contains(p); };

	//virtual bool Intersects(G3DMath::Line<double> &line) = 0;
	//virtual bool Intersects(G3DMath::LineSegment<double> &segment) = 0;
	//virtual bool Intersects(G3DMath::Rectangle<double> &rect) = 0;
	//virtual bool Intersects(G3DMath::Box<double> &box) = 0;
	//virtual bool Intersects(G3DMath::Circle<double> &circle) = 0;
	//virtual bool Intersects(G3DMath::Sphere<double> &sphere) = 0;

protected:
	void setVitals(uint32_t hp, uint32_t mp, uint32_t sp);
	void setAttributes(
		uint8_t str, uint8_t frt, uint8_t agi, uint8_t dex,
		uint8_t intl, uint8_t mnd, uint8_t chr, uint8_t luk
	);
	void setAffinities(
		int8_t fir, int8_t ice, int8_t wnd, int8_t wtr,
		int8_t eth, int8_t lit, int8_t lght, int8_t drk
	);

private:
	RenderObject *mRenderer;
	G3DMath::Vec3d mPosition;
	G3DMath::Vec2d mFacing;

	/****************/
	/* Lua-specific */
	/****************/
public:
	static void InitLua();

private:
	virtual const char* luaHandleToString() const override;

	LUA_FUNCTION(l_move);
	LUA_FUNCTION(l_setFacing);
	LUA_FUNCTION(l_setPosition);
	LUA_FUNCTION(l_setState);
};
