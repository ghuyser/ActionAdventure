#pragma once

#include "Entity.h"

#include "G3DMath.h"

#include <vector>

class EntityManager {
public:
	EntityManager();
	~EntityManager();

	void Add(Entity *entity);
	void Add(Entity &entity);

	void Remove(Entity *entity);
	void Remove(Entity &entity);

	void Update(double time) const;

	bool CollidesWith(Entity &e) const;

	void HandleAttack(Entity &from, G3DMath::Vec2d &pos) const;

private:
	std::vector<Entity*> mEntities;
};