#pragma once

#include "PlayerCharacter.h"

#include "Input/Joystick.h"
#include "Input/KeyboardController.h"
#include "Net/Packet.h"
#include "LuaObject.h"
#include "Signal.h"

class Player : public PlayerCharacter {
	LUA_OBJECT

public:
	Player();
	~Player();

	static void InitLua();

	Net::IState* PlayerState();

	void SetJoystick(size_t device);
	void SetKeyboardStates(uint8_t *keyStates);

	void Update(double elapsed) override;

protected:
	virtual const char* luaHandleToString() const override;

private:
	class State : public Net::IState
	{
	public:
		const uint32_t DataLength() const;
		const uint8_t* Data() const;

	private:
		static const uint32_t data_length = 8 + 8 + 8 + 2;
		uint8_t data[data_length];
	};

	void animationComplete();

	Input::Joystick mJoystick;
	Input::KeyboardController mKeyboard;

	double mLastTime;
};
