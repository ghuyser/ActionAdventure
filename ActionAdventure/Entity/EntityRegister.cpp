#include "EntityRegister.h"

#include "Entity.h"

#include "jsonobject.h"

static EntityRegister *instance = nullptr;

EntityRegister::EntityRegister()
{
	instance = this;
}

EntityRegister::~EntityRegister()
{
	for( auto i = mEntities.begin(); i != mEntities.end(); i++ ) {
		delete i->second;
	}
}

EntityRegister* EntityRegister::Instance()
{
	if( !instance )
		instance = new EntityRegister();
	return instance;
}

bool EntityRegister::CreateEntityData(const char *jsonFile)
{
	ResourceJSON *e = new ResourceJSON();
	e->Load(jsonFile);
	mEntities[(*e)["name"].GetString()] = e;
	return true;
}

void EntityRegister::InitializeEntity(const char *name, Entity &entity)
{
	std::string sname(name);

	if( mEntities.count(sname) == 0 )
		return;
	
	ResourceJSON *data = mEntities[sname];

	JSONObject &stats = (*data)["stats"];

	if( stats.hasKey("maxSpeed") ) {
		double s = stats["maxSpeed"].GetDouble();
		entity.MaxSpeed(s);
	}
	
	if( stats.hasKey("vitals") ) {
		JSONObject vitals = stats["vitals"];

		entity.setVitals(
			vitals.hasKey("HP") ? vitals["HP"].GetInteger() : 0,
			vitals.hasKey("MP") ? vitals["MP"].GetInteger() : 0,
			vitals.hasKey("SP") ? vitals["SP"].GetInteger() : 0
		);
	}

	if( stats.hasKey("attributes") ) {
		JSONObject attr = stats["attributes"];

		entity.setAttributes(
			attr.hasKey("str") ? attr["str"].GetInteger() : 0,
			attr.hasKey("frt") ? attr["frt"].GetInteger() : 0,
			attr.hasKey("agi") ? attr["agi"].GetInteger() : 0,
			attr.hasKey("dex") ? attr["dex"].GetInteger() : 0,
			attr.hasKey("int") ? attr["int"].GetInteger() : 0,
			attr.hasKey("mnd") ? attr["mnd"].GetInteger() : 0,
			attr.hasKey("chr") ? attr["chr"].GetInteger() : 0,
			attr.hasKey("luk") ? attr["luk"].GetInteger() : 0
		);
	}

	if( stats.hasKey("affinities") ) {
		JSONObject affn = stats["affinities"];

		entity.setAffinities(
			affn.hasKey("fire") ? affn["fire"].GetInteger() : 0,
			affn.hasKey("ice") ? affn["ice"].GetInteger() : 0,
			affn.hasKey("wind") ? affn["wind"].GetInteger() : 0,
			affn.hasKey("water") ? affn["water"].GetInteger() : 0,
			affn.hasKey("earth") ? affn["earth"].GetInteger() : 0,
			affn.hasKey("lightning") ? affn["lightning"].GetInteger() : 0,
			affn.hasKey("light") ? affn["light"].GetInteger() : 0,
			affn.hasKey("darkness") ? affn["darkness"].GetInteger() : 0
		);
	}
}

int EntityRegister::NumRegistered() const
{
	return mEntities.size();
}

EntityRegister::Iterator EntityRegister::Begin()
{
	return mEntities.begin();
}

EntityRegister::Iterator EntityRegister::End()
{
	return mEntities.end();
}
