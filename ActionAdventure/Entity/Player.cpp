#include "Player.h"

#include "Input/keycodes.h"
#include "logging.h"

#include "Image/ImageManager.h"
#include "Map/MapManager.h"

#define _USE_MATH_DEFINES
#include <math.h>

#include "common.h"
#include "resources.h"

#define SPRITE_NAME		"stick_sprite"

Player::Player() : PlayerCharacter()
{
	setVitals( 20, 0, 0 );
	setAttributes( 5, 4, 4, 4, 2, 2, 3, 3 );
	setAffinities( 0, 0, 0, 0, 0, 0, 0, 0 );
}

Player::~Player()
{
}

void Player::InitLua()
{
	LUA_OBJECT_TYPE_FROM(Player, Entity);
}

void Player::SetJoystick(size_t device)
{
	mJoystick.Select( device );
	mJoystick.Aquire();
}

void Player::SetKeyboardStates(uint8_t *keyStates)
{
	mKeyboard.SetKeyMemory(keyStates);
}

void Player::Update(double elapsed)
{
	using namespace Input;

	if( Entity::State() == EntityState::Attacking ) {
		return;
	}

	Map::IMap &map = *MapManager::CurrentMap();

	double dx = 0;
	double dy = 0;

	bool attack_pressed = false;

	if( mJoystick.Poll() == JOYSTICK_OK ) {
#ifdef _WIN32
		dx = (mJoystick.Axis(AXIS_LEFT_STICK_X) / 65535.0) * 2.0 - 1.0;
		dy = (mJoystick.Axis(AXIS_LEFT_STICK_Y) / 65535.0) * 2.0 - 1.0;
#else
		dx = mJoystick.Axis(AXIS_LEFT_STICK_X) / 32768.0;
		dy = mJoystick.Axis(AXIS_LEFT_STICK_Y) / 32768.0;
#endif

		if( fabs(dx) < 0.1 ) dx = 0;
		if( fabs(dy) < 0.1 ) dy = 0;

		attack_pressed = mJoystick[XBOX_A];
	}

	attack_pressed = attack_pressed || mKeyboard[XBOX_A];

	if( attack_pressed ) {
		if( Entity::State() != EntityState::Attacking ) {
			Entity::State(EntityState::Attacking);

			RenderObject *r = Renderer();
			if( r ) {
				r->AnimationComplete.connect( std::bind(&Player::animationComplete, this ) );
				r->RepeatCurrentAnimation(false);
			}

			AttackStart( Position() + Facing() );
		}
	} else {
		if( Entity::State() == EntityState::Attacking ) {
			Entity::State(EntityState::Standing);
		}
	}

	if( Entity::State() == EntityState::Standing || Entity::State() == EntityState::Walking ) {
		if( mKeyboard[DPAD_UP] ) { dx = 0; dy = -1.0; }
		if( mKeyboard[DPAD_DOWN] ) { dx = 0; dy = 1.0; }
		if( mKeyboard[DPAD_LEFT] ) { dx = -1.0; dy = 0; }
		if( mKeyboard[DPAD_RIGHT] ) { dx = 1.0; dy = 0; }

		if( dx != 0 || dy != 0 ) {
			Entity::State(EntityState::Walking);
			G3DMath::Vec2d facing(dx, dy);
			Facing(facing);

			double tps = MaxSpeed() * elapsed / 1000.0;
			dx *= tps;
			dy *= tps;

			map.GetMoveTo(Position().x, Position().y, dx, dy);

			Move(dx, dy);
		} else {
			Entity::State(EntityState::Standing);
		}
	}
}

void Player::animationComplete()
{
	if( Entity::State() == EntityState::Attacking ) {
		Renderer()->AnimationComplete.disconnect();
		Entity::State(EntityState::Standing);
		AttackFinish();
	}

}

Net::IState* Player::PlayerState()
{
	Player::State *s = new Player::State();
	Net::PlayerState *d = (Net::PlayerState*)s->Data();
	d->x_double = htond_g(X());
	d->y_double = htond_g(Y());
	d->z_double = htond_g(Z());
	//d->facing = (uint8_t)mCurrentFacing;
	d->state = (uint8_t)Entity::State();

	return s;
}

const char* Player::luaHandleToString() const
{
	return "Player";
}

const uint32_t Player::State::DataLength() const
{
	return data_length;
}

const uint8_t* Player::State::Data() const
{
	return data;
}
