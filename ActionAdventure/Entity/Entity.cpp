#include "Entity.h"

Entity::Entity()
	: LuaObject()
	, mRenderer(nullptr)
	, mMaxSpeed(12.422222) // Usain Bolt
	, mState(EntityState::Standing)
{
}

Entity::~Entity()
{
	luaHandleGC();
}

void Entity::Renderer(RenderObject *r)
{
	mRenderer = r;
	if( mRenderer ) {
		Moved.connect(
			[=](double x, double y, double z) {
				mRenderer->Move(G3DMath::Vec3d(x, y, z));
				mRenderer->SetFacing(mFacing);
			}
		);

		PositionUpdated.connect(
			std::bind( &RenderObject::SetPosition, mRenderer, std::placeholders::_1 )
		);

		StateChanged.connect(
			std::bind(&RenderObject::SetState, mRenderer, std::placeholders::_1)
		);
	}
}

RenderObject* Entity::Renderer()
{
	return mRenderer;
}

void Entity::Move(double dx, double dy)
{
	mPosition.x += dx;
	mPosition.y += dy;

	G3DMath::Vec2d f = G3DMath::Vec2d(dx, dy).unit();
	Facing( f );
	
	Moved(dx, dy, 0);
}

void Entity::Move(double dx, double dy, double dz)
{
	mPosition.x += dx;
	mPosition.y += dy;
	mPosition.z += dz;

	G3DMath::Vec2d f = G3DMath::Vec2d(dx, dy).unit();
	Facing( f );

	Moved(dx, dy, dz);
}

void Entity::Position(double x, double y)
{
	if( mPosition.x != x || mPosition.y != y ) {
		mPosition.x = x;
		mPosition.y = y;
		PositionUpdated(mPosition);
	}
}

void Entity::Position(double x, double y, double z)
{
	G3DMath::Vec3d p(x, y, z);
	if( p != mPosition ) {
		mPosition = p;
		PositionUpdated(mPosition);
	}
}

void Entity::Position(G3DMath::Vec2d &pos)
{
	if( pos != mPosition ) {
		mPosition.x = pos.x;
		mPosition.y = pos.y;
		PositionUpdated(mPosition);
	}
}

void Entity::Position(G3DMath::Vec3d &pos)
{
	if( pos != mPosition ) {
		mPosition = pos;
		PositionUpdated(mPosition);
	}
}

G3DMath::Vec3d Entity::Position()
{
	return mPosition;
}

double Entity::X()
{
	return mPosition.x;
}

double Entity::Y()
{
	return mPosition.y;
}

double Entity::Z()
{
	return mPosition.z;
}

void Entity::Facing(G3DMath::Vec2d &direction)
{
	G3DMath::Vec2d d = direction.unit();
	if( d != mFacing && d.lengthSq() != 0.0 ) {
		mFacing = d;
		FacingChanged();
	}
}

G3DMath::Vec2d Entity::Facing()
{
	return mFacing;
}

void Entity::State(EntityState s)
{
	if( mState != s ) {
		mState = s;
		StateChanged(mState);
	}
}

bool Entity::Collides(const Entity &e) const
{
	return GetHitBox().Collides(e.GetHitBox());
}

bool Entity::Contains(const G3DMath::Vec2d point) const
{
	return GetHitBox().ContainsPoint(point);
}

G3DMath::Shape Entity::GetHitBox() const
{
	return G3DMath::Rectangle<double>(
		G3DMath::Vec2d(mPosition) - G3DMath::Vec2d(0.5, 0.5),
		G3DMath::Vec2d(mPosition) + G3DMath::Vec2d(0.5, 0.5)
	);
}

void Entity::CalcDamage(Entity *attacker)
{
	uint32_t dmg = attacker->Str() / Frt();
	if(mHP <= dmg) {
		mHP = 0;
		State(EntityState::Incapacitated);
	} else {
		HP(HP() - dmg);
	}
}

void Entity::setVitals(uint32_t hp, uint32_t mp, uint32_t sp)
{
	mHP = hp;
	mMP = mp;
	mSP = sp;
}

void Entity::setAttributes(
	uint8_t str, uint8_t frt, uint8_t agi, uint8_t dex,
	uint8_t intl, uint8_t mnd, uint8_t chr, uint8_t luk
)
{
	mStr = str; mFrt = frt; mAgi = agi; mDex = dex;
	mInt = intl; mMnd = mnd; mChr = chr; mLuk = luk;
}

void Entity::setAffinities(
	int8_t fir, int8_t ice, int8_t wnd, int8_t wtr,
	int8_t eth, int8_t lit, int8_t lght, int8_t drk
)
{
	mFire = fir; mIce = ice; mWind = wnd; mWater = wtr;
	mEarth = eth; mLightning = lit; mLight = lght; mDarkness = drk;
}

void Entity::InitLua()
{
	LuaObjectType<Entity>::AddMethod("move", Entity::l_move);
	LuaObjectType<Entity>::AddMethod("setFacing", Entity::l_setFacing);
	LuaObjectType<Entity>::AddMethod("setPosition", Entity::l_setPosition);
	LuaObjectType<Entity>::AddMethod("setState", Entity::l_setState);
}


const char* Entity::luaHandleToString() const
{
	return "Entity";
}

int Entity::l_move(lua_State *L)
{
	//Lua_chkmparam(3, "x, y, z");

	Entity **p = (Entity**)lua_touserdata(L, 1);

	float x = (float)luaL_checknumber(L, 2);
	float y = (float)luaL_checknumber(L, 3);
	float z = (float)luaL_checknumber(L, 4);

	((Entity*)*p)->Move(x, y, z);

	return 0;
}

int Entity::l_setFacing(lua_State *L)
{
	//Lua_chkmparam(3, "x, y, z");

	Entity **p = (Entity**)lua_touserdata(L, 1);

	double x = (double)luaL_checknumber(L, 2);
	double y = (double)luaL_checknumber(L, 3);

	G3DMath::Vec2d f(x, y);
	((Entity*)*p)->Facing(f);

	return 0;
}

int Entity::l_setPosition(lua_State *L)
{
	//Lua_chkmparam(3, "x, y, z");

	Entity **p = (Entity**)lua_touserdata(L, 1);

	float x = (float)luaL_checknumber(L, 2);
	float y = (float)luaL_checknumber(L, 3);
	float z = (float)luaL_checknumber(L, 4);

	((Entity*)*p)->Position(x, y, z);

	return 0;
}

int Entity::l_setState(lua_State *L)
{
	//Lua_chkmparam(3, "x, y, z");

	Entity **p = (Entity**)lua_touserdata(L, 1);

	int s = (int)luaL_checknumber(L, 2);

	((Entity*)*p)->State(EntityState(s));

	return 0;
}
