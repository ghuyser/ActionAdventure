#include "EntityManager.h"

EntityManager::EntityManager()
{
}

EntityManager::~EntityManager()
{
}

void EntityManager::Add(Entity *entity)
{
	mEntities.push_back(entity);
}

void EntityManager::Add(Entity &entity)
{
	Add(&entity);
}

void EntityManager::Remove(Entity *entity)
{
	for(auto i = mEntities.begin(); i != mEntities.end(); i++) {
		if( *i == entity ) {
			mEntities.erase(i);
		}
	}
}

void EntityManager::Remove(Entity &entity)
{
	Remove(&entity);
}

void EntityManager::Update(double time) const
{
	for(auto i = mEntities.begin(); i != mEntities.end(); i++) {
		(*i)->Update(time);
	}
}

bool EntityManager::CollidesWith(Entity &e) const
{
	bool collides = false;
	for(auto i = mEntities.begin(); i != mEntities.end(); i++) {
		if( *i == &e )
			continue;

		collides |= (*i)->Collides(e);
		if( collides )
			break;
	}

	return collides;
}

void EntityManager::HandleAttack(Entity &from, G3DMath::Vec2d &pos) const
{
	for(auto i = mEntities.begin(); i != mEntities.end(); i++) {
		if( (*i)->Contains( pos ) ) {
			(*i)->CalcDamage( &from );
		}
	}
}
