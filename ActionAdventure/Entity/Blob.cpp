#include "Blob.h"

#include "EntityRegister.h"
#include "Image/ImageManager.h"
#include "resources.h"

#include <stdlib.h>

Blob::Blob() : Enemy()
{
	EntityRegister::Instance()->CreateEntityData(RESOURCE_DIR "blob_data.json");
	EntityRegister::Instance()->InitializeEntity("Blob", *this);

	mLastAction.Start();

	srand(System::TimeProfiler::CurrentIntTime());
}

Blob::~Blob()
{
}

void Blob::InitLua()
{
	LUA_OBJECT_TYPE_FROM(Blob, Entity);
}

const char* Blob::luaHandleToString() const
{
	return "Blob";
}

void Blob::Update(double time)
{
	switch( State() ) {
	default:
	case EntityState::Standing:
		// Determine what to do.
		if( mLastAction.GetIntElapsed() > 2000 ) {
			State(EntityState::Walking);
			mMoveTo.x = Position().x + (((double)rand() / (double)RAND_MAX) * 10.0 - 5.0);
			mMoveTo.y = Position().y + (((double)rand() / (double)RAND_MAX) * 10.0 - 5.0);
		}
		break;

	case EntityState::Walking:
		{
			double tps = MaxSpeed() * time / 1000.0;
			G3DMath::Vec2d vec( mMoveTo - Position() );
			double dx = vec.unit().x * tps;
			double dy = vec.unit().y * tps;
			Move( dx, dy );
			if( (mMoveTo - Position()).length() < 0.1 ) {
				State(EntityState::Standing);
				mLastAction.Start();
			}
		}
		break;

	case EntityState::Attacking:
		break;

	case EntityState::Incapacitated:
		break;
	}
}
