#pragma once

#include "ResourceJSON.h"

#include "jsonobject.h"

#include <unordered_map>

class Entity;

class EntityRegister
{
public:
	typedef std::unordered_map<std::string, ResourceJSON*>::iterator Iterator;

	EntityRegister();
	~EntityRegister();

	static EntityRegister* Instance();

	bool CreateEntityData(const char *jsonFile);

	void InitializeEntity(const char *name, Entity &entity);

	int NumRegistered() const;
	Iterator Begin();
	Iterator End();

private:
	std::unordered_map<std::string, ResourceJSON*> mEntities;
};