#include <stdio.h>
#include "Input/keycodes.h"
#include "System/Device.h"
#include "IGame.h"
#include "logging.h"

#include "Game.h"

using System::Device;

IGame* CreateGame()
{
	return new Game;
}

void DestroyGame(IGame *game)
{
	delete game;
}


unsigned char KeyCodeToAscii(int keyCode)
{
	int pos = 0;
	while( KeyCodeAsciiTable[pos][0] ) {
		if( keyCode == KeyCodeAsciiTable[pos][0] )
			return (unsigned char)(KeyCodeAsciiTable[pos][1]);
		pos++;
	}
	return 0;
}

unsigned short KeyCodeAsciiTable[][2] = {
	{IKEYCODE_0,				'0'},
	{IKEYCODE_1,				'1'},
	{IKEYCODE_2,				'2'},
	{IKEYCODE_3,				'3'},
	{IKEYCODE_4,				'4'},
	{IKEYCODE_5,				'5'},
	{IKEYCODE_6,				'6'},
	{IKEYCODE_7,				'7'},
	{IKEYCODE_8,				'8'},
	{IKEYCODE_9,				'9'},
	{IKEYCODE_STAR,				'*'},
	{IKEYCODE_POUND,			'#'},
	{IKEYCODE_A,				'A'},
	{IKEYCODE_B,				'B'},
	{IKEYCODE_C,				'C'},
	{IKEYCODE_D,				'D'},
	{IKEYCODE_E,				'E'},
	{IKEYCODE_F,				'F'},
	{IKEYCODE_G,				'G'},
	{IKEYCODE_H,				'H'},
	{IKEYCODE_I,				'I'},
	{IKEYCODE_J,				'J'},
	{IKEYCODE_K,				'K'},
	{IKEYCODE_L,				'L'},
	{IKEYCODE_M,				'M'},
	{IKEYCODE_N,				'N'},
	{IKEYCODE_O,				'O'},
	{IKEYCODE_P,				'P'},
	{IKEYCODE_Q,				'Q'},
	{IKEYCODE_R,				'R'},
	{IKEYCODE_S,				'S'},
	{IKEYCODE_T,				'T'},
	{IKEYCODE_U,				'U'},
	{IKEYCODE_V,				'V'},
	{IKEYCODE_W,				'W'},
	{IKEYCODE_X,				'X'},
	{IKEYCODE_Y,				'Y'},
	{IKEYCODE_Z,				'Z'},
	{IKEYCODE_COMMA,			','},
	{IKEYCODE_PERIOD,			'.'},
	{IKEYCODE_TAB,				'\t'},
	{IKEYCODE_SPACE,			' '},
	{IKEYCODE_GRAVE,			'`'},
	{IKEYCODE_MINUS,			'-'},
	{IKEYCODE_EQUALS,			'='},
	{IKEYCODE_LEFT_BRACKET,		'['},
	{IKEYCODE_RIGHT_BRACKET,	']'},
	{IKEYCODE_BACKSLASH,		'\\'},
	{IKEYCODE_SEMICOLON,		';'},
	{IKEYCODE_APOSTROPHE,		'\''},
	{IKEYCODE_SLASH,			'/'},
	{IKEYCODE_AT,				'@'},
	{0, 0}
};

unsigned char KeyCodeConvertShiftChar(unsigned char c)
{
	bool shift = Device::KeyboardShiftDown();
	if( Device::KeyboardCapsOn() )
		shift = !shift;

	if( c >= 'A' && c <= 'Z' && !shift ) {
		c -= ('A' - 'a');
		return c;
	}

	if( Device::KeyboardShiftDown() ) {
		switch( c ) {
		case '0': return ')';
		case '1': return '!';
		case '2': return '@';
		case '3': return '#';
		case '4': return '$';
		case '5': return '%';
		case '6': return '^';
		case '7': return '&';
		case '8': return '*';
		case '9': return '(';
		case '-': return '_';
		case '=': return '+';
		case '[': return '{';
		case ']': return '}';
		case '\\': return '|';
		case ';': return ':';
		case '\'': return '\"';
		case ',': return '<';
		case '.': return '>';
		case '/': return '?';
		case '`': return '~';
		}
	}

	return c;
}


#if defined USE_ALTERNATE_LOG
extern IGame *thegame;

void __alternate_log(int type, const char *tag, const char *file, int line, const char *fmt, ...)
{
	if( thegame == nullptr ) {
#ifdef _WIN32
#elif (defined __linux && !defined ANDROID)
#endif
		return;
	}

	va_list args;
	va_start(args, fmt);
	thegame->AddLogV((IGame::LogType)type, tag, file, line, fmt, &args);
	va_end(args);
}
#endif

#if defined _WIN32 && defined _DEBUG
void __winlog(const char *file, int line, const char *fmt, ...)
{
	static char buf[1024];
	static char buf2[1024];

	va_list args;
	va_start(args, fmt);
	vsprintf(buf, fmt, args);
	va_end(args);

	if( file != NULL ) {
		sprintf(buf2, "[%s:%d] %s\n", file, line, buf);
	} else {
		sprintf(buf2, "%s\n", buf);
	}
	OutputDebugStringA(buf2);
}
#endif

#if (defined __linux && !defined ANDROID) && defined _DEBUG
void __linuxlog(FILE *stream, const char *file, int line, const char *fmt, ...)
{
	static char buf[1024];

	va_list args;
	va_start(args, fmt);
	vsprintf(buf, fmt, args);
	va_end(args);

	if( file != NULL ) {
		fprintf(stream, "[%s:%d] %s\n", file, line, buf);
	} else {
		fprintf(stream, "%s\n", buf);
	}
}
#endif

#ifdef ANDROID
/*-
* Copyright (c) 1992, 1993
*      The Regents of the University of California.  All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 4. Neither the name of the University nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
* LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
* OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
* SUCH DAMAGE.
*
* @(#)qsort.c  8.1 (Berkeley) 6/4/93
* $FreeBSD: src/lib/libc/stdlib/qsort.c,v 1.15 2008/01/14 09:21:34 das Exp $
* $DragonFly: src/lib/libc/stdlib/qsort.c,v 1.5 2005/11/20 12:37:49 swildner Exp $
*/

#include <stdlib.h>

typedef int              cmp_t(void *, const void *, const void *);
static inline char      *med3(char *, char *, char *, cmp_t *, void *);
static inline void       swapfunc(char *, char *, int, int);

#define min(a, b)       (a) < (b) ? a : b

/*
* Qsort routine from Bentley & McIlroy's "Engineering a Sort Function".
*/
#define swapcode(TYPE, parmi, parmj, n) {               \
        long i = (n) / sizeof (TYPE);                   \
        TYPE *pi = (TYPE *) (parmi);            \
        TYPE *pj = (TYPE *) (parmj);            \
        do {                                            \
                TYPE    t = *pi;                \
                *pi++ = *pj;                            \
                *pj++ = t;                              \
        } while (--i > 0);                              \
}

#define SWAPINIT(a, es) swaptype = ((char *)a - (char *)0) % sizeof(long) || \
        es % sizeof(long) ? 2 : es == sizeof(long)? 0 : 1;

static inline void
swapfunc(char *a, char *b, int n, int swaptype)
{
	if( swaptype <= 1 )
		swapcode(long, a, b, n)
	else
		swapcode(char, a, b, n)
}

#define swap(a, b)                                      \
        if (swaptype == 0) {                            \
                long t = *(long *)(a);                  \
                *(long *)(a) = *(long *)(b);            \
                *(long *)(b) = t;                       \
        } else                                          \
                swapfunc(a, b, es, swaptype)

#define vecswap(a, b, n)        if ((n) > 0) swapfunc(a, b, n, swaptype)

#define CMP(t, x, y) (cmp((t), (x), (y)))

static inline char *
med3(char *a, char *b, char *c, cmp_t *cmp, void *thunk)
{
	return CMP(thunk, a, b) < 0 ?
		(CMP(thunk, b, c) < 0 ? b : (CMP(thunk, a, c) < 0 ? c : a))
		: (CMP(thunk, b, c) > 0 ? b : (CMP(thunk, a, c) < 0 ? a : c));
}

void qsort_r(void *a, size_t n, size_t es, cmp_t *cmp, void *thunk)
{
	char *pa, *pb, *pc, *pd, *pl, *pm, *pn;
	size_t d, r;
	int cmp_result;
	int swaptype, swap_cnt;

loop:   SWAPINIT(a, es);
	swap_cnt = 0;
	if( n < 7 ) {
		for( pm = (char *)a + es; pm < (char *)a + n * es; pm += es )
			for( pl = pm;
				pl >(char *)a && CMP(thunk, pl - es, pl) > 0;
				pl -= es )
				swap(pl, pl - es);
		return;
	}
	pm = (char *)a + (n / 2) * es;
	if( n > 7 ) {
		pl = (char*)a;
		pn = (char *)a + (n - 1) * es;
		if( n > 40 ) {
			d = (n / 8) * es;
			pl = med3(pl, pl + d, pl + 2 * d, cmp, thunk);
			pm = med3(pm - d, pm, pm + d, cmp, thunk);
			pn = med3(pn - 2 * d, pn - d, pn, cmp, thunk);
		}
		pm = med3(pl, pm, pn, cmp, thunk);
	}
	swap((char*)a, pm);
	pa = pb = (char *)a + es;

	pc = pd = (char *)a + (n - 1) * es;
	for( ;;) {
		while( pb <= pc && (cmp_result = CMP(thunk, pb, a)) <= 0 ) {
			if( cmp_result == 0 ) {
				swap_cnt = 1;
				swap(pa, pb);
				pa += es;
			}
			pb += es;
		}
		while( pb <= pc && (cmp_result = CMP(thunk, pc, a)) >= 0 ) {
			if( cmp_result == 0 ) {
				swap_cnt = 1;
				swap(pc, pd);
				pd -= es;
			}
			pc -= es;
		}
		if( pb > pc )
			break;
		swap(pb, pc);
		swap_cnt = 1;
		pb += es;
		pc -= es;
	}
	if( swap_cnt == 0 ) {  /* Switch to insertion sort */
		for( pm = (char *)a + es; pm < (char *)a + n * es; pm += es )
			for( pl = pm;
				pl >(char *)a && CMP(thunk, pl - es, pl) > 0;
				pl -= es )
				swap(pl, pl - es);
		return;
	}

	pn = (char *)a + n * es;
	r = min(pa - (char *)a, pb - pa);
	vecswap((char*)a, pb - r, r);
	r = min(pd - pc, pn - pd - es);
	vecswap(pb, pn - r, r);
	if( (r = pb - pa) > es )
		qsort_r(a, r / es, es, cmp, thunk);
	if( (r = pd - pc) > es ) {
		/* Iterate rather than recurse to save stack space */
		a = pn - r;
		n = r / es;
		goto loop;
	}
}
#endif
