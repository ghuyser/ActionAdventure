#include "Game.h"

#include "Input/keycodes.h"
#include "Input/input.h"
#include "Net/Client.h"
#include "Net/Server.h"
#include "System/Device.h"
#include "common.h"
#include "logging.h"

#include "actionadventure.h"
#include "resources.h"

#include "Image/Image.h"
#include "Image/ImageManager.h"
#include "Map/MapManager.h"

#include "Entity/Entity.h"
#include "Entity/PlayerCharacter.h"
#include "Entity/Enemy.h"
#include "Entity/Player.h"
#include "Entity/Blob.h"
#include "Renderer/RenderObject.h"
#include "Renderer/Sprite.h"

#include "DevTools/ImGUIElements.h"

#include "imgui.h"

#include <stdio.h>

#define GAME_WAITFLAG_OK        1
#define GAME_WAITFLAG_TIMEOUT   2
#define GAME_WAITFLAG_ERROR     0

#define GAME_LUAFLG_EXECUTE     0x00000001
#define GAME_LUAFLG_SCHEDADD    0x00000002
#define GAME_LUAFLG_EVENT       0x00000004
#define GAME_LUAFLG_WAITFUNC    0x00000008
#define GAME_LUAFLG_EXIT        0x80000000
#define GAME_LUAFLG_ALL         0xffffffff

constexpr unsigned int UnitSizeInPixels = 32;

Game::Game()
	: mClient(nullptr)
	, mServer(nullptr)
	, mGuiAppMenu(false)
	, mGuiDemoWindow(false)
	, mGuiLuaStateWindow(false)
	, mGuiLuaStackWindow(false)
	, mPixelScale(2.0)
{
	memcpy(mServerAddr, "127.0.0.1", 10);
	mServerPort = Net::DEFAULT_PORT;

	memset(mPressedKeys, 0, 256);

	mLua = NULL;

	mLuaCommandThread = nullptr;
	mLuaConsole = new ImGui::Console();
	gui_add_window("Console", mLuaConsole, false);

	gui_add_window("Log", new ImGui::LogWindow("General Log"), false);
	gui_add_window("SLog", new ImGui::LogWindow("Server Log"), false);
	gui_add_window("CLog", new ImGui::LogWindow("Client Log"), false);

	gui_add_window("CSetup", new ImGui::ClientSetup(&mClient, mServerAddr, 256, &mServerPort), false);
	gui_add_window("SStats", new ImGui::ServerStats(&mServer), false);

	gui_add_window("Render", new ImGui::RenderStats(), false);

	gui_add_window("Entity", new ImGui::EntityWindow(), false);

	mRenderWindow = static_cast<ImGui::RenderStats*>(mGuiWindows["Render"].second);
}

Game::~Game()
{
	if( mClient ) {
		mClient->Disconnect();
		while( mClient->CurrentState() != Net::Client::State::NoConnection ) {
			std::this_thread::sleep_for( std::chrono::milliseconds(100) );
		}
		delete mClient;
	}

	if( mServer ) {
		delete mServer;
	}

	//delete mPlayer->SpriteSheet();
	mPlayer->FreeLuaSelfReference();
	mPlayer = nullptr;

	if(mLuaCommandThread) {
		mLuaCommandThread->join();
		delete mLuaCommandThread;
	}
	set_flags(mLuaThread, GAME_LUAFLG_EXIT);
	mLuaThread.thread->join();

	lua_close(mLua);
	mLua = NULL;
}

void Game::InitLua()
{
	LuaObjectType<Game>::AddMethod("getPlayer", Game::lg_getplayer);
}

int Game::InitGL()
{
	using System::Device;

#ifdef _WIN32
	initGLExt();
#endif

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	//glFrontFace(GL_CW);
	//glEnable(GL_CULL_FACE);
	//glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
#if defined _WIN32 || (defined __linux && !defined ANDROID)
	glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
#endif

	mSpriteRenderer.LoadGLProgram(RESOURCE_DIR "shaders/sprite_shader.glsl");

	mView.setIdentity();
	mProj.setIdentity();

	mUnitWidth = Device::GetDPWidth() / (double)(UnitSizeInPixels * mPixelScale);
	mUnitHeight = Device::GetDPHeight() / (double)(UnitSizeInPixels * mPixelScale);

	mProj.ortho(0, (float)mUnitWidth, (float)mUnitHeight, 0, -1, 10);

	mSpriteRenderer.UpdateMatrices(mProj, mView);

	return 0;
}

int Game::InitGame()
{
	using namespace std::placeholders;

	initializeLua();

	Map::IMap *map = MapManager::LoadFromJSON("maps/first_map.json");
	map->InitGL();
	map->SetProjection(mProj);
	map->SetView(mView, false);

	mPlayer = LUA_CREATE_OBJECT(mLua, Player);
	if( !mPlayer ) {
		const char *err = luaL_checkstring(mLua, -1);
		LOGE(err);
		return -1;
	}
	mPlayer->SetJoystick(0);
	mPlayer->SetKeyboardStates(mPressedKeys);
	mPlayer->CreateLuaSelfReference(mLua, -1);
	lua_pop(mLua, 1);
	mPlayer->CreateLuaSelfReference(mLua, 1);
	mPlayer->Moved.connect( std::bind(&Game::MoveCamera, this, _1, _2, _3) );
	mPlayer->AttackStart.connect( [&](G3DMath::Vec2d pos) {
		mEntityManager.HandleAttack( *mPlayer, pos );
	});
	mEntityManager.Add(mPlayer);
	Sprite *playerSprite = new Sprite();
	playerSprite->SpriteSheet(
		ImageManager::LoadImageFromFile("sprites/stick_sprite.png")
	);
	playerSprite->LoadAnimation(
		RESOURCE_DIR "sprites/stick_sprite.json"
	);
	mPlayer->Renderer(playerSprite);
	mSpriteRenderer.Add(playerSprite);

	const int numBlobs = 100;
	const int blobSquare = (int)ceil(sqrt(numBlobs));
	for(int i = 0; i < numBlobs; i++) {
		Blob *blob = LUA_CREATE_OBJECT(mLua, Blob);
		Sprite *blobSprite = new Sprite();
		blobSprite->SpriteSheet(
			ImageManager::LoadImageFromFile("sprites/enemy_1.png")
		);
		blobSprite->LoadAnimation(
			RESOURCE_DIR "sprites/blob.json"
		);
		blob->Renderer(blobSprite);
		blob->Position(10 + (i % blobSquare), 10 + (i / blobSquare));
		mEntityManager.Add(blob);
		mSpriteRenderer.Add(blobSprite);
	}

	if( map->NumEntryPoints() > 0 ) {
		const G3DMath::Vec2d &entry = map->GetEntryPoint(0);
		movePlayer(entry.x, entry.y);
	} else {
		movePlayer( mUnitWidth / 2.0, mUnitHeight / 2.0 );
	}

	mGameTimer.Start();
	mUpdateTimer.Start();
	mFPSTimer.Start();
	mFPS = 0;

	return 0;
}

void Game::initializeLua()
{
	LUA_OBJECT_TYPE(Game);
	LUA_OBJECT_TYPE(Entity);
	LUA_OBJECT_TYPE(Player);
	LUA_OBJECT_TYPE(Blob);

	mLua = luaL_newstate();
	static_cast<ImGui::Console*>(mGuiWindows["Console"].second)->SetLuaExecuteCommand(
		[&] (const char *command) {
			char *cmd = (char*)malloc(strlen(command) + 1);
			strcpy(cmd, command);
			if(mLuaCommandThread) {
				mLuaCommandThread->join();
				delete mLuaCommandThread;
			}
			mLuaCommandThread = new std::thread(&Game::luaAddCommand, this, cmd);
		}
	);
	luaL_openlibs(mLua);

	// Global functions
	luaL_Reg funcs[] = {
		{"print", Game::l_print},
		{"sleep", Game::l_sleep},
		{NULL, NULL}
	};


	// Load functions into lua
	luaL_Reg *f = funcs;
	while(f->name != NULL) {
		lua_pushcfunction(mLua, f->func);
		lua_setglobal(mLua, f->name);
		f++;
	}

	LuaObjectType<Game>::Init(mLua);
	//LuaObjectType<Sprite>::InitMetatable(mLua);
	//LuaObjectType<Entity>::Init(mLua);
	LuaObjectType<Player>::Init(mLua);
	LuaObjectType<Blob>::Init(mLua);

	// Set the global "__game" variable to `this`
	lua_pushlightuserdata(mLua, this);
	lua_setglobal(mLua, "__game");

	// Set up lua threads based on the main lua state
	mLuaThreads[0] = lua_newthread(mLua);
	mLuaThreads[1] = lua_newthread(mLua);

	mLuaThread.thread = new std::thread(&Game::luaThread, this);
}

int Game::wait_flags(Thread &t, uint32_t *oflg, uint32_t mask)
{
	if(oflg == nullptr)
		return GAME_WAITFLAG_ERROR;

	std::unique_lock<std::mutex> lock(t.crit);
	while(!(t.flags & mask)) {
		t.cond.wait(lock);
	}
	*oflg = t.flags & mask;
	t.flags &= ~mask;
	return GAME_WAITFLAG_OK;
}

int Game::wait_flags_timed(Thread &t, uint32_t *oflg, uint32_t mask, int ms)
{
	if(oflg == NULL || ms <= 0)
		return GAME_WAITFLAG_ERROR;

	std::unique_lock<std::mutex> lock(t.crit);
	std::cv_status result = t.cond.wait_for(lock, std::chrono::milliseconds(ms));
	*oflg = t.flags & mask;
	t.flags &= ~mask;
	lock.unlock();

	if(result == std::cv_status::timeout)
		return GAME_WAITFLAG_TIMEOUT;

	return GAME_WAITFLAG_OK;
}

void Game::set_flags(Thread &t, uint32_t flags)
{
	std::unique_lock<std::mutex> lock(t.crit);
	t.flags |= flags;
	t.cond.notify_one();
}

void Game::DoActions()
{
	double elapsed = mUpdateTimer.GetElapsed();
	mUpdateTimer.Start();
	mEntityManager.Update(elapsed);

	if( mClient ) {
		Net::Client::State clientState = mClient->CurrentState();
		if( clientState == Net::Client::State::Connected ) {
			mClient->SendState(mPlayer->PlayerState());
		} else if( clientState == Net::Client::State::NoConnection ) {
			delete mClient;
			mClient = nullptr;
		}
	}
}

void Game::MoveCamera(double dx, double dy, double dz)
{
	updateMapBox();

	Map::IMap &map = *MapManager::CurrentMap();

	const double halfWidth = mUnitWidth / 2.0;
	const double halfHeight = mUnitHeight / 2.0;

	// Determine if the view matrix should be updated
	if( mPlayer->X() < halfWidth ) {
		// Player is on the left edge of the map
		mView[12] = 0.0;
		dx = 0;
	} else if( mPlayer->X() > map.Width() - halfWidth) {
		// Player is on the right edge of the map
		mView[12] = (float)(-(map.Width() - mUnitWidth) / halfWidth);
		dx = 0;
	}
	if( mPlayer->Y() < halfHeight) {
		// Player is at the top of the map
		mView[13] = 0.0;
		dy = 0;
	} else if( mPlayer->Y() > map.Height() - halfHeight) {
		// Player is at the bottom of the map
		mView[13] = (float)((map.Height() - mUnitHeight) / halfHeight);
		dy = 0;
	}

	// Update the view matrix with the proper values
	mView.translateSelf((float)(-dx / halfWidth), (float)(dy / halfHeight), 0);

	// Update the sprite program's view matrix
	mSpriteRenderer.UpdateView(mView);

	// Update the map's view matrix
	map.SetView(mView);
}

// Move the player to the specified location and update the view matrix.
void Game::movePlayer(double x, double y, double z)
{
	if( mPlayer ) {
		mPlayer->Position(x, y, z);
		updateMapBox();
	}

	Map::IMap &map = *MapManager::CurrentMap();

	const double halfWidth = mUnitWidth / 2.0;
	const double halfHeight = mUnitHeight / 2.0;

	double rx = (x < halfWidth ? 0 : (x > map.Width() - halfWidth ?
		-(map.Width() - mUnitWidth) / halfWidth : x));
	double ry = (y < halfHeight ? 0 : (y > map.Height() - halfHeight ?
		(map.Height() - mUnitHeight) / halfHeight : y));

	mView[12] = (float)rx;
	mView[13] = (float)ry;

	// Update the sprite program's view matrix
	mSpriteRenderer.UpdateView(mView);

	// Update the map's view matrix
	map.SetView(mView);
}

// Update the map box based on the current view parameters.
void Game::updateMapBox()
{
	using namespace G3DMath;

	if( !mPlayer )
		return;

	const Map::IMap &map = *MapManager::CurrentMap();

	const double halfWidth = mUnitWidth / 2.0;
	const double halfHeight = mUnitHeight / 2.0;

	// Set up rendering bounding box
	mMapBox = BoundingBox<Vec2d>(
		Vec2d(mPlayer->X() - halfWidth, mPlayer->Y() - halfHeight),
		Vec2d(mPlayer->X() + halfWidth, mPlayer->Y() + halfHeight)
		);

	// Clamp rending box to the map horizontally
	if(mMapBox.low.x < 0.0) {
		mMapBox.low.x = 0.0;
		mMapBox.high.x = mUnitWidth;
	} else if(mMapBox.high.x > map.Width()) {
		mMapBox.low.x = map.Width() - mUnitWidth;
		mMapBox.high.x = map.Width();
	}

	// Clamp rending box to the map vertically
	if(mMapBox.low.y < 0.0) {
		mMapBox.low.y = 0.0;
		mMapBox.high.y = mUnitHeight;
	} else if(mMapBox.high.y > map.Height()) {
		mMapBox.low.y = map.Height() - mUnitHeight;
		mMapBox.high.y = map.Height();
	}
}

void Game::ResizeGLScene(int width, int height)
{
}

bool Game::Render()
{
	Map::IMap &map = *MapManager::CurrentMap();

	mRenderTimer.Start();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	map.Render(mMapBox, 0, (int)mPlayer->Z());

	mMutexPlayerCharacters.lock();
	for( auto pc = mPlayerCharacters.begin(); pc != mPlayerCharacters.end(); pc++ ) {
		if( pc->second == nullptr ) {
			Sprite *s = new Sprite();
			s->SetPosition(mUnitWidth / 2.0, mUnitHeight / 2.0);

			Image *spriteimg = ImageManager::LoadImageFromFile("sprites/stick_sprite.png");

			//s->SpriteSheet(spriteimg);
			//s->LoadAnimation(RESOURCE_DIR "stick_sprite.json");

			pc->second = s;
			mSpriteRenderer.Add(s);
		}
	}
	mMutexPlayerCharacters.unlock();
	mSpriteRenderer.Render(mMapBox, mGameTimer.CurrentTime());

	map.Render(mMapBox, (int)mPlayer->Z() + 1, map.NumLayers() - 1);

	renderImGUI();

	mRenderWindow->AddRenderTime((float)mRenderTimer.GetElapsed());
	mFPS++;
	if( mFPSTimer.GetElapsed() > 1000.0 ) {
		mRenderWindow->AddFrame(mFPS);
		mFPS = 0;
		mFPSTimer.Start();
	}

	return true;
}

void Game::InputEventProc(InputType t, InputEvent e)
{
	switch( t ) {
	case IEVT_KEYDOWN:
		if( e.keyCode == IKEYCODE_F8 ) {
			startServer();
		} else if( e.keyCode == IKEYCODE_F9 ) {
			connectClient();
		} else if( e.keyCode == IKEYCODE_GRAVE ) {
			mGuiAppMenu = !mGuiAppMenu;
		} else {
			mPressedKeys[e.keyCode & 0xff] = 1;
		}
		break;

	case IEVT_KEYUP:
		mPressedKeys[e.keyCode & 0xff] = 0;
		break;
	}
}

void Game::startServer()
{
	if(mServer) {
		delete mServer;
		mServer = nullptr;
	} else {
		mServer = new Net::Server();
		if(!mServer->Start()) {
			delete mServer;
			mServer = nullptr;
		}
	}
}

void Game::connectClient()
{
	if(mClient && mClient->CurrentState() == Net::Client::State::Connected ) {
		mClient->Disconnect();
	} else {
		using namespace std::placeholders;

		mClient = new Net::Client();
		mClient->PlayerConnected = std::bind( &Game::playerConnected, this, _1, _2, _3 );
		mClient->UpdatePlayerState = std::bind( &Game::updatePlayerState, this, _1, _2 );
		mClient->Connect(mServerAddr, mServerPort);
	}
}

void Game::playerConnected(bool connected, Identifiers::UUID user_id, void *data)
{
	if( connected ) {
		LOGI("A player has connected: %s", user_id.getString().c_str());

		const std::lock_guard<std::mutex> lock(mMutexPlayerCharacters);
		if( !mPlayerCharacters.count(user_id) ) {
			mPlayerCharacters[user_id] = nullptr;
		}
	} else {
		if( user_id == Identifiers::UUID::Null() ) {
			LOGI("Disconnected from server, removing all players.");

			mMutexPlayerCharacters.lock();
			for( auto i : mPlayerCharacters ) {
				// Set up sprite for deletion
				if( i.second )
					mSpriteRenderer.Remove( i.second );
			}
			mPlayerCharacters.clear();
			mMutexPlayerCharacters.unlock();
		} else {
			LOGI("A player has left: %s", user_id.getString().c_str());

			mMutexPlayerCharacters.lock();
			if( mPlayerCharacters.count(user_id) ) {
				if( mPlayerCharacters[user_id] != nullptr ) {
					// Set up sprite for deletion
					mSpriteRenderer.Remove( mPlayerCharacters[user_id] );
				}
				mPlayerCharacters.erase(user_id);
			}
			mMutexPlayerCharacters.unlock();
		}
	}
}

void Game::updatePlayerState(Identifiers::UUID user_id, void *data)
{
	std::lock_guard<std::mutex> lock(mMutexPlayerCharacters);

	if( mPlayerCharacters.count(user_id) ) {
		Net::PlayerState *state = (Net::PlayerState*)data;
		Sprite *pc = mPlayerCharacters[user_id];
		if( pc ) {
			pc->SetPosition( ntohd_g(state->x_double), ntohd_g(state->y_double) );
			pc->SetFacing( Sprite::Facing(state->facing) );
			pc->SetState( EntityState(state->state) );
		}
	}
}

char* Game::ExecuteLua(const char* command)
{
	return 0;
}

char* Game::PollLuaBuffer()
{
	return 0;
}

void Game::AddLog(LogType type, const char *tag, const char *filename, int line, const char *fmt, ...)
{
	ImGui::LogWindow *log;
	if( !strcmp(tag, "Server") ) {
		log = static_cast<ImGui::LogWindow*>(mGuiWindows["SLog"].second);
	} else if( !strcmp(tag, "Client") ) {
		log = static_cast<ImGui::LogWindow*>(mGuiWindows["CLog"].second);
	} else {
		log = static_cast<ImGui::LogWindow*>(mGuiWindows["Log"].second);
	}

	ImGui::LogWindow::LogType logType;

	va_list args;
	va_start(args, fmt);

	switch(type) {
	default:
	case LogType::Debug:
	case LogType::Info:
		logType = ImGui::LogWindow::LogType::Normal;
		break;

	case LogType::Warning:
		logType = ImGui::LogWindow::LogType::Warning;
		break;

	case LogType::Error:
		logType = ImGui::LogWindow::LogType::Error;
		break;
	};

	log->AddLog(logType, filename, line, fmt, args);
	va_end(args);
}

void Game::AddLogV(LogType type, const char *tag, const char *filename, int line, const char *fmt, void *valist)
{
	va_list *args = (va_list*)valist;

	ImGui::LogWindow *log;
	if(!strcmp(tag, "Server")) {
		log = static_cast<ImGui::LogWindow*>(mGuiWindows["SLog"].second);
	} else if(!strcmp(tag, "Client")) {
		log = static_cast<ImGui::LogWindow*>(mGuiWindows["CLog"].second);
	} else {
		log = static_cast<ImGui::LogWindow*>(mGuiWindows["Log"].second);
	}

	ImGui::LogWindow::LogType logType;

	va_list args_copy;
	va_copy(args_copy, *args);

	switch(type) {
	default:
	case LogType::Debug:
	case LogType::Info:
		logType = ImGui::LogWindow::LogType::Normal;
		break;

	case LogType::Warning:
		logType = ImGui::LogWindow::LogType::Warning;
		break;

	case LogType::Error:
		logType = ImGui::LogWindow::LogType::Error;
		break;
	};

	log->AddLog(logType, filename, line, fmt, *args);
	va_end(*args);
}

void Game::renderImGUI()
{
	if( mGuiAppMenu ) gui_app_menu();
	if( mGuiDemoWindow ) ImGui::ShowDemoWindow(&mGuiDemoWindow);
	if( mGuiLuaStateWindow ) gui_lua_state_window();
	if( mGuiLuaStackWindow ) gui_lua_stack_window();

	auto it = mGuiWindows.begin();
	while( it != mGuiWindows.end() ) {
		if( it->second.first ) {
			ImGui::IWindow *window = static_cast<ImGui::IWindow*>(it->second.second);
			window->Draw(&it->second.first);
		}
		it++;
	}
}

void Game::gui_app_menu()
{
	if(ImGui::BeginMainMenuBar())
	{
		if(ImGui::BeginMenu("Developer"))
		{
			ImGui::MenuItem("General logs", NULL, gui_get_toggle_bool("Log"));
			if( ImGui::MenuItem("Show all logs") ) {
				gui_show_window("Log");
				gui_show_window("SLog");
				gui_show_window("CLog");
			}
			ImGui::Separator();
			ImGui::MenuItem("Show console", NULL, gui_get_toggle_bool("Console"));
			ImGui::Separator();
			ImGui::MenuItem("Lua state", NULL, &mGuiLuaStateWindow);
			ImGui::MenuItem("Lua stack", NULL, &mGuiLuaStackWindow);
			ImGui::EndMenu();
		}
		if(ImGui::BeginMenu("Net"))
		{
			ImGui::MenuItem("Client settings", NULL, gui_get_toggle_bool("CSetup"));
			if( ImGui::MenuItem(
				(mClient && mClient->CurrentState() == Net::Client::State::Connected ?
					"Disconnect from server" : "Connect to server"), "F9") )
			{
				connectClient();
			}
			ImGui::MenuItem("Client logs", NULL, gui_get_toggle_bool("CLog"));
			ImGui::Separator();
			if( ImGui::MenuItem("Server settings") ) {}
			if( ImGui::MenuItem(
				(mServer && mServer->IsRunning() ? "Stop server" : "Start server"), "F8") )
			{
				startServer();
			}
			ImGui::MenuItem("Server logs", NULL, gui_get_toggle_bool("SLog"));
			ImGui::MenuItem("Server Stats", NULL, gui_get_toggle_bool("SStats"));
			ImGui::EndMenu();
		}
		if(ImGui::BeginMenu("Graphics"))
		{
			ImGui::MenuItem("Rendering Stats", NULL, gui_get_toggle_bool("Render"));
			ImGui::EndMenu();
		}
		if(ImGui::BeginMenu("Game Data"))
		{
			ImGui::MenuItem("Entity Register", NULL, gui_get_toggle_bool("Entity"));
			ImGui::EndMenu();
		}
		#ifdef _DEBUG
		if(ImGui::BeginMenu("Tools"))
		{
			ImGui::MenuItem("Show demo window", NULL, &mGuiDemoWindow);
			ImGui::EndMenu();
		}
		#endif
		ImGui::EndMainMenuBar();
	}
}

void Game::gui_lua_state_window()
{
	static const char *statustxt[] = {
		"LUA_OK", "LUA_YIELD", "LUA_ERRRUN", "LUA_ERRSYNTAX",
		"LUA_ERRMEM", "LUA_ERRGCMM", "LUA_ERRERR"
	};
	static const char *states[] = {
		"Main", "-Game", "-Console"
	};
	static int state_current = 0;

	lua_State *L = (state_current == 0 ? mLua : mLuaThreads[state_current-1]);

	const lua_Number *version = lua_version(L);
	int status = lua_status(L);
	int sizekb = lua_gc(L, LUA_GCCOUNT, 0);
	int sizeb = lua_gc(L, LUA_GCCOUNTB, 0);
	int canyeild = lua_isyieldable(L);

	ImGui::SetNextWindowSize(ImVec2(500, 400), ImGuiCond_FirstUseEver);
	ImGui::Begin("Lua State", &mGuiLuaStateWindow);
	ImGui::Combo("state", &state_current, states, IM_ARRAYSIZE(states));
	if(ImGui::Button("Start garbage collector")) {
		lua_gc(L, LUA_GCCOLLECT, 0);
	}
	ImGui::Text("Version   : %d.%d", (int)(*version) / 100, (int)(*version) % 100);
	ImGui::Text("Status    : %s", statustxt[status]);
	ImGui::Text("Bytes used: %d", sizekb * 1024 + sizeb);
	ImGui::Text("Can yield : %s", canyeild ? "Yes" : "No");
	ImGui::End();
}

void Game::gui_lua_stack_window()
{
	static const char *states[] = {
		"Main", "-Game", "-Console"
	};
	static int state_current = 0;

	lua_State *L = (state_current == 0 ? mLua : mLuaThreads[state_current - 1]);

	int slen = lua_gettop(L);
	ImGui::SetNextWindowSize(ImVec2(500, 400), ImGuiCond_FirstUseEver);
	ImGui::Begin("Lua Stack", &mGuiLuaStackWindow);
	ImGui::Combo("state", &state_current, states, IM_ARRAYSIZE(states));

	for(int stk = 1; stk <= slen; stk++) {
		if(lua_isstring(L, stk)) {
			ImGui::Text("[% 3d]: %s", stk, luaL_checkstring(L, stk));
		} else if(lua_isnumber(L, stk)) {
			ImGui::Text("[% 3d]: %f", stk, luaL_checknumber(L, stk));
		} else if(lua_isfunction(L, stk)) {
			ImGui::Text("[% 3d]: Function", stk);
		} else if(lua_isboolean(L, stk)) {
			ImGui::Text("[% 3d]: (bool) %s", stk, lua_toboolean(L, stk) ? "true" : "false");
		} else if(lua_islightuserdata(L, stk)) {
			ImGui::Text("[% 3d]: Light User Data", stk);
		} else if(lua_isnoneornil(L, stk)) {
			ImGui::Text("[% 3d]: Nothing", stk);
		} else if(lua_istable(L, stk)) {
			ImGui::Text("[% 3d]: Table", stk);
		} else if(lua_isuserdata(L, stk)) {
			ImGui::Text("[% 3d]: User Data", stk);
		} else {
			ImGui::Text("[% 3d]: Unknown", stk);
		}
	}

	ImGui::End();
}

void Game::gui_add_window(const std::string name, void *window, bool show)
{
	if( mGuiWindows.count(name) ) {
		LOGE("Window already exists, deleting old...");
		delete static_cast<ImGui::IWindow*>(mGuiWindows[name].second);
	}

	mGuiWindows[name].first = show;
	mGuiWindows[name].second = window;
}

bool Game::gui_toggle_window(const std::string name)
{
	if( mGuiWindows.count(name) ) {
		mGuiWindows[name].first = !mGuiWindows[name].first;
		return true;
	}
	return false;
}

bool Game::gui_show_window(const std::string name, bool show)
{
	if( mGuiWindows.count(name) ) {
		mGuiWindows[name].first = show;
		return true;
	}
	return false;
}

bool* Game::gui_get_toggle_bool(const std::string name)
{
	if( mGuiWindows.count(name) ) {
		return &mGuiWindows[name].first;
	}
	return nullptr;
}

void Game::luaThread()
{
	uint32_t flags;
	int result;

	while( 1 ) {
		result = wait_flags(mLuaThread, &flags, GAME_LUAFLG_ALL);

		if( flags & GAME_LUAFLG_EXIT ) {
			break;
		}

		if( flags & GAME_LUAFLG_EXECUTE ) {
			std::lock_guard<std::mutex> lock(mLuaMutex);
			int err = lua_pcall(mLuaThreads[1], 0, 0, 0);
			if(err != LUA_OK && err != LUA_YIELD) {
				const char *s = lua_tostring(mLuaThreads[1], -1);
				lua_pop(mLuaThreads[1], 1);
				if(s) {
					((ImGui::Console*)mLuaConsole)->AddLogS(s);
				}
			}
		}
	}
}

void Game::luaAddCommand(char *command)
{
	std::lock_guard<std::mutex> lock(mLuaMutex);
	int err = luaL_loadbuffer(mLuaThreads[1], command, strlen(command), "console");
	if(err != LUA_OK) {
		const char *s = lua_tostring(mLuaThreads[1], -1);
		lua_pop(mLuaThreads[1], 1);
		if(s) {
			((ImGui::Console*)mLuaConsole)->AddLogS(s);
		}
	} else {
		set_flags(mLuaThread, GAME_LUAFLG_EXECUTE);
	}
	free(command);
}

int Game::lg_getplayer(lua_State *L)
{
	lua_getglobal(L, "__game");
	Game *g = (Game*)lua_touserdata(L, -1);
	g->mPlayer->PushToStack(L);
	return 1;
}

int Game::l_print(lua_State *L)
{
	int n = lua_gettop(L);  /* number of arguments */
	int i;
	lua_getglobal(L, "tostring");
	for(i=1; i<=n; i++) {
		const char *s;
		size_t l;
		lua_pushvalue(L, -1);  /* function to be called */
		lua_pushvalue(L, i);   /* value to print */
		lua_pcall(L, 1, 1, 0);
		s = lua_tolstring(L, -1, &l);  /* get result */
		if(s == NULL)
			return luaL_error(L, LUA_QL("tostring") " must return a string to " LUA_QL("print"));
		//if(!me->mGLInit && i>1) me->luai_writestring("\t", 1);
		//me->luai_writestring(s, l);
		LOGI(s);
		lua_pop(L, 1);  /* pop result */
	}
	//if( !me->mGLInit ) me->luai_writeline();
	return 0;
}

int Game::l_sleep(lua_State *L)
{
	int ms = (int)(lua_tonumber(L, 1));
	std::this_thread::sleep_for( std::chrono::milliseconds(ms) );
	return 0;
}
