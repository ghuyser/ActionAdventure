#define _CRT_SECURE_DEPRECATE_MEMORY

#include <windows.h>
#include <tchar.h>
#include <gl/gl.h>
#include <gl/wglext.h>

#include <memory.h>
#include <stdlib.h>
#include <stdio.h>

#include "System/Device.h"
#include "System/TimeProfiler.h"
#include "common.h"

#include "actionadventure.h"

#include "imgui.h"
#include "imgui_impl_opengl3.h"
#include "imgui_impl_win32.h"

#define CLASS_NAME   TEXT("ActionAdventure")
#define WINDOW_TITLE TEXT("Action Adventure")

#define FULLSCREEN	0

#define W_WIDTH		1200
#define W_HEIGHT	800

// Full screen anti-aliasing passes
#define NUM_FSAA	8

using System::Device;
using System::TimeProfiler;

PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB = NULL;
PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB = NULL;

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void EnableOpenGL(HWND hWnd, HDC * hDC, HGLRC * hRC);
void EnableOpenGL_ARB(HWND hWnd, HDC * hDC, HGLRC * hRC);
void DisableOpenGL(HWND hWnd, HDC hDC, HGLRC hRC);
int readfile(const char *filename, char **buf);

IGame *thegame = 0;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, 
				   LPSTR lpCmdLine, int iCmdShow)
{
	WNDCLASS wc;
	HWND hWnd;
	HDC hDC;
	HGLRC hRC;
	MSG msg;
	BOOL quit = FALSE;

	int screen_width = GetSystemMetrics( SM_CXSCREEN );
	int screen_height = GetSystemMetrics( SM_CYSCREEN );
	int window_width = (FULLSCREEN ? screen_width : W_WIDTH);
	int window_height = (FULLSCREEN ? screen_height : W_HEIGHT);

	// register window class
	wc.style = CS_OWNDC;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = CLASS_NAME;
	RegisterClass(&wc);

	// create main window
	hWnd = CreateWindow(
		CLASS_NAME, WINDOW_TITLE,
		(FULLSCREEN ? 0 : WS_CAPTION) | (FULLSCREEN ? WS_POPUP : WS_POPUPWINDOW),
		0, 0, window_width, window_height,
		NULL, NULL, hInstance, NULL);

	// enable OpenGL for the window
	EnableOpenGL( hWnd, &hDC, &hRC );

	if( wglCreateContextAttribsARB && wglChoosePixelFormatARB ) {
		DisableOpenGL( hWnd, hDC, hRC );
		DestroyWindow( hWnd );

		RECT winSize = { 0, 0, window_width, window_height };
		AdjustWindowRect( &winSize, WS_CAPTION, false );

		hWnd = CreateWindow(
			CLASS_NAME, WINDOW_TITLE,
			(FULLSCREEN ? 0 : WS_CAPTION) | (FULLSCREEN ? WS_POPUP : WS_POPUPWINDOW),
			0, 0, winSize.right - winSize.left, winSize.bottom - winSize.top,
			NULL, NULL, hInstance, NULL);

		EnableOpenGL_ARB( hWnd, &hDC, &hRC );
	}

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	ImGui::StyleColorsDark();

	const char* glsl_version = "#version 330";
	ImGui_ImplOpenGL3_Init(glsl_version);
	ImGui_ImplWin32_Init(hWnd);

	RECT r;
	GetClientRect(hWnd, &r);

	// Set device properties (allow for config file)
	Device::SetDPI(145);
	Device::SetScreenResolution(r.right, r.bottom);
	Device::SetDPResolution(DP_SURFACE_WIDTH, (int)(DP_SURFACE_WIDTH * ((double)r.bottom / (double)r.right)));

	double frameTimeLimit = (1.0 / 60.0) * 1000.0;
	TimeProfiler frameTimer;
	frameTimer.Start();

	thegame = CreateGame();
	thegame->ReadTextAsset = &readfile;
	thegame->InitGL();
	thegame->InitGame();

	thegame->ResizeGLScene( r.right, r.bottom );

	char wTitle[256];
	GetWindowText(hWnd, wTitle, 256);
	sprintf(wTitle, "%s [%s]", wTitle, glGetString(GL_VERSION));
	SetWindowText(hWnd, wTitle);
	ShowWindow( hWnd, SW_SHOW );

	// program main loop
	while ( !quit )
	{
		// check for messages
		if ( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE )  )
		{
			// handle or dispatch messages
			if ( msg.message == WM_QUIT ) 
			{
				quit = TRUE;
			} 
			else 
			{
				TranslateMessage( &msg );
				DispatchMessage( &msg );
			}
			
		} 
		else 
		{
			if( frameTimer.GetElapsed() > frameTimeLimit ) {
				frameTimer.Start();

				ImGui_ImplOpenGL3_NewFrame();
				ImGui_ImplWin32_NewFrame();
				ImGui::NewFrame();

				thegame->DoActions();
				thegame->Render();

				ImGui::Render();
				ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

				SwapBuffers( hDC );
			}
		}
		
	}

	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();

	DestroyGame(thegame);

	// shutdown OpenGL
	DisableOpenGL( hWnd, hDC, hRC );
	
	// destroy the window explicitly
	DestroyWindow( hWnd );
	
	return msg.wParam;
	
}

// Window Procedure
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static InputEvent evt;
	
	if(ImGui_ImplWin32_WndProcHandler(hWnd, message, wParam, lParam))
		return true;

	switch (message)
	{
		
	case WM_CREATE:
		return 0;
		
	case WM_CLOSE:
		PostQuitMessage( 0 );
		return 0;
		
	case WM_DESTROY:
		return 0;
		
	case WM_KEYDOWN:
		switch ( wParam )
		{
		case VK_NONAME:
			return 0;
			
		case VK_ESCAPE:
			PostQuitMessage(0);
			return 0;

		default:
			if( thegame ) {
				evt.keyCode = wParam;
				thegame->InputEventProc(IEVT_KEYDOWN, evt);
			}
			return 0;
			
		}
		return 0;

	case WM_KEYUP:
		if( wParam == VK_NONAME ) {
			return 0;
		}
		if( thegame ) {
			evt.keyCode = wParam;
			thegame->InputEventProc(IEVT_KEYUP, evt);
		}
		return 0;

	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
	case WM_MBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
	case WM_MBUTTONUP:
	case WM_MOUSEMOVE:
		if( thegame ) {
			evt.x = (int)(lParam & 0xffff);
			evt.y = (int)((lParam >> 16) & 0xffff);
			thegame->InputEventProc((InputType)message, evt);
		}
		return 0;
	
	default:
		return DefWindowProc( hWnd, message, wParam, lParam );
			
	}
	
}


// Enable OpenGL

void EnableOpenGL(HWND hWnd, HDC * hDC, HGLRC * hRC)
{
	PIXELFORMATDESCRIPTOR pfd;
	int format;
	
	// get the device context (DC)
	*hDC = GetDC( hWnd );
	
	// set the pixel format for the DC
	ZeroMemory( &pfd, sizeof( pfd ) );
	pfd.nSize = sizeof( pfd );
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 24;
	pfd.cDepthBits = 32;
	pfd.iLayerType = PFD_MAIN_PLANE;
	format = ChoosePixelFormat( *hDC, &pfd );
	SetPixelFormat( *hDC, format, &pfd );
	
	// create and enable the render context (RC)
	*hRC = wglCreateContext( *hDC );
	wglMakeCurrent( *hDC, *hRC );

	// Get the ARB entry points
	wglCreateContextAttribsARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC)wglGetProcAddress("wglCreateContextAttribsARB");
	wglChoosePixelFormatARB = (PFNWGLCHOOSEPIXELFORMATARBPROC)wglGetProcAddress("wglChoosePixelFormatARB");
}

// Enable OpenGL ARB

void EnableOpenGL_ARB(HWND hWnd, HDC * hDC, HGLRC * hRC)
{
	PIXELFORMATDESCRIPTOR pfd;

	*hDC = GetDC( hWnd );
	
	int nPixelFormat;
	BOOL bValidPixFormat;
	UINT nMaxFormats = 1;
	UINT nNumFormats;
	float pfAttribFList[] = { 0, 0 };
	int piAttribIList[] = {
		WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
		WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
		WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
		WGL_ACCELERATION_ARB,   WGL_FULL_ACCELERATION_ARB,
		WGL_COLOR_BITS_ARB,     32,
		WGL_ALPHA_BITS_ARB,     8,
		WGL_DEPTH_BITS_ARB,     24,
		WGL_STENCIL_BITS_ARB,   0,
		WGL_DOUBLE_BUFFER_ARB,  GL_TRUE,
		WGL_SAMPLE_BUFFERS_ARB, GL_TRUE,
		/*WGL_SAMPLES_ARB,        NUM_FSAA,*/
		0
	};

	bValidPixFormat = wglChoosePixelFormatARB( *hDC, piAttribIList, pfAttribFList, nMaxFormats, &nPixelFormat, &nNumFormats );
	if( !bValidPixFormat ) {
		exit( 0 );
	}

	DescribePixelFormat( *hDC, nPixelFormat, sizeof(pfd), &pfd);
	SetPixelFormat( *hDC, nPixelFormat, &pfd );

	const int attribs[] = {
		WGL_CONTEXT_MAJOR_VERSION_ARB, 4,
		WGL_CONTEXT_MINOR_VERSION_ARB, 6,
		WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_DEBUG_BIT_ARB,
		WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB,
		0
	};

	*hRC = wglCreateContextAttribsARB( *hDC, 0, attribs );
	if( !wglMakeCurrent(*hDC, *hRC) ) {
		exit( 0 );
	}
}

// Disable OpenGL

void DisableOpenGL(HWND hWnd, HDC hDC, HGLRC hRC)
{
	wglMakeCurrent( NULL, NULL );
	wglDeleteContext( hRC );
	ReleaseDC( hWnd, hDC );
}


int readfile(const char *filename, char **buf)
{
	int sz, num;
	FILE *fp;
	char fn[256];

	if( !filename )
		return ERR_PARAMETER;
	
	sprintf(fn, "shaders/%s", filename);
	fp = fopen(fn, "r");
	if( !fp )
		return ERR_FILE_NOT_FOUND;
	fseek(fp, 0, SEEK_END);
	sz = ftell(fp);
	*buf = (char*)malloc(sz+1);
	if( !*buf )
		return ERR_ALLOCATION;
	fseek(fp, 0, SEEK_SET);
	memset(*buf, 0, sz+1);
	num = fread(*buf, 1, sz, fp);
	fclose(fp);

	return num;
}

int ReadBinFile(const char *filename, char **out_buffer, int *out_size)
{
	if( !filename || !out_buffer || !out_size )
		return ERR_PARAMETER;

	int sz, num;
	FILE *fp;
	
	fp = fopen(filename, "rb");
	if( !fp )
		return ERR_FILE_NOT_FOUND;
	fseek(fp, 0, SEEK_END);
	sz = ftell(fp);
	*out_buffer = (char*)malloc(sz);
	if( !*out_buffer )
		return ERR_ALLOCATION;
	fseek(fp, 0, SEEK_SET);
	num = fread(*out_buffer, 1, sz, fp);
	fclose(fp);

	*out_size = sz;
	return num;
}

int GetFilesInDirectory(const char *path, char **list)
{
	WIN32_FIND_DATA w32fd;
	HANDLE hFind;
	DWORD dwAtt;
	TCHAR acPath[MAX_PATH];
	TCHAR acBase[MAX_PATH];
	TCHAR param1[MAX_PATH];
	PTCHAR tmplist = NULL;

	if( path == NULL || list == NULL ) {
		return ERR_PARAMETER;
	}

#ifdef UNICODE
	MultiByteToWideChar(CP_ACP, MB_COMPOSITE, path, -1, param1, MAX_PATH);
#else
	_tcscpy_s(param1, MAX_PATH, path);
#endif

	// Ignore . and ..
	if( '.' == *(param1 + lstrlen( param1 ) - 1) ) {
		return ERR_IO;
	}

	// Set up initial path
	_tcscpy_s( acPath, MAX_PATH, param1 );
	_tcscpy_s( acBase, MAX_PATH, acPath );

	// Get attributes of node
	dwAtt = GetFileAttributes( acPath );
	if( 0xffffffff == dwAtt ) {
		// error ...
		return ERR_IO;
	}

	// Node is directory
	if( FILE_ATTRIBUTE_DIRECTORY & dwAtt ) {
		if( '\\' == acPath[lstrlen( acPath ) - 1] ) {
			_tcscat_s( acPath, TEXT("*.*") );
		} else {
			_tcscat_s( acPath, TEXT("\\*.*") );
		}
	}

	// Get first file
	hFind = FindFirstFile( acPath, &w32fd );
	if( INVALID_HANDLE_VALUE == hFind ) {
		// error
		return ERR_IO;
	}

	// For each file in the directory...
	do {
		// recurse if directory...
		if( FILE_ATTRIBUTE_DIRECTORY == w32fd.dwFileAttributes ) {
			//WalkTree( w32fd.cFileName, acBase );
		} else {
			// Is a file, process the file
			//HandleFile( acBase, &w32fd );
			int characters = lstrlen(tmplist) + lstrlen(w32fd.cFileName) + 2;
			PTCHAR newlist = (PTCHAR)malloc( characters * sizeof(TCHAR) );
			
			if( newlist == NULL ) {
				free( tmplist );
				return ERR_ALLOCATION;
			}

			if( tmplist != NULL ) {
				_stprintf_s(newlist, characters, TEXT("%s|%s"), tmplist, w32fd.cFileName);
			} else {
				_stprintf_s(newlist, characters, TEXT("%s"), w32fd.cFileName);
			}

			free( tmplist );
			tmplist = newlist;
		}
	} while( FindNextFile( hFind, &w32fd ) );

	if( ERROR_NO_MORE_FILES != GetLastError() )
	{
		// error
		free( tmplist );
		return ERR_IO;
	}
	FindClose( hFind );

	int finalsize = lstrlen(tmplist);
	*list = (char*)malloc(finalsize + 1);
#ifdef UNICODE
	size_t conv;
	wcstombs_s( &conv, *list, finalsize + 1, tmplist, finalsize );
#else
	memcpy(*list, tmplist, finalsize);
#endif
	(*list)[finalsize] = 0;

	free( tmplist );

	return OK;
}

int GetFilesInDirectoryEx(const char *path, char **list)
{
	return GetFilesInDirectory(path, list);
}

void* OpenAsset(const char *filename, const char *mode )
{
	FILE *fp = fopen( filename, mode );
	return fp;
}
