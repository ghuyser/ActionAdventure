#pragma once

#include "IGame.h"

// The width of the display surface in "Display Pixels"
// A higher number will make smaller UI elements
// Note: Currently has no effect on font size
#define DP_SURFACE_WIDTH	1200

// Number of pixels wide a tile should be
// Example: If source is a 32 pixel wide tile, this will make
// each pixel of the tile take up 4 pixels
#define DP_TILE_PIXELS        64

// External functions below MUST return greater or equal to 0 (OK) on success
// or one of the defined values below on an error condition.
#define OK					0		// No error
#define ERR_PARAMETER		-1		// A parameter was invalid
#define ERR_FILE_NOT_FOUND	-2		// The specified file or directory not found
#define ERR_ALLOCATION		-3		// Memory allocation error
#define ERR_IO				-4		// Generic I/O error

// Create a game object
IGame* CreateGame();

// Release a game object
void DestroyGame(IGame *game);

// ReadBinFile
// Reads [filename] from the asset location and places the contents into the
// memory location pointed to by [out_buffer].  The size of the data is then
// placed in the memory location pointed to by [out_size].
extern int ReadBinFile(const char *filename, char **out_buffer, int *out_size);

// GetFilesInDirectory
// Examines the directory contents in the asset location in [path] and returns
// a '|' separated list of file names (no directory names) into the memory
// location pointed to by [list].
extern int GetFilesInDirectory(const char *path, char **list);

// GetFilesInDirectoryEx
// Same as GetFilesInDirectory, but examines external memory
extern int GetFilesInDirectoryEx(const char *path, char **list);

// OpenAsset
// Opens an asset and returns a pointer to the asset descriptor.
// Used with libvorbis.  libvorbis will close the asset when it has completed
// its use of the descriptor.
extern void* OpenAsset(const char *filename, const char *mode);
