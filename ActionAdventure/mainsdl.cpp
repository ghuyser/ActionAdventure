#include "SDL.h"
#include "SDL_opengl.h"

#include <memory.h>
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>

#include "System/Device.h"
#include "System/TimeProfiler.h"
#include "common.h"

#include "actionadventure.h"

#include "imgui.h"
#include "imgui_impl_opengl3.h"
#include "imgui_impl_sdl.h"

#define FULLSCREEN	0

#define W_WIDTH		1200
#define W_HEIGHT	800

#define APP_NAME	"Action Adventure"

using System::Device;
using System::TimeProfiler;

int readfile(const char *filename, char **buf);

IGame *thegame = 0;
SDL_Window *thewindow = 0;
SDL_GLContext thecontext;

static void setup_sdl() 
{
	SDL_SetHint(SDL_HINT_JOYSTICK_ALLOW_BACKGROUND_EVENTS, "1");

	if ( SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER) < 0 ) {
		fprintf(stderr, "Couldn't initialize SDL: %s\n", SDL_GetError());
		exit(1);
	}

	/* Quit SDL properly on exit */
	atexit(SDL_Quit);

	/* Set the minimum requirements for the OpenGL window */
	SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 5 );
	SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 5 );
	SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 5 );
	SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16 );
	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

	thewindow = SDL_CreateWindow(APP_NAME, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, W_WIDTH, W_HEIGHT, SDL_WINDOW_OPENGL);
	if( thewindow == 0 ) {
		fprintf(stderr, "Couldn't set video mode: %s\n", SDL_GetError());
		exit(1);
	}
	
	thecontext = SDL_GL_CreateContext(thewindow);
}


static void main_loop() 
{
	static InputEvent evt;
	SDL_Event event;
	bool run = true;
	Uint8 laststate = 0;

	double frameTimeLimit = (1.0 / 60.0) * 1000.0;
	TimeProfiler frameTimer;
	frameTimer.Start();

	while (run) {
		/* process pending events */
		while( SDL_PollEvent( &event ) ) {
			ImGui_ImplSDL2_ProcessEvent(&event);

			if( ImGui::GetIO().WantCaptureKeyboard )
				continue;

			switch( event.type ) {
			case SDL_KEYDOWN:
				switch ( event.key.keysym.sym ) {
				case SDLK_ESCAPE:
					run = false;
					break;

				default:
					if( thegame ) {
						evt.keyCode = event.key.keysym.sym;
						thegame->InputEventProc(IEVT_KEYDOWN, evt);
					}
					break;
				}
				break;
			
			case SDL_KEYUP:
				if( thegame ) {
					evt.keyCode = event.key.keysym.sym;
					thegame->InputEventProc(IEVT_KEYUP, evt);
				}
				break;

			case SDL_MOUSEBUTTONDOWN:
				if( thegame ) {
					int type = IEVT_NONE;
					Uint8 state = SDL_GetMouseState(&evt.x, &evt.y);
					Uint8 dif = state ^ laststate;
					if( dif & SDL_BUTTON(1) ) {
						type = IEVT_LMOUSEDOWN;
					} else if( state & SDL_BUTTON(2) ) {
						type = IEVT_RMOUSEDOWN;
					} else if( state & SDL_BUTTON(3) ) {
						type = IEVT_MMOUSEDOWN;
					}
					laststate = state;
					thegame->InputEventProc((InputType)type, evt);
				}
				break;
				
			case SDL_MOUSEBUTTONUP:
				if( thegame ) {
					int type = IEVT_NONE;
					Uint8 state = SDL_GetMouseState(&evt.x, &evt.y);
					Uint8 dif = state ^ laststate;
					if( dif & SDL_BUTTON(1) ) {
						type = IEVT_LMOUSEUP;
					} else if( state & SDL_BUTTON(2) ) {
						type = IEVT_RMOUSEUP;
					} else if( state & SDL_BUTTON(3) ) {
						type = IEVT_MMOUSEUP;
					}
					laststate = state;
					thegame->InputEventProc((InputType)type, evt);
				}
				break;
				
			case SDL_MOUSEMOTION:
				if( thegame ) {
					SDL_GetMouseState(&evt.x, &evt.y);
					thegame->InputEventProc(IEVT_MOUSEMOVE, evt);
				}
				break;

			case SDL_QUIT:
				run = false;
				break;
			
			default:
				break;
			}
		}

		if( frameTimer.GetElapsed() > frameTimeLimit ) {
			frameTimer.Start();
			
			ImGui_ImplOpenGL3_NewFrame();
			ImGui_ImplSDL2_NewFrame(thewindow);
			ImGui::NewFrame();

			thegame->DoActions();
			thegame->Render();

			ImGui::Render();
			ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

			//SDL_GL_SwapBuffers();
			SDL_GL_SwapWindow(thewindow);
		}
	}
}


int main(int argc, char* argv[])
{
	setup_sdl();

	Device::SetDPI(145);
	Device::SetScreenResolution(W_WIDTH, W_HEIGHT);
	Device::SetDPResolution(DP_SURFACE_WIDTH, (int)(DP_SURFACE_WIDTH * ((double)W_HEIGHT / (double)W_WIDTH)));

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	ImGui::StyleColorsDark();

	const char* glsl_version = "#version 130";
	ImGui_ImplOpenGL3_Init(glsl_version);
	ImGui_ImplSDL2_InitForOpenGL(thewindow, thecontext);

	thegame = CreateGame();
	thegame->ReadTextAsset = &readfile;
	thegame->InitGL();
	thegame->InitGame();
	thegame->ResizeGLScene( W_WIDTH, W_HEIGHT );

	main_loop();

	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplSDL2_Shutdown();
	ImGui::DestroyContext();

	DestroyGame(thegame);
	   
	exit(0);
	
	return 0;
}

int readfile(const char *filename, char **buf)
{
	int sz, num;
	FILE *fp;
	char fn[256];

	if( !filename )
		return ERR_PARAMETER;
	
	sprintf(fn, "shaders/%s", filename);
	fp = fopen(fn, "r");
	if( !fp )
		return ERR_FILE_NOT_FOUND;
	fseek(fp, 0, SEEK_END);
	sz = ftell(fp);
	*buf = (char*)malloc(sz+1);
	if( !*buf )
		return ERR_ALLOCATION;
	fseek(fp, 0, SEEK_SET);
	memset(*buf, 0, sz+1);
	num = fread(*buf, 1, sz, fp);
	fclose(fp);

	return num;
}

int ReadBinFile(const char *filename, char **out_buffer, int *out_size)
{
	if( !filename || !out_buffer || !out_size )
		return ERR_PARAMETER;

	int sz, num;
	FILE *fp;
	
	fp = fopen(filename, "rb");
	if( !fp )
		return ERR_FILE_NOT_FOUND;
	fseek(fp, 0, SEEK_END);
	sz = ftell(fp);
	*out_buffer = (char*)malloc(sz);
	if( !*out_buffer )
		return ERR_ALLOCATION;
	fseek(fp, 0, SEEK_SET);
	num = fread(*out_buffer, 1, sz, fp);
	fclose(fp);

	*out_size = sz;
	return num;
}

int GetFilesInDirectory(const char *path, char **list)
{
	struct dirent *de;
	char *tmplist = NULL;

	if( path == NULL || list == NULL ) {
		return ERR_PARAMETER;
	}

	DIR* dir = opendir(path);

	while( (de = readdir(dir)) ) {
		if( de->d_type != DT_REG ) {
			continue;
		}

		int characters;
		if( tmplist == NULL ) {
			characters = strlen(de->d_name) + 2;
		} else {
			characters = strlen(tmplist) + strlen(de->d_name) + 2;
		}

		char *newlist = (char*)malloc( characters );

		if( newlist == NULL ) {
			free( tmplist );
			return ERR_ALLOCATION;
		}

		if( tmplist != NULL ) {
			snprintf(newlist, characters, "%s|%s", tmplist, de->d_name);
		} else {
			snprintf(newlist, characters, "%s", de->d_name);
		}

		free( tmplist );
		tmplist = newlist;
	}

	closedir(dir);

	if( tmplist == NULL ) {
		return ERR_IO;
	}

	int finalsize = strlen(tmplist);
	*list = (char*)malloc(finalsize + 1);
	memcpy(*list, tmplist, finalsize);
	(*list)[finalsize] = 0;

	free( tmplist );

	return OK;
}

int GetFilesInDirectoryEx(const char *path, char **list)
{
	return GetFilesInDirectory(path, list);
}

void* OpenAsset(const char *filename, const char *mode )
{
	FILE *fp = fopen( filename, mode );
	return fp;
}
