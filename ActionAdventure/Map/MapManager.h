#pragma once

#include "IMap.h"

#include <string>
#include <unordered_map>

class MapManager
{
public:
	enum class MapType {
		Auto,
		Tiled,
	};

	static MapManager* Instance();

	static Map::IMap* LoadFromJSON(const std::string &path, MapType type = MapType::Auto);
	static Map::IMap* LoadFromJSON(const char *path, MapType type = MapType::Auto);

	static Map::IMap* CurrentMap();

private:
	MapManager();
	~MapManager();

	Map::IMap *mCurrentMap;

	std::unordered_map<std::string, Map::IMap*> mMaps;
};
