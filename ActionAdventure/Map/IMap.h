#pragma once

#include "G3DMath.h"

class GLProgram;
class Image;

namespace Map
{
	class IMap
	{
	public:
		virtual void MapTexturePalette(const Image *map, Image *texture, const Image &palette) = 0;
		virtual void LoadFromJSON(const char *path) = 0;

		virtual int WidthInt() const = 0;
		virtual int HeightInt() const = 0;

		virtual double Width() const = 0;
		virtual double Height() const = 0;

		virtual int NumLayers() const = 0;

		virtual G3DMath::Vec2d GetEntryPoint(unsigned int index) const = 0;
		virtual unsigned int NumEntryPoints() const = 0;

		virtual void GetMoveTo(double base_x, double base_y, double &offset_x, double &offset_y) const = 0;

		virtual bool InitGL() = 0;
		virtual void UseProgram() = 0;
		virtual void SetProjection(const G3DMath::Matrix4f &proj, bool useProgram = true) = 0;
		virtual void SetView(const G3DMath::Matrix4f &view, bool useProgram = true) = 0;
		virtual bool Render(int left, int top, int width, int height, int layer_start, int layer_end) = 0;
		virtual bool Render(const G3DMath::BoundingBox<G3DMath::Vec2d> &area, int layer_start, int layer_end) = 0;
	};
}
