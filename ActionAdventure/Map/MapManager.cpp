#include "MapManager.h"

#include "TiledMap.h"
#include "resources.h"

static MapManager *instance = nullptr;

MapManager::MapManager()
{
	instance = this;

	mCurrentMap = nullptr;
}

MapManager::~MapManager()
{
	for(auto i : mMaps) {
		delete i.second;
	}
}

MapManager* MapManager::Instance()
{
	if(!instance) {
		instance = new MapManager();
	}

	return instance;
}

Map::IMap* MapManager::LoadFromJSON(const char *path, MapType type)
{
	return LoadFromJSON(std::string(path), type);
}

Map::IMap* MapManager::LoadFromJSON(const std::string &path, MapType type)
{
	MapManager *im = Instance();

	std::string full_path = std::string(RESOURCE_DIR).append(path);

	if(im->mMaps.count(full_path) > 0) {
		return im->mMaps[full_path];
	}

	MapType derived_type = type;
	if(type == MapType::Auto) {
		// Currently only have one map type
		derived_type = MapType::Tiled;
	}

	Map::IMap *map;

	switch(derived_type) {
	default: // Currently only support TiledMap
	case MapType::Tiled:
		map = new Map::TiledMap();
		break;
	}

	if( !im->mCurrentMap ) {
		im->mCurrentMap = map;
	}

	map->LoadFromJSON(full_path.c_str());

	im->mMaps[full_path] = map;

	return map;
}

Map::IMap* MapManager::CurrentMap()
{
	MapManager *im = Instance();
	return im->mCurrentMap;
}
