#include "TiledMap.h"

#include "common.h"
#include "gl_fabric.h"
#include "jsonobject.h"
#include "lodepng.h"
#include "resources.h"

#include "Image/Image.h"
#include "Image/ImageManager.h"

using namespace Map;

bool checkForKeys(JSONObject &obj, std::vector<std::string> keylist)
{
	bool exists = true;

	for( auto i = keylist.begin(); i != keylist.end(); i++ ) {
		exists &= obj.hasKey(*i);
	}

	return exists;
}

TiledMap::TiledMap()
{
	gl_prog = NULL;

	texatlas_width = 0;
	texatlas_height = 0;

	layers = 0;
	teximg = NULL;

	tilebuf = 0;
	indexbuf = 0;

	atlasbuf = 0;
}

TiledMap::~TiledMap()
{
	if( tilebuf || indexbuf ) {
		glDeleteBuffers(1, &tilebuf);
		glDeleteBuffers(1, &indexbuf);
	}

	clearTextures();
}

void TiledMap::clearTextures()
{
	for( auto img : mapimg ) {
		SAFE_DELETE(img);
	}
	SAFE_DELETE(teximg);
}

void TiledMap::makePalette(const Image *img_ptr)
{
	const Image &img = *img_ptr;

	palette.clear();

	for( int y = 0; y < img.Height(); y++ ) {
		for( int x = 0; x < img.Width(); x++ ) {
			int imgidx = x * img.Depth() + y * (img.Width() * img.Depth());
			unsigned char r = img[imgidx];
			unsigned char g = img[imgidx + 1];
			unsigned char b = img[imgidx + 2];
			size_t val = r << 16 | g << 8 | b;
			palette[val] = y * img.Height() + x;
		}
	}

	texatlas_width = img.Width();
	texatlas_height = img.Height();
}

void TiledMap::setMapProperties(const Image *map, const Image *texture)
{
	map_width = map->Width();
	map_height = map->Height();

	int tile_width = texture->Width() / texatlas_width;
	int tile_height = texture->Height() / texatlas_height;

	tile_width_pct = (float)tile_width / (float)texture->Width();
	tile_height_pct = (float)tile_height / (float)texture->Height();
}

void TiledMap::MapTexturePalette(const Image *map, Image *texture, const Image &pal)
{
	mapimg.clear();
	mapimg.push_back(map);
	teximg = texture;

	makePalette(&pal);
	setMapProperties(map, texture);
}

void TiledMap::LoadFromJSON(const char *path)
{
	if( !path ) return;

	const char *fn = strrchr(path, '/') + 1;
	if( fn == NULL ) {
		fn = strrchr(path, '\\');
		if( fn == NULL )
			return;
	}

	if( strlen(fn) <= 0 )
		return;

	FILE *fp = fopen(path, "r");
	if( !fp ) return;
	fseek(fp, 0, SEEK_END);
	size_t sz = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	char *buf = (char*)malloc(sz);
	if( !buf ) {
		fclose(fp);
		return;
	}
	fread(buf, 1, sz, fp);
	fclose(fp);

	JSONObject o(buf);
	free(buf);

	if( o.hasKey("entry_points") && o["entry_points"].Type() == JSONType::Array ) {
		for( auto i = o["entry_points"].begin(); i != o["entry_points"].end(); i++ ) {
			const JSONObject &op = *i;
			if( op.Type() == JSONType::Array && op.Count() > 1 )
				mEntryPoints.push_back( G3DMath::Vec2d( op[0].GetDouble(), op[1].GetDouble()) );
		}
	}

	if( !checkForKeys(o, {"map", "texture", "palette", "vertex_shader", "fragment_shader"}) )
		return;

	if( !loadShader(o["vertex_shader"], o["fragment_shader"]) )
		return;

	clearTextures();

	teximg = ImageManager::LoadImageFromFile(o["texture"].GetString());
	makePalette(ImageManager::LoadImageFromFile(o["palette"].GetString()));

	JSONObject map = o["map"];
	for( int i = 0; i < map.Count(); i++ ) {
		mapimg.push_back(ImageManager::LoadImageFromFile(map[i].GetString()));
	}

	setMapProperties(mapimg[0], teximg);
}

int TiledMap::loadShader(JSONObject &vshaderobj, JSONObject &fshaderobj)
{
	if( !checkForKeys(vshaderobj, {"type", "data"}) || !checkForKeys(fshaderobj, {"type", "data"}))
		return 0;

	bool isVFile = !vshaderobj["type"].GetString().compare("file");
	bool isFFile = !fshaderobj["type"].GetString().compare("file");

	gl_prog = new GLProgram();
	if( isVFile && isFFile ) {
		gl_prog->init(
			std::string(RESOURCE_DIR).append(vshaderobj["data"].GetString()).c_str(),
			std::string(RESOURCE_DIR).append(fshaderobj["data"].GetString()).c_str()
		);
	} else if( !isVFile && !isFFile ) {
		gl_prog->init(
			vshaderobj["data"].GetCString(), vshaderobj["data"].GetString().size(),
			fshaderobj["data"].GetCString(), fshaderobj["data"].GetString().size()
		);
	} else {
		// TODO: handle the case when they arent the same
		return 0;
	}

	return 1;
}

size_t TiledMap::getIndex(const Image *img, size_t x, size_t y) const
{
	int w = img->Width();
	int h = img->Height();
	int d = img->Depth();
	int ia = img->IgnoreAlpha();

	if( (int)x > w || (int)y > h )
		return -1;

	if( (*img)[(x * d) + (y * w * d) + (d - 1)] == 0 )
		return -1;

	size_t out = 0;
	for( int i = 0; i < d - ia; i++ ) {
		out |= (*img)[x * d + y * (w * d) + i] << ((d - ia - 1 - i) * 8);
	}

	if( palette.count(out) )
		return palette.at(out);

	return 0;
}

uint8_t TiledMap::getAlpha(const Image *img, size_t x, size_t y) const
{
	const int w = img->Width();
	const int h = img->Height();
	const int d = img->Depth();

	if( (int)x > w || (int)y > h )
		return 0;

	return (*img)[(x * d) + (y * w * d) + (d - 1)];
}

bool TiledMap::InitGL()
{
	teximg->InitGL();

	attr_pos = 0; //gl_prog->attribs("a_Position");
	attr_texcoord = 1; //gl_prog->attribs("a_TexCoordinate");

	return true;
}

void TiledMap::UseProgram()
{
	gl_prog->use();
}

void TiledMap::SetProjection(const G3DMath::Matrix4f &proj, bool useProgram)
{
	if( useProgram )
		gl_prog->use();

	glUniformMatrix4fv(gl_prog->uniforms("u_Proj"), 1, GL_FALSE, &proj);
}

void TiledMap::SetView(const G3DMath::Matrix4f &view, bool useProgram)
{
	if( useProgram )
		gl_prog->use();

	glUniformMatrix4fv(gl_prog->uniforms("u_View"), 1, GL_FALSE, &view);
}

void TiledMap::GetMoveTo(double base_x, double base_y, double &offset_x, double &offset_y) const
{
	// Check east/west bounds
	if( base_x + offset_x < 0.5 ) {
		offset_x = 0.5 - base_x;
	} else if( base_x + offset_x > (double)map_width - 0.5 ) {
		offset_x = (double)map_width - 0.5 - base_x;
	}

	// Check north/south bounds
	if( base_y + offset_y < 0.5 ) {
		offset_y = 0.5 - base_y;
	} else if( base_y + offset_y > (double)map_height - 0.5 ) {
		offset_y = (double)map_height - 0.5 - base_y;
	}

	// Check for hole in east/west layer
	uint8_t a = getAlpha(mapimg[0], (size_t)(base_x + offset_x), (size_t)(base_y));
	if( a != 0xff ) {
		//offset_x = (double)(size_t)(base_x + offset_x);
		offset_x = 0;
	}

	// Check for hole in north/south layer
	a = getAlpha(mapimg[0], (size_t)(base_x), (size_t)(base_y + offset_y));
	if( a != 0xff ) {
		offset_y = 0;
	}
}

bool TiledMap::Render(int left, int top, int width, int height, int layer_start, int layer_end)
{
	bool result = false;

	if( !gl_prog )
		return false;

	gl_prog->use();

	if( top != dtop || left != dleft || width != dwidth || height != dheight )
		result = redraw(left, top, width, height);

	if( layer_end >= layeridx_num )
		layer_end = layeridx_num - 1;

	dtop = top; dleft = left; dwidth = width; dheight = height;

	teximg->Bind();

	glBindBuffer(GL_ARRAY_BUFFER, tilebuf);
	glEnableVertexAttribArray(attr_pos);
	glEnableVertexAttribArray(attr_texcoord);

	glVertexAttribPointer(attr_pos, 3, GL_FLOAT, false, sizeof(float) * 5, 0);
	glVertexAttribPointer(attr_texcoord, 2, GL_FLOAT, false, sizeof(float) * 5, (void*)(sizeof(float) * 3));

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexbuf);
	glDrawElements(GL_TRIANGLES, layeridx[layer_end + 1] - layeridx[layer_start], GL_UNSIGNED_SHORT, (GLvoid*)((size_t)(layeridx[layer_start] * 2)));

	glDisableVertexAttribArray(attr_pos);
	glDisableVertexAttribArray(attr_texcoord);

	return true;
}

bool TiledMap::Render(const G3DMath::BoundingBox<G3DMath::Vec2d> &area, int layer_start, int layer_end)
{
	bool result = false;

	if( !gl_prog )
		return false;

	gl_prog->use();

	if( mLastArea != area ) {
		result = redraw(area);
		mLastArea = area;
	}

	if( layer_end >= layeridx_num )
		layer_end = layeridx_num - 1;

	teximg->Bind();

	glBindBuffer(GL_ARRAY_BUFFER, tilebuf);
	glEnableVertexAttribArray(attr_pos);
	glEnableVertexAttribArray(attr_texcoord);

	glVertexAttribPointer(attr_pos, 3, GL_FLOAT, false, sizeof(float) * 5, 0);
	glVertexAttribPointer(attr_texcoord, 2, GL_FLOAT, false, sizeof(float) * 5, (void*)(sizeof(float) * 3));

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexbuf);
	glDrawElements(GL_TRIANGLES, layeridx[layer_end + 1] - layeridx[layer_start], GL_UNSIGNED_SHORT, (GLvoid*)((size_t)(layeridx[layer_start] * 2)));

	glDisableVertexAttribArray(attr_pos);
	glDisableVertexAttribArray(attr_texcoord);

	return true;
}

bool TiledMap::redraw(int left, int top, int width, int height)
{
	float *buf;
	short *ibuf;

	numtiles = width * height;
	size_t bufsize = numtiles * 4 * 5;
	int numindices = numtiles * 6;
	size_t numlayers = mapimg.size();
	bufsize *= numlayers;
	numindices *= numlayers;

	buf = new float[bufsize];
	ibuf = new short[numindices];

	short cidx = 0;

	int ioffset = 0;
	layeridx_num = 0;
	layeridx[0] = 0;
	for( size_t i = 0; i < numlayers && i < TILEDMAP_MAX_LAYERS; i++ ) {
		for( int y = top; y < top + height; y++ ) {
			for( int x = left; x < left + width; x++ ) {
				int offset = (ioffset / 6) * 20;

				size_t idx = getIndex(mapimg[i], x, y);
				if( idx == -1 )
					continue;

				float tbx = (idx % texatlas_width) * tile_width_pct;
				float tby = (idx / texatlas_height) * tile_height_pct;

				buf[offset + 0] = (float)x;
				buf[offset + 1] = (float)y;
				buf[offset + 2] = (float)i;
				buf[offset + 3] = tbx;
				buf[offset + 4] = tby;

				buf[offset + 5] = (float)x;
				buf[offset + 6] = y + 1.0f;
				buf[offset + 7] = (float)i;
				buf[offset + 8] = tbx;
				buf[offset + 9] = tby + tile_height_pct;

				buf[offset + 10] = x + 1.0f;
				buf[offset + 11] = (float)y;
				buf[offset + 12] = (float)i;
				buf[offset + 13] = tbx + tile_width_pct;
				buf[offset + 14] = tby;

				buf[offset + 15] = x + 1.0f;
				buf[offset + 16] = y + 1.0f;
				buf[offset + 17] = (float)i;
				buf[offset + 18] = tbx + tile_width_pct;
				buf[offset + 19] = tby + tile_height_pct;

				ibuf[ioffset + 0] = cidx + 0;
				ibuf[ioffset + 1] = cidx + 2;
				ibuf[ioffset + 2] = cidx + 1;
				ibuf[ioffset + 3] = cidx + 1;
				ibuf[ioffset + 4] = cidx + 2;
				ibuf[ioffset + 5] = cidx + 3;

				ioffset += 6;
				cidx += 4;
			}
		}
		layeridx[++layeridx_num] = ioffset;
	}

	// update counts based on generated tiles
	numindices = ioffset;
	numtiles = numindices / 6;
	bufsize = numtiles * 4 * 5;

	if( tilebuf || indexbuf ) {
		glDeleteBuffers(1, &tilebuf);
		glDeleteBuffers(1, &indexbuf);
	}

	glGenBuffers(1, &tilebuf);
	glGenBuffers(1, &indexbuf);

	glBindBuffer(GL_ARRAY_BUFFER, tilebuf);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * bufsize, buf, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexbuf);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(short) * numindices, ibuf, GL_STATIC_DRAW);

	delete buf;
	delete ibuf;

	return true;
}

bool TiledMap::redraw(const G3DMath::BoundingBox<G3DMath::Vec2d> &area)
{
	const int left = (int)floor(area.low.x);
	const int top = (int)floor(area.low.y);
	const int width = (int)ceil(area.high.x - area.low.x) + 1;
	const int height = (int)ceil(area.high.y - area.low.y) + 1;
	return redraw(left, top, width, height);
}
