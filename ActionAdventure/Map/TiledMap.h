#pragma once

#include "IMap.h"

#include "opengl.h"

#include <map>
#include <vector>

#define TILEDMAP_MAX_LAYERS		10

class JSONObject;

namespace Map
{
	class TiledMap : public IMap
	{
	public:
		TiledMap();
		~TiledMap();

		void MapTexturePalette(const Image *map, Image *texture, const Image &palette) override;

		void LoadFromJSON(const char *path) override;

		int WidthInt() const override {
			return map_width;
		}

		int HeightInt() const override {
			return map_height;
		}

		double Width() const override {
			return (double)map_width;
		}

		double Height() const override {
			return (double)map_width;
		}

		int NumLayers() const override {
			return layeridx_num;
		}

		G3DMath::Vec2d GetEntryPoint(unsigned int index) const override {
			if( index < mEntryPoints.size() )
				return mEntryPoints.at(index);

			return G3DMath::Vec2d();
		}

		unsigned int NumEntryPoints() const override {
			return mEntryPoints.size();
		}

		void GetMoveTo(double base_x, double base_y, double &offset_x, double &offset_y) const override;

		bool InitGL() override;
		void UseProgram() override;
		void SetProjection(const G3DMath::Matrix4f &proj, bool useProgram = true) override;
		void SetView(const G3DMath::Matrix4f &view, bool useProgram = true) override;
		bool Render(int left, int top, int width, int height, int layer_start, int layer_end) override;
		bool Render(const G3DMath::BoundingBox<G3DMath::Vec2d> &area, int layer_start, int layer_end) override;

	private:
		int loadShader(JSONObject &vshaderobj, JSONObject &fshaderobj);
		void clearTextures();
		void makePalette(const Image *img);
		void setMapProperties(const Image *map, const Image *texture);

		size_t getIndex(const Image *img, size_t x, size_t y) const;
		uint8_t getAlpha(const Image *img, size_t x, size_t y) const;
		bool redraw(int left, int top, int width, int height);
		bool redraw(const G3DMath::BoundingBox<G3DMath::Vec2d> &area);

		GLProgram *gl_prog;

		size_t map_width;
		size_t map_height;

		size_t texatlas_width;
		size_t texatlas_height;

		float tile_width_pct;
		float tile_height_pct;

		size_t max_palette;
		std::map<size_t, size_t> palette;

		std::vector<G3DMath::Vec2d> mEntryPoints;

		G3DMath::BoundingBox2D mLastArea;
		int dtop, dleft, dwidth, dheight;
		int numtiles;
		GLuint tilebuf;
		GLuint indexbuf;

		int layeridx[TILEDMAP_MAX_LAYERS + 1];
		int layeridx_num;

		GLuint atlasbuf;

		GLuint attr_pos;
		GLuint attr_texcoord;

		size_t layers;
		std::vector<const Image*> mapimg;
		Image *teximg;
	};
}
