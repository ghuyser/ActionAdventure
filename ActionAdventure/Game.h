#pragma once

#include "IGame.h"

#include "Identifiers/UUID.h"
#include "System/TimeProfiler.h"
#include "G3DMath.h"
#include "gl_fabric.h"
#include "LuaObject.h"
#include "opengl.h"

#include "Entity/EntityManager.h"
#include "Map/IMap.h"
#include "Renderer/Sprite.h"
#include "Renderer/SpriteRenderer.h"

#include "lua.hpp"

#include <condition_variable>
#include <mutex>
#include <string>
#include <thread>
#include <unordered_map>
#include <utility>

class Enemy;
class Player;

namespace Net {
	class Client;
	class Server;
}

namespace ImGui {
	class RenderStats;
}

class Game : public IGame, virtual public LuaObject {
	LUA_OBJECT

public:
	Game();
	~Game() noexcept;

	int InitGL();
	int InitGame();

	static void InitLua();

	void DoActions();

	void ResizeGLScene(int width, int height);
	bool Render();

	void InputEventProc(InputType t, InputEvent e);

	char* ExecuteLua(const char* command);
	char* PollLuaBuffer();

	void MoveCamera(double dx, double dy, double dz);

	void AddLog(LogType, const char*, const char*, int, const char*, ...);
	void AddLogV(LogType, const char*, const char*, int, const char*, void*);

private:
	// Game control functions
	void movePlayer(double x, double y, double z = 0);
	void updateMapBox();

	// Thread handling
	struct Thread {
		std::thread *thread;
		std::mutex crit;
		std::condition_variable cond;
		uint32_t flags;
	};
	int wait_flags(Thread &t, uint32_t *oflg, uint32_t mask);
	int wait_flags_timed(Thread &t, uint32_t *oflg, uint32_t mask, int ms);
	void set_flags(Thread &t, uint32_t flags);

	// Main ImGUI rendering function
	void renderImGUI();

	// ImGUI widgets
	void gui_app_menu();
	void gui_lua_state_window();
	void gui_lua_stack_window();
	
	// ImGUI control functions
	void gui_add_window(const std::string name, void *window, bool show = true);
	bool gui_toggle_window(const std::string name);
	bool gui_show_window(const std::string name, bool show = true);
	bool* gui_get_toggle_bool(const std::string name);

	// ImGUI states
	bool mGuiAppMenu;
	bool mGuiDemoWindow;
	bool mGuiLuaStateWindow;
	bool mGuiLuaStackWindow;
	std::unordered_map<std::string, std::pair<bool, void*>> mGuiWindows;

	// Net functions
	void startServer();
	void connectClient();

	void playerConnected(bool connected, Identifiers::UUID user_id, void *data);
	void updatePlayerState(Identifiers::UUID user_id, void *data);

	System::TimeProfiler mGameTimer;
	System::TimeProfiler mUpdateTimer;

	// Performance tracking
	ImGui::RenderStats *mRenderWindow;
	System::TimeProfiler mRenderTimer;
	System::TimeProfiler mFPSTimer;
	int mFPS;

	SpriteRenderer mSpriteRenderer;
	G3DMath::Matrix4<float> mView;
	G3DMath::Matrix4<float> mProj;

	double mPixelScale;
	double mUnitWidth, mUnitHeight;
	G3DMath::BoundingBox<G3DMath::Vec2d> mMapBox;

	Player *mPlayer;

	std::mutex mMutexPlayerCharacters;
	std::unordered_map<Identifiers::UUID, Sprite*> mPlayerCharacters;

	EntityManager mEntityManager;

	uint8_t mPressedKeys[256];

	// -----------------------------------------------
	// Lua section
	// -----------------------------------------------
	void initializeLua();
	void luaThread();

	void luaAddCommand(char *command);

	void *mLuaConsole;

	Thread mLuaThread;
	std::mutex mLuaMutex;
	std::thread *mLuaCommandThread;

	lua_State *mLua;
	lua_State *mLuaThreads[2];

	char mServerAddr[256];
	uint16_t mServerPort;
	Net::Client *mClient;
	Net::Server *mServer;

	// Game class lua functions
	LUA_FUNCTION(lg_getplayer);

	// Global lua functions
	LUA_FUNCTION(l_print);
	LUA_FUNCTION(l_sleep);
};
