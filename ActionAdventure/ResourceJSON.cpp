#include "ResourceJSON.h"

#include <memory.h>
#include <stdio.h>
#include <stdlib.h>

#define TAB_STR "	"

ResourceJSON::ResourceJSON()
	: mSourceName( nullptr )
{
}

ResourceJSON::~ResourceJSON()
{
	if( mSourceName ) {
		free( mSourceName );
	}
}

void ResourceJSON::Load(const char *sourceFile)
{
	if( mSourceName ) {
		free( mSourceName );
		mSourceName = nullptr;
		mBase.MakeNull();
	}

	FILE *fp = fopen(sourceFile, "r");
	if( !fp ) {
		return;
	}

	fseek(fp, 0, SEEK_END);
	int size_bytes = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	char *data = (char*)malloc(size_bytes);
	if( !data ) {
		fclose(fp);
		return;
	}
	fread(data, 1, size_bytes, fp);
	fclose(fp);

	mBase.Parse(data, size_bytes);
	free(data);

	mSourceName = (char*)malloc(strlen(sourceFile) + 1);
	memcpy(mSourceName, sourceFile, strlen(sourceFile) + 1);
}

void ResourceJSON::Save(bool pretty)
{
	std::string serialized = mBase.Serialize();
	std::string prettyStr;
	std::string *writeStr = &serialized;

	if( pretty ) {
		prettyStr = makePretty(serialized);
		writeStr = &prettyStr;
	}

	// Write the file
	FILE *fp = fopen(mSourceName, "w");
	fwrite(writeStr->c_str(), 1, writeStr->size(), fp);
	fclose(fp);
}

JSONObject& ResourceJSON::operator[](const std::string& key)
{
	return mBase[key];
}

const JSONObject& ResourceJSON::operator[](const std::string& key) const
{
	return mBase[key];
}

std::string ResourceJSON::makePretty(const std::string &src, int depth)
{
	std::string res;

	int d = 0;
	bool quote = false;
	for(size_t i = 0; i < src.length(); i++) {
		char c = src[i];
		
		if( quote ) {
			if( c == '"' ) {
				quote = false;
			}
			res += c;
			continue;
		}

		if( c == '{' || c == '[' ) {
			d++;
			res += c;
			res += '\n';
			for(int t = 0; t < d; t++) {
				res += TAB_STR;
			}
		} else if( c == '}' || c == ']' ) {
			--d;
			res += '\n';
			for(int t = 0; t < d; t++) {
				res += TAB_STR;
			}
			res += c;
		} else if( c == ',' ) {
			res += c;
			res += '\n';
			for(int t = 0; t < d; t++) {
				res += TAB_STR;
			}
		} else if( c == ':' ) {
			res += c;
			res += ' ';
		} else {
			res += c;
			if( c == ':' ) {
				res += ' ';
			} else if( c == '"' ) {
				quote = true;
			}
		}
	}

	res += '\n';

	return res;
}
