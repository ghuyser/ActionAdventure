#pragma once

#include "imgui.h"

namespace ImGui
{
	constexpr int NumFramesStored = 30;
	constexpr int NumRenderTimesStored = 60 * 5;

	class IWindow
	{
	public:
		virtual void Draw(bool*) = 0;
	};
}
