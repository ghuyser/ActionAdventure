#include "DevTools/ClientSetup.h"

#include "Net/Client.h"

#include <stdio.h>
#include <stdlib.h>

namespace ImGui
{

ClientSetup::ClientSetup(Net::Client **client, char *addr, size_t addr_sz, uint16_t *port)
	: mClient( client )
	, mAddress( addr )
	, mAddressSize( addr_sz )
	, mPort( port )
{
}

void ClientSetup::Draw(bool *p_open)
{
	ImGui::SetNextWindowSize(ImVec2(350, 80), ImGuiCond_Always);

	if(!ImGui::Begin("Client Settings", p_open))
	{
		ImGui::End();
		return;
	}

	ImGui::InputText("Server Address", mAddress, mAddressSize, ImGuiInputTextFlags_None);

	static char cPort[6];
	snprintf(cPort, 6, "%hu", *mPort);
	struct TextFilters { static int FilterPort(ImGuiInputTextCallbackData* data) { return (data->EventChar < '0' || data->EventChar > '9'); } };
	ImGui::InputText("Server Port", cPort, IM_ARRAYSIZE(cPort), ImGuiInputTextFlags_CallbackCharFilter, TextFilters::FilterPort);
	*mPort = (uint16_t)atoi(cPort);
	ImGui::End();
}

} /* namespace ImGui */
