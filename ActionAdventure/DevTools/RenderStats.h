#pragma once

#include "DevTools/IWindow.h"

namespace ImGui
{
	class RenderStats : public IWindow
	{
	public:
		RenderStats();

		void AddFrame(int i);
		void AddRenderTime(float t);
		void Draw(bool *p_open = NULL);

	private:
		float mFrames[NumFramesStored];
		int mNumFrames;

		float mRenderTimes[NumRenderTimesStored];
		int mNumRendertimes;
	};
}
