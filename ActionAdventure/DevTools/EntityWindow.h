#pragma once

#pragma once

#include "IWindow.h"

#include <string>
#include <unordered_map>

namespace ImGui
{
	class EntityWindow : public IWindow
	{
	public:
		EntityWindow();

		void Draw(bool *p_open = NULL);

	private:
		std::unordered_map<std::string, bool> mSelected;
	};
}
