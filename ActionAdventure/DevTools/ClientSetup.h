#pragma once

#include "IWindow.h"

#include "types.h"

namespace Net {
	class Client;
}

namespace ImGui
{
	class ClientSetup : public IWindow
	{
	public:
		// addr MUST be a char[16]
		ClientSetup(Net::Client **client, char *addr, size_t addr_size, uint16_t *port);

		void Draw(bool *p_open = NULL);

	private:
		Net::Client **mClient;
		char *mAddress;
		size_t mAddressSize;
		uint16_t *mPort;
	};
}
