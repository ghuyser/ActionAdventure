#pragma once

#include "DevTools/IWindow.h"

#include <mutex>

namespace ImGui
{
	class LogWindow : public IWindow
	{
	public:
		enum class LogType {
			Normal,
			Warning,
			Error
		};

		LogWindow(const char *title);

		void Clear();
		void AddLog(LogType type, const char* filename, int lineno, const char* fmt, va_list args);

		void Draw(bool* p_open = NULL);

	private:
		void addLog(LogType type, const char* filename, int lineno, const char* text_start, const char *text_end = 0);
		void DrawLine(int line_no);

		ImGuiTextBuffer     Buf;
		ImGuiTextFilter     Filter;
		ImVector<int>       LineOffsets;        // Index to lines offset. We maintain this with AddLog() calls, allowing us to have a random access on lines
		ImGuiTextBuffer mFilenames;
		ImVector<int>   mFilenameOffsets;
		ImVector<int>   mLineNumbers;
		ImVector<LogType> mLineTypes;
		bool                AutoScroll;     // Keep scrolling if already at the bottom
		const char *mTitle;

		std::mutex mMutexAddText;
	};
}
