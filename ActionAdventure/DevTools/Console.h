#pragma once

#include "IWindow.h"

#include <functional>

struct lua_State;

namespace ImGui
{
	class Console : public IWindow
	{
	public:
		Console();
		~Console();

		void AddLog(const char *fmt, ...) IM_FMTARGS(2);
		void AddLogS(const char *log);
		void ClearLog();

		void Draw(bool* p_open = NULL);

		void ExecCommand(const char* command_line);

		void SetLuaExecuteCommand(std::function<void(const char*)> f);

		int TextEditCallback(ImGuiInputTextCallbackData* data);

	private:
		std::function<void(const char*)> mExecute;
		char                  InputBuf[256];
		ImVector<char*>       Items;
		ImVector<const char*> Commands;
		ImVector<char*>       History;
		int                   HistoryPos;    // -1: new line, 0..History.Size-1 browsing history.
		ImGuiTextFilter       Filter;
		bool                  AutoScroll;
		bool                  ScrollToBottom;
	};
}
