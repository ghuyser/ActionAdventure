#include "DevTools/RenderStats.h"

#include <stdio.h>

namespace ImGui
{

RenderStats::RenderStats()
{
	memset(mFrames, 0, sizeof(float) * NumFramesStored);
	mNumFrames = 0;

	memset(mRenderTimes, 0, sizeof(float) * NumRenderTimesStored);
	mNumRendertimes = 0;
}

void RenderStats::AddFrame(int i)
{
	if(mNumFrames == NumFramesStored) {
		memcpy(mFrames, mFrames + 1, sizeof(float) * (NumFramesStored - 1));
		mFrames[NumFramesStored - 1] = (float)i;
	} else {
		mFrames[mNumFrames++] = (float)i;
	}
}

void RenderStats::AddRenderTime(float t)
{
	if(mNumRendertimes == NumRenderTimesStored) {
		memcpy(mRenderTimes, mRenderTimes + 1, sizeof(float) * (NumRenderTimesStored - 1));
		mRenderTimes[NumRenderTimesStored - 1] = t;
	} else {
		mRenderTimes[mNumRendertimes++] = t;
	}
}

void RenderStats::Draw(bool *p_open)
{
	ImGui::SetNextWindowSize(ImVec2(500, 400), ImGuiCond_FirstUseEver);

	if(!ImGui::Begin("Rendering Stats", p_open))
	{
		ImGui::End();
		return;
	}

	char overlay[32];
	sprintf(overlay, "%d fps", (int)mFrames[mNumFrames - 1]);
	ImGui::PlotHistogram("Rendering Rate", mFrames, mNumFrames, 0, overlay, 0, 75, ImVec2(0, 80));

	sprintf(overlay, "%f ms", mRenderTimes[mNumRendertimes - 1]);
	ImGui::PlotLines("Rendering Time", mRenderTimes, mNumRendertimes, 0, overlay, 0, 7, ImVec2(0, 80));

	ImGui::End();
}

} /* namespace ImGui */
