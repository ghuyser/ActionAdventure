#include "DevTools/EntityWindow.h"

#include "Entity/EntityRegister.h"

namespace ImGui
{

EntityWindow::EntityWindow()
{
}

void EntityWindow::Draw(bool *p_open)
{
	ImGui::SetNextWindowSize(ImVec2(500, 400), ImGuiCond_FirstUseEver);

	if(!ImGui::Begin("Entities Registered", p_open))
	{
		ImGui::End();
		return;
	}

	static ResourceJSON *selected(nullptr);

	if( ImGui::ListBoxHeader("", EntityRegister::Instance()->NumRegistered(), 8) ) {
		for(auto i = EntityRegister::Instance()->Begin(); i != EntityRegister::Instance()->End(); i++) {
			if( mSelected.count(i->first) == 0 ) {
				mSelected[i->first] = false;
			}
			if( mSelected[i->first] && selected != i->second ) {
				selected = i->second;

				// Force "maxSpeed" to be a double value.
				JSONObject &ms = (*selected)["stats"]["maxSpeed"];
				double tmp = ms.GetDouble();
				ms = 0.1;
				ms = tmp;
			}
			ImGui::Selectable((*(i->second))["name"].GetCString(), &mSelected[i->first]);
		}
		ImGui::ListBoxFooter();
	}

	bool updated = false;
	if( selected ) {
		ResourceJSON &r = *selected;
		JSONObject &s = r["stats"];
		JSONObject &v = s["vitals"];
		JSONObject &atr = s["attributes"];
		JSONObject &afn = s["affinities"];
		const uint32_t u8_min = 1, u8_max = 255;
		const int32_t afn_min = -100, afn_max = 100;
		const uint32_t vit_min = 0, vit_max = 9999;
		const double sp_min = 0.1, sp_max = 30.0;
		ImGui::Text("Name: %s", r["name"].GetCString());
		ImGui::Text("Animation: %s", r["animation"].GetCString());
		updated |= ImGui::SliderScalar("Level", ImGuiDataType_U8, s["level"].Raw(), &u8_min, &u8_max);
		updated |= ImGui::SliderScalar("Max speed (m/s)", ImGuiDataType_Double, s["maxSpeed"].Raw(), &sp_min, &sp_max);
		updated |= ImGui::SliderScalar("HP",        ImGuiDataType_U32, v["HP"].Raw(), &vit_min, &vit_max);
		updated |= ImGui::SliderScalar("MP",        ImGuiDataType_U32, v["MP"].Raw(), &vit_min, &vit_max);
		updated |= ImGui::SliderScalar("SP",        ImGuiDataType_U32, v["SP"].Raw(), &vit_min, &vit_max);
		updated |= ImGui::SliderScalar("STR",       ImGuiDataType_U32, atr["str"].Raw(), &u8_min, &u8_max);
		updated |= ImGui::SliderScalar("FRT",       ImGuiDataType_U32, atr["frt"].Raw(), &u8_min, &u8_max);
		updated |= ImGui::SliderScalar("AGI",       ImGuiDataType_U32, atr["agi"].Raw(), &u8_min, &u8_max);
		updated |= ImGui::SliderScalar("DEX",       ImGuiDataType_U32, atr["dex"].Raw(), &u8_min, &u8_max);
		updated |= ImGui::SliderScalar("MND",       ImGuiDataType_U32, atr["mnd"].Raw(), &u8_min, &u8_max);
		updated |= ImGui::SliderScalar("INT",       ImGuiDataType_U32, atr["int"].Raw(), &u8_min, &u8_max);
		updated |= ImGui::SliderScalar("CHR",       ImGuiDataType_U32, atr["chr"].Raw(), &u8_min, &u8_max);
		updated |= ImGui::SliderScalar("LUK",       ImGuiDataType_U32, atr["luk"].Raw(), &u8_min, &u8_max);
		updated |= ImGui::SliderScalar("Fire",      ImGuiDataType_S32, afn["fire"].Raw(), &afn_min, &afn_max);
		updated |= ImGui::SliderScalar("Ice",       ImGuiDataType_S32, afn["ice"].Raw(), &afn_min, &afn_max);
		updated |= ImGui::SliderScalar("Wind",      ImGuiDataType_S32, afn["wind"].Raw(), &afn_min, &afn_max);
		updated |= ImGui::SliderScalar("Water",     ImGuiDataType_S32, afn["water"].Raw(), &afn_min, &afn_max);
		updated |= ImGui::SliderScalar("Earth",     ImGuiDataType_S32, afn["earth"].Raw(), &afn_min, &afn_max);
		updated |= ImGui::SliderScalar("Lightning", ImGuiDataType_S32, afn["lightning"].Raw(), &afn_min, &afn_max);
		updated |= ImGui::SliderScalar("Light",     ImGuiDataType_S32, afn["light"].Raw(), &afn_min, &afn_max);
		updated |= ImGui::SliderScalar("Darkness",  ImGuiDataType_S32, afn["darkness"].Raw(), &afn_min, &afn_max);
	}

	ImGui::End();

	if( updated ) {
		selected->Save(true);
	}
}

} /* namespace ImGui */
