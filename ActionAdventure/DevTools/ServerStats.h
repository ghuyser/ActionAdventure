#pragma once

#include "DevTools/IWindow.h"

namespace Net {
	class Server;
}

namespace ImGui
{
	class ServerStats : public IWindow
	{
	public:
		ServerStats(Net::Server **server);

		void Draw(bool *p_open = NULL);

	private:
		Net::Server **mServer;
	};
}
