#include "DevTools/ServerStats.h"

#include "Net/Server.h"

namespace ImGui
{

ServerStats::ServerStats(Net::Server **server)
	: mServer( server )
{
}

void ServerStats::Draw(bool *p_open)
{
	ImGui::SetNextWindowSize(ImVec2(500, 400), ImGuiCond_FirstUseEver);

	if(!ImGui::Begin("Server Stats", p_open))
	{
		ImGui::End();
		return;
	}

	if( *mServer ) {
		float *rvalues = (*mServer)->GetReceiveRates();
		float *svalues = (*mServer)->GetSendRates();
		size_t num_divisions = Net::RateNumDivisions - 1;
		float rmax = 100.f, smax = 100.f;
		for(size_t i = 0; i < num_divisions; i++) {
			while(rvalues[i] > rmax)
				rmax *= 2;

			while(svalues[i] > smax)
				smax *= 2;
		}
		char overlay[32];
		sprintf(overlay, "%.0f bytes/s", rvalues[num_divisions-1]);
		ImGui::PlotLines("Receive Rate", rvalues, num_divisions, 0, overlay, 0, rmax, ImVec2(0,80));

		sprintf(overlay, "%.0f bytes/s", svalues[num_divisions-1]);
		ImGui::PlotLines("Send Rate", svalues, num_divisions, 0, overlay, 0, smax, ImVec2(0,80));

		ImGui::Text("Connected clients:");

		std::vector<Identifiers::UUID> clients = (*mServer)->GetClients();
		ImGui::ListBoxHeader("", clients.size(), 8);
		for( auto i : clients )
			ImGui::Text("%s", i.getString().c_str());
		ImGui::ListBoxFooter();

	} else {
		ImGui::PlotLines("Receive Rate", 0, 0, 0, 0, 0, 0, ImVec2(0,80));
		ImGui::PlotLines("Send Rate", 0, 0, 0, 0, 0, 0, ImVec2(0,80));
	}
	ImGui::End();
}

} /* namespace ImGui */
