#include "DevTools/LogWindow.h"

#include <stdio.h>
#include <stdlib.h>

namespace ImGui
{

LogWindow::LogWindow(const char *title)
	: mTitle(title)
{
	AutoScroll = true;
	Clear();
}

void LogWindow::Clear()
{
	Buf.clear();
	LineOffsets.clear();
	LineOffsets.push_back(0);
	mFilenames.clear();
	mFilenameOffsets.clear();
	mLineNumbers.clear();
}

void LogWindow::AddLog(LogType type, const char* filename, int lineno, const char* fmt, va_list args)
{
	size_t sz = strlen(fmt) * 2;
	char *buf = (char*)malloc(sz);

	va_list args_copy;
	va_copy(args_copy, args);
	int n = vsnprintf(buf, sz, fmt, args);
	if( n <= 0 ) {
		va_end(args_copy);
		return;
	}

	if( (size_t)n >= sz ) {
		sz = (size_t)n + 1;
		buf = (char*)realloc(buf, sz);
		n = vsnprintf(buf, sz, fmt, args_copy);
	}
	buf[n] = 0;
	va_end(args_copy);

	int start = 0;
	for(size_t i = 0; i < strlen(buf); i++) {
		if( buf[i] == '\n' ) {
			const char *sstart = buf + start;
			const char *send = buf + i;
			start = i + 1;
			addLog(type, filename, lineno, sstart, send);
		}
	}
	if( start != strlen(buf) ) {
		addLog(type, filename, lineno, buf + start);
	}
}

void LogWindow::addLog(LogType type, const char* filename, int lineno, const char* text_start, const char *text_end)
{
	std::lock_guard<std::mutex> guard(mMutexAddText);

	int old_size = Buf.size();

	switch(type) {
	default:
	case LogType::Normal:
		Buf.appendf("[info] ");
		break;

	case LogType::Warning:
		Buf.appendf("[warn] ");
		break;

	case LogType::Error:
		Buf.appendf("[err ] ");
		break;
	};

	Buf.append(text_start, text_end);

	if(*Buf.end() != '\n')
		Buf.appendf("\n");

	for(int new_size = Buf.size(); old_size < new_size; old_size++)
		if(Buf[old_size] == '\n')
			LineOffsets.push_back(old_size + 1);

	mFilenameOffsets.push_back(mFilenames.size());
	if(filename) {
		mFilenames.append(filename);
	}
	mLineNumbers.push_back(lineno);
	mLineTypes.push_back(type);
}

void LogWindow::Draw(bool* p_open)
{
	std::lock_guard<std::mutex> guard(mMutexAddText);

	ImGui::SetNextWindowSize(ImVec2(500, 400), ImGuiCond_FirstUseEver);

	if(!ImGui::Begin(mTitle, p_open))
	{
		ImGui::End();
		return;
	}

	// Options menu
	if(ImGui::BeginPopup("Options"))
	{
		ImGui::Checkbox("Auto-scroll", &AutoScroll);
		ImGui::EndPopup();
	}

	// Main window
	if(ImGui::Button("Options"))
		ImGui::OpenPopup("Options");
	ImGui::SameLine();
	bool clear = ImGui::Button("Clear");
	ImGui::SameLine();
	bool copy = ImGui::Button("Copy");
	ImGui::SameLine();
	Filter.Draw("Filter", -100.0f);

	ImGui::Separator();
	ImGui::BeginChild("scrolling", ImVec2(0, 0), false, ImGuiWindowFlags_HorizontalScrollbar);

	if(clear)
		Clear();
	if(copy)
		ImGui::LogToClipboard();

	ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0, 0));
	const char* buf = Buf.begin();
	const char* buf_end = Buf.end();
	if(Filter.IsActive())
	{
		for(int line_no = 0; line_no < LineOffsets.Size; line_no++)
		{
			const char* line_start = buf + LineOffsets[line_no];
			const char* line_end = (line_no + 1 < LineOffsets.Size) ? (buf + LineOffsets[line_no + 1] - 1) : buf_end;
			if(Filter.PassFilter(line_start, line_end))
				DrawLine(line_no);
		}
	} else
	{
		ImGuiListClipper clipper;
		clipper.Begin(LineOffsets.Size);
		while(clipper.Step())
		{
			for(int line_no = clipper.DisplayStart; line_no < clipper.DisplayEnd; line_no++)
			{
				DrawLine(line_no);
			}
		}
		clipper.End();
	}
	ImGui::PopStyleVar();

	if(AutoScroll && ImGui::GetScrollY() >= ImGui::GetScrollMaxY())
		ImGui::SetScrollHereY(1.0f);

	ImGui::EndChild();
	ImGui::End();
}

void LogWindow::DrawLine(int line_no)
{
	const char* line_start = Buf.begin() + LineOffsets[line_no];
	const char* line_end = (line_no + 1 < LineOffsets.Size) ? (Buf.begin() + LineOffsets[line_no + 1] - 1) : Buf.end();

	if(line_start == line_end)
		return;

	if(mLineTypes[line_no] == LogType::Error) {
		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.0f, 0.0f, 0.0f, 1.0f));
	} else if(mLineTypes[line_no] == LogType::Warning) {
		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.0f, 1.0f, 0.0f, 1.0f));
	} else {
		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.0f, 1.0f, 1.0f, 1.0f));
	}
	ImGui::TextUnformatted(line_start, line_end);
	if(ImGui::IsItemHovered()) {
		const char *fnbuf = mFilenames.begin();
		const char *f_start = fnbuf + mFilenameOffsets[line_no];
		const char *f_end = (line_no + 1 < mFilenameOffsets.Size) ? (fnbuf + mFilenameOffsets[line_no + 1]) : mFilenames.end();
		if(f_end > f_start) {
			ImGui::SetTooltip("%.*s:%d", static_cast<int>(f_end - f_start), f_start, mLineNumbers[line_no]);
		}
	}
	ImGui::PopStyleColor();
}

} /* namespace ImGui */
