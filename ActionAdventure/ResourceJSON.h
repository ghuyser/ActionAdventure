#pragma once

#include "jsonobject.h"

class ResourceJSON
{
public:
	ResourceJSON();
	~ResourceJSON();

	void Load(const char *sourceFile);
	void Save(bool pretty = false);

	      JSONObject& operator[](const std::string& key);
	const JSONObject& operator[](const std::string& key) const;

private:
	std::string makePretty(const std::string &src, int depth = 0);

	char *mSourceName;
	JSONObject mBase;
};
