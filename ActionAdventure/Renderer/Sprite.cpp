#include "Sprite.h"

#include "SpriteRenderer.h"

#include <math.h>

const char *strStates[] = {
	"Standing",
	"Walking",
	"Attacking",
	"Incapacitated"
};

const char *strFacings[] = {
	"North",
	"NorthEast",
	"East",
	"SouthEast",
	"South",
	"SouthWest",
	"West",
	"NorthWest",
	"All"
};

const size_t frameShift = (size_t)log2l((long)Sprite::Facing::None) + 1;
const size_t maxStateFacing = (size_t)EntityState::None << frameShift | (size_t)Sprite::Facing::None;

inline const char* getStateString(EntityState s)
{
	return strStates[(unsigned int)s];
}

inline const char* getFacingString(Sprite::Facing f)
{
	return strFacings[(unsigned int)f];
}

EntityState getStateValue(const char *str)
{
	for(int i = 0; i < sizeof(strStates) / sizeof(const char*); i++) {
		if( !strcmp(str, strStates[i]) )
			return (EntityState)i;
	}

	return EntityState::None;
}

Sprite::Facing getFacingValue(const char *str)
{
	for(int i = 0; i < sizeof(strFacings) / sizeof(const char*); i++) {
		if(!strcmp(str, strFacings[i]))
			return (Sprite::Facing)i;
	}

	return Sprite::Facing::None;
}

Sprite::Sprite()
	: mParent(nullptr)
	, mPosition(1.0, 1.0, 0.0)
	, mSize(0, 0)
	, mRepeatAnimation(true)
{
	mSheet = NULL;

	mCurrentState = EntityState::Standing;
	mCurrentFacing = Facing::South;
	mMyTime = -1.0;

	mFrames.resize(maxStateFacing);
	mLastFrame = 0;

	memset(mSubSprites, 0, sizeof(Sprite*) * (size_t)EntityState::None);
}

Sprite::~Sprite()
{
	for(size_t i = 0; i < (size_t)EntityState::None; i++) {
		if( mSubSprites[i] )
			delete mSubSprites[i];
	}
}

/*void Sprite::InitLua()
{
	LUA_OBJECT_TYPE(Sprite);
	LuaObjectType<Sprite>::AddMethod("move", Sprite::l_move);
	LuaObjectType<Sprite>::AddMethod("setFacing", Sprite::l_setFacing);
	LuaObjectType<Sprite>::AddMethod("setPosition", Sprite::l_setPosition);
	LuaObjectType<Sprite>::AddMethod("setState", Sprite::l_setState);

	// TODO: make a Facing table as a subtable of Sprite table?
	// TODO: Find a better way to handle enums?
	LuaObjectType<Sprite>::AddConst("FacingNorth",     (int)Sprite::Facing::North);
	LuaObjectType<Sprite>::AddConst("FacingNorthEast", (int)Sprite::Facing::NorthEast);
	LuaObjectType<Sprite>::AddConst("FacingEast",      (int)Sprite::Facing::East);
	LuaObjectType<Sprite>::AddConst("FacingSouthEast", (int)Sprite::Facing::SouthEast);
	LuaObjectType<Sprite>::AddConst("FacingSouth",     (int)Sprite::Facing::South);
	LuaObjectType<Sprite>::AddConst("FacingSouthWest", (int)Sprite::Facing::SouthWest);
	LuaObjectType<Sprite>::AddConst("FacingWest",      (int)Sprite::Facing::West);
	LuaObjectType<Sprite>::AddConst("FacingNorthWest", (int)Sprite::Facing::NorthWest);
}*/

void Sprite::SpriteSheet(Image *sprite_sheet)
{
	if( sprite_sheet )
		mSheet = sprite_sheet;
}

Image* Sprite::SpriteSheet()
{
	return mSheet;
}

void Sprite::Size(GLuint width, GLuint height)
{
	mSize.x = width;
	mSize.y = height;
}

int Sprite::getVectorIndex(EntityState s, Facing f)
{
	int a = static_cast<int>(s);
	int b = static_cast<int>(f);
	return a << frameShift | b;
}

void Sprite::LoadAnimation(const char *filename)
{
	if( !filename ) return;
	FILE *fp = fopen(filename, "r");
	if( !fp ) return;
	fseek(fp, 0, SEEK_END);
	size_t sz = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	void *buf = malloc(sz);
	if( !buf ) {
		fclose(fp);
		return;
	}
	fread(buf, 1, sz, fp);
	fclose(fp);
	LoadAnimation(buf, sz);
	free(buf);
}

void Sprite::LoadAnimation(const void *buffer, size_t buffersz)
{
	JSONObject o((const char*)buffer, buffersz);
	LoadAnimation(o);
}

void Sprite::LoadAnimation(const JSONObject &obj, EntityState subSprite)
{
	if( !obj.hasKey("width") || !obj.hasKey("height") )
		return;

	Size(obj["width"].GetInteger(), obj["height"].GetInteger());

	if( subSprite != EntityState::None ) {
		loadAnimState( subSprite, obj );
	}

	// Keep track of the states that copy their frames from
	// another state.  Put them in the deferred buffer to ensure
	// their source is loaded before trying to copy.
	std::vector<std::pair<EntityState, JSONObject>> deferred;

	for(auto i = obj.begin(); i != obj.end(); i++) {
		const EntityState s = getStateValue( i.Key().c_str() );
		if( s != EntityState::None ) {
			if( i.Value().hasKey("SameAs") ) {
				deferred.push_back( std::pair<EntityState, JSONObject>(s, i.Value()) );
			} else {
				loadAnimState( s, i.Value() );
			}
		}
	}

	// Now the deferred states can be loaded since their source states
	// are garaunteed to be loaded now.
	for( auto i = deferred.begin(); i != deferred.end(); i++ ) {
		loadAnimState( i->first, i->second );
	}
}

void Sprite::loadAnimState(EntityState s, const JSONObject &o)
{
	if( s == EntityState::None )
		return;

	if( o.hasKey("SameAs") ) {
		EntityState copyState = getStateValue( o["SameAs"].GetCString() );
		copyStateAnimations( copyState, s );
	}

	if( o.hasKey("SubSprite") ) {
		Sprite *sprite = new Sprite();
		sprite->mParent = this;
		sprite->LoadAnimation( o["SubSprite"], s );
		if( mSubSprites[(size_t)s] ) {
			delete mSubSprites[(size_t)s];
		}
		sprite->SetState( s );
		mSubSprites[(size_t)s] = sprite;
	}

	for(auto i = o.begin(); i != o.end(); i++) {
		const Facing f = getFacingValue( i.Key().c_str() );
		if( f != Facing::None )
			loadAnimFacing( s, f, i.Value() );
	}
}

void Sprite::loadAnimFacing(EntityState s, Facing f, const JSONObject &o)
{
	if( s == EntityState::None || f == Facing::None )
		return;

	for( int i = 0; i < o.Count(); i++ ) {
		JSONObject v = o[i];
		if( !v.hasKey("x") || !v.hasKey("y") || !v.hasKey("duration") )
			continue;

		AddFrame(s, f, v["x"].GetInteger(), v["y"].GetInteger(), v["duration"].GetDouble());
	}
}

void Sprite::AddFrame(EntityState s, Facing f, GLuint x_pos, GLuint y_pos, double duration)
{
	frame_info fi = {
		x_pos, y_pos, duration
	};
	mFrames[getVectorIndex(s, f)].push_back(fi);
}

void Sprite::copyStateAnimations(EntityState from, EntityState to)
{
	size_t fromMask = (size_t)from << frameShift;
	size_t toMask = (size_t)to << frameShift;

	for(size_t i = 0; i < (size_t)Facing::None; i++) {
		mFrames[toMask | i] = mFrames[fromMask | i];
	}
}

Sprite::frame_info* Sprite::updateFrameInfo(double curtime)
{
	std::vector<frame_info> &fi = mFrames[getVectorIndex(mCurrentState, mCurrentFacing)];
	
	if( fi.size() == 0 )
		fi = mFrames[getVectorIndex(mCurrentState, Facing::All)];

	if( fi.size() == 0 )
		return nullptr;

	if( (int)fi.size() <= mLastFrame ) {
		mLastFrame = 0;
	}

	if( mMyTime < 0.0 ) {
		mMyTime = curtime;
		mLastFrame = 0;
	}

	while( curtime - mMyTime > fi[mLastFrame].duration && fi[mLastFrame].duration != 0 ) {
		double d = fi[mLastFrame].duration;
		mMyTime += fi[mLastFrame].duration;
		mLastFrame++;
		if( mLastFrame >= (int)fi.size() ) {
			AnimationComplete();
			if( !mRepeatAnimation ) {
				mRepeatAnimation = true;
				return nullptr;
			}
			mLastFrame = 0;
		}
	}

	return &fi[mLastFrame];
}

void Sprite::SetState(EntityState s)
{
	if( mCurrentState == s )
		return;

	mCurrentState = s;
	mMyTime = -1.0;


	Sprite *subSprite = mSubSprites[(size_t)mCurrentState];
	if( subSprite ) {
		subSprite->mMyTime = -1.0;
		updateSubSprite();
	}
}

void Sprite::SetFacing(G3DMath::Vec2d v)
{
	Facing f = Facing::None;
	mFacingDir = v;

	double rad = atan2(-v.y, v.x);
	double s = M_PI_2 + M_PI_4;
	int fi = 0;
	while( fi < (int)Facing::All ) {
		if( s >= rad && rad > (s - M_PI_2) ) {
			SetFacing( (Facing)fi );
			return;
		}
		fi += 2;
		s -= M_PI_2;
		if( s < -M_PI ) {
			s += M_PI;
		}
	}
}

void Sprite::SetFacing(Facing f)
{
	mCurrentFacing = f;

	Sprite *subSprite = mSubSprites[(size_t)mCurrentState];
	if( subSprite ) {
		subSprite->SetFacing( mCurrentFacing );
	}
}

void Sprite::SetPosition(const Sprite *sprite)
{
	mPosition = sprite->mPosition;
}

void Sprite::SetPosition(const Sprite &sprite)
{
	mPosition = sprite.mPosition;
}

void Sprite::SetPosition(double x, double y)
{
	mPosition.x = x;
	mPosition.y = y;

	updateSubSprite();
}

void Sprite::SetPosition(double x, double y, double z)
{
	mPosition.x = x;
	mPosition.y = y;
	mPosition.z = z;

	updateSubSprite();
}

void Sprite::RepeatCurrentAnimation(bool r)
{
	mRepeatAnimation = r;
	Sprite *subSprite = mSubSprites[(size_t)mCurrentState];
	if( subSprite ) {
		subSprite->mRepeatAnimation = r;
		subSprite->AnimationComplete.disconnect();
		subSprite->AnimationComplete = AnimationComplete;
	}
}

void Sprite::updateSubSprite()
{
	Sprite *subSprite = mSubSprites[(size_t)mCurrentState];
	if( subSprite ) {
		subSprite->mPosition = mPosition + mFacingDir;
	}
}

Sprite* Sprite::getSubSprite()
{
	return mSubSprites[(size_t)mCurrentState];
}
