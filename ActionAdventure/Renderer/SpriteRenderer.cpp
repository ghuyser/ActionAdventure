#include "SpriteRenderer.h"

SpriteRenderer::SpriteRenderer()
{
	mIdxBuf = 0;
	mLastRenderTime = 0;
}

SpriteRenderer::~SpriteRenderer()
{
	glDeleteBuffers(1, &mIdxBuf);
}

void SpriteRenderer::InitGL()
{
	mSpInfo = mProgram.uniforms("spinfo");
	mSpSize = mProgram.uniforms("spsize");
	mTxInfo = mProgram.uniforms("txinfo");

	if( mIdxBuf )
		return;

	glGenBuffers(1, &mIdxBuf);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIdxBuf);
	short ibufa[] = {
		0, 1, 2, 2, 3, 1
	};
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(short) * 6, ibufa, GL_STATIC_DRAW);
}

bool SpriteRenderer::LoadGLProgram(const char *vertexfragment)
{
	bool result = mProgram.init(vertexfragment);

	if( result ) {
		InitGL();
	}

	return result;
}

bool SpriteRenderer::LoadGLProgram(const char *vertex, const char *fragment)
{
	bool result =  mProgram.init(vertex, fragment);

	if( result ) {
		InitGL();
	}

	return result;
}

bool SpriteRenderer::LoadGLProgram(const char *buffer, size_t buffer_size)
{
	// TODO: Parse buffer for vector and fragment areas and pass to mProgram.init().
	return false;
}

void SpriteRenderer::Add(Sprite *sprite)
{
	mSprites.push_back(sprite);
	/*sprite->PositionUpdated.connect( [&]() {
		mNeedsSort = true;
	});*/
}

void SpriteRenderer::Add(Sprite &sprite)
{
	Add(&sprite);
}

void SpriteRenderer::Remove(Sprite *sprite)
{
	for( auto i = mSprites.begin(); i != mSprites.end(); i++ ) {
		if( *i == sprite ) {
			// TODO: disconnect PositionUpdated signal
			mSprites.erase( i );
			break;
		}
	}
}

void SpriteRenderer::Remove(Sprite &sprite)
{
	Remove(&sprite);
}

void SpriteRenderer::Render(const G3DMath::BoundingBox<G3DMath::Vec2d> &area, double time)
{
	if( mNeedsSort )
		sort();

	mProgram.use();

	Image *sheet = nullptr;

	for( auto i = mSprites.begin(); i != mSprites.end(); i++ ) {
		Sprite *sprite = *i;

		// TODO: expand area to allow for sprites to show partially
		if( !area.contains( sprite->mPosition ) )
			continue;

		if( sheet != sprite->mSheet ) {
			sheet = sprite->mSheet;
			sheet->Bind();
		}

		Sprite *subSprite = sprite->getSubSprite();
		Sprite *renderFirst = sprite;
		Sprite *renderSecond = subSprite;

		if( subSprite ) {
			if( sprite->mPosition.y > subSprite->mPosition.y ) {
				renderFirst = subSprite;
				renderSecond = sprite;
			}
		}

		renderSprite(renderFirst, sheet, time);
		renderSprite(renderSecond, sheet, time);
	}
}

void SpriteRenderer::renderSprite(Sprite *sprite, Image *sheet, double time)
{
	if( sprite == nullptr || sheet == nullptr )
		return;

	Sprite::frame_info *fi = sprite->updateFrameInfo(time);
	if( !fi )
		return;

	glUniform4f(mTxInfo, (float)fi->x, (float)fi->y, (float)sheet->Width(), (float)sheet->Height());

	glUniform3f(mSpInfo, (float)sprite->mPosition.x, (float)sprite->mPosition.y, (float)sprite->mPosition.z);
	glUniform2f(mSpSize, (float)sprite->mSize.x, (float)sprite->mSize.y);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIdxBuf);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);
}

void SpriteRenderer::UpdateMatrices(G3DMath::Matrix4f &proj, G3DMath::Matrix4f &view)
{
	mProgram.use();
	glUniformMatrix4fv(mProgram.uniforms("u_Proj"), 1, GL_FALSE, &proj);
	glUniformMatrix4fv(mProgram.uniforms("u_View"), 1, GL_FALSE, &view);
}

void SpriteRenderer::UpdateView(G3DMath::Matrix4f &view)
{
	mProgram.use();
	glUniformMatrix4fv(mProgram.uniforms("u_View"), 1, GL_FALSE, &view);
}

void SpriteRenderer::sort()
{
	mSprites.sort( [](Sprite *a, Sprite *b)->bool {
		return (a->Y() + a->Z()) < (b->Y() + a->Z());
	});
	mNeedsSort = false;
}
