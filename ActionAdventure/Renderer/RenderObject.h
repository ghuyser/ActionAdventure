#pragma once

#include "G3DMath.h"
#include "Signal.h"

enum class EntityState;

class RenderObject
{
public:
	virtual void SetPosition(G3DMath::Vec3d) = 0;
	virtual void Move(G3DMath::Vec3d) = 0;
	virtual void SetFacing(G3DMath::Vec2d) = 0;
	virtual void SetState(EntityState) = 0;

	virtual void RepeatCurrentAnimation(bool) = 0;

	Signal<> AnimationComplete;
};
