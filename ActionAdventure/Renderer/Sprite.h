#pragma once

#include "Renderer/RenderObject.h"

#include "Entity/Entity.h"

#include "G3DMath.h"
#include "jsonobject.h"
#include "types.h"

#include "Image/Image.h"

#include <functional>
#include <unordered_map>

class SpriteRenderer;

class Sprite : public RenderObject {
	friend SpriteRenderer;

public:
	enum class Facing {
		North,
		NorthEast,
		East,
		SouthEast,
		South,
		SouthWest,
		West,
		NorthWest,
		All,
		None
	};

	Sprite();
	~Sprite();

	static void InitLua();

	void SpriteSheet(Image *sprite_sheet);
	Image* SpriteSheet();

	void LoadAnimation(const char *filename);
	void LoadAnimation(const void *buffer, size_t buffersz);
	void LoadAnimation(const JSONObject &obj, EntityState subSprite = EntityState::None);

	void Size(GLuint width, GLuint height);
	void AddFrame(EntityState s, Facing f, GLuint x_pos, GLuint y_pos, double duration);

	double X() const {
		return mPosition.x;
	}
	
	double Y() const {
		return mPosition.y;
	}

	double Z() const {
		return mPosition.z;
	}

	bool Collides(G3DMath::Vec2d point) {
		G3DMath::BoundingBox2D box(
			G3DMath::Vec2d(mPosition) - G3DMath::Vec2d(0.5, 0.5),
			G3DMath::Vec2d(mPosition) + G3DMath::Vec2d(0.5, 0.5)
		);

		return box.contains(point);
	}

	void SetPosition(const Sprite *sprite);
	void SetPosition(const Sprite &sprite);
	void SetPosition(double x, double y);
	void SetPosition(double x, double y, double z);
	void SetPosition(G3DMath::Vec3d v) override {
		mPosition = v;
	}
	void Move(G3DMath::Vec3d v) override {
		mPosition += v;
	}

	void SetFacing(G3DMath::Vec2d v) override;
	void SetState(EntityState s) override;
	EntityState GetState() const {
		return mCurrentState;
	};

	void SetFacing(Facing f);
	Facing GetFacing() const {
		return mCurrentFacing;
	};

	bool IsSubSprite() {
		return mParent != nullptr;
	}

	void RepeatCurrentAnimation(bool r);

protected:
	struct frame_info {
		GLuint x;
		GLuint y;
		double duration;
	};

	int getVectorIndex(EntityState s, Facing f);
	frame_info* updateFrameInfo(double timedif);

	void loadAnimState(EntityState s, const JSONObject &o);
	void loadAnimFacing(EntityState s, Facing f, const JSONObject &o);

	void copyStateAnimations(EntityState from, EntityState to);

	virtual void updateSubSprite();
	Sprite* getSubSprite();

	double mMyTime;
	int mLastFrame;
	bool mRepeatAnimation;

	EntityState mCurrentState;
	Facing mCurrentFacing;

	Image *mSheet;
	G3DMath::Vec2<GLuint> mSize;

	G3DMath::Vec3d mPosition;
	G3DMath::Vec2d mFacingDir;

	std::vector<std::vector<frame_info>> mFrames;

	Sprite *mSubSprites[(size_t)EntityState::None];
	Sprite *mParent;
};
