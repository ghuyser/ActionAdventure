#pragma once

#include "G3DMath.h"
#include "gl_fabric.h"

#include "Sprite.h"

#include <memory>
#include <list>

class SpriteRenderer
{
public:
	SpriteRenderer();
	~SpriteRenderer();

	void InitGL();

	bool LoadGLProgram(const char *vertexfragment);
	bool LoadGLProgram(const char *vertex, const char *fragment);
	bool LoadGLProgram(const char *buffer, size_t buffer_size);

	void Add(Sprite *sprite);
	void Add(Sprite &sprite);

	void Remove(Sprite *sprite);
	void Remove(Sprite &sprite);

	void Render(const G3DMath::BoundingBox<G3DMath::Vec2d> &area, double time);

	void UpdateMatrices(G3DMath::Matrix4f &proj, G3DMath::Matrix4f &view);
	void UpdateView(G3DMath::Matrix4f &view);

private:
	void renderSprite(Sprite *sprite, Image *sheet, double time);
	void sort();

	GLProgram mProgram;
	std::list<Sprite*> mSprites;

	GLint mSpInfo;
	GLint mSpSize;
	GLint mTxInfo;

	GLuint mIdxBuf;

	double mLastRenderTime;
	bool mNeedsSort;
};
