GAMEDIR=ActionAdventure
COREDIR=CoreLayer
LUADIR=lua-5.3.4
IMGUIDIR=imgui
GAMELIB=actionadventure.a
CORELIB=corelayer.a
LUALIB=lua.a
IMGUILIB=imgui.a
LDFLAGS=-lGL -lGLU -lpthread `sdl2-config --libs` -luuid -ldl
DEBUG=-D_DEBUG -g
EXECUTABLE=actionadventure

export GAMEDIR
export GAMELIB
export COREDIR
export CORELIB
export LUADIR
export LUALIB
export IMGUIDIR
export IMGUILIB
export DEBUG

ifneq ("$(wildcard .hack-windec.tmp)", "")
	CXXFLAGS=-DUSE_WINDOW_DECORATION_OFFSET
	export CXXFLAGS
endif

all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(GAMELIB) $(CORELIB) $(LUALIB) $(IMGUILIB) $(OBJECTS)
	@echo -n "Creating $(EXECUTABLE)..."
	@$(CXX) $(OBJECTS) $(GAMEDIR)/$(GAMELIB) $(COREDIR)/$(CORELIB) $(LUADIR)/$(LUALIB) $(IMGUIDIR)/$(IMGUILIB) $(LDFLAGS) -o $@
	@echo complete.

.cpp.o:
	@echo "\t$< --> $@"
	@$(CXX) $(INCLUDES) $(CXXFLAGS) $< -o $@

$(GAMELIB):
	@echo "Compiling game library:"
	@make -s -C $(GAMEDIR)

$(CORELIB):
	@echo "Compiling core library:"
	@make -s -C $(COREDIR)

$(LUALIB):
	@echo "Compiling lua library:"
	@make -s -C $(LUADIR)

$(IMGUILIB):
	@echo "Compiling imgui library:"
	@make -s -C $(IMGUIDIR)

clean:
	@echo "Cleaning $(EXECUTABLE)..."
	@make --no-print-directory -C $(GAMEDIR) clean
	@make --no-print-directory -C $(COREDIR) clean
	@rm -f $(OBJECTS) $(EXECUTABLE)
	@echo "Finished."

clean-lua:
	@echo "Cleaning lua..."
	@make --no-print-directory -C $(LUADIR) clean
	@echo "Finished."

clean-imgui:
	@echo "Cleaning imgui..."
	@make --no-print-directory -C $(IMGUIDIR) clean
	@echo "Finished."

clean-all:
	@echo "Cleaning $(EXECUTABLE)..."
	@make --no-print-directory -C $(GAMEDIR) clean
	@make --no-print-directory -C $(COREDIR) clean
	@make --no-print-directory -C $(LUADIR) clean
	@make --no-print-directory -C $(IMGUIDIR) clean
	@rm -f $(OBJECTS) $(EXECUTABLE)
	@echo "Finished."
