uniform sampler2D u_Sheet;
varying vec2 cvert;

void main()
{
	//gl_FragColor = vec4(cvert.x, cvert.y, 0.5, 1);
	gl_FragColor = texture2D(u_Sheet, cvert);
}
