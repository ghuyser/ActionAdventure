[vertex shader]
#version 330
uniform mat4 u_View;
uniform mat4 u_Proj;

// Sprite position info
uniform vec3 spinfo;

// Sprite size
uniform vec2 spsize;

// Texture info: pos x, pos y, tex size x, tex size y
uniform vec4 txinfo;

const vec2 texfactor[4] = vec2[4] (
	vec2(0, 1),
	vec2(0, 0),
	vec2(1, 1),
	vec2(1, 0)
);

out vec2 cvert;

void main()
{
	float ratio = spsize.x / spsize.y;
	float yy = 1.0 / ratio;

	vec2 verts[4] = vec2[4] (
		vec2(-0.5, 0.5),
		vec2(-0.5, 0.5 - yy),
		vec2( 0.5, 0.5),
		vec2( 0.5, 0.5 - yy)
	);

	vec2 vert = verts[gl_VertexID];
	vec2 texf = texfactor[gl_VertexID];
	cvert = vec2(
		(txinfo.x + (texf.x * spsize.x)) / txinfo.z,
		(txinfo.y + (texf.y * spsize.y)) / txinfo.w
	);

	gl_Position = (u_View * u_Proj) * vec4(spinfo.x + vert.x, spinfo.y + vert.y, spinfo.z, 1);
}


[fragment shader]
#version 330
uniform sampler2D u_Sheet;
in vec2 cvert;

void main()
{
	gl_FragColor = texture2D(u_Sheet, cvert);
}
