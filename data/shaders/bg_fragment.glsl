#version 330

uniform sampler2D u_Atlas;

in vec2 v_TexCoordinate;

void main()
{
	gl_FragColor = texture2D(u_Atlas, v_TexCoordinate);
}
