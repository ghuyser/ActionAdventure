#version 330

uniform mat4 u_View;
uniform mat4 u_Proj;

layout (location = 0) in vec3 a_Position;
layout (location = 1) in vec2 a_TexCoordinate;

out vec2 v_TexCoordinate;

void main()
{
	gl_Position = (u_View * u_Proj) * vec4(a_Position.x, a_Position.y, a_Position.z, 1);
	v_TexCoordinate = a_TexCoordinate;
}
