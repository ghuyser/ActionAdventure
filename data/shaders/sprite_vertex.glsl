uniform mat4 u_View;
uniform mat4 u_Proj;
uniform vec3 spinfo;

attribute vec4 a_verts;

/*const vec2 verts[4] = vec2[] (
	vec2(-0.5,  0.5),
	vec2(-0.5, -0.5),
	vec2( 0.5,  0.5),
	vec2( 0.5, -0.5)
);*/

varying vec2 cvert;

void main()
{
	//vec2 vert = verts[gl_VertexID];
	//cvert = vec2(vert.x + 0.5, vert.y + 0.5);
	cvert = vec2(a_verts.z, a_verts.w);
	//gl_Position = (u_View * u_Proj) * vec4(spinfo.x + vert.x, spinfo.y + vert.y, 0, 1);
	//gl_Position = (u_View * u_Proj) * vec4(spinfo.x + a_verts.x, spinfo.y + a_verts.y, 0, 1);
	gl_Position = (u_View * u_Proj) * vec4(spinfo.x + a_verts.x, spinfo.y + a_verts.y, spinfo.z + 0.01, 1);
}
