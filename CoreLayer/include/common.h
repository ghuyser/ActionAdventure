#pragma once

#include <stdio.h>
#include <stdlib.h>
#include "types.h"

#ifndef NULL
#define NULL				0
#endif

#define SAFE_DELETE(x)		if( x ) { delete x; x = NULL; }
#define SAFE_FREE(x)		if( x ) { free( x ); x = NULL; }

// Functions named differently but are the same...
#ifdef _WIN32
#define qsort_r				qsort_s
#elif defined ANDROID
void qsort_r(void *a, size_t n, size_t es, int (*cmp)(void*, const void*, const void*), void *thunk);
#endif

#if defined(ANDROID)
#if _BYTE_ORDER == _LITTLE_ENDIAN
#define byteswap16(x)		((x>>8) | (x<<8))
#define	byteswap32(x)		__builtin_bswap32((int32_t)x)
#define byteswap64(x)		__builtin_bswap64((int64_t)x)
#else
#define byteswap16(x)		(x)
#define	byteswap32(x)		(x)
#define byteswap64(x)		(x)
#endif
#elif defined _WIN32
#define byteswap16(x)		_byteswap_ushort((uint16_t)x)
#define	byteswap32(x)		_byteswap_ulong((uint32_t)x)
#define byteswap64(x)		_byteswap_uint64((uint64_t)x)
#elif defined __APPLE__ || (defined __linux && !defined ANDROID) || defined EMSCRIPTEN
#define byteswap16(x)		((x>>8) | (x<<8))
#define	byteswap32(x)		__builtin_bswap32((int32_t)x)
#define byteswap64(x)		__builtin_bswap64((int64_t)x)
#endif

// Convert host to network float
inline uint32_t htonf_g(float value)
{
	return (uint32_t)byteswap32((*(int32_t*)(&value)));
}

// Convert network to host float
inline float ntohf_g(uint32_t value)
{
	int32_t d = byteswap32(value);
	return *(float*)(&d);
}

// Convert host to network double
inline uint64_t htond_g(double value)
{
	return (uint64_t)byteswap64((*(int64_t*)(&value)));
}

// Convert network to host double
inline double ntohd_g(uint64_t value)
{
	int64_t d = byteswap64(value);
	return *(double*)(&d);
}


// file reading functions
inline short readShort(FILE *fp) {
	short ret;
	fread(&ret, sizeof(short), 1, fp);
	return byteswap16(ret);
}

inline int readInt(FILE *fp) {
	int ret;
	fread(&ret, sizeof(int), 1, fp);
	return byteswap32(ret);
}

inline float readFloat(FILE *fp) {
	return (float)readInt(fp);
}

inline int64_t readLong(FILE *fp) {
	int64_t ret;
	fread(&ret, sizeof(int64_t), 1, fp);
	return byteswap64(ret);
}
