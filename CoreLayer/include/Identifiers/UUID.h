#pragma once

#include <string>

namespace Identifiers
{
	class UUID
	{
	public:
		UUID() = default;
		UUID(const UUID &uuid) = default;
		UUID(const std::string uuid);
		UUID(const char *uuid);

		~UUID() = default;

		bool operator==(const UUID &u) const;
		bool operator!=(const UUID &u) const;

		void clear();
		void generate();

		std::string getString() const;

		bool isNull() const;

		UUID hton() const;

		static UUID Null();

	private:
		char mUUID[16];
	};

}

namespace std {
	template<> struct hash<Identifiers::UUID>
	{
		size_t operator()(const Identifiers::UUID&) const noexcept;
	};
}
