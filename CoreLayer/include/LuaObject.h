#pragma once

#include <cstring>
#include <string>
#include <unordered_map>

#include "lua.hpp"

#define LUA_OBJECT_TYPE(c)           LuaObjectType<c>::create(#c)
#define LUA_OBJECT_TYPE_FROM(c, p)   LuaObjectType<c>::create(#c); LuaObjectType<c>::LuaParentClass<p>()

#define LUA_FUNCTION(f)              static int f(lua_State*)

#define LUA_OBJECT                   template<class T> friend class LuaObjectType;

#define LUA_CREATE_OBJECT(L, c)      (luaL_dostring(mLua, "return "#c".new()") ? nullptr : *(c**)lua_touserdata(L, -1))

template<class T> class LuaObjectType;

class LuaObject
{
	LUA_OBJECT

public:
	LuaObject()
		: mLua(nullptr)
		, mSelfRef(LUA_NOREF)
	{
	}

	~LuaObject()
	{
		if( mLua && mSelfRef != LUA_NOREF && mSelfRef != LUA_REFNIL ) {
			FreeLuaSelfReference();
		}
	}

	// Derived classes MUST create a static void InitLua()

	int CreateLuaSelfReference(lua_State *L, int idx)
	{
		// Clear out old self reference
		if(mSelfRef != LUA_NOREF && mSelfRef != LUA_REFNIL || L == NULL) {
			// Reference already exists, just exit
			return mSelfRef;
		}

		// Make a copy of itself on the stack because luaL_ref pops a value off
		lua_pushvalue(L, idx);

		// Create the self reference
		mSelfRef = luaL_ref(L, LUA_REGISTRYINDEX);
		mLua = L;

		return mSelfRef;
	}

	void FreeLuaSelfReference()
	{
		luaL_unref(mLua, LUA_REGISTRYINDEX, mSelfRef);
		mSelfRef = LUA_NOREF;
		mLua = nullptr;
	}

	void PushToStack(lua_State *L)
	{
		if( mSelfRef != LUA_NOREF && mSelfRef != LUA_REFNIL ) {
			lua_rawgeti(L, LUA_REGISTRYINDEX, mSelfRef);
		}
	}

protected:
	virtual void luaHandleNew() {}
	virtual void luaHandleGC() {}
	virtual void luaHandleIndex() {}
	virtual void luaHandleNewIndex() {}
	virtual const char* luaHandleToString() const { return nullptr; }

private:
	// -------------------------------------------------------------------
	// Lua object member variables.
	// -------------------------------------------------------------------
	lua_State *mLua;
	int mSelfRef;
};

template<class T>
class LuaObjectType
{
public:
	template<class U>
	friend class LuaObjectType;

	static void create(const char *type_name)
	{
		LuaObjectType<T> &obj = instance();
		if(type_name && !obj.mLuaInitialized && !obj.mLuaMethods.size()) {
			obj.mLuaTypeName = type_name;

			AddMethod("new", lua__new);
			AddMethod("__gc", lua__gc);
			AddMethod("__tostring", lua__tostring);
			AddMethod("__index", lua__index);
			AddMethod("__newindex", lua__newindex);

			T::InitLua();
		}
	}

	static void Init(lua_State* L)
	{
		// Set all the methods that were added to a new table.
		// Modeled after luaL_newlib.
		lua_createtable(L, 0, instance().mLuaMethods.size());

		for(auto i : instance().mLuaMethods) {
			lua_pushcclosure(L, i.second, 0);
			lua_setfield(L, -2, i.first.c_str());
		}

		// Set them to the current object name.
		lua_setglobal(L, instance().mLuaTypeName.c_str());

		instance().mLuaInitialized = true;
	}

	static void InitMetatable(lua_State *L)
	{
		lua_createtable(L, 0, 1);

		lua_pushcclosure(L, lua__const, 0);
		lua_setfield(L, -2, "__index");

		const char *name = instance().mLuaTypeName.c_str();
		lua_setglobal(L, name);
		lua_getglobal(L, name);
		lua_setmetatable(L, -1);

		instance().mMetaInitialized = true;
	}

	static void AddMethod(std::string name, lua_CFunction func)
	{
		if(!instance().mLuaInitialized)
			instance().mLuaMethods[name] = func;
	}

	static void AddConst(std::string name, int value)
	{
		if(!instance().mMetaInitialized)
			instance().mLuaConst[name] = value;
	}

	static std::string& TypeName()
	{
		return instance().mLuaTypeName;
	}

	template<class U>
	static void LuaParentClass()
	{
		if( !instance().mLuaParentFuncs.size() ) {
			instance().mLuaParentFuncs["new"] = LuaObjectType<U>::lua__new;
			instance().mLuaParentFuncs["__gc"] = LuaObjectType<U>::lua__gc;
			instance().mLuaParentFuncs["__tostring"] = LuaObjectType<U>::lua__tostring;
			instance().mLuaParentFuncs["__index"] = LuaObjectType<U>::lua__index;
			instance().mLuaParentFuncs["__newindex"] = LuaObjectType<U>::lua__newindex;
		}
	}

protected:

private:
	static LuaObjectType& instance()
	{
		static LuaObjectType *instance = new LuaObjectType();
		return *instance;
	}

	bool mLuaInitialized = false;
	bool mMetaInitialized = false;
	std::unordered_map<std::string, lua_CFunction> mLuaMethods;
	std::unordered_map<std::string, int> mLuaConst;
	std::string mLuaTypeName;
	std::unordered_map<std::string, lua_CFunction> mLuaParentFuncs;


	// -------------------------------------------------------------------
	// Basic lua object handlers.
	// -------------------------------------------------------------------
	static int lua__const(lua_State *L) {
		int nargs = lua_gettop(L);
		const char *key = luaL_checkstring(L, 2);

		for(auto i : instance().mLuaConst) {
			if( !strcmp(key, i.first.c_str()) ) {
				lua_pushinteger(L, i.second);
				return 1;
			}
		}

		return 0;
	}

	static int lua__new(lua_State *L) {
		T **p = (T **)lua_newuserdata(L, sizeof(T*));
		*p = new T();
		lua_getglobal(L, instance().mLuaTypeName.c_str());
		lua_setmetatable(L, -2);
		// Lua part of this class is now created, all that is left
		// is any further initialization this class needs in C++
		(*p)->luaHandleNew();

		return 1;
	}

	static int lua__gc(lua_State *L) {
		T **p = (T**)lua_touserdata(L, 1);

		if(*p) {
			delete *p;
			*p = nullptr;
		}

		return 0;
	}

	static int lua__tostring(lua_State *L) {
		T **p = (T**)lua_touserdata(L, 1);
		const char *s = (*p)->luaHandleToString();
		
		if( s ) {
			lua_pushstring(L, s);
			return 1;
		}

		return 0;
	}

	static int lua__index(lua_State *L) {
		int nargs = lua_gettop(L);
		const char *key = luaL_checkstring(L, 2);

		// Getters
		LuaObjectType<T> &obj = instance();

		if(0 /* TODO: check if a property was named */) {
		} else {
			// Not a member variable, check if it is a member function request
			for(auto i : obj.mLuaMethods) {
				if(!strcmp(key, i.first.c_str())) {
					// Just treat this as a function that returns the cfunc
					// matching the string name provided, so all we need
					// to do is push the 'result' onto the stack and return

					lua_pushcfunction(L, i.second); // Push on a reference to the real thing
					return 1;
				}
			}
		}

		return obj.mLuaParentFuncs["__index"] ? obj.mLuaParentFuncs["__index"](L) : 0;
	}

	static int lua__newindex(lua_State *L) {
		const char *key = luaL_checkstring(L, 2);
		// Setters
		// TODO: check if a property was named and handle it.

		return instance().mLuaParentFuncs["__newindex"] ? instance().mLuaParentFuncs["__newindex"](L) : 0;
	}

};
