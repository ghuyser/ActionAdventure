#pragma once

#include <functional>
#include <vector>

// Simple signals. Not thread safe!!
template<class... Args>
class Signal
{
public:
	void connect(std::function<void(Args...)> func) {
		mSlots.push_back(func);
	}

	void disconnect() {
		mSlots.clear();
	}

	void operator()(Args... args) {
		for(auto i = mSlots.begin(); i != mSlots.end(); i++) {
			(*i)(args...);
		}
	}

private:
	std::vector<std::function<void(Args...)>> mSlots;
};
