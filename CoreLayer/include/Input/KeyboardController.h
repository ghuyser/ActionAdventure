#pragma once

#include "keycodes.h"
#include "Input/Joystick.h"
#include "types.h"

namespace Input
{
	// Keyboard controller object.
	// Assists with keeping keyboard mappings to controller buttons.
	// IMPORTANT: In order to reduce overhead, bounds checking is not done.
	class KeyboardController
	{
	public:
		KeyboardController() {
			mKeyMap[XBOX_A]     = IKEYCODE_Z;
			mKeyMap[XBOX_B]     = IKEYCODE_X;
			mKeyMap[XBOX_X]     = IKEYCODE_A;
			mKeyMap[XBOX_Y]     = IKEYCODE_S;
			mKeyMap[XBOX_LB]    = IKEYCODE_Q;
			mKeyMap[XBOX_RB]    = IKEYCODE_C;
			mKeyMap[XBOX_LS]    = IKEYCODE_W;
			mKeyMap[XBOX_RS]    = IKEYCODE_D;
			mKeyMap[XBOX_VIEW]  = IKEYCODE_E;
			mKeyMap[XBOX_MENU]  = IKEYCODE_ENTER;

			mKeyMap[DPAD_UP]    = IKEYCODE_DPAD_UP;
			mKeyMap[DPAD_LEFT]  = IKEYCODE_DPAD_LEFT;
			mKeyMap[DPAD_RIGHT] = IKEYCODE_DPAD_RIGHT;
			mKeyMap[DPAD_DOWN]  = IKEYCODE_DPAD_DOWN;
		}

		inline void Map(uint8_t joyButton, uint8_t keycode) {
			mKeyMap[joyButton] = keycode;
		}

		inline bool isPressed(uint8_t joyButton) {
			return mKeyStates[mKeyMap[joyButton]];
		}

		inline void SetKeyMemory(uint8_t *keyStates) {
			mKeyStates = keyStates;
		}

		inline bool operator[](size_t b) {
			return mKeyStates[mKeyMap[b]];
		}
		inline const bool operator[](size_t b) const {
			return mKeyStates[mKeyMap[b]];
		}

	private:
		uint8_t mKeyMap[JOYSTICK_MAX_BUTTONS + DPAD_NUM];
		uint8_t *mKeyStates;
	};
}
