#pragma once

#ifdef _WIN32
#define DIRECTINPUT_VERSION 0x0800

#include <dinput.h>
#else
#include <SDL2/SDL_joystick.h>
#include <SDL2/SDL_gamecontroller.h>
#endif

#include "types.h"

#define JOYSTICK_MAX_DEVICES 8
#define JOYSTICK_MAX_BUTTONS 32
#define JOYSTICK_MAX_AXIS 7

#define JOYSTICK_OK     0
#define JOYSTICK_ERROR -1

namespace Input
{

	enum XBoxButtons {
		XBOX_A = 0,
		XBOX_B,
		XBOX_X,
		XBOX_Y,
		XBOX_LB,
		XBOX_RB,
		XBOX_LS,
		XBOX_RS,
		XBOX_VIEW,
		XBOX_MENU
	};

	enum DPadButtons {
		DPAD_UP = JOYSTICK_MAX_BUTTONS,
		DPAD_LEFT,
		DPAD_RIGHT,
		DPAD_DOWN,
		DPAD_NUM = 4
	};

	enum AxisNames {
		AXIS_LEFT_STICK_X = 0,
		AXIS_LEFT_STICK_Y,
		AXIS_RIGHT_STICK_X,
		AXIS_RIGHT_STICK_Y,
		AXIS_TRIGGERS
	};

	class Joystick {
	public:
		Joystick();
		~Joystick();

		int NumDevices() const;
	#ifdef _WIN32
		const TCHAR * const DeviceName(size_t) const;
	#else
		const char * const DeviceName(size_t) const;
	#endif
		int Select(size_t);

		int Aquire();
		int Unaquire();

		int Poll();

		int32_t Axis(size_t);

		int NumButtons() const;

			  int operator[](size_t);
		const int operator[](size_t) const;

	private:
	#ifdef _WIN32
		LPDIRECTINPUTDEVICE8 joystick;
		DIDEVCAPS capabilities;

		static BOOL CALLBACK EnumDevicesCallback(const DIDEVICEINSTANCE* instance, VOID* context);
	#else
		SDL_GameController *controller;
		SDL_Joystick *joystick;
	#endif

		uint8_t num_buttons;
		uint8_t buttons[JOYSTICK_MAX_BUTTONS + DPAD_NUM];
		int32_t axis[JOYSTICK_MAX_AXIS];
	};
}
