#pragma once

#include "Input/input.h"

class IGame
{
public:
	virtual ~IGame() {};
	virtual void DoActions() = 0;
	virtual int InitGL() = 0;
	virtual int InitGame() = 0;
	virtual void ResizeGLScene(int width, int height) = 0;
	virtual bool Render() = 0;

	virtual void InputEventProc(InputType t, InputEvent e) = 0;

	int (*ReadTextAsset)(const char *filename, char **buf);

	enum class LogType {
		Debug,
		Info,
		Warning,
		Error
	};
	virtual void AddLog(LogType, const char*, const char*, int, const char*, ...) {}
	virtual void AddLogV(LogType, const char*, const char*, int, const char*, void*) {}
};
