#pragma once
/*
* Copyright (c) 2013 Gary Huyser <gary.huyser@gmail.com>
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*
*/



/*
 * String utility functions
 */
void split_string(char *src, int srclen, char split, char ***outarray, int *outcount);
void free_split(char **splitarray, int count);
char* trim(const char *s);
int find_string(const char *src, int srclen, const char *find);
char* get_line(const char *src, int srclen, int *nextline);

inline char* string_duplicate(const char *src);
char* string_duplicate(const char *src, int srclen);

char* string_replace(const char *src, const char *search, const char *replace);

char* encode_uri_string(const char *src);
int is_uri_reserved(char c);
