#pragma once

#ifdef _WIN32
#include <Windows.h>
#endif
#include "opengl.h"
#include "userext.h"

class GLShader {
public:
	GLShader();
	GLShader(const char *file_name, GLenum shader_type);
	GLShader(const char *buffer, int buffer_size, GLenum shader_type);
	~GLShader();

	bool loadShaderFile(const char *file_name, GLenum shader_type);
	bool loadShaderBuffer(const char *buffer, int buffer_size, GLenum shader_type);

	GLchar *script;
	GLint   script_length;
	GLuint  handle;
	GLenum  type;

private:
	void freeResources();
};


class GLProgram {
public:
	GLProgram();
	~GLProgram();

	bool init(const char *vertex_file, const char *fragment_file);
	bool init(const char *shader_file);
	bool init(const char *vbuf, int vbuf_sz, const char *fbuf, int fbuf_sz);
	bool init(const char *buffer, int vbuf_start, int vbuf_sz, int fbuf_start, int fbuf_sz);

	int attribs(const char *attrib_name);
	int uniforms(const char *unfiorm_name);

	inline void use() { glUseProgram(handle); };
	void enableAttribs();
	void disableAttribs();

	inline GLuint getHandle() { return handle; };

private:
	struct Resource {
		int    count;
		char **names;
		int   *ids;
	};

	void freeResources();
	void buildNames(Resource &resource, const char *match, const char *buffer, int buf_sz);
	void generateResourceIDs(Resource &resource, int type);

	GLuint handle;

	GLShader *vertex;
	GLShader *fragment;

	Resource m_attribs;
	Resource m_uniforms;
};
