#pragma once

#define USE_ALTERNATE_LOG

#ifdef USE_ALTERNATE_LOG
	#define LOG_TYPE_DEBUG 0
	#define LOG_TYPE_INFO  1
	#define LOG_TYPE_WARN  2
	#define LOG_TYPE_ERR   3

	extern void __alternate_log(int, const char*, const char*, int, const char*, ...);

	#ifndef TAG_NAME
	#define TAG_NAME ""
	#endif

	#define LOGI(...)	__alternate_log(LOG_TYPE_INFO, TAG_NAME, NULL, 0, __VA_ARGS__)
	#define LOGW(...)	__alternate_log(LOG_TYPE_WARN, TAG_NAME, __FILE__, __LINE__, __VA_ARGS__)
	#define LOGE(...)	__alternate_log(LOG_TYPE_ERR , TAG_NAME, __FILE__, __LINE__, __VA_ARGS__)
#else
	#if defined _DEBUG || defined ANDROID || defined EMSCRIPTEN
		#ifdef ANDROID
			#include <android/log.h>
			#define LOG_TAG    "" EXECUTABLE
			#define LOGI(...)	__android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
			#define LOGW(...)	__android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
			#define LOGE(...)	__android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
		#elif defined _WIN32
			extern void __winlog(const char*, int, const char*, ...);
			#define LOGI(...)	__winlog(NULL, 0, __VA_ARGS__)
			#define LOGW(...)	__winlog(__FILE__, __LINE__, __VA_ARGS__)
			#define LOGE(...)	__winlog(__FILE__, __LINE__, __VA_ARGS__)
		#elif defined __APPLE__
			#define LOGI(...)
			#define LOGW(...)
			#define LOGE(...)
		#elif (defined __linux && !defined ANDROID)
			extern void __linuxlog(FILE*, const char*, int, const char*, ...);
			#define LOGI(...)	__linuxlog(stdout, NULL, 0, __VA_ARGS__)
			#define LOGW(...)	__linuxlog(stdout, __FILE__, __LINE__, __VA_ARGS__)
			#define LOGE(...)	__linuxlog(stderr, __FILE__, __LINE__, __VA_ARGS__)
		#elif defined EMSCRIPTEN
			extern void __emscriptenlog(const char*, int, const char*, ...);
			#define LOGI(...)	__emscriptenlog(NULL, 0, __VA_ARGS__)
			#define LOGW(...)	__emscriptenlog(__FILE__, __LINE__, __VA_ARGS__)
			#define LOGE(...)	__emscriptenlog(__FILE__, __LINE__, __VA_ARGS__)
		#endif
	#else  // _DEBUG
		#define LOGI(...)
		#define LOGW(...)
		#define LOGE(...)
	#endif // _DEBUG
#endif
