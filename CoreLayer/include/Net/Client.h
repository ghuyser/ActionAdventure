#pragma once

#include "Identifiers/UUID.h"
#include "Net/SocketListener.h"
#include "Net/Packet.h"
#include "types.h"

#include <functional>

namespace Net
{

class Client : public SocketListener
{
public:
	enum class State {
		NoConnection,
		PendingConnection,
		Connected,
		Disconnecting,
		Error
	};

	Client();
	~Client();

	bool Connect();
	bool Connect(const char *ip);
	bool Connect(const char *ip, const uint16_t port);
	void Disconnect();

	const char* GetServerAddress();
	uint16_t GetServerPort();

	State CurrentState() const;

	int SendState(const IState *state);

	std::function<void(bool connected, Identifiers::UUID, void*)> PlayerConnected;
	std::function<void(Identifiers::UUID, void*)> UpdatePlayerState;

private:
	void processTimeouts();
	void processRecvData(const void*, const void*);
	void threadEnding();

	bool sendPacket(Packet p);

	char *mServerAddress;
	uint16_t mServerPort;

	State mState;
	Identifiers::UUID mClientID;
};

}