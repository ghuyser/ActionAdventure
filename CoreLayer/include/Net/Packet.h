#pragma once

#include "Identifiers/UUID.h"
#include "types.h"

namespace Net
{
	enum class ClientMessage
	{
		Join,
		Leave,
		State,
		AckDisconnect
	};

	enum class ServerMessage
	{
		JoinResult,
		Disconnect,
		NoClient,
		PlayerConnected,
		PlayerDisconnected,
		State
	};

	enum class ConnectionStatus
	{
		Error,
		Connected,
		LogoutSuccess,
	};

	#pragma pack(push, 1)
	struct PacketHeader
	{
		uint16_t packet_sz;
		uint8_t type;
		Identifiers::UUID client_id;
		uint32_t timestamp;
	};

	struct ConnectionResponse
	{
		uint8_t status;
		uint16_t players_connected;
	};

	struct PlayerState
	{
		uint64_t x_double;
		uint64_t y_double;
		uint64_t z_double;
		uint8_t facing;
		uint8_t state;
	};

	struct PlayerInfo
	{
		Identifiers::UUID player_id;
		PlayerState state;
	};

	struct PlayerConnection
	{
		Identifiers::UUID player_id;
	};
#pragma pack(pop)

	class Packet
	{
	public:
		Packet(const Identifiers::UUID &uuid, uint8_t type, uint32_t timestamp = 0, const void *data = nullptr, size_t data_sz = 0);
		Packet(const PacketHeader *packet);
		Packet(const Packet &packet);
		~Packet();

		void SetHeader(const Identifiers::UUID &uuid, uint8_t type, uint32_t timestamp = 0);
		void SetData(const void *data, size_t size);

		const void* Payload() const;
		const PacketHeader* Header() const;
		const void* Data() const;

		size_t Size() const;
		size_t DataSize() const;

	private:
		void *mPayload;
		size_t mSize;
	};

	class IState
	{
	public:
		virtual const unsigned int DataLength() const = 0;
		virtual const uint8_t* Data() const = 0;
		
	private:
	};
}
