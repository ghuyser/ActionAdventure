#pragma once

#include "Net/SocketListener.h"

#include "Identifiers/UUID.h"
#include "Net/Packet.h"
#include "types.h"

#include <thread>
#include <unordered_map>
#include <vector>


namespace Net
{
	constexpr auto DEFAULT_PORT = 0xACAD;
	constexpr auto TIMEOUT_MS = 10000;

	class Server : public SocketListener
	{
	public:
		Server();
		~Server();

		bool Start();
		void Stop();

		bool IsRunning() const;

		std::vector<Identifiers::UUID> GetClients() const;

	private:
		struct IP_Endpoint
		{
			uint32_t address;
			uint16_t port;
			int last_update;
			bool disconnect;
		};

		void processTimeouts();
		void processRecvData(const void*, const void*);
		void threadEnding();

		bool checkForTimeout();
		void updateClientStates();

		bool addClient(IP_Endpoint *new_client, Identifiers::UUID *new_uuid);
		bool removeClient(const Identifiers::UUID &client_id);

		void* appendData(const void *first, const size_t first_sz, const void *second, const size_t second_sz);

		bool sendPacket(const IP_Endpoint *to, const Packet &p);
		bool sendAllExcept(const IP_Endpoint *except, const Packet &p);
		bool sendToAll(const Packet &p);

		std::unordered_map<Identifiers::UUID, IP_Endpoint> mClients;
		std::unordered_map<Identifiers::UUID, PlayerState> mClientState;

		int mLastStateUpdate;
	};
}
