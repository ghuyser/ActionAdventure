#pragma once

#include "types.h"

#include <functional>
#include <thread>

namespace Net
{
	constexpr size_t DefaultThreadSleepMs = 10;
	constexpr size_t ReceiveBufferSize = 1024;
	constexpr size_t WriteBufferSize = 1024;

	constexpr uint32_t RateResolutionMs = 1000;
	constexpr uint32_t RateTimeLengthMs = 61000;
	constexpr size_t   RateNumDivisions = static_cast<size_t>(RateTimeLengthMs / RateResolutionMs);

	class SocketListener
	{
	public:
		SocketListener(const char *tag_name = nullptr);
		SocketListener(const char *ip, const uint16_t port, const char *tag_name = nullptr);
		~SocketListener();

		float* GetReceiveRates();
		float* GetSendRates();

	protected:
		// Get C String representation of the address pointed to by src.
		// address_family will default to AF_INET if left unspecified.
		// Returns a pointer to a statically allocated buffer, no need to free().
		static const char* getCStringIP(const void *src, int address_family = 0);

		bool createSocket(const char *ip, const uint16_t port);

		bool start();
		void stop();

		void listenThread();

		void setProcessTimeouts(std::function<void()>);
		void setProcessRecvData(std::function<void(const void*, const void*)>);
		void setThreadEnding(std::function<void()>);

		bool sendTo(const uint32_t addr, const uint16_t port, const void *data, const size_t size);
		bool sendTo(const void *addr, const void *data, const size_t size);

		void updateRecvRate(uint32_t num_bytes);
		void updateSendRate(uint32_t num_bytes);

		bool getHostName(const char *hostname, char *ip) const;

		void *_socket;
		void *_addr;
		uint16_t mPort;

		std::thread *mThread;
		bool mRunning;

		int mLastContact;

		std::function<void()> mProcessTimeouts;
		std::function<void(const void*, const void*)> mProcessRecvData;
		std::function<void()> mThreadEnding;

		size_t mThreadSleep;

		uint8_t mRecvBuff[ReceiveBufferSize];
		uint8_t mWriteBuff[WriteBufferSize];

		uint32_t mBytesReceived;
		uint32_t mBytesSent;
		uint32_t mRecvRate[RateNumDivisions];
		uint32_t mSendRate[RateNumDivisions];
		uint32_t mRateOffset;

		uint32_t mBytesLastUpdate;

		const char *mTagName;
	};
}
