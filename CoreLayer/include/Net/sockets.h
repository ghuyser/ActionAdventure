#pragma once

// Sockets wrappers for WinSock2 and Berkeley sockets

#ifdef _WIN32
	#undef _WINSOCKAPI_
	#include <WinSock2.h>
	#include <ws2tcpip.h>
	#include <stdio.h>

	#pragma comment(lib, "Ws2_32.lib")
#elif defined ANDROID || defined __APPLE__ || defined __linux
	#include <sys/socket.h>
	#include <netdb.h>
	#include <arpa/inet.h>
	#include <unistd.h>
	#include <fcntl.h>
	#include <errno.h>
#endif

#ifdef _WIN32
extern WSADATA __wsa_data;
extern int __wsa_start;  // initialize to -1
extern int __wsa_count;  // initialize to 0
#define socket_t                   SOCKET
#define socklen_t                  int
#define is_socket_valid(x)         ((x) != INVALID_SOCKET)
#define is_socket_invalid(x)       ((x) == INVALID_SOCKET)
#define winsock_startup()          (__wsa_count++ == 0 ? __wsa_start = WSAStartup(MAKEWORD(2, 2), &__wsa_data) : 0)
#define winsock_cleanup()          if(--__wsa_count == 0) { WSACleanup(); }
#define socket_error               WSAGetLastError()
#define is_socket_error(x)         ((x) == SOCKET_ERROR)
#define socket_data(x)             (char*)(x)
#define set_blocking(x)            ioctlsocket((x), FIONBIO, (u_long*)"\000")
#define set_nonblocking(x)         ioctlsocket((x), FIONBIO, (u_long*)"\001")
#define sock_addr_int(x)           ((x).sin_addr.S_un.S_addr)
#elif defined ANDROID || defined __APPLE__ || defined __linux
#define socket_t                   intptr_t
#define INVALID_SOCKET             -1
#define is_socket_valid(x)         ((x) >= 0)
#define is_socket_invalid(x)       ((x) < 0)
#define winsock_startup()          0
#define winsock_cleanup()          
#define socket_error               errno
#define closesocket(x)             close(x)
#define is_socket_error(x)         ((x) == -1)
#define socket_data(x)             (void*)(x)
#define set_blocking(x)            fcntl((x), F_SETFL, fcntl((x), F_GETFL, 0) ^ O_NONBLOCK)
#define set_nonblocking(x)         fcntl((x), F_SETFL, fcntl((x), F_GETFL, 0) | O_NONBLOCK)
#define sock_addr_int(x)           ((x).sin_addr.s_addr)
#endif


// Define normal error constants as WSA constants for non-windows
// This is done so that the same constant names can be used across platforms
// Since windows values are already unique, errno.h values will be aliased
#ifndef _WIN32
#define WSAEINTR                   EINTR
#define WSAEWOULDBLOCK             EWOULDBLOCK
#define WSAEINPROGRESS             EINPROGRESS
#define WSAEALREADY                EALREADY
#define WSAENOTSOCK                ENOTSOCK
#define WSAEDESTADDRREQ            EDESTADDRREQ
#define WSAEMSGSIZE                EMSGSIZE
#define WSAEPROTOTYPE              EPROTOTYPE
#define WSAENOPROTOOPT             ENOPROTOOPT
#define WSAEPROTONOSUPPORT         EPROTONOSUPPORT
#define WSAESOCKTNOSUPPORT         ESOCKTNOSUPPORT
#define WSAEOPNOTSUPP              EOPNOTSUPP
#define WSAEPFNOSUPPORT            EPFNOSUPPORT
#define WSAEAFNOSUPPORT            EAFNOSUPPORT
#define WSAEADDRINUSE              EADDRINUSE
#define WSAEADDRNOTAVAIL           EADDRNOTAVAIL
#define WSAENETDOWN                ENETDOWN
#define WSAENETUNREACH             ENETUNREACH
#define WSAENETRESET               ENETRESET
#define WSAECONNABORTED            ECONNABORTED
#define WSAECONNRESET              ECONNRESET
#define WSAENOBUFS                 ENOBUFS
#define WSAEISCONN                 EISCONN
#define WSAENOTCONN                ENOTCONN
#define WSAESHUTDOWN               ESHUTDOWN
#define WSAETOOMANYREFS            ETOOMANYREFS
#define WSAETIMEDOUT               ETIMEDOUT
#define WSAECONNREFUSED            ECONNREFUSED
#define WSAELOOP                   ELOOP
#define WSAENAMETOOLONG            ENAMETOOLONG
#define WSAEHOSTDOWN               EHOSTDOWN
#define WSAEHOSTUNREACH            EHOSTUNREACH
#define WSAENOTEMPTY               ENOTEMPTY
#define WSAEPROCLIM                EPROCLIM
#define WSAEUSERS                  EUSERS
#define WSAEDQUOT                  EDQUOT
#define WSAESTALE                  ESTALE
#define WSAEREMOTE                 EREMOTE
#endif