/*
 * Copyright (c) 2014 Gary Huyser <gary.huyser@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#pragma once

#include <string>

enum class JSONType {
	Null,
	Object,
	Array,
	String,
	Number,
	Boolean,
};

enum class JSONNumberType {
	Integer,
	Float,
	Double,
};

class JSONIteratorException;

class JSONObject {
public:
	JSONObject();
	JSONObject(const JSONObject& obj);
	JSONObject(const char *json);
	JSONObject(const char *json, const int json_length);
	JSONObject(const std::string& json);
	~JSONObject();

	void Parse(const char *json);
	void Parse(const char *json, const int json_length);
	void Parse(const std::string& json);

	std::string Serialize() const;
	const char* CSerialize() const;

	JSONType Type() const;
	JSONNumberType NumberType() const;

	std::string GetString() const;
	const char* GetCString() const;
	int	        GetInteger() const;
	float       GetFloat() const;
	double      GetDouble() const;
	bool        GetBoolean() const;

	static JSONObject& Null();

	void MakeNull();
	void MakeEmptyArray();
	void MakeEmptyObject();

	bool IsEmpty() const;

	int Count() const;

	bool hasKey(const char *key) const;
	bool hasKey(const std::string& key) const;

	void* Raw() const;

	JSONObject& operator=(const JSONObject& obj);
	JSONObject& operator=(const char *string);
	JSONObject& operator=(const std::string& string);
	JSONObject& operator=(const int& number);
	JSONObject& operator=(const float& number);
	JSONObject& operator=(const double& number);
	JSONObject& operator=(const bool& boolean);

	bool operator==(const JSONObject& obj) const;
	bool operator==(const char *string) const;
	bool operator==(const std::string& string) const;
	bool operator==(const int& number) const;
	bool operator==(const float& number) const;
	bool operator==(const double& number) const;
	bool operator==(const bool& boolean) const;

	inline bool operator!=(const JSONObject& obj) const {return !this->operator==(obj);}
	inline bool operator!=(const char *string) const    {return !this->operator==(string);}
	inline bool operator!=(const std::string str) const {return !this->operator==(str);}
	inline bool operator!=(const int& number) const     {return !this->operator==(number);}
	inline bool operator!=(const float& number) const   {return !this->operator==(number);}
	inline bool operator!=(const double& number) const  {return !this->operator==(number);}
	inline bool operator!=(const bool& boolean) const   {return !this->operator==(boolean);}

	       bool operator< (const JSONObject& obj) const;
	       bool operator< (const char *string) const;
	       bool operator< (const std::string& str) const;
	       bool operator< (const int& number) const;
	       bool operator< (const float& number) const;
	       bool operator< (const double& number) const;
	inline bool operator< (const bool& boolean) const   {return false;}

	inline bool operator> (const JSONObject& obj) const {return  obj.operator< (*this);}
	       bool operator> (const char *string) const;
	       bool operator> (const std::string& str) const;
	       bool operator> (const int& number) const;
	       bool operator> (const float& number) const;
	       bool operator> (const double& number) const;
	inline bool operator> (const bool& boolean) const   {return false;}

	inline bool operator<=(const JSONObject& obj) const {return !this->operator> (obj);}
	inline bool operator<=(const char *string) const    {return !this->operator> (string);}
	inline bool operator<=(const std::string& str) const{return !this->operator> (str);}
	inline bool operator<=(const int& number) const     {return !this->operator> (number);}
	inline bool operator<=(const float& number) const   {return !this->operator> (number);}
	inline bool operator<=(const double& number) const  {return !this->operator> (number);}
	inline bool operator<=(const bool& boolean) const   {return  this->operator==(boolean);}

	inline bool operator>=(const JSONObject& obj) const {return !this->operator< (obj);}
	inline bool operator>=(const char *string) const    {return !this->operator< (string);}
	inline bool operator>=(const std::string& str) const{return !this->operator< (str);}
	inline bool operator>=(const int& number) const     {return !this->operator< (number);}
	inline bool operator>=(const float& number) const   {return !this->operator< (number);}
	inline bool operator>=(const double& number) const  {return !this->operator< (number);}
	inline bool operator>=(const bool& boolean) const   {return  this->operator==(boolean);}

	      JSONObject& operator[](const JSONObject& keyidx);
	const JSONObject& operator[](const JSONObject& keyidx) const;

	      JSONObject& operator[](const char*key);
	const JSONObject& operator[](const char*key) const;

	      JSONObject& operator[](const std::string& key);
	const JSONObject& operator[](const std::string& key) const;

	      JSONObject& operator[](const int& index);
	const JSONObject& operator[](const int& index) const;

	class Iterator {
		friend class JSONObject;
	public:
		Iterator();
		~Iterator();
		Iterator(const Iterator& obj);

		const std::string Key() const;
		const JSONObject Value() const;

		Iterator& operator++();
		Iterator operator++(int);
		//Iterator& operator--();
		//Iterator operator--(int);
		const JSONObject* operator->() const;
		const JSONObject& operator*() const;
		bool operator==(const Iterator& obj) const;
		bool operator!=(const Iterator& obj) const {return !this->operator==(obj);}

	private:
		class JIter_impl;
		JIter_impl *pimpl;
	};

	Iterator begin() const;
	Iterator find(const char *key) const;
	Iterator end() const;

private:
	class JObj_impl;
	JObj_impl *pimpl;
};
