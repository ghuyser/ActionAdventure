#pragma once

#ifdef _WIN32
#include <windows.h>
#else
#include <time.h>
#endif

namespace System
{
	class TimeProfiler
	{
	public:
	#ifdef _WIN32
		TimeProfiler();
	#endif

		void Start();

		static int CurrentIntTime();
		static double CurrentTime();

		int GetIntElapsed();
		double GetElapsed();

	private:
	#ifdef _WIN32
		LARGE_INTEGER frequency;
		LARGE_INTEGER t1, t2;
	#else
		struct timespec diff(struct timespec start, struct timespec end);
		struct timespec res, t1, t2;
	#endif
	};
}
