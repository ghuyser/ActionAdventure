/*
 * Copyright (c) 2013 Gary Huyser <gary.huyser@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#pragma once

// Threading wrappers for windows threads and pthreads

#ifdef ANDROID
	#include <pthread.h>
	#include <errno.h>
#elif defined _WIN32
	#include <windows.h>
#elif defined __APPLE__
	#include <pthread.h>
	#include <errno.h>
#elif (defined __linux && !defined ANDROID)
	#include <pthread.h>
	#include <errno.h>
	#include <sys/time.h>
#endif

#if defined ANDROID || defined __APPLE__ || defined __linux
#define msleep(ms)							{ struct timespec ts; ts.tv_sec = ms/1000; ts.tv_nsec = (ms%1000)*1000000; nanosleep(&ts, NULL); }
#define thread_t							pthread_t
#define thread_create(func, param, id)		pthread_create(&id, NULL, func, param)
#define thread_join(id)						pthread_join(id, NULL)
#define thread_join_ret(id, retval)			pthread_join(id, &retval)
#define mutex_t								pthread_mutex_t
#define mutex_init(mutex)					pthread_mutex_init(&mutex, NULL)
#define mutex_lock(mutex)					pthread_mutex_lock(&mutex)
#define mutex_unlock(mutex)					pthread_mutex_unlock(&mutex)
#define mutex_close(mutex)					pthread_mutex_destroy(&mutex)
#define critical_t							pthread_mutex_t
#define critical_init(critical)				pthread_mutex_init(&critical, NULL)
#define enter_critical(critical)			pthread_mutex_lock(&critical)
#define leave_critical(critical)			pthread_mutex_unlock(&critical)
#define critical_close(critical)			pthread_mutex_destroy(&mutex)
#define cond_t								pthread_cond_t
#define cond_init(cond)						pthread_cond_init(&cond, NULL)
#define cond_wait(cond, critical)			pthread_cond_wait(&cond, &critical)
#define cond_timedwait(cond, critical, ms)	cond_timedwait_f(&cond, &critical, ms)
#define cond_signal(cond)					pthread_cond_signal(&cond)
#define is_timeout(retval)					(retval == ETIMEDOUT)
#define THREAD_CALLBACK_RETURN				void*
#define THREAD_CALLBACK						THREAD_CALLBACK_RETURN
#define THREAD_CALLBACK_PARAM				void*
inline int cond_timedwait_f(cond_t *cond, critical_t *crit, int ms) {
	struct timeval tp;
	struct timespec ts;
	gettimeofday(&tp, NULL);
	ts.tv_sec = tp.tv_sec + ms/1000;
	ts.tv_nsec = (tp.tv_usec * 1000) + ((ms%1000)*1000000);
	return pthread_cond_timedwait(cond, crit, &ts);
}
#elif defined _WIN32
#define msleep(ms)							Sleep(ms)
#define thread_t							HANDLE
#define thread_create(func, param, id)		{ id = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)func, param, 0, NULL); }
#define thread_join(id)						WaitForSingleObject(id, INFINITE)
#define thread_join_ret(id, retval)			{ retval = WaitForSingleObject(id, INFINITE); }
#define mutex_t								HANDLE
#define mutex_init(mutex)					mutex = CreateMutex(NULL, false, NULL)
#define mutex_lock(mutex)					WaitForSingleObject(mutex, INFINITE)
#define mutex_unlock(mutex)					ReleaseMutex(mutex)
#define mutex_close(mutex)					CloseHandle(mutex)
#define critical_t							CRITICAL_SECTION
#define critical_init(critical)				InitializeCriticalSection(&critical)
#define enter_critical(critical)			EnterCriticalSection(&critical)
#define leave_critical(critical)			LeaveCriticalSection(&critical)
#define critical_close(critical)			DeleteCriticalSection(&critical)
#define cond_t								CONDITION_VARIABLE
#define cond_init(cond)						InitializeConditionVariable(&cond)
#define cond_wait(cond, critical)			SleepConditionVariableCS(&cond, &critical, INFINITE)
#define cond_timedwait(cond, critical, ms)	SleepConditionVariableCS(&cond, &critical, ms)
#define cond_signal(cond)					WakeAllConditionVariable(&cond)
#define is_timeout(retval)					(retval == 0)
#define THREAD_CALLBACK_RETURN				DWORD
#define THREAD_CALLBACK						THREAD_CALLBACK_RETURN WINAPI
#define THREAD_CALLBACK_PARAM				LPVOID
#endif
