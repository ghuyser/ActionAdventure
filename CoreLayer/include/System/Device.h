#pragma once

namespace System
{
	class Device {
	public:
		static Device* Instance();

		static void SetDPI(int dpi);
		static int GetDPI();

		static double GetScreenSize();

		static void SetScreenResolution(int width, int height);
		static int ScreenWidth();
		static int ScreenHeight();
		static double ScreenRatio();

		static int GetDPWidth();
		static int GetDPHeight();
		static void SetDPResolution(int width, int height);
	
		static double PxToDP(double px);
		static double DPToPx(double dp);

	#if defined _WIN32 || (defined __linux && !defined ANDROID)
		static const int TouchSlop = 1;
	#elif defined(ANDROID)
		static const int TouchSlop = 16;
	#endif

		static bool KeyboardPresent();
		static void ShowSoftKeyboard();
		static void HideSoftKeyboard();
		static bool IsSoftKeyboardVisible();

		static bool KeyboardShiftDown();
		static bool KeyboardCapsOn();
	
	private:
		Device();
		Device(Device const&) {};
		Device& operator=(Device const&) { return *this; };
		static Device *mInstance;

		// Screen properties
		double mScreenSize;
		double mScreenWidth, mScreenHeight;
		double mDPWidth, mDPHeight;
		int mDPI;
		double mDPId;

		// Keyboard properties
		bool mIsSoftKeyboardVisible;
	};
}
