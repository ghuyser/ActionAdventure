#pragma once

#define _USE_MATH_DEFINES
#include <math.h>
#include <memory.h>
#include <float.h>

#if defined(_WIN32) && defined(_MSC_VER) && (_MSC_VER < 1900)
#define fminf(a,b)            (((a) < (b)) ? (a) : (b))
#define fmaxf(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#define SMALL_NUM	0.000001f


namespace G3DMath {

	template <class T> class Vec2;
	template <class T> class Vec3;
	template <class T> class Vec4;

	typedef Vec2<float>  Vec2f;
	typedef Vec2<double> Vec2d;
	typedef Vec3<float>  Vec3f;
	typedef Vec3<double> Vec3d;
	typedef Vec4<float>  Vec4f;
	typedef Vec4<double> Vec4d;

	typedef Vec2f Point2f;
	typedef Vec2d Point2d;
	typedef Vec3f Point3f;
	typedef Vec3d Point3d;
	typedef Vec4f Point4f;
	typedef Vec4d Point4d;

	template <class T> class Matrix4;

	typedef Matrix4<float>  Matrix4f;
	typedef Matrix4<double> Matrix4d;

	template <class T> class Line;
	template <class T> class LineSegment;
	template <class T> class Line2D;
	template <class T> class LineSegment2D;
	template <class T> class Plane;

	template <class T> class Frustum;

	enum class IntersectResult {
		Miss,
		Intersect,
		Coincide,
		Complete,
		Partial
	};

	enum class Side {
		Left,
		Right,
		Top,
		Bottom,
		Front,
		Back
	};

	bool unProjectf(float winx, float winy, float winz, const Matrix4f& modelview, const Matrix4f& projection, const int viewport[], Vec3f& objectCoordinate);
	bool unProjectf(float winx, float winy, float winz, const Matrix4f& mvpMatrix, const int viewport[], Vec3f& objectCoordinate);

	template <typename T> int sign(T val) {
		return (T(0) < val) - (val < T(0));
	}

	
	template <class T>
	struct BoundingBox {
		T low;
		T high;

		BoundingBox() {}
		BoundingBox(T _low, T _high) : low(_low), high(_high) {}

		bool operator==(const BoundingBox<T> &b) const
			{
				return low == b.low && high == b.high;
			}

		bool operator!=(const BoundingBox<T> &b) const
			{
				return low != b.low || high != b.high;
			}

		const bool isInside(const BoundingBox<T> &x) const
			{
				return low >= x.low && high <= x.high;
			}

		const bool contains(const T &p) const
			{
				return p >= low && p <= high;
			}

		void include(const T &p)
			{
				low = T::min(low, p);
				high = T::max(high, p);
			}

		template <class U>
		operator BoundingBox<U>() const
			{
				BoundingBox<U> b;
				b.low = low;
				b.high = high;
				return b;
			}
	};


	// In C++11 this should work, but type aliases are not supported in VS2102
/*	template <class T> using BoundingBox3 = BoundingBox<Vec3<T>>;
	template <class T> using BoundingBox2 = BoundingBox<Vec2<T>>;

	typedef BoundingBox3<float> BoundingBox3D;
	typedef BoundingBox2<float> BoundingBox2D;*/

	#define BoundingBox3(x)	BoundingBox<Vec3<x>>
	#define BoundingBox2(x) BoundingBox<Vec2<x>>
	typedef BoundingBox3(float) BoundingBox3D;
	typedef BoundingBox2(float) BoundingBox2D;


	template <class T>
	class Vec2 {
	public:
		/*
		 * Constructors / Destructor
		 */
		Vec2()                 : x(0), y(0)       {}
		Vec2(T _x, T _y)       : x(_x), y(_y)     {}
		Vec2(const T v[])      : x(v[0]), y(v[1]) {}
		Vec2(const Vec2<T> &v) : x(v.x), y(v.y)   {}
		Vec2(const Vec3<T> &v) : x(v.x), y(v.y)   {}
		Vec2(const Vec4<T> &v) : x(v.x), y(v.y)   {}
		~Vec2() {}

		template <class U> Vec2(U _x, U _y)  : x(static_cast<T>(_x)), y(static_cast<T>(_y))     {}
		template <class U> Vec2(const U v[]) : x(static_cast<T>(v[0])), y(static_cast<T>(v[1])) {}

		template <class U>
		operator Vec2<U>() const
			{
				return Vec2<U>(static_cast<U>(x), static_cast<U>(y));
			}

		Vec2<T>& operator=(const Vec2<T> &v)
			{
				x = v.x;
				y = v.y;
				return *this;
			}

		Vec2<T>& operator=(const T a[]) const
			{
				x = a[0];
				y = a[1];
				return *this;
			}
	

		bool operator==(const Vec2<T> &v) const
			{
				return x == v.x && y == v.y;
			}

		bool operator==(const T a[]) const
			{
				return x == a[0] && y == a[1];
			}

		bool operator!=(const Vec2<T>& v) const
			{
				return !this->operator==(v);
			}

		bool operator!=(const T a[]) const
			{
				return !this->operator==(a);
			}

		bool operator>(const Vec2<T> &v) const
			{
				return x > v.x && y > v.y;
			}

		bool operator>(const T a[]) const
			{
				return x > a[0] && y > a[1];
			}

		bool operator<(const Vec2<T> &v) const
			{
				return x < v.x && y < v.y;
			}

		bool operator<(const T a[]) const
			{
				return x < a[0] && y < a[1];
			}

		bool operator>=(const Vec2<T> &v) const
			{
				return x >= v.x && y >= v.y;
			}

		bool operator>=(const T a[]) const
			{
				return x >= a[0] && y >= a[1];
			}

		bool operator<=(const Vec2<T> &v) const
			{
				return x <= v.x && y <= v.y;
			}

		bool operator<=(const T a[]) const
			{
				return x <= a[0] && y <= a[1];
			}

		Vec2<T> operator+(const Vec2<T> &v) const
			{
				return Vec2<T>(x + v.x, y + v.y);
			}

		Vec2<T> operator+(const T a[]) const
			{
				return Vec2<T>(x + a[0], y + a[1]);
			}
	
		Vec2<T> operator-(const Vec2<T> &v) const
			{
				return Vec2<T>(x - v.x, y - v.y);
			}

		Vec2<T> operator-(const T a[]) const
			{
				return Vec2<T>(x - a[0], y - a[1]);
			}

		Vec2<T> operator*(const Vec2<T> &v) const
			{
				return Vec2<T>(x * v.x, y * v.y);
			}

		Vec2<T> operator*(const T a[]) const
			{
				return Vec2<T>(x * a[0], y * a[1]);
			}

		Vec2<T> operator*(const T s) const
			{
				return Vec2<T>(x * s, y * s);
			}

		Vec2<T> operator/(const Vec2<T> &v) const
			{
				return Vec2<T>(x / v.x, y / v.y);
			}

		Vec2<T> operator/(const T a[]) const
			{
				return Vec2<T>(x / a[0], y / a[1]);
			}

		Vec2<T> operator/(const T s) const
			{
				return Vec2<T>(x / s, y / s);
			}

		Vec2<T>& operator+=(const Vec2<T> &v)
			{
				x += v.x;
				y += v.y;
				return *this;
			}

		Vec2<T>& operator+=(const T a[])
			{
				x += a[0];
				y += a[1];
				return *this;
			}

		Vec2<T>& operator-=(const Vec2<T> &v)
			{
				x -= v.x;
				y -= v.y;
				return *this;
			}

		Vec2<T>& operator-=(const T a[])
			{
				x -= a[0];
				y -= a[1];
				return *this;
			}

		Vec2<T>& operator*=(const Vec2<T> &v)
			{
				x *= v.x;
				y *= v.y;
				return *this;
			}

		Vec2<T>& operator*=(const T a[])
			{
				x *= a[0];
				y *= a[1];
				return *this;
			}

		Vec2<T>& operator*=(const T s)
			{
				x *= s;
				y *= s;
				return *this;
			}

		Vec2<T>& operator/=(const Vec2<T> &v)
			{
				x /= v.x;
				y /= v.y;
				return *this;
			}

		Vec2<T>& operator/=(const T a[])
			{
				x /= a[0];
				y /= a[1];
				return *this;
			}

		Vec2<T>& operator/=(const T s)
			{
				x /= s;
				y /= s;
				return *this;
			}

		T& operator[](const int& index)
			{
				return *(&x + index);
			}

		const T operator[](const int& index) const
			{
				return *(&x + index);
			}

		T* operator&() const
			{
				return (T*)(this);
			}

		Vec2<T> operator-() const
			{
				return Vec2<T>(-x, -y);
			}

		T dot(const Vec2<T> &v) const
			{
				return x * v.x + y * v.y;
			}

		T dot(const T a[]) const
			{
				return x * a[0] + y * a[1];
			}

		T cross(const Vec2<T> &v) const
			{
				return x * v.y - y * v.x;
			}

		T cross(const T a[]) const
			{
				return x * a[1] - y * a[0];
			}

		T length() const
			{
				return sqrt(x * x + y * y);
			}

		T lengthSq() const
			{
				return x * x + y * y;
			}

		Vec2<T> unit() const
			{
				T len = length();
				if( len == 0.0f )
					return Vec2<T>();
	
				return Vec2<T>(x / len, y / len);
			}

		Vec2<T> projectOnto(const Line2D<T>& l) const
			{
				Vec2<T> AP = *this - l.point;
				return l.point + (l.vector * (AP.dot(l.vector) / l.vector.dot(l.vector)));
			}

		bool  projectOnto(const LineSegment2D<T>& ls, Vec2<T>& iPoint) const
			{
				Vec2<T> AP = *this - ls.p1;
				Vec2<T> AB = ls.p2 - ls.p1;
				Vec2<T> p = ls.p1 + (AB * (AP.dot(AB) / AB.dot(AB)));
				if( ls.contains(p) ) {
					iPoint = p;
					return true;
				}
				return false;
			}

		static Vec2<T> min(const Vec2<T> &a, const Vec2<T> &b)
			{
				return Vec2<T>(fminf(a.x, b.x), fminf(a.y, b.y));
			}

		static Vec2<T> max(const Vec2<T> &a, const Vec2<T> &b)
			{
				return Vec2<T>(fmaxf(a.x, b.x), fmaxf(a.y, b.y));
			}

		T x;
		T y;
	};

	template <class T>
	class Vec3 {
	public:
		Vec3()                 : x(0), y(0), z(0)          {}
		Vec3(T _x, T _y, T _z) : x(_x), y(_y), z(_z)       {}
		Vec3(const T v[])      : x(v[0]), y(v[1]), z(v[2]) {}
		Vec3(const Vec2<T> &v) : x(v.x), y(v.y), z(0)      {}
		Vec3(const Vec3<T> &v) : x(v.x), y(v.y), z(v.z)    {}
		Vec3(const Vec4<T> &v) : x(v.x), y(v.y), z(v.z)    {}
		~Vec3() {}

		template <class U> Vec3(U _x, U _y, U _z)  : x(static_cast<T>(_x)), y(static_cast<T>(_y)), z(static_cast<T>(_z))      {}
		template <class U> Vec3(const U v[])       : x(static_cast<T>(v[0])), y(static_cast<T>(v[1])), z(static_cast<T>(v[2])) {}

		template <class U>
		operator Vec3<U>() const
			{
				return Vec3<U>(static_cast<U>(x), static_cast<U>(y), static_cast<U>(z));
			}

		Vec3<T>& operator=(const Vec3<T>&v)
			{
				x = v.x;
				y = v.y;
				z = v.z;
				return *this;
			}

		Vec3<T>& operator=(const T a[])
			{
				x = a[0];
				y = a[1];
				z = a[2];
				return *this;
			}
	
		bool operator==(const Vec3<T>&v) const
			{
				return x == v.x && y == v.y && z == v.z;
			}

		bool operator==(const T a[]) const
			{
				return x == a[0] && y == a[1] && z == a[2];
			}

		bool operator!=(const Vec3<T>& v) const
			{
				return !this->operator==(v);
			}

		bool operator!=(const T a[]) const
			{
				return !this->operator==(a);
			}

		bool operator>(const Vec3<T> &v) const
			{
				return x > v.x && y > v.y && z > v.z;
			}

		bool operator>(const T a[]) const
			{
				return x > a[0] && y > a[1] && z > a[2];
			}

		bool operator<(const Vec3<T> &v) const
			{
				return x < v.x && y < v.y && z < v.z;
			}

		bool operator<(const T a[]) const
			{
				return x < a[0] && y < a[1] && z < a[2];
			}

		bool operator>=(const Vec3<T> &v) const
			{
				return x >= v.x && y >= v.y && z >= v.z;
			}

		bool operator>=(const T a[]) const
			{
				return x >= a[0] && y >= a[1] && z >= a[2];
			}

		bool operator<=(const Vec3<T> &v) const
			{
				return x <= v.x && y <= v.y && z <= v.z;
			}

		bool operator<=(const T a[]) const
			{
				return x <= a[0] && y <= a[1] && z <= a[2];
			}

		Vec3<T> operator+(const Vec3<T> &v) const
			{
				return Vec3<T>(x + v.x, y + v.y, z + v.z);
			}

		Vec3<T> operator+(const T a[]) const
			{
				return Vec3<T>(x + a[0], y + a[1], z + a[2]);
			}
	
		Vec3<T> operator-(const Vec3<T> &v) const
			{
				return Vec3<T>(x - v.x, y - v.y, z - v.z);
			}

		Vec3<T> operator-(const T a[]) const
			{
				return Vec3<T>(x - a[0], y - a[1], z - a[2]);
			}

		Vec3<T> operator*(const Vec3<T> &v) const
			{
				return Vec3<T>(x * v.x, y * v.y, z * v.z);
			}

		Vec3<T> operator*(const T a[]) const
			{
				return Vec3<T>(x * a[0], y * a[1], z * a[2]);
			}

		Vec3<T> operator*(const T s) const
			{
				return Vec3<T>(x * s, y * s, z * s);
			}

		Vec3<T> operator/(const Vec3<T> &v) const
			{
				return Vec3<T>(x / v.x, y / v.y, z / v.z);
			}

		Vec3<T> operator/(const T a[]) const
			{
				return Vec3<T>(x / a[0], y / a[1], z / a[2]);
			}

		Vec3<T> operator/(const T s) const
			{
				return Vec3<T>(x / s, y / s, z / s);
			}

		Vec3<T>& operator+=(const Vec3<T> &v)
			{
				x += v.x;
				y += v.y;
				z += v.z;
				return *this;
			}

		Vec3<T>& operator+=(const T a[])
			{
				x += a[0];
				y += a[1];
				z += a[2];
				return *this;
			}

		Vec3<T>& operator-=(const Vec3<T> &v)
			{
				x -= v.x;
				y -= v.y;
				z -= v.z;
				return *this;
			}

		Vec3<T>& operator-=(const T a[])
			{
				x -= a[0];
				y -= a[1];
				z -= a[2];
				return *this;
			}

		Vec3<T>& operator*=(const Vec3<T> &v)
			{
				x *= v.x;
				y *= v.y;
				z *= v.z;
				return *this;
			}

		Vec3<T>& operator*=(const T a[])
			{
				x *= a[0];
				y *= a[1];
				z *= a[2];
				return *this;
			}

		Vec3<T>& operator*=(const T s)
			{
				x *= s;
				y *= s;
				z *= s;
				return *this;
			}

		Vec3<T>& operator/=(const Vec3<T> &v)
			{
				x /= v.x;
				y /= v.y;
				z /= v.z;
				return *this;
			}

		Vec3<T>& operator/=(const T a[])
			{
				x /= a[0];
				y /= a[1];
				z /= a[2];
				return *this;
			}

		Vec3<T>& operator/=(const T s)
			{
				x /= s;
				y /= s;
				z /= s;
				return *this;
			}

		T& operator[](const int& index)
			{
				return *(&x + index);
			}

		const T operator[](const int& index) const
			{
				return *(&x + index);
			}

		T* operator&() const
			{
				return (T*)(this);
			}

		Vec3<T> operator-() const
			{
				return Vec3<T>(-x, -y, -z);
			}

		T dot(const Vec3<T> &v) const
			{
				return x * v.x + y * v.y + z * v.z;
			}

		T dot(const T a[]) const
			{
				return x * a[0] + y * a[1] + z * a[2];
			}

		Vec3<T> cross(const Vec3<T> &v) const
			{
				return Vec3<T>(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x);
			}

		Vec3<T> cross(const T a[]) const
			{
				return Vec3<T>(y * a[2] - z * a[1], z * a[0] - x * a[2], x * a[1] - y * a[0]);
			}

		T length() const
			{
				return (T)sqrt(x * x + y * y + z * z);
			}

		T lengthSq() const
			{
				return x * x + y * y + z * z;
			}

		Vec3<T> unit() const
			{
				T len = length();
				if( len == 0.0f )
					return Vec3<T>();
	
				return Vec3<T>(x / len, y / len, z / len);
			}

		Vec3<T> projectOnto(const Line<T>& l) const
			{
				Vec3<T> AP = *this - l.point;
				return l.point + (l.vector * (AP.dot(l.vector) / l.vector.dot(l.vector)));
			}

		bool  projectOnto(const LineSegment<T>& ls, Vec3<T>& iPoint) const
			{
				Vec3<T> AP = *this - ls.p1;
				Vec3<T> AB = ls.p2 - ls.p1;
				Vec3<T> p = ls.p1 + (AB * (AP.dot(AB) / AB.dot(AB)));
				if( ls.contains(p) ) {
					iPoint = p;
					return true;
				}
				return false;
			}

		static Vec3<T> min(const Vec3<T> &a, const Vec3<T> &b)
			{
				return Vec3<T>(fminf(a.x, b.x), fminf(a.y, b.y), fminf(a.z, b.z));
			}

		static Vec3<T> max(const Vec3<T> &a, const Vec3<T> &b)
			{
				return Vec3<T>(fmaxf(a.x, b.x), fmaxf(a.y, b.y), fmaxf(a.z, b.z));
			}

		T x;
		T y;
		T z;
	};

	template <class T>
	class Vec4 {
	public:
		Vec4()                       : x(0), y(0), z(0), w(1)             {}
		Vec4(T _x, T _y, T _z, T _w) : x(_x), y(_y), z(_z), w(_w)         {}
		Vec4(const T v[])            : x(v[0]), y(v[1]), z(v[2]), w(v[3]) {}
		Vec4(const Vec2<T> &v)       : x(v.x), y(v.y), z(0), w(1)         {}
		Vec4(const Vec3<T> &v)       : x(v.x), y(v.y), z(v.z), w(1)       {}
		Vec4(const Vec4<T> &v)       : x(v.x), y(v.y), z(v.z), w(v.w)     {}
		~Vec4() {}

		template <class U> Vec4(U _x, U _y, U _z, U _w)  : x(static_cast<T>(_x)), y(static_cast<T>(_y)), z(static_cast<T>(_z)), w(static_cast<T>(_w))        {}
		template <class U> Vec4(const U v[])             : x(static_cast<T>(v[0])), y(static_cast<T>(v[1])), z(static_cast<T>(v[2])), w(static_cast<T>(v[3])) {}

		template <class U>
		operator Vec4<U>() const
			{
				return Vec4<U>(static_cast<U>(x), static_cast<U>(y), static_cast<U>(z), static_cast<U>(w));
			}

		Vec4<T>& operator=(const Vec4<T> &v)
			{
				x = v.x;
				y = v.y;
				z = v.z;
				w = v.w;
				return *this;
			}

		Vec4<T>& operator=(const T a[])
			{
				x = a[0];
				y = a[1];
				z = a[2];
				w = a[3];
				return *this;
			}
	
		bool operator==(const Vec4<T> &v) const
			{
				return x == v.x && y == v.y && z == v.z && w == v.w;
			}

		bool operator==(const T a[]) const
			{
				return x == a[0] && y == a[1] && z == a[2] && w == a[3];
			}

		bool operator!=(const Vec4<T>& v) const
			{
				return !this->operator==(v);
			}

		bool operator!=(const T a[]) const
			{
				return !this->operator==(a);
			}

		bool operator>(const Vec4<T> &v) const
			{
				return (x/w) > (v.x/v.w) && (y/w) > (v.y/v.w) && (z/w) > (v.z/v.w);
			}

		bool operator>(const T a[]) const
			{
				return (x/w) > (a[0]/a[3]) && (y/w) > (a[1]/a[3]) && (z/w) > (a[2]/a[3]);
			}

		bool operator<(const Vec4<T> &v) const
			{
				return (x/w) < (v.x/v.w) && (y/w) < (v.y/v.w) && (z/w) < (v.z/v.w);
			}

		bool operator<(const T a[]) const
			{
				return (x/w) < (a[0]/a[3]) && (y/w) < (a[1]/a[3]) && (z/w) < (a[2]/a[3]);
			}

		bool operator>=(const Vec4<T> &v) const
			{
				return (x/w) >= (v.x/v.w) && (y/w) >= (v.y/v.w) && (z/w) >= (v.z/v.w);
			}

		bool operator>=(const T a[]) const
			{
				return (x/w) >= (a[0]/a[3]) && (y/w) >= (a[1]/a[3]) && (z/w) >= (a[2]/a[3]);
			}

		bool operator<=(const Vec4<T> &v) const
			{
				return (x/w) <= (v.x/v.w) && (y/w) <= (v.y/v.w) && (z/w) <= (v.z/v.w);
			}

		bool operator<=(const T a[]) const
			{
				return (x/w) <= (a[0]/a[3]) && (y/w) <= (a[1]/a[3]) && (z/w) <= (a[2]/a[3]);
			}

		Vec4<T> operator+(const Vec4<T> &v) const
			{
				return Vec4f(x + v.x, y + v.y, z + v.z, w + v.w);
			}

		Vec4<T> operator+(const T a[]) const
			{
				return Vec4f(x + a[0], y + a[1], z + a[2], w + a[3]);
			}
	
		Vec4<T> operator-(const Vec4<T> &v) const
			{
				return Vec4f(x - v.x, y - v.y, z - v.z, w - v.w);
			}

		Vec4<T> operator-(const T a[]) const
			{
				return Vec4f(x - a[0], y - a[1], z - a[2], w - a[3]);
			}

		Vec4<T> operator*(const Vec4<T> &v) const
			{
				return Vec4f(x * v.x, y * v.y, z * v.z, w * v.w);
			}

		Vec4<T> operator*(const T a[]) const
			{
				return Vec4f(x * a[0], y * a[1], z * a[2], w * a[3]);
			}

		Vec4<T> operator*(const T s) const
			{
				return Vec4f(x * s, y * s, z * s, w * s);
			}

		Vec4<T> operator/(const Vec4<T> &v) const
			{
				return Vec4f(x / v.x, y / v.y, z / v.z, w / v.w);
			}

		Vec4<T> operator/(const T a[]) const
			{
				return Vec4f(x / a[0], y / a[1], z / a[2], w / a[3]);
			}

		Vec4<T> operator/(const T s) const
			{
				return Vec4f(x / s, y / s, z / s, w / s);
			}

		Vec4<T>& operator+=(const Vec4<T> &v)
			{
				x += v.x;
				y += v.y;
				z += v.z;
				w += v.w;
				return *this;
			}

		Vec4<T>& operator+=(const T a[])
			{
				x += a[0];
				y += a[1];
				z += a[2];
				w += a[3];
				return *this;
			}

		Vec4<T>& operator-=(const Vec4<T> &v)
			{
				x -= v.x;
				y -= v.y;
				z -= v.z;
				w -= v.w;
				return *this;
			}

		Vec4<T>& operator-=(const T a[])
			{
				x -= a[0];
				y -= a[1];
				z -= a[2];
				w -= a[3];
				return *this;
			}

		Vec4<T>& operator*=(const Vec4<T> &v)
			{
				x *= v.x;
				y *= v.y;
				z *= v.z;
				w *= v.w;
				return *this;
			}

		Vec4<T>& operator*=(const T a[])
			{
				x *= a[0];
				y *= a[1];
				z *= a[2];
				w *= a[3];
				return *this;
			}

		Vec4<T>& operator*=(const T s)
			{
				x *= s;
				y *= s;
				z *= s;
				w *= s;
				return *this;
			}

		Vec4<T>& operator/=(const Vec4<T> &v)
			{
				x /= v.x;
				y /= v.y;
				z /= v.z;
				w /= v.w;
				return *this;
			}

		Vec4<T>& operator/=(const T a[])
			{
				x /= a[0];
				y /= a[1];
				z /= a[2];
				w /= a[3];
				return *this;
			}

		Vec4<T>& operator/=(const T s)
			{
				x /= s;
				y /= s;
				z /= s;
				w /= s;
			}

		T& operator[](const int& index)
			{
				return *(&x + index);
			}

		const T  operator[](const int& index) const
			{
				return *(&x + index);
			}

		T* operator&() const
			{
				return (T*)(this);
			}

		Vec4<T> operator-() const
			{
				return Vec4f(-x, -y, -z, -w);
			}

		T dot(const Vec4<T> &v) const
			{
				return x * v.x + y * v.y + z * v.z + w * v.w;
			}

		T dot(const T a[]) const
			{
				return x * a[0] + y * a[1] + z * a[2] + w * a[3];
			}

		T length() const
			{
				return sqrt(x * x + y * y + z * z + w * w);
			}

		T lengthSq() const
			{
				return x * x + y * y + z * z + w * w;
			}

		Vec4<T> unit() const
			{
				T len = length();
				if( len == 0.0f )
					return Vec4f();
	
				return Vec4f(x / len, y / len, z / len, w / len);
			}

		T x;
		T y;
		T z;
		T w;
	};



	template <class T>
	class Matrix4 {
	public:
		Matrix4()
			{
				setIdentity();
			}

		Matrix4(const T a[])
			{
				memcpy(m, a, 16 * sizeof(T));
			}

		Matrix4(const Matrix4<T> &mat)
			{
				memcpy(m, mat.m, 16 * sizeof(T));
			}

		~Matrix4() {}

		template <class U>
		operator Matrix4<U>() const
			{
				Matrix4<U> n;
				for(int i = 0; i < 16; i++)
					n[i] = static_cast<U>(m[i]);
				return n;
			}

		Matrix4<T>& operator=(const Matrix4<T> &mat)
			{
				memcpy(m, mat.m, 16 * sizeof(T));
				return *this;
			}

		Matrix4<T>& operator=(const T a[])
			{
				memcpy(m, a, 16 * sizeof(T));
				return *this;
			}
	
		bool operator==(const Matrix4<T> &mat) const
			{
				return memcmp(m, mat.m, 16 * sizeof(T)) == 0;
			}

		bool operator==(const T a[]) const
			{
				return memcmp(m, a, 16 * sizeof(T)) == 0;
			}

		bool operator!=(const Matrix4<T>& r) const
			{
				return !this->operator==(r);
			}

		bool operator!=(const T a[]) const
			{
				return !this->operator==(a);
			}

		Matrix4<T> operator*(const Matrix4<T> &mat) const
			{
				const T *m0 = m;
				const T *m1 = mat.m;
				Matrix4<T> result;
				T *d = result.m;

				d[0]  = m0[0] *  m1[0] + m0[4] *  m1[1] +  m0[8] *  m1[2] + m0[12] *  m1[3];
				d[1]  = m0[1] *  m1[0] + m0[5] *  m1[1] +  m0[9] *  m1[2] + m0[13] *  m1[3];
				d[2]  = m0[2] *  m1[0] + m0[6] *  m1[1] + m0[10] *  m1[2] + m0[14] *  m1[3];
				d[3]  = m0[3] *  m1[0] + m0[7] *  m1[1] + m0[11] *  m1[2] + m0[15] *  m1[3];
				d[4]  = m0[0] *  m1[4] + m0[4] *  m1[5] +  m0[8] *  m1[6] + m0[12] *  m1[7];
				d[5]  = m0[1] *  m1[4] + m0[5] *  m1[5] +  m0[9] *  m1[6] + m0[13] *  m1[7];
				d[6]  = m0[2] *  m1[4] + m0[6] *  m1[5] + m0[10] *  m1[6] + m0[14] *  m1[7];
				d[7]  = m0[3] *  m1[4] + m0[7] *  m1[5] + m0[11] *  m1[6] + m0[15] *  m1[7];
				d[8]  = m0[0] *  m1[8] + m0[4] *  m1[9] +  m0[8] * m1[10] + m0[12] * m1[11];
				d[9]  = m0[1] *  m1[8] + m0[5] *  m1[9] +  m0[9] * m1[10] + m0[13] * m1[11];
				d[10] = m0[2] *  m1[8] + m0[6] *  m1[9] + m0[10] * m1[10] + m0[14] * m1[11];
				d[11] = m0[3] *  m1[8] + m0[7] *  m1[9] + m0[11] * m1[10] + m0[15] * m1[11];
				d[12] = m0[0] * m1[12] + m0[4] * m1[13] +  m0[8] * m1[14] + m0[12] * m1[15];
				d[13] = m0[1] * m1[12] + m0[5] * m1[13] +  m0[9] * m1[14] + m0[13] * m1[15];
				d[14] = m0[2] * m1[12] + m0[6] * m1[13] + m0[10] * m1[14] + m0[14] * m1[15];
				d[15] = m0[3] * m1[12] + m0[7] * m1[13] + m0[11] * m1[14] + m0[15] * m1[15];

				return result;
			}

		Vec4<T> operator*(const Vec4<T> &v) const
			{
				Vec4<T> d;

				d[0] = m[0] * v[0] + m[4] * v[1] +  m[8] * v[2] + m[12] * v[3];
				d[1] = m[1] * v[0] + m[5] * v[1] +  m[9] * v[2] + m[13] * v[3];
				d[2] = m[2] * v[0] + m[6] * v[1] + m[10] * v[2] + m[14] * v[3];
				d[3] = m[3] * v[0] + m[7] * v[1] + m[11] * v[2] + m[15] * v[3];

				return d;
			}

		const Matrix4<T>& operator*=(const Matrix4<T> &mat)
			{
				*this = *this * mat;
				return *this;
			}

		T& operator[](const int& index)
			{
				return m[index];
			}

		const T operator[](const int& index) const
			{
				return m[index];
			}

		T* operator&() const
			{
				return (T*)m;
			}

		Matrix4<T> multiply(const Matrix4<T>& r) const
			{
				return this->operator*(r);
			}

		Vec4<T> multiply(const Vec4<T>& v) const
			{
				return this->operator*(v);
			}

		void frustum(T left, T right, T bottom, T top, T near_, T far_)
			{
				if( left == right ) {
					return;
				}
				if( top == bottom ) {
					return;
				}
				if( near_ == far_ ) {
					return;
				}
				if( near_ <= 0.0f ) {
					return;
				}
				if( far_ <= 0.0f ) {
					return;
				}
				T r_width  = (T)1.0 / (right - left);
				T r_height = (T)1.0 / (top - bottom);
				T r_depth  = (T)1.0 / (near_ - far_);
				T x = (T)2.0 * (near_ * r_width);
				T y = (T)2.0 * (near_ * r_height);
				T A = (T)2.0 * ((right + left) * r_width);
				T B = (top + bottom) * r_height;
				T C = (far_ + near_) * r_depth;
				T D = (T)2.0 * (far_ * near_ * r_depth);
				m[ 0] = x;
				m[ 5] = y;
				m[ 8] = A;
				m[ 9] = B;
				m[10] = C;
				m[14] = D;
				m[11] = (T)-1.0;
				m[ 1] = 0;
				m[ 2] = 0;
				m[ 3] = 0;
				m[ 4] = 0;
				m[ 6] = 0;
				m[ 7] = 0;
				m[12] = 0;
				m[13] = 0;
				m[15] = 0;
			}

		void ortho(T left, T right, T bottom, T top, T near_, T far_)
			{
				if( left == right ) {
					return;
				}
				if( bottom == top ) {
					return;
				}
				if( near_ == far_ ) {
					return;
				}

				T r_width  = (T)1.0 / (right - left);
				T r_height = (T)1.0 / (top - bottom);
				T r_depth  = (T)1.0 / (far_ - near_);
				T x = (T) 2.0 * (r_width);
				T y = (T) 2.0 * (r_height);
				T z = (T)-2.0 * (r_depth);
				T tx = -(right + left) * r_width;
				T ty = -(top + bottom) * r_height;
				T tz = -(far_ + near_) * r_depth;
				m[ 0] = x;
				m[ 5] = y;
				m[10] = z;
				m[12] = tx;
				m[13] = ty;
				m[14] = tz;
				m[15] = (T)1.0;
				m[ 1] = 0;
				m[ 2] = 0;
				m[ 3] = 0;
				m[ 4] = 0;
				m[ 6] = 0;
				m[ 7] = 0;
				m[ 8] = 0;
				m[ 9] = 0;
				m[11] = 0;
			}

		void perspective(T fovy, T aspect, T znear, T zfar)
			{
				T f = (T)1.0 / tan(fovy * ((T)M_PI / (T)360.0));
				T rangeReciprocal = (T)1.0 / (znear - zfar);

				m[ 0] = f / aspect;
				m[ 1] = 0;
				m[ 2] = 0;
				m[ 3] = 0;

				m[ 4] = 0;
				m[ 5] = f;
				m[ 6] = 0;
				m[ 7] = 0;

				m[ 8] = 0;
				m[ 9] = 0;
				m[10] = (zfar + znear) * rangeReciprocal;
				m[11] = (T)-1;

				m[12] = 0;
				m[13] = 0;
				m[14] = (T)2.0 * zfar * znear * rangeReciprocal;
				m[15] = 0;
			}

		void setIdentity()
			{
				m[ 0] = 1; m[ 4] = 0; m[ 8] = 0; m[12] = 0;
				m[ 1] = 0; m[ 5] = 1; m[ 9] = 0; m[13] = 0;
				m[ 2] = 0; m[ 6] = 0; m[10] = 1; m[14] = 0;
				m[ 3] = 0; m[ 7] = 0; m[11] = 0; m[15] = 1;
			}

		bool isIdentity()
			{
				Matrix4<T> i;
				return *this == i;
			}

		void setLookAt(T eyeX, T eyeY, T eyeZ, T centerX, T centerY, T centerZ, T upX, T upY, T upZ)
			{
				// See the OpenGL GLUT documentation for gluLookAt for a description
				// of the algorithm. We implement it in a straightforward way:

				T fx = centerX - eyeX;
				T fy = centerY - eyeY;
				T fz = centerZ - eyeZ;

				// Normalize f
				T rlf = (T)1.0 / (T)sqrt(fx*fx+fy*fy+fz*fz);
				fx *= rlf;
				fy *= rlf;
				fz *= rlf;

				// compute s = f x up (x means "cross product")
				T sx = fy * upZ - fz * upY;
				T sy = fz * upX - fx * upZ;
				T sz = fx * upY - fy * upX;

				// and normalize s
				T rls = (T)1.0 / (T)sqrt(sx*sx+sy*sy+sz*sz);
				sx *= rls;
				sy *= rls;
				sz *= rls;

				// compute u = s x f
				T ux = sy * fz - sz * fy;
				T uy = sz * fx - sx * fz;
				T uz = sx * fy - sy * fx;

				m[ 0] = sx;
				m[ 1] = ux;
				m[ 2] = -fx;
				m[ 3] = 0;

				m[ 4] = sy;
				m[ 5] = uy;
				m[ 6] = -fy;
				m[ 7] = 0;

				m[ 8] = sz;
				m[ 9] = uz;
				m[10] = -fz;
				m[11] = 0;

				m[12] = 0;
				m[13] = 0;
				m[14] = 0;
				m[15] = 1;

				translateSelf(-eyeX, -eyeY, -eyeZ);
			}

		void setLookAt(const Vec3<T>& eye, const Vec3<T>& center, const Vec3<T>& up)
			{
				setLookAt(eye.x, eye.y, eye.z, center.x, center.y, center.z, up.x, up.y, up.z);
			}

		Matrix4<T> inverse() const
			{
				Matrix4<T> inv;

				inv[ 0] =  m[5] * m[10] * m[15] -  m[5] * m[11] * m[14] -  m[9] * m[6] * m[15]
						 + m[9] *  m[7] * m[14] + m[13] *  m[6] * m[11] - m[13] * m[7] * m[10];
				inv[ 4] = -m[4] * m[10] * m[15] +  m[4] * m[11] * m[14] +  m[8] * m[6] * m[15]
						 - m[8] *  m[7] * m[14] - m[12] *  m[6] * m[11] + m[12] * m[7] * m[10];
				inv[ 8] =  m[4] *  m[9] * m[15] -  m[4] * m[11] * m[13] -  m[8] * m[5] * m[15]
						 + m[8] *  m[7] * m[13] + m[12] *  m[5] * m[11] - m[12] * m[7] *  m[9];
				inv[12] = -m[4] *  m[9] * m[14] +  m[4] * m[10] * m[13] +  m[8] * m[5] * m[14]
						 - m[8] *  m[6] * m[13] - m[12] *  m[5] * m[10] + m[12] * m[6] *  m[9];
				inv[ 1] = -m[1] * m[10] * m[15] +  m[1] * m[11] * m[14] +  m[9] * m[2] * m[15]
						 - m[9] *  m[3] * m[14] - m[13] *  m[2] * m[11] + m[13] * m[3] * m[10];
				inv[ 5] =  m[0] * m[10] * m[15] -  m[0] * m[11] * m[14] -  m[8] * m[2] * m[15]
						 + m[8] *  m[3] * m[14] + m[12] *  m[2] * m[11] - m[12] * m[3] * m[10];
				inv[ 9] = -m[0] *  m[9] * m[15] +  m[0] * m[11] * m[13] +  m[8] * m[1] * m[15]
						 - m[8] *  m[3] * m[13] - m[12] *  m[1] * m[11] + m[12] * m[3] *  m[9];
				inv[13] =  m[0] *  m[9] * m[14] -  m[0] * m[10] * m[13] -  m[8] * m[1] * m[14]
						 + m[8] *  m[2] * m[13] + m[12] *  m[1] * m[10] - m[12] * m[2] *  m[9];
				inv[ 2] =  m[1] *  m[6] * m[15] -  m[1] *  m[7] * m[14] -  m[5] * m[2] * m[15]
						 + m[5] *  m[3] * m[14] + m[13] *  m[2] *  m[7] - m[13] * m[3] *  m[6];
				inv[ 6] = -m[0] *  m[6] * m[15] +  m[0] *  m[7] * m[14] +  m[4] * m[2] * m[15]
						 - m[4] *  m[3] * m[14] - m[12] *  m[2] *  m[7] + m[12] * m[3] *  m[6];
				inv[10] =  m[0] *  m[5] * m[15] -  m[0] *  m[7] * m[13] -  m[4] * m[1] * m[15]
						 + m[4] *  m[3] * m[13] + m[12] *  m[1] *  m[7] - m[12] * m[3] *  m[5];
				inv[14] = -m[0] *  m[5] * m[14] +  m[0] *  m[6] * m[13] +  m[4] * m[1] * m[14]
						 - m[4] *  m[2] * m[13] - m[12] *  m[1] *  m[6] + m[12] * m[2] *  m[5];
				inv[ 3] = -m[1] *  m[6] * m[11] +  m[1] *  m[7] * m[10] +  m[5] * m[2] * m[11]
						 - m[5] *  m[3] * m[10] -  m[9] *  m[2] *  m[7] +  m[9] * m[3] *  m[6];
				inv[ 7] =  m[0] *  m[6] * m[11] -  m[0] *  m[7] * m[10] -  m[4] * m[2] * m[11]
						 + m[4] *  m[3] * m[10] +  m[8] *  m[2] *  m[7] -  m[8] * m[3] *  m[6];
				inv[11] = -m[0] *  m[5] * m[11] +  m[0] *  m[7] *  m[9] +  m[4] * m[1] * m[11]
						 - m[4] *  m[3] *  m[9] -  m[8] *  m[1] *  m[7] +  m[8] * m[3] *  m[5];
				inv[15] =  m[0] *  m[5] * m[10] -  m[0] *  m[6] *  m[9] -  m[4] * m[1] * m[10]
						 + m[4] *  m[2] *  m[9] +  m[8] *  m[1] *  m[6] -  m[8] * m[2] *  m[5];

				T det = m[0]*inv[0] + m[1]*inv[4] + m[2]*inv[8] + m[3]*inv[12];
				if( det == (T)0.0 )
						return Matrix4<T>();

				det = (T)1.0 / det;

				for( int i = 0; i < 16; i++ )
					inv[i] *= det;

				return inv;
			}

		bool invert()
			{
				T inv[16];

				inv[ 0] =  m[5] * m[10] * m[15] -  m[5] * m[11] * m[14] -  m[9] * m[6] * m[15]
						 + m[9] *  m[7] * m[14] + m[13] *  m[6] * m[11] - m[13] * m[7] * m[10];
				inv[ 4] = -m[4] * m[10] * m[15] +  m[4] * m[11] * m[14] +  m[8] * m[6] * m[15]
						 - m[8] *  m[7] * m[14] - m[12] *  m[6] * m[11] + m[12] * m[7] * m[10];
				inv[ 8] =  m[4] *  m[9] * m[15] -  m[4] * m[11] * m[13] -  m[8] * m[5] * m[15]
						 + m[8] *  m[7] * m[13] + m[12] *  m[5] * m[11] - m[12] * m[7] *  m[9];
				inv[12] = -m[4] *  m[9] * m[14] +  m[4] * m[10] * m[13] +  m[8] * m[5] * m[14]
						 - m[8] *  m[6] * m[13] - m[12] *  m[5] * m[10] + m[12] * m[6] *  m[9];
				inv[ 1] = -m[1] * m[10] * m[15] +  m[1] * m[11] * m[14] +  m[9] * m[2] * m[15]
						 - m[9] *  m[3] * m[14] - m[13] *  m[2] * m[11] + m[13] * m[3] * m[10];
				inv[ 5] =  m[0] * m[10] * m[15] -  m[0] * m[11] * m[14] -  m[8] * m[2] * m[15]
						 + m[8] *  m[3] * m[14] + m[12] *  m[2] * m[11] - m[12] * m[3] * m[10];
				inv[ 9] = -m[0] *  m[9] * m[15] +  m[0] * m[11] * m[13] +  m[8] * m[1] * m[15]
						 - m[8] *  m[3] * m[13] - m[12] *  m[1] * m[11] + m[12] * m[3] *  m[9];
				inv[13] =  m[0] *  m[9] * m[14] -  m[0] * m[10] * m[13] -  m[8] * m[1] * m[14]
						 + m[8] *  m[2] * m[13] + m[12] *  m[1] * m[10] - m[12] * m[2] *  m[9];
				inv[ 2] =  m[1] *  m[6] * m[15] -  m[1] *  m[7] * m[14] -  m[5] * m[2] * m[15]
						 + m[5] *  m[3] * m[14] + m[13] *  m[2] *  m[7] - m[13] * m[3] *  m[6];
				inv[ 6] = -m[0] *  m[6] * m[15] +  m[0] *  m[7] * m[14] +  m[4] * m[2] * m[15]
						 - m[4] *  m[3] * m[14] - m[12] *  m[2] *  m[7] + m[12] * m[3] *  m[6];
				inv[10] =  m[0] *  m[5] * m[15] -  m[0] *  m[7] * m[13] -  m[4] * m[1] * m[15]
						 + m[4] *  m[3] * m[13] + m[12] *  m[1] *  m[7] - m[12] * m[3] *  m[5];
				inv[14] = -m[0] *  m[5] * m[14] +  m[0] *  m[6] * m[13] +  m[4] * m[1] * m[14]
						 - m[4] *  m[2] * m[13] - m[12] *  m[1] *  m[6] + m[12] * m[2] *  m[5];
				inv[ 3] = -m[1] *  m[6] * m[11] +  m[1] *  m[7] * m[10] +  m[5] * m[2] * m[11]
						 - m[5] *  m[3] * m[10] -  m[9] *  m[2] *  m[7] +  m[9] * m[3] *  m[6];
				inv[ 7] =  m[0] *  m[6] * m[11] -  m[0] *  m[7] * m[10] -  m[4] * m[2] * m[11]
						 + m[4] *  m[3] * m[10] +  m[8] *  m[2] *  m[7] -  m[8] * m[3] *  m[6];
				inv[11] = -m[0] *  m[5] * m[11] +  m[0] *  m[7] *  m[9] +  m[4] * m[1] * m[11]
						 - m[4] *  m[3] *  m[9] -  m[8] *  m[1] *  m[7] +  m[8] * m[3] *  m[5];
				inv[15] =  m[0] *  m[5] * m[10] -  m[0] *  m[6] *  m[9] -  m[4] * m[1] * m[10]
						 + m[4] *  m[2] *  m[9] +  m[8] *  m[1] *  m[6] -  m[8] * m[2] *  m[5];

				T det = m[0]*inv[0] + m[1]*inv[4] + m[2]*inv[8] + m[3]*inv[12];
				if( det == (T)0.0 )
						return false;

				det = (T)1.0 / det;

				for( int i = 0; i < 16; i++ )
					inv[i] *= det;

				this->operator=(inv);
				return true;
			}

		void rotateSelf(T a, T x, T y, T z)
			{
				this->operator=(multiply(getRotate(a, x, y, z)));
			}

		void rotateSelf(T a, const Vec3<T>& v)
			{
				this->operator=(multiply(getRotate(a, v.x, v.y, v.z)));
			}

		Matrix4<T> rotate(T a, T x, T y, T z) const
			{
				return multiply(getRotate(a, x, y, z));
			}

		Matrix4<T> rotate(T a, const Vec3<T>& v) const
			{
				return multiply(getRotate(a, v.x, v.y, v.z));
			}


		void scaleSelf(T x, T y, T z)
			{
				for( int i = 0; i < 4; i++ ) {
					m[     i] *= x;
					m[ 4 + i] *= y;
					m[ 8 + i] *= z;
				}
			}

		void scaleSelf(const Vec3<T>& v)
			{
				for( int i = 0; i < 4; i++ ) {
					m[     i] *= v.x;
					m[ 4 + i] *= v.y;
					m[ 8 + i] *= v.z;
				}
			}

		Matrix4<T> scale(T x, T y, T z) const
			{
				Matrix4<T> r;
				for( int i = 0; i < 4; i++ ) {
					r[     i] = m[     i] * x;
					r[ 4 + i] = m[ 4 + i] * y;
					r[ 8 + i] = m[ 8 + i] * z;
					r[12 + i] = m[12 + i];
				}
				return r;
			}

		Matrix4<T> scale(const Vec3<T>& v) const
			{
				Matrix4<T> r;
				for( int i = 0; i < 4; i++ ) {
					r[     i] = m[     i] * v.x;
					r[ 4 + i] = m[ 4 + i] * v.y;
					r[ 8 + i] = m[ 8 + i] * v.z;
					r[12 + i] = m[12 + i];
				}
				return r;
			}


		void translateSelf(T x, T y, T z)
			{
				for( int i = 0; i < 4; i++ ) {
					m[12 + i] += m[i] * x + m[4 + i] * y + m[8 + i] * z;
				}
			}

		void translateSelf(const Vec3<T>& v)
			{
				for( int i = 0; i < 4; i++ ) {
					m[12 + i] += m[i] * v.x + m[4 + i] * v.y + m[8 + i] * v.z;
				}
			}

		Matrix4<T> translate(T x, T y, T z) const
			{
				Matrix4<T> tm;
				for( int i = 0; i < 12; i++ ) {
					tm[i] = m[i];
				}

				for( int i = 0; i < 4; i++ ) {
					tm[12 + i] = m[i] * x + m[4 + i] * y + m[8 + i] * z + m[12 + i];
				}

				return tm;
			}

		Matrix4<T> translate(const Vec3<T>& v) const
			{
				Matrix4<T> tm;
				for( int i = 0; i < 12; i++ ) {
					tm[i] = m[i];
				}

				for( int i = 0; i < 4; i++ ) {
					tm[12 + i] = m[i] * v.x + m[4 + i] * v.y + m[8 + i] * v.z + m[12 + i];
				}

				return tm;
			}

		Matrix4<T> transpose() const
			{
				Matrix4<T> mTrans;
				for( int i = 0; i < 4; i++ ) {
					int mBase = i * 4;
					mTrans[i] = m[mBase];
					mTrans[i + 4] = m[mBase + 1];
					mTrans[i + 8] = m[mBase + 2];
					mTrans[i + 12] = m[mBase + 3];
				}
				return mTrans;
			}

	private:
		Matrix4<T> getRotateEuler(T x, T y, T z) const
			{
				Matrix4<T> rm;

				x *= (T) (M_PI / 180.0);
				y *= (T) (M_PI / 180.0);
				z *= (T) (M_PI / 180.0);
				T cx = cos(x);
				T sx = sin(x);
				T cy = cos(y);
				T sy = sin(y);
				T cz = cos(z);
				T sz = sin(z);
				T cxsy = cx * sy;
				T sxsy = sx * sy;

				rm[0]  =   cy * cz;
				rm[1]  =  -cy * sz;
				rm[2]  =   sy;
				rm[3]  =  0;

				rm[4]  =  cxsy * cz + cx * sz;
				rm[5]  = -cxsy * sz + cx * cz;
				rm[6]  =  -sx * cy;
				rm[7]  =  0;

				rm[8]  = -sxsy * cz + sx * sz;
				rm[9]  =  sxsy * sz + sx * cz;
				rm[10] =  cx * cy;
				rm[11] =  0;

				rm[12] =  0;
				rm[13] =  0;
				rm[14] =  0;
				rm[15] =  1;

				return rm;
			}

		Matrix4<T> getRotate(T a, T x, T y, T z) const
			{
				Matrix4<T> rm;
				rm[3] = 0;
				rm[7] = 0;
				rm[11]= 0;
				rm[12]= 0;
				rm[13]= 0;
				rm[14]= 0;
				rm[15]= 1;
				a *= (T) (M_PI / 180.0);
				T s = (T)sin(a);
				T c = (T)cos(a);
				if( (T)1.0 == x && (T)0.0 == y && (T)0.0 == z ) {
					rm[5] = c;   rm[10]= c;
					rm[6] = s;   rm[9] = -s;
					rm[1] = 0;   rm[2] = 0;
					rm[4] = 0;   rm[8] = 0;
					rm[0] = 1;
				} else if( (T)0.0 == x && (T)1.0 == y && (T)0.0 == z ) {
					rm[0] = c;   rm[10]= c;
					rm[8] = s;   rm[2] = -s;
					rm[1] = 0;   rm[4] = 0;
					rm[6] = 0;   rm[9] = 0;
					rm[5] = 1;
				} else if( (T)0.0 == x && (T)0.0 == y && (T)1.0 == z ) {
					rm[0] = c;   rm[5] = c;
					rm[1] = s;   rm[4] = -s;
					rm[2] = 0;   rm[6] = 0;
					rm[8] = 0;   rm[9] = 0;
					rm[10]= 1;
				} else {
					T len = (T)sqrt(x*x+y*y+z*z);
					if (1.0f != len) {
						T recipLen = (T)1.0 / len;
						x *= recipLen;
						y *= recipLen;
						z *= recipLen;
					}
					T nc = (T)1.0 - c;
					T xy = x * y;
					T yz = y * z;
					T zx = z * x;
					T xs = x * s;
					T ys = y * s;
					T zs = z * s;
					rm[ 0] = x*x*nc +  c;
					rm[ 4] =  xy*nc - zs;
					rm[ 8] =  zx*nc + ys;
					rm[ 1] =  xy*nc + zs;
					rm[ 5] = y*y*nc +  c;
					rm[ 9] =  yz*nc - xs;
					rm[ 2] =  zx*nc - ys;
					rm[ 6] =  yz*nc + xs;
					rm[10] = z*z*nc +  c;
				}

				return rm;
			}

		T m[16];
	};


	template <class T>
	class Line {
		friend class Vec3<T>;
		friend class Plane<T>;
	public:
		Line() {}

		Line(const Vec3<T>& p, const Vec3<T>& v) : point(p), vector(v)
			{
				vector = vector.unit();
			}

		Line(const T p[], const T v[]) : point(p), vector(v)
			{
				vector = vector.unit();
			}

		Line(const Line<T> &l)
			{
				point = l.point;
				vector = l.vector;
			}

		template <class U>
		operator Line<U>() const
			{
				return Line<U>(point, vector);
			}

		Line<T>& operator=(const Line<T> &l)
			{
				point = l.point;
				vector = l.vector;
				return *this;
			}

		bool  operator==(const Line<T> &l) const
			{
				// floats are not exact, this requires them to be... currently
				if( vector == l.vector || -vector == l.vector ) {
					return contains(l.point);
				}
				return false;
			}

		bool  operator!=(const Line<T> &l) const
			{
				return !this->operator==(l);
			}

		bool contains(const Vec3<T> &p) const
			{
				// floats are not exact, this requires them to be... currently
				Vec3<T> n = (p - point).unit();
				return vector == n || -vector == n;
			}


		IntersectResult intersects(const Line<T> &l, Vec3<T> &iPoint) const
			{
				Vec3<T> d = l.point - point;
				Vec3<T> x1 = vector.cross(l.vector);
	
				if( x1.lengthSq() < SMALL_NUM )
					return IntersectResult::Coincide;

				T dot = d.dot(x1);

				if( dot != 0 )
					return IntersectResult::Miss;

				Vec3<T> x2 = d.cross(l.vector);
				T s = x2.dot(x1) / x1.lengthSq();

				iPoint = point + vector * s;
				return IntersectResult::Intersect;
			}

		IntersectResult intersects(const Plane<T> &p, Vec3<T> &iPoint) const
			{
				Vec3<T> w = point - p.point;
	
				T d = p.normal.dot(vector);
				T n = -p.normal.dot(w);

				if( fabsf(d) < SMALL_NUM ) {
					if( n == 0 )
						return IntersectResult::Coincide;
					return IntersectResult::Miss;
				}

				T si = n / d;
				iPoint = point + (vector * si);

				return IntersectResult::Intersect;
			}

	private:
		Vec3<T> point;
		Vec3<T> vector;
	};

	
	template <class T>
	class LineSegment {
		friend class Vec3<T>;
		friend class Plane<T>;
	public:
		LineSegment() {}

		LineSegment(const Vec3<T> &_p1, const Vec3<T> &_p2) : p1(_p1), p2(_p2) {}

		LineSegment(const LineSegment<T> &s)
			{
				p1 = s.p1;
				p2 = s.p2;
			}

		template <class U>
		operator LineSegment<U>() const
			{
				return LineSegment<U>(p1, p2);
			}

		LineSegment<T>& operator=(const LineSegment<T> &s)
			{
				p1 = s.p1;
				p2 = s.p2;
				return *this;
			}

		bool contains(const Vec3<T> &p) const
			{
				Vec3<T> u = p2 - p1;
				Vec3<T> w = p - p1;
				Vec3<T> v = p2 - p;

				T l1 = u.lengthSq();
				T l2 = w.lengthSq() + v.lengthSq();

				return fabsf(l2 - l1) < SMALL_NUM;
			}

		T length() const
			{
				return (p2 - p1).length();
			}

		IntersectResult intersects(const LineSegment<T> &l, Vec3<T> &iPoint) const
			{
				Vec3<T> da = p2 - p1;
				Vec3<T> db = l.p2 - l.p1;
				Vec3<T> dc = l.p1 - p1;
				Vec3<T> x1 = da.cross(db);
				T dot = dc.dot(x1);

				if( dot != 0 )
					return IntersectResult::Miss;

				Vec3<T> x2 = dc.cross(db);
				T s = x2.dot(x1) / x1.lengthSq();

				if( s >= 0 && s <= (T)1.0 ) {
					iPoint = p1 + da * s;
					return IntersectResult::Intersect;
				}

				return IntersectResult::Miss;
			}

		IntersectResult intersects(const Plane<T> &p, Vec3<T> &iPoint) const
			{
				Vec3<T> u = p2 - p1;
				Vec3<T> w = p1 - p.point;
	
				T d = p.normal.dot(u);
				T n = -p.normal.dot(w);

				if( fabsf(d) < SMALL_NUM ) {
					if( n == 0 )
						return IntersectResult::Coincide;
					return IntersectResult::Miss;
				}

				T si = n / d;
				if( si < 0 || si > 1 ) {
					return IntersectResult::Miss;
				}

				iPoint = p1 + (u * si);

				return IntersectResult::Intersect;
			}

		Vec3<T> midPoint() const
			{
				return (p1 + p2) / (T)2.0;
			}

	private:
		Vec3<T> p1;
		Vec3<T> p2;
	};




	template <class T>
	class Line2D {
		friend class Vec2<T>;
	public:
		Line2D() {}

		Line2D(const Vec2<T> &p, const Vec2<T> &v) : point(p), vector(v)
			{
				vector = vector.unit();
			}

		Line2D(const T p[], const T v[]) : point(p), vector(v)
			{
				vector = vector.unit();
			}

		Line2D(const Line2D<T> &l)
			{
				point = l.point;
				vector = l.vector;
			}

		template <class U>
		operator Line2D<U>() const
			{
				return Line2D<U>(point, vector);
			}

		Line2D<T>& operator=(const Line2D<T> &l)
			{
				point = l.point;
				vector = l.vector;
				return *this;
			}

		bool    operator==(const Line2D<T> &l) const
			{
				// floats are not exact, this requires them to be... currently
				if( vector == l.vector || -vector == l.vector ) {
					return contains(l.point);
				}
				return false;
			}

		bool    operator!=(const Line2D<T> &l) const
			{
				return !this->operator==(l);
			}

		bool contains(const Vec2<T> &p) const
			{
				// floats are not exact, this requires them to be... currently
				Vec2<T> n = (p - point).unit();
				return vector == n || -vector == n;
			}

		IntersectResult intersects(const Line2D<T> &l, Vec2<T> &iPoint) const
			{
				T rsx = vector.cross(l.vector);
				T qprx = (l.point - point).cross(vector);

				if( rsx == (T)0.0 ) {
					if( qprx == (T)0.0 )
						return IntersectResult::Coincide;
					else
						return IntersectResult::Miss;
				}

				T qpsx = (l.point - point).cross(l.vector);
				T t = qpsx / rsx;
				Vec2<T> res = vector * t;
				iPoint = point + res;
				return IntersectResult::Intersect;
			}

	private:
		Vec2<T> point;
		Vec2<T> vector;
	};



	template <class T>
	class LineSegment2D {
		friend class Vec2<T>;
	public:
		LineSegment2D() {}

		LineSegment2D(const Vec2<T> &_p1, const Vec2<T> &_p2) : p1(_p1), p2(_p2) {}

		LineSegment2D(const LineSegment2D<T> &s)
			{
				p1 = s.p1;
				p2 = s.p2;
			}

		template <class U>
		operator LineSegment2D<U>() const
			{
				return LineSegment2D<U>(p1, p2);
			}

		LineSegment2D& operator=(const LineSegment2D &s)
			{
				p1 = s.p1;
				p2 = s.p2;
				return *this;
			}

		bool contains(const Vec2<T> &p) const
			{
				Point2f ab = p2 - p1;
				Point2f ac = p - p1;

				T k = ab.dot(ac);
				if( k < 0 )
					return false;

				T j = ab.dot(ab);
				return k <= j;
			}

		T length() const
			{
				return (p2 - p1).length();
			}

		T distanceFrom(const Vec2<T> &p) const
			{
				T n = fabs(((p2.y - p1.y) * p.x - (p2.x - p1.x) * p.y) + (p2.x * p1.y - p2.y * p1.x));
				T d = sqrt((p2.y - p1.y) * (p2.y - p1.y) + (p2.x - p1.x) * (p2.x - p1.x));
				return n / d;
			}

		IntersectResult intersects(const LineSegment2D<T> &s, Vec2<T> &iPoint) const
			{
				T d = ((p1.x - p2.x) * (s.p1.y - s.p2.y)) - ((p1.y - p2.y) * (s.p1.x - s.p2.x));

				// if d is 0, the lines are parallel, and therefore do not intersect
				if( d < SMALL_NUM && d > -SMALL_NUM )
					return IntersectResult::Miss;

				// get the point of intersection
				Vec2<T> p(
					(((p1.x * p2.y - p1.y * p2.x) * (s.p1.x - s.p2.x)) - ((p1.x - p2.x) * (s.p1.x * s.p2.y - s.p1.y * s.p2.x))) / d,
					(((p1.x * p2.y - p1.y * p2.x) * (s.p1.y - s.p2.y)) - ((p1.y - p2.y) * (s.p1.x * s.p2.y - s.p1.y * s.p2.x))) / d
				);

				// but this point could be anywhere along the line, since a line in infinite
				// we must determine if the point falls on both line segments
				if( contains(p) && s.contains(p) ) {
					iPoint = p;
					return IntersectResult::Intersect;
				}

				return IntersectResult::Miss;
			}

		IntersectResult intersects(const BoundingBox2(T) &bbox, Vec2<T> &p) const
			{
				Side s;
				return intersects(bbox, p, s);
			}

		IntersectResult intersects(const BoundingBox2(T) &bbox, Vec2<T> &p, Side &side) const
			{
				Vec2<T> tl(bbox.low.x, bbox.high.y);
				Vec2<T> br(bbox.high.x, bbox.low.y);
				LineSegment2D<T> l(bbox.low, tl), r(br, bbox.high), t(tl, bbox.high), b(bbox.low, br);

				if( intersects(l, p) == IntersectResult::Intersect ) {
					side = Side::Left;
					return IntersectResult::Intersect;
				} else if( intersects(r, p) == IntersectResult::Intersect ) {
					side = Side::Right;
					return IntersectResult::Intersect;
				} else if( intersects(t, p) == IntersectResult::Intersect ) {
					side = Side::Top;
					return IntersectResult::Intersect;
				} else if( intersects(b, p) == IntersectResult::Intersect ) {
					side = Side::Bottom;
					return IntersectResult::Intersect;
				}

				return IntersectResult::Miss;
			}

		Point2f midPoint() const
			{
				return (p1 + p2) / (T)2.0;
			}


		const Vec2<T>& P1() const { return p1; }

		const Vec2<T>& P2() const { return p2; }

	private:
		Vec2<T> p1;
		Vec2<T> p2;
	};



	template <class T>
	class Plane {
		friend class Line<T>;
		friend class LineSegment<T>;
	public:
		Plane() {}

		Plane(const Vec3<T> &p, const Vec3<T> &n) : point(p), normal(n.unit()) {}

		Plane(T A, T B, T C, T D)
			{
				set(A, B, C, D);
			}

		Plane(const Plane<T> &p)
			{
				point = p.point;
				normal = p.normal;
			}

		template <class U>
		operator Plane<U>() const
			{
				return Plane<U>(point, normal);
			}

		void set(const Vec3<T> &p, const Vec3<T> &n)
			{
				point = p;
				normal = n.unit();
			}

		void set(T A, T B, T C, T D)
			{
				normal.x = A;
				normal.y = B;
				normal.z = C;

				T l = normal.length();
				normal = normal.unit();

				T d = -D / l;
				point = normal * d;
			}

		bool contains(const Vec3<T> &p) const
			{
				return fabsf(distance(p)) < SMALL_NUM;
			}

		T distance(const Vec3<T> &p) const
			{
				return -normal.dot(point);
			}

		IntersectResult intersects(const Plane<T> &p, Line<T> &iLine) const
			{
				Vec3<T> u = normal.cross(p.normal);
				Vec3<T> a(fabsf(u.x), fabsf(u.y), fabsf(u.z));

				if( a.x + a.y + a.z <= SMALL_NUM ) {
					Vec3<T> vs = p.point - point;
					if( normal.dot(vs) == 0 ) {
						return IntersectResult::Coincide;
					}
					return IntersectResult::Miss;
				}

				int maxc;
				if( a.x > a.y ) {
					if( a.x > a.z ) {
						maxc = 1;
					} else {
						maxc = 3;
					}
				} else {
					if( a.y > a.z ) {
						maxc = 2;
					} else {
						maxc = 3;
					}
				}

				T p1A, p1B, p1C, p1D, p2A, p2B, p2C, p2D;
				getCoefficients(p1A, p1B, p1C, p1D);
				p.getCoefficients(p2A, p2B, p2C, p2D);
				switch( maxc ) {
				case 1:
					iLine.point.x = 0;
					iLine.point.y = (p1C*p2D - p2C*p1D) / (p1B*p2C - p2B*p1C);
					iLine.point.z = (p2B*p1D - p1B*p2D) / (p1B*p2C - p2B*p1C);
					break;

				case 2:
					iLine.point.x = (p1C*p2D - p2C*p1D) / (p1A*p2C - p2A*p1C);
					iLine.point.y = 0;
					iLine.point.z = (p2A*p1D - p1A*p2D) / (p1A*p2C - p2A*p1C);
					break;

				case 3:
					iLine.point.x = (p1B*p2D - p2B*p1D) / (p1A*p2B - p2A*p1B);
					iLine.point.y = (p2A*p1D - p1A*p2D) / (p1A*p2B - p2A*p1B);
					iLine.point.z = 0;
					break;
				};

				iLine.vector = u.unit();

				return IntersectResult::Intersect;
			}

		Plane transform(Matrix4<T> &m) const
			{
				Vec4<T> o(point.x, point.y, point.z, 1);
				Vec4<T> n(normal.x, normal.y, normal.z, 0);

				o = m * o;
				//n = m.inverse().transpose() * n;

				return Plane<T>(Vec3<T>(o.x, o.y, o.z), Vec3<T>(n.x, n.y, n.z));
			}

		Vec3<T> project(const Vec3<T> &p) const
			{
				T d = normal.x * -point.x + normal.y * -point.y + normal.z * -point.z;
				T dist = normal.dot(p) + d;

				if( dist < SMALL_NUM )
					return p;

				return p - normal * dist;
			}

		Vec2<T> project2D(const Vec3<T> &p) const
			{
				// project point p onto the plane where the plane's normal vector is [0,0,1] in 3D space
				Vec3<T> nn(0, 0, 1);
				Vec3<T> u = normal.cross(nn);
				if( u.lengthSq() == 0 ) {
					u.x = 1;
				}
				T angle = acos(normal.dot(nn) / (normal.length() * nn.length()));

				T c = cos(angle);
				T ci = 1 - c;
				T s = sin(angle);
				T m[9] = {
					c + (u.x*u.x)*ci,
					(u.y*u.x)*ci+(u.z*s),
					(u.z*u.x)*ci-(u.y*s),
					(u.x*u.y)*ci-(u.z*s),
					c + (u.y*u.y)*ci,
					(u.z*u.y)*ci+(u.x*s),
					(u.x*u.z)*ci+(u.y*s),
					(u.y*u.z)*ci-(u.x*s),
					c + (u.z*u.z)*ci
				};

				Vec3<T> q = project(p);

				T r[] = {
					m[0]*q.x + m[3]*q.y + m[6]*q.z,
					m[1]*q.x + m[4]*q.y + m[7]*q.z,
					m[2]*q.x + m[5]*q.y + m[8]*q.z
				};

				return Vec2<T>(r);
			}

		void getCoefficients(T &A, T &B, T &C, T &D) const
			{
				A = normal.x;
				B = normal.y;
				C = normal.z;
				D = -normal.dot(point);
			}

	private:
		Vec3<T> point;
		Vec3<T> normal;
	};



	template <class T>
	class Frustum {
	public:
		Frustum() {}

		Frustum(const Matrix4<T>& mvp)
			{
				set(mvp);
			}

		Frustum(const T mvp[])
			{
				set(mvp);
			}

		template <class U>
		Frustum(const U mvp[])
			{
				// Right clipping plane.
				mPlanes[0].set( (T)(mvp[3]-mvp[0]), (T)(mvp[7]-mvp[4]), (T)(mvp[11]-mvp[8]), (T)(mvp[15]-mvp[12]) );
				// Left clipping plane.
				mPlanes[1].set( (T)(mvp[3]+mvp[0]), (T)(mvp[7]+mvp[4]), (T)(mvp[11]+mvp[8]), (T)(mvp[15]+mvp[12]) );
				// Bottom clipping plane.
				mPlanes[2].set( (T)(mvp[3]+mvp[1]), (T)(mvp[7]+mvp[5]), (T)(mvp[11]+mvp[9]), (T)(mvp[15]+mvp[13]) );
				// Top clipping plane.
				mPlanes[3].set( (T)(mvp[3]-mvp[1]), (T)(mvp[7]-mvp[5]), (T)(mvp[11]-mvp[9]), (T)(mvp[15]-mvp[13]) );
				// Far clipping plane.
				mPlanes[4].set( (T)(mvp[3]-mvp[2]), (T)(mvp[7]-mvp[6]), (T)(mvp[11]-mvp[10]), (T)(mvp[15]-mvp[14]) );
				// Near clipping plane.
				mPlanes[5].set( (T)(mvp[3]+mvp[2]), (T)(mvp[7]+mvp[6]), (T)(mvp[11]+mvp[10]), (T)(mvp[15]+mvp[14]) );
			}

		template <class U>
		operator Frustum<U>() const
			{
				Frustum<U> f;
				f.mPlanes[0] = mPlanes[0];
				f.mPlanes[1] = mPlanes[1];
				f.mPlanes[2] = mPlanes[2];
				f.mPlanes[3] = mPlanes[3];
				f.mPlanes[4] = mPlanes[4];
				f.mPlanes[5] = mPlanes[5];
				return f;
			}

		void set(const Matrix4<T> &mvp)
			{
				// Right clipping plane.
				mPlanes[0].set( mvp[3]-mvp[0], mvp[7]-mvp[4], mvp[11]-mvp[8], mvp[15]-mvp[12] );
				// Left clipping plane.
				mPlanes[1].set( mvp[3]+mvp[0], mvp[7]+mvp[4], mvp[11]+mvp[8], mvp[15]+mvp[12] );
				// Bottom clipping plane.
				mPlanes[2].set( mvp[3]+mvp[1], mvp[7]+mvp[5], mvp[11]+mvp[9], mvp[15]+mvp[13] );
				// Top clipping plane.
				mPlanes[3].set( mvp[3]-mvp[1], mvp[7]-mvp[5], mvp[11]-mvp[9], mvp[15]-mvp[13] );
				// Far clipping plane.
				mPlanes[4].set( mvp[3]-mvp[2], mvp[7]-mvp[6], mvp[11]-mvp[10], mvp[15]-mvp[14] );
				// Near clipping plane.
				mPlanes[5].set( mvp[3]+mvp[2], mvp[7]+mvp[6], mvp[11]+mvp[10], mvp[15]+mvp[14] );
			}

		void set(const T mvp[])
			{
				// Right clipping plane.
				mPlanes[0].set( mvp[3]-mvp[0], mvp[7]-mvp[4], mvp[11]-mvp[8], mvp[15]-mvp[12] );
				// Left clipping plane.
				mPlanes[1].set( mvp[3]+mvp[0], mvp[7]+mvp[4], mvp[11]+mvp[8], mvp[15]+mvp[12] );
				// Bottom clipping plane.
				mPlanes[2].set( mvp[3]+mvp[1], mvp[7]+mvp[5], mvp[11]+mvp[9], mvp[15]+mvp[13] );
				// Top clipping plane.
				mPlanes[3].set( mvp[3]-mvp[1], mvp[7]-mvp[5], mvp[11]-mvp[9], mvp[15]-mvp[13] );
				// Far clipping plane.
				mPlanes[4].set( mvp[3]-mvp[2], mvp[7]-mvp[6], mvp[11]-mvp[10], mvp[15]-mvp[14] );
				// Near clipping plane.
				mPlanes[5].set( mvp[3]+mvp[2], mvp[7]+mvp[6], mvp[11]+mvp[10], mvp[15]+mvp[14] );
			}

		IntersectResult sphereIntersects(const Vec3<T>& center, T radius) const
			{
				int c = 0;
				for( int i = 0; i < 6; i++ ) {
					T distance = mPlanes[i].distance(center);
					if( distance <= -radius )
						return IntersectResult::Miss;
					else if( distance < radius )
						c++;
				}
				return c == 6 ? IntersectResult::Complete : IntersectResult::Partial;
			}

		IntersectResult boxIntersects(const Vec3<T>& min, const Vec3<T>& max) const
			{
				int c;
				int c2 = 0;
				for( int p = 0; p < 6; p++ ) {
					T A, B, C, D;
					mPlanes[p].getCoefficients(A, B, C, D);

					c = 0;
					if( A * min.x + B * min.y + C * min.z + D > 0 )
						c++;

					if( A * max.x + B * min.y + C * min.z + D > 0 )
						c++;

					if( A * min.x + B * max.y + C * min.z + D > 0 )
						c++;

					if( A * max.x + B * max.y + C * min.z + D > 0 )
						c++;

					if( A * min.x + B * min.y + C * max.z + D > 0 )
						c++;

					if( A * max.x + B * min.y + C * max.z + D > 0 )
						c++;

					if( A * min.x + B * max.y + C * max.z + D > 0 )
						c++;

					if( A * max.x + B * max.y + C * max.z + D > 0 )
						c++;

					if( c == 0 )
						return IntersectResult::Miss;

					if( c == 8 )
						c2++;
				}
				return c2 == 6 ? IntersectResult::Complete : IntersectResult::Partial;
			}

		template <class U>
		IntersectResult boxIntersects(U minx, U miny, U minz, U maxx, U maxy, U maxz) const
			{
				int c;
				int c2 = 0;
				for( int p = 0; p < 6; p++ ) {
					T A, B, C, D;
					mPlanes[p].getCoefficients(A, B, C, D);

					c = 0;
					if( A * minx + B * miny + C * minz + D > 0 )
						c++;

					if( A * maxx + B * miny + C * minz + D > 0 )
						c++;

					if( A * minx + B * maxy + C * minz + D > 0 )
						c++;

					if( A * maxx + B * maxy + C * minz + D > 0 )
						c++;

					if( A * minx + B * miny + C * maxz + D > 0 )
						c++;

					if( A * maxx + B * miny + C * maxz + D > 0 )
						c++;

					if( A * minx + B * maxy + C * maxz + D > 0 )
						c++;

					if( A * maxx + B * maxy + C * maxz + D > 0 )
						c++;

					if( c == 0 )
						return IntersectResult::Miss;

					if( c == 8 )
						c2++;
				}
				return c2 == 6 ? IntersectResult::Complete : IntersectResult::Partial;
			}

		IntersectResult planeIntersects(const Plane<T>& p, BoundingBox3(T)& b) const
			{
				// Calculate the 8 frustum corner points
				Vec3<T> spts[4];
				Vec3<T> epts[4];
				Line<T> l;
	
				// right plane intersecting bottom plane
				mPlanes[0].intersects(mPlanes[2], l);
				// far bottom right point
				l.intersects(mPlanes[4], spts[3]);
				// near bottom right point
				l.intersects(mPlanes[5], epts[0]);

				// right plane intersecting top plane
				mPlanes[0].intersects(mPlanes[3], l);
				// near top right point
				l.intersects(mPlanes[5], spts[2]);
				// far top right point
				l.intersects(mPlanes[4], epts[1]);

				// left plane intersecting bottom plane
				mPlanes[1].intersects(mPlanes[2], l);
				// near bottom left point
				l.intersects(mPlanes[5], spts[1]);
				// far bottom left point
				l.intersects(mPlanes[4], epts[2]);

				// left plane intersecting top plane
				mPlanes[1].intersects(mPlanes[3], l);
				// far top left point
				l.intersects(mPlanes[4], spts[0]);
				// near top left point
				l.intersects(mPlanes[5], epts[3]);

				// Using the corner points on the frustum, calculate where all 12 sides
				// intersect the given plane.  The plane can cross the frustum in either
				// 3, 4, 5, or 6 points.
				// See: http://cococubed.asu.edu/images/raybox/five_shapes_800.png
				Vec3<T> pnt[6];
				int curpnt = 0;

				for(int i = 0; i < 4; i++) {
					for(int j = 0; j < 4; j++) {
						if( i == j )
							continue;

						LineSegment<T> ls(spts[i], epts[j]);
						if( ls.intersects(p, pnt[curpnt]) == IntersectResult::Intersect ) {
							curpnt++;
						}
						if( curpnt >= 6 )
							return IntersectResult::Miss;
					}
				}

				if( curpnt < 3 )
					return IntersectResult::Miss;

	
				// Examine all points to create a bounding box
				b.low = Vec3<T>(FLT_MAX, FLT_MAX, FLT_MAX);
				b.high = Vec3<T>(-FLT_MAX, -FLT_MAX, -FLT_MAX);

				for(int i = 0; i < curpnt; i++) {
					if( pnt[i].x < b.low.x )
						b.low.x = pnt[i].x;
					if( pnt[i].x > b.high.x )
						b.high.x = pnt[i].x;

					if( pnt[i].y < b.low.y )
						b.low.y = pnt[i].y;
					if( pnt[i].y > b.high.y )
						b.high.y = pnt[i].y;

					if( pnt[i].z < b.low.z )
						b.low.z = pnt[i].z;
					if( pnt[i].z > b.high.z )
						b.high.z = pnt[i].z;
				}

				return IntersectResult::Intersect;
			}

		IntersectResult planeIntersects(const Plane<T>& p, BoundingBox2(T)& b) const
			{
				BoundingBox3(T) b3d;
				IntersectResult res = planeIntersects(p, b3d);
	
				if( res == IntersectResult::Intersect ) {
					b.low  = p.project2D(b3d.low);
					b.high = p.project2D(b3d.high);
				}

				return res;
			}

	private:
		Plane<T> mPlanes[6];
	};


	template <class T> class Circle;
	template <class T> class Rectangle;
	template <class T> class Square;

	class Shape {
	public:
		virtual bool Collides(const Shape &s) const { return false; }
		virtual bool ContainsPoint(Vec2<float> p) const { return false; }
		virtual bool ContainsPoint(Vec2<double> p) const { return false; }

		virtual bool IntersectsCircle(const Shape *s) const { return false; }
		virtual bool IntersectsRectangle(const Shape *s) const { return false; }
	};



	template <class T>
	class Circle : public Shape {
	public:
		Vec2<T> center;
		T radius;

		Circle(Vec2<T> center, T radius) : center(center), radius(radius) {}

		bool Collides(const Shape &s) const {
			return s.IntersectsCircle(this);
		}

		bool ContainsPoint(Vec2<T> p) const {
			return LineSegment2D<T>(center, p).length() <= radius;
		}

		bool IntersectsCircle(const Shape *s) const {
			const Circle<T> *c = static_cast<const Circle<T>*>(s);
			LineSegment2D<T> seg(center, c->center);
			return seg.length() <= (radius + c->radius);
		}

		bool IntersectsRectangle(const Shape *s) const {
			const Rectangle<T> *r = static_cast<const Rectangle<T>*>(s);
			return r->IntersectsCircle(this);
		}

	private:
	};



	template <class T>
	class Rectangle : public Shape {
	public:
		Vec2<T> low;
		Vec2<T> high;

		Rectangle(Vec2<T> low, Vec2<T> high) : low(low), high(high) {}
		Rectangle(T lowx, T highx, T lowy, T highy) : low(lowx, lowy), high(highx, highy) {}

		virtual bool Collides(const Shape &s) const {
			return s.IntersectsRectangle(this);
		}

		Vec2<T> Center() const {
			return Vec2<T>(
				(low.x + high.x) / 2,
				(low.y + high.y) / 2
			);
		}

		bool ContainsPoint(Vec2<T> p) const {
			return p >= low && p <= high;
		}

		virtual bool IntersectsCircle(const Shape *s) const {
			const Circle<T> *c = static_cast<const Circle<T>*>(s);

			bool intersects = ContainsPoint(c->center);
			if( !intersects ) {
				intersects |= LineSegment2D<T>(low, Vec2<T>(low.x, high.y)).distanceFrom(c->center) <= c->radius;
				intersects |= LineSegment2D<T>(Vec2<T>(low.x, high.y), high).distanceFrom(c->center) <= c->radius;
				intersects |= LineSegment2D<T>(high, Vec2<T>(high.x, low.y)).distanceFrom(c->center) <= c->radius;
				intersects |= LineSegment2D<T>(Vec2<T>(high.x, low.y), low).distanceFrom(c->center) <= c->radius;
			}

			return intersects;
		}

		virtual bool IntersectsRectangle(const Shape *s) const {
			const Rectangle<T> *r = static_cast<const Rectangle<T>*>(s);
			return low >= r->low && high <= r->high;
		}
	};



	template <class T>
	class Square : public Rectangle<T> {
		Square(Vec2<T> center, T width) : G3DMath::Rectangle<T>(
			Vec2<T>(center - width / 2, center - width / 2),
			Vec2<T>(center + width / 2, center + width / 2) ) {}
	};



	class Camera {
	public:
		Camera();

		void setOrtho(int width, int height, float view_width, float zoom, float near_, float far_);
		void setPerspective(int width, int height, float size, float near_, float far_);
		void setPerspective(float fovyInDegrees, float aspectRatio, float znear, float zfar);
		void setLookAt(float eyeX, float eyeY, float eyeZ, float lookX, float lookY, float lookZ, float upX, float upY, float upZ);
		void setLookAt(const Point3f& eye, const Point3f& look, const Vec3f& up);

		const Matrix4f& getModelMatrix() const;
		const Matrix4f& getViewMatrix() const;
		const Matrix4f& getProjectionMatrix() const;
		const Matrix4f& getModelViewMatrix();
		const Matrix4f& getMVPMatrix();

		void setViewMatrix(const Matrix4f &view);
		void setProjectionMatrix(const Matrix4f &projection);
		void setModelMatrix(const Matrix4f &model);
	
		float getFrustumWidth() const;
		float getFrustumHeight() const;

		Vec3f getEyeVector() const;
		Vec3f getLookAtVector() const;
		Vec3f getUpVector() const;
		Vec3f getForwardVector();
		Vec3f getRightVector() const;

		void setScale(float x, float y, float z);
		void setScale(const Vec3f& p);
	
		void setViewCenter(float x, float y, float z);
		void setViewCenter(const Point3f& p);
		void moveView(float x, float y, float z);
		void moveView(const Vec3f& v);
		void moveViewTo(float x, float y, float z);
		void moveViewTo(const Point3f& p);
		void moveModel(float x, float y, float z);
		void moveModel(const Vec3f& v);
		void moveModelTo(float x, float y, float z);
		void moveModelTo(const Point3f& p);

		void rotateView(float angle, float x, float y, float z);
		void rotateView(float angle, const Vec3f& v);
		void rotateViewTo(float angle, float x, float y, float z);
		void rotateViewTo(float angle, const Vec3f& v);

		void moveRight(float delta);
		void moveForward(float delta);
		void moveLookAt(float delta);
		void moveUp(float delta);

		bool isUpdated();
		void markUpdated() {mUpdated = true;}

	private:
		void initMatrices();

		Matrix4f mProjection;
		Matrix4f mView;
		Matrix4f mModel;

		Matrix4f mModelView;
		Matrix4f mModelViewProjection;
	
		float mZoom;

		bool mUpdated;
	};
};
