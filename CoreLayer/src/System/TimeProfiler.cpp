#include "System/TimeProfiler.h"

namespace System {

#ifdef _WIN32
TimeProfiler::TimeProfiler()
{
	QueryPerformanceFrequency(&frequency);
}

void TimeProfiler::Start()
{
	QueryPerformanceCounter(&t1);
}
double TimeProfiler::CurrentTime()
{
	LARGE_INTEGER freq, current;
	QueryPerformanceFrequency(&freq);
	QueryPerformanceCounter(&current);
	return (current.QuadPart * 1000.0 / freq.QuadPart);
}

double TimeProfiler::GetElapsed()
{
	QueryPerformanceCounter(&t2);
	return ((t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart);
}
#else
void TimeProfiler::Start()
{
	clock_gettime(CLOCK_MONOTONIC, &t1);
}

struct timespec TimeProfiler::diff(struct timespec start, struct timespec end)
{
	struct timespec temp;
	if ((end.tv_nsec-start.tv_nsec)<0) {
		temp.tv_sec = end.tv_sec-start.tv_sec-1;
		temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
	} else {
		temp.tv_sec = end.tv_sec-start.tv_sec;
		temp.tv_nsec = end.tv_nsec-start.tv_nsec;
	}
	return temp;
}

double TimeProfiler::CurrentTime()
{
	struct timespec current;
	clock_gettime(CLOCK_MONOTONIC, &current);
	return current.tv_sec*1000.0 + current.tv_nsec/1000000.0;
}

double TimeProfiler::GetElapsed()
{
	clock_gettime(CLOCK_MONOTONIC, &t2);
	struct timespec elapsed = diff(t1, t2);
	return elapsed.tv_sec*1000.0 + elapsed.tv_nsec/1000000.0;
}
#endif

// Common functions

int TimeProfiler::CurrentIntTime()
{
	return (int)CurrentTime();
}

int TimeProfiler::GetIntElapsed()
{
	return (int)(GetElapsed() + 0.5);
}

} /* namespace System */
