#include "System/Device.h"

#ifdef _WIN32
#include <windows.h>
#elif defined(ANDROID)
#include <android_native_app_glue.h>
#include <android/configuration.h>
extern struct android_app* app;
extern AInputEvent *_android_key_event;
#elif defined(__linux)
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "SDL.h"
#endif

#include <math.h>

namespace System {

Device *Device::mInstance = NULL;

Device::Device()
{
	mScreenSize = 0;
	mScreenWidth = 0;
	mScreenHeight = 0;
	mDPWidth = 0;
	mDPHeight = 0;
	mDPI = 0;
	mDPId = 0.0;

	mIsSoftKeyboardVisible = false;
}

Device* Device::Instance()
{
	if( !mInstance )
		mInstance = new Device;

	return mInstance;
}

void Device::SetScreenResolution(int width, int height)
{
	if( width <= 0 || height <= 0 )
		return;

	Device *d = Device::Instance();
	d->mScreenWidth = width;
	d->mScreenHeight = height;
}

int Device::ScreenWidth()
{
	Device *d = Device::Instance();
	return (int)d->mScreenWidth;
}

int Device::ScreenHeight()
{
	Device *d = Device::Instance();
	return (int)d->mScreenHeight;
}

double Device::ScreenRatio()
{
	Device *d = Device::Instance();
	return d->mScreenWidth / d->mScreenHeight;
}

int Device::GetDPWidth()
{
	Device *d = Device::Instance();
	return (int)d->mDPWidth;
}

int Device::GetDPHeight()
{
	Device *d = Device::Instance();
	return (int)d->mDPHeight;
}

void Device::SetDPResolution(int width, int height)
{
	if( width <= 0 || height <= 0 )
		return;

	Device *d = Device::Instance();
	d->mDPWidth = width;
	d->mDPHeight = height;
}

void Device::SetDPI(int dpi)
{
	if( dpi <= 0 )
		return;

	Device *d = Device::Instance();
	d->mDPI = dpi;
	d->mDPId = (double)dpi;
}

int Device::GetDPI()
{
	Device *d = Device::Instance();
	if( d->mDPI == 0 ) {
#ifdef _WIN32
		SetProcessDPIAware();
		HDC screen = GetDC(NULL);
		double hppi = GetDeviceCaps(screen, LOGPIXELSX);
		double vppi = GetDeviceCaps(screen, LOGPIXELSY);
		ReleaseDC(NULL, screen);
		d->mDPI = (int)((hppi + vppi) / 2);
#elif defined(ANDROID)
		d->mDPI = AConfiguration_getDensity(app->config);
#endif
		d->mDPId = (double)d->mDPI;

	}

	return d->mDPI;
}

double Device::GetScreenSize()
{
	Device *d = Device::Instance();
	double dpi = (double)Device::GetDPI();
	if( d->mScreenSize == 0.0 && d->mScreenWidth != 0.0 && d->mScreenHeight != 0.0 ) {
		double szh = d->mScreenHeight / dpi;
		double szw = d->mScreenWidth / dpi;
		d->mScreenSize = sqrt( (szh*szh) + (szw*szw) );
	}

	return d->mScreenSize;
}


// Android display pixel to dpi formula
// px = dp * (dpi / 160)

// Convert Pixels to DisplayPixels
double Device::PxToDP(double px)
{
	Device *d = Device::Instance();
	if( d->mDPI == 0 ) Device::GetDPI();
//#ifdef _WIN32
	return px * (d->mDPWidth / d->mScreenWidth);
//#elif defined(ANDROID)
//	return px / (d->mDPId / 160.0);
//#endif
}

// Convert DisplayPixels to Pixels
double Device::DPToPx(double dp)
{
	Device *d = Device::Instance();
	if( d->mDPI == 0 ) Device::GetDPI();
//#ifdef _WIN32
	return dp * (d->mScreenWidth / d->mDPWidth);
//#elif defined(ANDROID)
//	return dp * (d->mDPId / 160.0);
//#endif
}

bool Device::KeyboardPresent()
{
#ifdef _WIN32
	UINT nDevices;
	PRAWINPUTDEVICELIST pRawInputDeviceList;
	if( GetRawInputDeviceList(NULL, &nDevices, sizeof(RAWINPUTDEVICELIST)) != 0 ) {
		return false;
	}
	
	if( (pRawInputDeviceList = (PRAWINPUTDEVICELIST)malloc(sizeof(RAWINPUTDEVICELIST) * nDevices)) == NULL ) {
		return false;
	}
	
	if( GetRawInputDeviceList(pRawInputDeviceList, &nDevices, sizeof(RAWINPUTDEVICELIST)) == (UINT)-1 ) {
		return false;
	}

	for( UINT i = 0; i < nDevices; i++ ) {
		RID_DEVICE_INFO info;
		info.cbSize = sizeof(RID_DEVICE_INFO);
		UINT sz = sizeof(info);
		UINT res = GetRawInputDeviceInfo( pRawInputDeviceList[i].hDevice, RIDI_DEVICEINFO, &info, &sz );
		if( res < 0 ) {
			continue;
		}
		if( info.dwType == RIM_TYPEKEYBOARD ) {
			PRID_DEVICE_INFO_KEYBOARD kb = &info.keyboard;
			if( kb->dwNumberOfKeysTotal > 30 ) {
				free(pRawInputDeviceList);
				return true;
			}
		}
	}

	free(pRawInputDeviceList);
	return false;
#elif defined(ANDROID)
	int32_t kb = AConfiguration_getKeyboard(app->config);
	return kb == ACONFIGURATION_KEYBOARD_QWERTY;
#elif defined(__linux)
	FILE *fp = fopen("/proc/bus/input/devices", "r");
	if( !fp )
		return false;
	
	size_t sz = 0;
	char *ln = 0;
	do {
		ssize_t lsz = getline(&ln, &sz, fp);
		if( lsz < 0 )
			break;
		
		if( lsz < 8 )
			continue;
		
		ssize_t pos = 0;
		while( pos < lsz - 8 ) {
			if( ln[pos] == 'K' || ln[pos] == 'k' ) {
				if( !strncmp("eyboard", &(ln[pos + 1]), 7) ) {
					fclose(fp);
					free(ln);
					return true;
				}
			}
			pos++;
		}
	} while( !feof(fp) );
	fclose(fp);
	free(ln);
	return false;
#endif
}

void Device::ShowSoftKeyboard()
{
	if( KeyboardPresent() || IsSoftKeyboardVisible() ) {
		return;
	}

#ifdef _WIN32
	LPTSTR prog = TEXT("C:\\Program Files\\Common Files\\Microsoft Shared\\ink\\TabTip.exe");
	ShellExecute( 0, 0, prog, 0, 0, 0);
#elif defined(ANDROID)
	// Attaches the current thread to the JVM.
	jint lResult;
	jint lFlags = 0;

	JavaVM* lJavaVM = app->activity->vm;
	JNIEnv* lJNIEnv = app->activity->env;

	JavaVMAttachArgs lJavaVMAttachArgs;
	lJavaVMAttachArgs.version = JNI_VERSION_1_6;
	lJavaVMAttachArgs.name = "NativeThread";
	lJavaVMAttachArgs.group = NULL;

	lResult=lJavaVM->AttachCurrentThread(&lJNIEnv, &lJavaVMAttachArgs);
	if (lResult == JNI_ERR) {
		return;
	}

	// Retrieves NativeActivity.
	jobject lNativeActivity = app->activity->clazz;
	jclass ClassNativeActivity = lJNIEnv->GetObjectClass(lNativeActivity);

	// Retrieves Context.INPUT_METHOD_SERVICE.
	jclass ClassContext = lJNIEnv->FindClass("android/content/Context");
	jfieldID FieldINPUT_METHOD_SERVICE = lJNIEnv->GetStaticFieldID(ClassContext, "INPUT_METHOD_SERVICE", "Ljava/lang/String;");
	jobject INPUT_METHOD_SERVICE = lJNIEnv->GetStaticObjectField(ClassContext, FieldINPUT_METHOD_SERVICE);
	//jniCheck(INPUT_METHOD_SERVICE);

	// Runs getSystemService(Context.INPUT_METHOD_SERVICE).
	jclass ClassInputMethodManager = lJNIEnv->FindClass("android/view/inputmethod/InputMethodManager");
	jmethodID MethodGetSystemService = lJNIEnv->GetMethodID(ClassNativeActivity, "getSystemService", "(Ljava/lang/String;)Ljava/lang/Object;");
	jobject lInputMethodManager = lJNIEnv->CallObjectMethod(lNativeActivity, MethodGetSystemService, INPUT_METHOD_SERVICE);

	// Runs getWindow().getDecorView(). 
	jmethodID MethodGetWindow = lJNIEnv->GetMethodID(ClassNativeActivity, "getWindow", "()Landroid/view/Window;");
	jobject lWindow = lJNIEnv->CallObjectMethod(lNativeActivity, MethodGetWindow);
	jclass ClassWindow = lJNIEnv->FindClass("android/view/Window");
	jmethodID MethodGetDecorView = lJNIEnv->GetMethodID(ClassWindow, "getDecorView", "()Landroid/view/View;");
	jobject lDecorView = lJNIEnv->CallObjectMethod(lWindow, MethodGetDecorView);

	// Runs lInputMethodManager.showSoftInput(...). 
	jmethodID MethodShowSoftInput = lJNIEnv->GetMethodID(ClassInputMethodManager, "showSoftInput", "(Landroid/view/View;I)Z");
	jboolean lbResult = lJNIEnv->CallBooleanMethod(lInputMethodManager, MethodShowSoftInput, lDecorView, lFlags);

	// Finished with the JVM.
	lJavaVM->DetachCurrentThread();
#endif

	Device *d = Device::Instance();
	d->mIsSoftKeyboardVisible = true;
}

void Device::HideSoftKeyboard()
{
	if( KeyboardPresent() || !IsSoftKeyboardVisible() ) {
		return;
	}

#ifdef _WIN32
	HWND wKB = FindWindow(TEXT("IPTip_Main_Window"), NULL);
	if(wKB != NULL && IsWindowVisible(wKB)) {
		PostMessage(wKB, WM_SYSCOMMAND, SC_CLOSE, 0);
	}
#elif defined(ANDROID)
	// Attaches the current thread to the JVM.
	jint lResult;
	jint lFlags = 0;

	JavaVM* lJavaVM = app->activity->vm;
	JNIEnv* lJNIEnv = app->activity->env;

	JavaVMAttachArgs lJavaVMAttachArgs;
	lJavaVMAttachArgs.version = JNI_VERSION_1_6;
	lJavaVMAttachArgs.name = "NativeThread";
	lJavaVMAttachArgs.group = NULL;

	lResult=lJavaVM->AttachCurrentThread(&lJNIEnv, &lJavaVMAttachArgs);
	if (lResult == JNI_ERR) {
		return;
	}

	// Retrieves NativeActivity.
	jobject lNativeActivity = app->activity->clazz;
	jclass ClassNativeActivity = lJNIEnv->GetObjectClass(lNativeActivity);

	// Retrieves Context.INPUT_METHOD_SERVICE.
	jclass ClassContext = lJNIEnv->FindClass("android/content/Context");
	jfieldID FieldINPUT_METHOD_SERVICE = lJNIEnv->GetStaticFieldID(ClassContext, "INPUT_METHOD_SERVICE", "Ljava/lang/String;");
	jobject INPUT_METHOD_SERVICE = lJNIEnv->GetStaticObjectField(ClassContext, FieldINPUT_METHOD_SERVICE);
	//jniCheck(INPUT_METHOD_SERVICE);

	// Runs getSystemService(Context.INPUT_METHOD_SERVICE).
	jclass ClassInputMethodManager = lJNIEnv->FindClass("android/view/inputmethod/InputMethodManager");
	jmethodID MethodGetSystemService = lJNIEnv->GetMethodID(ClassNativeActivity, "getSystemService", "(Ljava/lang/String;)Ljava/lang/Object;");
	jobject lInputMethodManager = lJNIEnv->CallObjectMethod(lNativeActivity, MethodGetSystemService, INPUT_METHOD_SERVICE);

	// Runs getWindow().getDecorView(). 
	jmethodID MethodGetWindow = lJNIEnv->GetMethodID(ClassNativeActivity, "getWindow", "()Landroid/view/Window;");
	jobject lWindow = lJNIEnv->CallObjectMethod(lNativeActivity, MethodGetWindow);
	jclass ClassWindow = lJNIEnv->FindClass("android/view/Window");
	jmethodID MethodGetDecorView = lJNIEnv->GetMethodID(ClassWindow, "getDecorView", "()Landroid/view/View;");
	jobject lDecorView = lJNIEnv->CallObjectMethod(lWindow, MethodGetDecorView);

	// Runs lWindow.getViewToken()
	jclass ClassView = lJNIEnv->FindClass("android/view/View");
	jmethodID MethodGetWindowToken = lJNIEnv->GetMethodID(ClassView, "getWindowToken", "()Landroid/os/IBinder;");
	jobject lBinder = lJNIEnv->CallObjectMethod(lDecorView, MethodGetWindowToken);

	// lInputMethodManager.hideSoftInput(...).
	jmethodID MethodHideSoftInput = lJNIEnv->GetMethodID(ClassInputMethodManager, "hideSoftInputFromWindow", "(Landroid/os/IBinder;I)Z");
	jboolean lRes = lJNIEnv->CallBooleanMethod(lInputMethodManager, MethodHideSoftInput, lBinder, lFlags);

	// Finished with the JVM.
	lJavaVM->DetachCurrentThread();
#endif

	Device *d = Device::Instance();
	d->mIsSoftKeyboardVisible = false;
}

bool Device::IsSoftKeyboardVisible()
{
	Device *d = Device::Instance();
	return d->mIsSoftKeyboardVisible;
}

bool Device::KeyboardShiftDown()
{
#ifdef _WIN32
	return (GetKeyState(VK_LSHIFT) & 0x8000 || GetKeyState(VK_RSHIFT) & 0x8000) != 0;
#elif defined(ANDROID)
	return AKeyEvent_getMetaState(_android_key_event) & AMETA_SHIFT_ON;
#elif defined(__linux)
	const uint8_t *state = SDL_GetKeyboardState(NULL);
	return state[SDL_SCANCODE_LSHIFT] || state[SDL_SCANCODE_RSHIFT];
#endif
}

bool Device::KeyboardCapsOn()
{
#ifdef _WIN32
	return (GetKeyState(VK_CAPITAL) & 0x1) != 0;
#elif defined(ANDROID)
	return false;
#elif defined(__linux)
	return SDL_GetModState() & KMOD_CAPS; 
#endif
}

} /* namespace System */
