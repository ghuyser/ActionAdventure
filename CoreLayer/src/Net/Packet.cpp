#ifdef _WIN32
#undef _WINSOCKAPI_
#include <WinSock2.h>
#elif defined __linux
#include <arpa/inet.h>
#endif

#include "Net/Packet.h"

#include <stdlib.h>
#include <memory.h>

namespace Net
{

Packet::Packet(const Identifiers::UUID &uuid, uint8_t type, uint32_t timestamp, const void *data, size_t data_sz)
{
	mSize = sizeof(PacketHeader) + data_sz;
	mPayload = malloc(mSize);
	SetHeader(uuid, type, timestamp);
	SetData(data, data_sz);
}

Packet::Packet(const PacketHeader *packet)
{
	mSize = ntohs(packet->packet_sz);
	mPayload = malloc(mSize);
	memcpy(mPayload, packet, mSize);
}

Packet::Packet(const Packet &packet)
{
	mPayload = malloc(packet.mSize);
	memcpy(mPayload, packet.mPayload, packet.mSize);
	mSize = packet.mSize;
}

Packet::~Packet()
{
	if( mPayload )
		free( mPayload );
}

void Packet::SetHeader(const Identifiers::UUID &uuid, uint8_t type, uint32_t timestamp)
{
	PacketHeader *header = (PacketHeader*)mPayload;
	header->client_id = uuid.hton();
	header->type = type;
	header->timestamp = htonl(timestamp);
	header->packet_sz = htons((uint16_t)mSize);
}

void Packet::SetData(const void *data, size_t size)
{
	if( !data || sizeof(PacketHeader) + size > 0xffff )
		return;
	
	if( mSize != sizeof(PacketHeader) + size) {
		void *data_new = realloc(mPayload, sizeof(PacketHeader) + size);
		if( !data_new ) {
			return;
		}
		mPayload = data_new;
		mSize = sizeof(PacketHeader) + size;
	}

	void *payload_data = (void*)((uint8_t*)mPayload + sizeof(PacketHeader));
	memcpy(payload_data, data, size);
	((PacketHeader*)mPayload)->packet_sz = htons((uint16_t)mSize);
}

const void* Packet::Payload() const
{
	return mPayload;
}

const PacketHeader* Packet::Header() const
{
	return (PacketHeader*)mPayload;
}

const void* Packet::Data() const
{
	if( DataSize() > 0 )
		return (void*)((uint8_t*)mPayload + sizeof(PacketHeader));
	
	return nullptr;
}

size_t Packet::Size() const
{
	return mSize;
}

size_t Packet::DataSize() const
{
	return mSize - sizeof(PacketHeader);
}

}
