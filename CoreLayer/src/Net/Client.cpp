#include "Net/sockets.h"

#include "Net/Client.h"
#include "Net/Server.h"
#include "System/TimeProfiler.h"
#include "common.h"

#define TAG_NAME "Client"
#include "logging.h"

#include <string.h>
#include <functional>

#define MAX_PACKET_SZ	1024

#define mSocket ((socket_t)_socket)
#define mAddress (*(struct sockaddr_in*)_addr)

namespace Net
{

using namespace System;

Client::Client()
	: SocketListener( TAG_NAME )
	, PlayerConnected( nullptr )
	, UpdatePlayerState( nullptr )
	, mState( State::NoConnection )
	, mServerAddress( nullptr )
	, mServerPort( DEFAULT_PORT )
{
	using namespace std::placeholders;

	setProcessTimeouts( std::bind(&Client::processTimeouts, this) );
	setProcessRecvData( std::bind(&Client::processRecvData, this, _1, _2) );
	setThreadEnding( std::bind(&Client::threadEnding, this) );
}

Client::~Client()
{
	if( mState == State::Connected ) {
		Disconnect();
	}
}

bool Client::Connect()
{
	return Connect(mServerAddress, mServerPort);
}

bool Client::Connect(const char *ip)
{
	return Connect(ip, mServerPort);
}

bool Client::Connect(const char *ip, uint16_t port)
{
	if(mState != State::NoConnection && mState != State::Error)
		return false;

	char to_ip[32];
	if( !getHostName(ip, to_ip) ) {
		LOGE("Cannot resolve host: %s", ip);
		return false;
	}
	
	LOGI("Connecting to %s...", to_ip);

	if( mServerAddress )
		free( mServerAddress );
	
	const char *default_addr = "127.0.0.1";
	const char *i_ip = to_ip ? to_ip : default_addr;
	mServerAddress = (char*)malloc(strlen(i_ip) + 1);
	memcpy(mServerAddress, i_ip, strlen(i_ip) + 1);
	mServerPort = port >= 1024 ? port : DEFAULT_PORT;

	if( !createSocket(mServerAddress, mServerPort) ) {
		mState = State::Error;
		return false;
	}

	mClientID.clear();

	bool result = sendPacket(Packet(mClientID, (uint8_t)ClientMessage::Join));
	if(result) {
		LOGE("Cannot connect to server.");
		return false;
	}

	mState = State::PendingConnection;

	start();

	return true;
}

void Client::Disconnect()
{
	LOGI("Disconnecting from server...");

	bool result = sendPacket( Packet(mClientID, (uint8_t)ClientMessage::Leave) );
	if( result ) {
		LOGE("Server communication error.");
	}

	mState = State::Disconnecting;
}

const char* Client::GetServerAddress()
{
	return mServerAddress;
}

uint16_t Client::GetServerPort()
{
	return mServerPort;
}

Client::State Client::CurrentState() const
{
	return mState;
}

int Client::SendState(const IState *state)
{
	const int currentTime = TimeProfiler::CurrentIntTime();

	Packet p(mClientID, (uint8_t)ClientMessage::State, currentTime, state->Data(), state->DataLength());
	bool result = sendPacket( p );

	if( result ) {
		if( mLastContact + TIMEOUT_MS < currentTime ) {
			LOGE("No contact with server, stopping.");
			mState = State::NoConnection;
			stop();
			return -1;
		}
		LOGE("Server communication error.");
		return 0;
	}

	return p.Size();
}

void Client::processTimeouts()
{
	if( mLastContact + TIMEOUT_MS < TimeProfiler::CurrentIntTime() ) {
		LOGE("No contact with server, stopping.");
		mState = State::Disconnecting;
		mRunning = false;
	}
}

void Client::processRecvData(const void*, const void*)
{
	Packet recv((PacketHeader*)(mRecvBuff));
	const PacketHeader *header = recv.Header();

	const int currentTime = TimeProfiler::CurrentIntTime();

	ServerMessage sMsg = static_cast<ServerMessage>(header->type);
	switch(sMsg) {
		case ServerMessage::JoinResult:
	{
		mClientID = header->client_id.hton();
		ConnectionResponse *response = (ConnectionResponse*)recv.Data();
		if( response->status != (uint8_t)ConnectionStatus::Connected ) {
			if( mState == State::PendingConnection ) {
				LOGE("Login failed.");
			}
			mState = State::Disconnecting;
			mRunning = false;
			break;
		}
		mState = State::Connected;
		LOGI("Logged in with user id: %s", mClientID.getString().c_str());

		if( PlayerConnected ) {
			uint16_t num = htons(response->players_connected);
			PlayerInfo *pi = (PlayerInfo*)((uint8_t*)response + sizeof(ConnectionResponse));
			for( uint16_t i = 0; i < num; i++ ) {
				Identifiers::UUID id = pi->player_id.hton();
				if( mClientID != id )
					PlayerConnected(true, id, &pi->state);
				pi++;
			}
		}
	}
	break;

	case ServerMessage::Disconnect:
	{
		Packet p(mClientID, (uint8_t)ClientMessage::AckDisconnect, currentTime);
		sendPacket( p );

		mRunning = false;
		mState = State::Disconnecting;
	}
	break;

	case ServerMessage::NoClient:
	{
		mState = State::Disconnecting;
		mRunning = false;
	}
	break;

	case ServerMessage::PlayerConnected:
	{
		PlayerConnection *player = (PlayerConnection*)recv.Data();
		LOGI("Player joined: %s", player->player_id.hton().getString().c_str());

		if( PlayerConnected ) {
			PlayerConnected(true, player->player_id.hton(), nullptr);
		}
	}
	break;

	case ServerMessage::PlayerDisconnected:
	{
		PlayerConnection *player = (PlayerConnection*)recv.Data();
		LOGI("Player left: %s", player->player_id.hton().getString().c_str());

		if( PlayerConnected ) {
			PlayerConnected(false, player->player_id.hton(), nullptr);
		}
	}
	break;

	case ServerMessage::State:
	{
		if( UpdatePlayerState ) {
			uint8_t *state_data = (uint8_t*)recv.Data();
			uint16_t num_sates = htons( *((uint16_t*)state_data) );
			state_data += sizeof(uint16_t);

			for( uint16_t i = 0; i < num_sates; i++, state_data += sizeof(PlayerInfo) ) {
				PlayerInfo *info = (PlayerInfo*)state_data;
				Identifiers::UUID client_id = info->player_id.hton();

				if( client_id != mClientID ) {
					UpdatePlayerState(client_id, &info->state);
				}
			}
		}
	}
	break;
	}
}

void Client::threadEnding()
{
	if(mState == State::Disconnecting) {
		if(PlayerConnected) {
			PlayerConnected(false, Identifiers::UUID::Null(), nullptr);
		}

		mState = State::NoConnection;
		mClientID.clear();
		LOGI("Disconnected.");
	}
}

bool Client::sendPacket(Packet p)
{
	const bool result = sendTo(_addr, p.Payload(), p.Size());
	return result;
}

}
