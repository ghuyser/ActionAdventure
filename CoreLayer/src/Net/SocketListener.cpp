#include "Net/sockets.h"
#ifdef _WIN32
#include <windns.h>
#endif

#include "Net/SocketListener.h"
#include "System/TimeProfiler.h"

#define TAG_NAME mTagName
#include "logging.h"

#include <cstring>
#include <chrono>
#include <vector>

#define mSocket ((socket_t)_socket)
#define mAddress (*(struct sockaddr_in*)_addr)

#ifdef _WIN32
WSADATA __wsa_data;
int __wsa_start = -1;
int __wsa_count = 0;
#endif

namespace Net
{

using namespace System;

SocketListener::SocketListener(const char *tag_name)
	: mRunning( false )
	, mThread( nullptr )
	, mThreadSleep( DefaultThreadSleepMs )
	, mPort( 0 )
	, mProcessRecvData( nullptr )
	, mProcessTimeouts( nullptr )
	, mThreadEnding( nullptr )
	, mBytesReceived( 0 )
	, mBytesSent( 0 )
	, mBytesLastUpdate( 0 )
	, mTagName( tag_name == nullptr ? "" : tag_name )
{
	mRateOffset = 0;
	memset(mRecvRate, 0, sizeof(mRecvRate));
	memset(mSendRate, 0, sizeof(mSendRate));

	// Start windows socket error reporting
	int res = winsock_startup();
	if(res != 0) {
		LOGE("Error WSAStartup: %d", res);
	}
}

SocketListener::SocketListener(const char *ip, const uint16_t port, const char *tag_name)
	: mRunning( false )
	, mThread( nullptr )
	, mThreadSleep( DefaultThreadSleepMs )
	, mProcessRecvData( nullptr )
	, mProcessTimeouts( nullptr )
	, mThreadEnding( nullptr )
	, mBytesReceived( 0 )
	, mBytesSent( 0 )
	, mBytesLastUpdate( 0 )
	, mTagName( tag_name == nullptr ? "" : tag_name )
{
	mRateOffset = 0;
	memset(mRecvRate, 0, sizeof(mRecvRate));
	memset(mSendRate, 0, sizeof(mSendRate));

	// Start windows socket error reporting
	int res = winsock_startup();
	if(res != 0) {
		LOGE("Error WSAStartup: %d", res);
	}

	createSocket(ip, port);
}

SocketListener::~SocketListener()
{
	if( mRunning ) {
		stop();
	}

	winsock_cleanup();
}

float* SocketListener::GetReceiveRates()
{
	static float res[RateNumDivisions - 1];
	for(size_t i = 0; i < RateNumDivisions - 1; i++) {
		res[i] = (float)mRecvRate[(mRateOffset + i + 1) % RateNumDivisions];
	}
	return res;
}

float* SocketListener::GetSendRates()
{
	static float res[RateNumDivisions - 1];
	for(size_t i = 0; i < RateNumDivisions - 1; i++) {
		res[i] = (float)mSendRate[(mRateOffset + i + 1) % RateNumDivisions];
	}
	return res;
}

const char* SocketListener::getCStringIP(const void *src, int address_family)
{
	static char addr[INET6_ADDRSTRLEN];
	if( address_family == 0 ) {
		address_family = AF_INET;
	}
	return inet_ntop(address_family, src, addr, INET6_ADDRSTRLEN);
}

bool SocketListener::createSocket(const char *ip, const uint16_t port)
{
	// User ports are 1024 and above
	if( port < 1024 ) {
		return false;
	}

	// Create the socket for incoming TCP connections
	_socket = reinterpret_cast<void*>(socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP));
	if(is_socket_invalid(mSocket)) {
		LOGE("socket() failed - %d", socket_error);
		return false;
	}

	// Create the local address structure
	_addr = malloc(sizeof(struct sockaddr_in));
	memset(_addr, 0, sizeof(struct sockaddr_in));
	mAddress.sin_family = AF_INET;
	mAddress.sin_port = htons(port);
	mPort = port;

	// If an IP is provided, set up as client.
	// Otherwise set up as server.
	if( ip != nullptr ) {
		inet_pton(AF_INET, ip, &mAddress.sin_addr);
		set_blocking(mSocket);
	} else {
		mAddress.sin_addr.s_addr = htonl(INADDR_ANY);

		// Bind the socket to the local address
		if(is_socket_error(bind(mSocket, (struct sockaddr *)_addr, sizeof(struct sockaddr)))) {
			LOGE("bind() failed - %d", socket_error);
			return false;
		}

		set_nonblocking(mSocket);
	}

	mRateOffset = 0;
	memset(mRecvRate, 0, sizeof(mRecvRate));
	memset(mSendRate, 0, sizeof(mSendRate));
	mBytesLastUpdate = TimeProfiler::CurrentIntTime();

	return true;
}

bool SocketListener::start()
{
	set_nonblocking(mSocket);

	if( !mRunning ) {
		mRunning = true;
		if( mThread ) {
			delete mThread;
		}
		mThread = new std::thread(&SocketListener::listenThread, this);
	}

	return mRunning;
}

void SocketListener::stop()
{
	if( mRunning ) {
		mRunning = false;
		mThread->join();
		delete mThread;
		mThread = nullptr;
	}
}

void SocketListener::listenThread()
{
	struct sockaddr_in from;
	socklen_t from_size = sizeof(from);

	fd_set read_fds;
	struct timeval tv;

	while( mRunning ) {
		FD_ZERO(&read_fds);
		FD_SET(mSocket, &read_fds);
		tv.tv_sec = 0;
		tv.tv_usec = DefaultThreadSleepMs * 1000;
		int result = select(mSocket + 1, &read_fds, NULL, NULL, &tv);
		updateRecvRate( 0 );
		if( mProcessTimeouts ) mProcessTimeouts();
		if( is_socket_error(result) || result == 0 )
		{
			continue;
		}

		int bytes_received = recvfrom(mSocket, (char*)mRecvBuff, ReceiveBufferSize, 0, (struct sockaddr*)&from, &from_size);

		if(is_socket_error(bytes_received))
		{
			continue;
		}

		updateRecvRate( bytes_received );

		if( mProcessRecvData ) mProcessRecvData(&from, &from_size);
	}

	if( mThreadEnding ) mThreadEnding();
}

void SocketListener::setProcessTimeouts(std::function<void()> func)
{
	mProcessTimeouts = func;
}

void SocketListener::setProcessRecvData(std::function<void(const void*, const void*)> func)
{
	mProcessRecvData = func;
}

void SocketListener::setThreadEnding(std::function<void()> func)
{
	mThreadEnding = func;
}

bool SocketListener::sendTo(const uint32_t addr, const uint16_t port, const void *data, const size_t size)
{
	const int flags = 0;
	struct sockaddr_in tosock;
	tosock.sin_family = AF_INET;
	sock_addr_int(tosock) = addr;
	tosock.sin_port = port;
	socklen_t tosock_len = sizeof(tosock);

	const bool result = is_socket_error(
		sendto(mSocket, (const char*)data, size, flags, (struct sockaddr*)&tosock, tosock_len)
	);

	if( !result ) {
		mLastContact = TimeProfiler::CurrentIntTime();
	}

	updateSendRate( size );

	return result;
}

bool SocketListener::sendTo(const void *addr, const void *data, const size_t size)
{
	int flags = 0;
	struct sockaddr *to = (struct sockaddr*)addr;
	socklen_t to_length = sizeof(struct sockaddr_in);

	const bool result = is_socket_error(
		sendto(mSocket, (const char*)data, size, flags, to, to_length)
	);

	if(!result) {
		mLastContact = TimeProfiler::CurrentIntTime();
	}

	updateSendRate( size );

	return result;
}

void SocketListener::updateRecvRate(uint32_t num_bytes)
{
	uint32_t ctime = TimeProfiler::CurrentIntTime();
	mBytesReceived += num_bytes;
	if( mBytesLastUpdate + RateResolutionMs < ctime ) {
		mBytesLastUpdate = ctime;

		if( ++mRateOffset == RateNumDivisions )
			mRateOffset = 0;
		
		mRecvRate[mRateOffset] = 0;
		mSendRate[mRateOffset] = 0;
	}

	mRecvRate[mRateOffset] += num_bytes;
}

void SocketListener::updateSendRate(uint32_t num_bytes)
{
	mBytesSent += num_bytes;
	mSendRate[mRateOffset] += num_bytes;
}

bool SocketListener::getHostName(const char *hostname, char *ip) const
{
#ifdef __linux
	int sockfd;
	struct addrinfo hints, *servinfo, *p;
	struct sockaddr_in *h;
	int rv;

	memset( &hints, 0, sizeof(hints) );
	hints.ai_family = AF_UNSPEC; // use AF_INET6 to force IPv6
	hints.ai_socktype = SOCK_STREAM;

	if( (rv = getaddrinfo( hostname, "http", &hints, &servinfo)) != 0 ) {
		LOGE("getaddrinfo error: %s", gai_strerror(rv));
		return false;
	}

	// loop through all the results and connect to the first we can
	for( p = servinfo; p != NULL; p = p->ai_next ) {
		h = (struct sockaddr_in *) p->ai_addr;
		strcpy( ip , inet_ntoa( h->sin_addr ) );
		break; // TODO: dont assume first is valid
	}

	freeaddrinfo( servinfo );
#else
	DNS_STATUS status;
	PDNS_RECORD pDnsRecord;
	IN_ADDR ipaddr;

	status = DnsQuery_A(hostname, DNS_TYPE_A, DNS_QUERY_BYPASS_CACHE, NULL, &pDnsRecord, NULL);

	if( status ) {
		LOGE("DnsQuery_A failed: %d", status);
		return false;
	}

	ipaddr.S_un.S_addr = (pDnsRecord->Data.A.IpAddress);
	strcpy( ip, getCStringIP(&ipaddr.S_un.S_addr));

	DnsRecordListFree(pDnsRecord, DnsFreeRecordListDeep);
#endif

	return true;
}

}
