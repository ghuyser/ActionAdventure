#include "Net/sockets.h"
#include "Net/Server.h"
#include "common.h"
#include "System/TimeProfiler.h"

#define TAG_NAME "Server"
#include "logging.h"

#include <cstring>
#include <chrono>
#include <functional>
#include <vector>

#define mSocket ((socket_t)_socket)
#define mAddress (*(struct sockaddr_in*)_addr)

namespace Net
{

using Identifiers::UUID;
using namespace System;

constexpr auto StateUpdateRateMs = 10;

Server::Server()
	: SocketListener( TAG_NAME )
	, mLastStateUpdate( 0 )
{
	using namespace std::placeholders;

	setProcessRecvData( std::bind(&Server::processRecvData, this, _1, _2) );
	setProcessTimeouts( std::bind(&Server::processTimeouts, this) );
	setThreadEnding( std::bind(&Server::threadEnding, this) );
}

Server::~Server()
{
	Stop();
	winsock_cleanup();
}

bool Server::Start()
{
	bool result = createSocket(nullptr, DEFAULT_PORT);
	if( result ) {
		result = start();
		if( result ) {
			LOGI("Started server on port: %d", mPort);
		}
	}

	return result;
}

void Server::Stop()
{
	LOGI("Shutting down server.");

	stop();

	LOGI("Server has shut down.");
}

bool Server::IsRunning() const
{
	return mRunning;
}

std::vector<Identifiers::UUID> Server::GetClients() const
{
	std::vector<Identifiers::UUID> list;

	for( auto i = mClients.begin(); i != mClients.end(); i++ )
		list.push_back( i->first );

	return list;
}

void Server::processTimeouts()
{
	checkForTimeout();

	if( mLastStateUpdate + StateUpdateRateMs < TimeProfiler::CurrentIntTime() ) {
		updateClientStates();
		mLastStateUpdate = TimeProfiler::CurrentIntTime();
	}
}

void Server::processRecvData(const void *from_ptr, const void *from_size_ptr)
{
	struct sockaddr_in from = *((struct sockaddr_in*)from_ptr);
	socklen_t from_size = *((socklen_t*)from_size_ptr);

	const int currentTime = TimeProfiler::CurrentIntTime();

	IP_Endpoint from_endpoint = {
		sock_addr_int(from),
		from.sin_port,
		currentTime,
		false
	};

	PacketHeader *header = (PacketHeader*)(mRecvBuff);
	UUID client_id = header->client_id.hton();
	ClientMessage cMsg = static_cast<ClientMessage>(header->type);

	if( mClients.count(client_id) == 0 && cMsg != ClientMessage::Join )
	{
		LOGE("Client does not exist.");
		Packet p(UUID::Null(), (uint8_t)ServerMessage::NoClient, currentTime);
		sendPacket( &from_endpoint, p );
		return;
	}

	switch(cMsg) {
	case ClientMessage::Join:
	{
		if(!addClient(&from_endpoint, &client_id)) {
			LOGE("Client uuid generation error.");
			break;
		}

		LOGI("Client joined: %s (%s)", client_id.getString().c_str(), getCStringIP(&from.sin_addr));

		uint16_t num_players = (uint16_t)mClients.size();
		ConnectionResponse cr{(uint8_t)ConnectionStatus::Connected, htons(num_players)};
		uint8_t *state_data = (uint8_t*)malloc(sizeof(PlayerInfo) * num_players);
		auto it = mClientState.begin();
		for( size_t i = 0; it != mClientState.end(); i++, it++ ) {
			PlayerInfo *info = (PlayerInfo*)(state_data + (sizeof(PlayerInfo) * i));
			info->player_id = it->first.hton();
			memcpy(&info->state, &it->second, sizeof(PlayerState));
		}
		uint8_t *response = (uint8_t*)appendData(&cr, sizeof(ConnectionResponse), state_data, sizeof(PlayerInfo) * num_players);
		std::shared_ptr<uint8_t> response_holder(response);
		free( state_data );
		Packet p(client_id, (uint8_t)ServerMessage::JoinResult, currentTime, response, sizeof(ConnectionResponse) + sizeof(PlayerInfo) * num_players);
		bool result = sendPacket(&from_endpoint, p);
		if(!result) {
			LOGE("Send error to client %d", client_id);
			break;
		}

		PlayerConnection player{client_id.hton()};
		p.SetHeader(UUID::Null(), (uint8_t)ServerMessage::PlayerConnected, currentTime);
		p.SetData(&player, sizeof(PlayerConnection));
		result = sendAllExcept(&from_endpoint, p);
		if(!result) {
			LOGW("Data was not sent to all clients");
			break;
		}
	}
	break;

	case ClientMessage::Leave:
	{
		if(!removeClient(client_id)) {
			LOGE("Client does not exist.");
			break;
		}

		LOGI("Client left: %s (%s)", client_id.getString().c_str(), getCStringIP(&from.sin_addr));

		ConnectionResponse cr{(uint8_t)ConnectionStatus::LogoutSuccess};
		Packet p(client_id, (uint8_t)ServerMessage::JoinResult, currentTime, &cr, sizeof(ConnectionResponse));
		sendPacket(&from_endpoint, p);

		PlayerConnection player{client_id.hton()};
		p.SetHeader(UUID::Null(), (uint8_t)ServerMessage::PlayerDisconnected, currentTime);
		p.SetData(&player, sizeof(PlayerConnection));
		bool result = sendAllExcept(&from_endpoint, p);
		if(!result) {
			LOGW("Data was not sent to all clients");
			break;
		}

		LOGI("Removed client.");
	}
	break;

	case ClientMessage::State:
	{
		PlayerState *state = (PlayerState*)(mRecvBuff + sizeof(PacketHeader));
		mClients[client_id].last_update = currentTime;
		mClientState[client_id] = *state;
	}
	break;

	case ClientMessage::AckDisconnect:
	{
		removeClient(client_id);

		LOGI("Client left: %s (%s)", client_id.getString().c_str(), getCStringIP(&from.sin_addr));
	}
	break;
	}
}

void Server::threadEnding()
{
	// Send disconnect messages to any connected client
	Packet p(UUID::Null(), (uint8_t)ServerMessage::Disconnect, TimeProfiler::CurrentIntTime());
	sendToAll(p);

	// Wait until all clients have acknowledged the disconnect, or the timeout expires
	struct sockaddr_in from;
	socklen_t from_size = sizeof(from);
	while(checkForTimeout()) {
		std::this_thread::sleep_for(std::chrono::milliseconds(100));

		int bytes_received = recvfrom(mSocket, (char*)mRecvBuff, ReceiveBufferSize, 0, (struct sockaddr*)&from, &from_size);

		if(is_socket_error(bytes_received))
		{
			continue;
		}

		processRecvData(&from, &from_size);

		LOGI("Clients remaining: %d", mClients.size());
	}
}

bool Server::checkForTimeout()
{
	std::vector<const UUID*> toRemove;

	for( auto i : mClients ) {
		if( i.second.last_update + TIMEOUT_MS < TimeProfiler::CurrentIntTime() ) {
			LOGI("Client timed out: %s (%s)", i.first.getString().c_str(), getCStringIP(&i.second.address));
			toRemove.push_back( &i.first );
		}
	}

	for( auto i : toRemove ) {
		removeClient( *i );
	}

	return mClients.size() > 0;
}

void Server::updateClientStates()
{
	size_t num_clients = mClients.size();

	if( num_clients ) {
		uint32_t state_data_len = sizeof(uint16_t) + sizeof(PlayerInfo) * num_clients;
		uint8_t *state_data = (uint8_t*)malloc( state_data_len );
		*((uint16_t*)state_data) = htons((uint16_t)num_clients);
		auto it = mClientState.begin();
		for(size_t i = 0; it != mClientState.end(); i++, it++) {
			PlayerInfo *info = (PlayerInfo*)(state_data + sizeof(uint16_t) + (sizeof(PlayerInfo) * i));
			info->player_id = it->first.hton();
			memcpy(&info->state, &it->second, sizeof(PlayerState));
		}
		Packet p(UUID::Null(), (uint8_t)ServerMessage::State, TimeProfiler::CurrentIntTime(), state_data, state_data_len);
		sendToAll( p );
		free( state_data );
	}
}

bool Server::addClient(IP_Endpoint *new_client, UUID *new_uuid)
{
	UUID client_id;
	client_id.generate();

	if( mClients.count(client_id) ) {
		return false;
	}

	*new_uuid = client_id;
	mClients.insert(std::make_pair(client_id, *new_client));
	mClientState.insert(std::make_pair(client_id, PlayerState{}));

	return true;
}

bool Server::removeClient(const UUID &client_id)
{
	if( !mClients.count(client_id) ) {
		return false;
	}

	mClients.erase(client_id);
	mClientState.erase(client_id);

	return true;
}

void* Server::appendData(const void *first, const size_t first_sz, const void *second, const size_t second_sz)
{
	void *result = malloc(first_sz + second_sz);
	memcpy(result, first, first_sz);
	memcpy((void*)((uint8_t*)result + first_sz), second, second_sz);

	return result;
}

bool Server::sendPacket(const IP_Endpoint *to, const Packet &p)
{
	const bool result = sendTo(to->address, to->port, p.Payload(), p.Size());
	return !result;
}

bool Server::sendAllExcept(const IP_Endpoint *except, const Packet &p)
{
	bool result = true;

	for(auto i : mClients) {
		if( i.second.address == except->address && i.second.port == except->port )
			continue;

		const bool err = sendTo(i.second.address, i.second.port, p.Payload(), p.Size());
		if(err) {
			LOGI("Client cannot be reached: %s (%s)", i.first.getString().c_str(), getCStringIP(&i.second.address));
		}

		result &= !err;
	}

	return result;
}

bool Server::sendToAll(const Packet &p)
{
	bool result = true;

	for( auto i : mClients ) {
		const bool err = sendTo(i.second.address, i.second.port, p.Payload(), p.Size());
		if( err ) {
			LOGI("Client cannot be reached: %s (%s)", i.first.getString().c_str(), getCStringIP(&i.second.address));
		}

		result &= !err;
	}

	return result;
}

}
