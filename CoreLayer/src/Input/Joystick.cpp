#include "Input/Joystick.h"
#include "common.h"
#include "logging.h"

namespace Input {

#ifdef _WIN32
struct JoystickDevices {
	LPDIRECTINPUT8 DirectInput = NULL;
	GUID device_guid[JOYSTICK_MAX_DEVICES];
	TCHAR *device_name[JOYSTICK_MAX_DEVICES];
	uint8_t num_devices = 0;
	uint8_t ref_count = 0;
};
JoystickDevices devices;

Joystick::Joystick()
{
	memset(buttons, 0, sizeof(buttons));

	if( devices.ref_count == 0 ) {
		devices.num_devices = 0;
		HRESULT hr;
		if( FAILED(hr = DirectInput8Create(GetModuleHandle(NULL), DIRECTINPUT_VERSION, IID_IDirectInput8, (VOID**)&devices.DirectInput, NULL)) ) {
			throw hr;
		}

		if( FAILED(hr = devices.DirectInput->EnumDevices(DI8DEVCLASS_GAMECTRL, Joystick::EnumDevicesCallback, NULL, DIEDFL_ATTACHEDONLY)) ) {
			throw hr;
		}
	}

	devices.ref_count++;
}

Joystick::~Joystick()
{
	Unaquire();

	if( --devices.ref_count == 0 ) {
		devices.DirectInput->Release();

		for( int i = 0; i < devices.num_devices; i++ ) {
			free(devices.device_name[i]);
		}
	}
}

int Joystick::NumDevices() const
{
	return devices.num_devices;
}

const TCHAR * const Joystick::DeviceName(size_t i) const
{
	if( i >= devices.num_devices )
		return NULL;

	return devices.device_name[i];
}

int Joystick::Select(size_t i)
{
	if( i >= devices.num_devices )
		return JOYSTICK_ERROR;

	if( joystick ) {
		Unaquire();
	}

	HRESULT hr = devices.DirectInput->CreateDevice(devices.device_guid[i], &joystick, NULL);

	if( FAILED(hr) ) {
		return JOYSTICK_ERROR;
	}

	if( !joystick )
		return JOYSTICK_ERROR;

	capabilities.dwSize = sizeof(DIDEVCAPS);
	if( FAILED(hr = joystick->GetCapabilities(&capabilities)) ) {
		return JOYSTICK_ERROR;
	}

	num_buttons = (uint8_t)capabilities.dwButtons;

	LOGI("Joystick Axes: %d", capabilities.dwAxes);
	LOGI("Joystick Buttons: %d", num_buttons);
	LOGI("Joystick POVs: %d", capabilities.dwPOVs);
	LOGI("Joystick Size: %d", capabilities.dwSize);

	if( FAILED(hr = joystick->SetDataFormat(&c_dfDIJoystick)) ) {
		return JOYSTICK_ERROR;
	}

	return JOYSTICK_OK;
}

int Joystick::NumButtons() const
{
	return num_buttons;
}

int Joystick::operator[](size_t b)
{
	if( b >= JOYSTICK_MAX_BUTTONS + DPAD_NUM ) return 0;
	return buttons[b];
}

const int Joystick::operator[](size_t b) const
{
	if( b >= JOYSTICK_MAX_BUTTONS + DPAD_NUM ) return 0;
	return buttons[b];
}

int Joystick::Aquire()
{
	HRESULT hr;
	if( joystick && FAILED(hr = joystick->Acquire()) ) {
		return JOYSTICK_ERROR;
	}

	return JOYSTICK_OK;
}

int Joystick::Unaquire()
{
	if( joystick )
		joystick->Unacquire();

	return JOYSTICK_OK;
}

int32_t Joystick::Axis(size_t a)
{
	if( a < JOYSTICK_MAX_AXIS ) {
		return axis[a];
	}

	return 0;
}

int Joystick::Poll()
{
	HRESULT hr;
	DIJOYSTATE js;

	if( joystick != NULL ) {
		hr = joystick->Poll();
		if( FAILED(hr) ) {
			if( hr == DIERR_NOTINITIALIZED )
				LOGI("Not initialized.");
			else if( hr == DIERR_NOTACQUIRED )
				LOGI("Not aquired.");
			else if( hr == DIERR_INPUTLOST )
				LOGI("Input lost.");

			hr = joystick->Acquire();
			while( hr == DIERR_INPUTLOST ) {
				hr = joystick->Acquire();
			}

			// If we encounter a fatal error, return failure.
			if( (hr == DIERR_INVALIDPARAM) || (hr == DIERR_NOTINITIALIZED) ) {
				return JOYSTICK_ERROR;
			}

			// If another application has control of this device, return successfully.
			// We'll just have to wait our turn to use the joystick.
			if( hr == DIERR_OTHERAPPHASPRIO ) {
				return JOYSTICK_OK;
			}
		}

		if( FAILED(hr = joystick->GetDeviceState(sizeof(DIJOYSTATE), &js)) ) {
			return JOYSTICK_ERROR; // The device should have been acquired during the Poll()
		} else {
			axis[AXIS_LEFT_STICK_X] = js.lX;
			axis[AXIS_LEFT_STICK_Y] = js.lY;
			axis[AXIS_RIGHT_STICK_X] = js.lRx;
			axis[AXIS_RIGHT_STICK_Y] = js.lRy;
			axis[AXIS_TRIGGERS] = js.lZ;

			memcpy(buttons, js.rgbButtons, sizeof(BYTE) * JOYSTICK_MAX_BUTTONS);
			memset(buttons + JOYSTICK_MAX_BUTTONS, 0, DPAD_NUM);
			if( js.rgdwPOV[0] == 0 ) {
				buttons[DPAD_UP] = 1;
			} else if( js.rgdwPOV[0] == 9000 ) {
				buttons[DPAD_RIGHT] = 1;
			} else if( js.rgdwPOV[0] == 18000 ) {
				buttons[DPAD_DOWN] = 1;
			} else if( js.rgdwPOV[0] == 27000 ) {
				buttons[DPAD_LEFT] = 1;
			}

			return JOYSTICK_OK;
		}
	}

	return JOYSTICK_ERROR;
}

BOOL CALLBACK Joystick::EnumDevicesCallback(const DIDEVICEINSTANCE* instance, VOID* context)
{
	size_t i = devices.num_devices;
	size_t namelen = strnlen_s(instance->tszInstanceName, MAX_PATH);
	devices.device_guid[i] = instance->guidInstance;
	devices.device_name[i] = (TCHAR*)malloc(sizeof(TCHAR) * namelen + 1);
	memcpy(devices.device_name[i], instance->tszInstanceName, namelen + 1);

	devices.num_devices++;

	if(devices.num_devices >= JOYSTICK_MAX_DEVICES )
		return DIENUM_STOP;

	return DIENUM_CONTINUE;
}
#elif defined(__linux)

struct JoystickDevices {
	SDL_JoystickGUID device_guid[JOYSTICK_MAX_DEVICES];
	char * device_name[JOYSTICK_MAX_DEVICES];
	uint8_t num_devices = 0;
	uint8_t ref_count = 0;
};
JoystickDevices devices;

Joystick::Joystick()
{
	controller = NULL;
	joystick = NULL;

	if( devices.ref_count == 0 ) {
		devices.num_devices = SDL_NumJoysticks();

		memset(buttons, 0, sizeof(buttons));
		memset(devices.device_guid, 0, sizeof(SDL_JoystickGUID) * JOYSTICK_MAX_DEVICES);
		memset(devices.device_name, 0, sizeof(const char*) * JOYSTICK_MAX_DEVICES);

		for(int i = 0; i < devices.num_devices; i++) {
			if( SDL_IsGameController(i) ) {
				SDL_GameController *c = SDL_GameControllerOpen(i);
				if( c != NULL ) {
					SDL_Joystick *joystick = SDL_GameControllerGetJoystick(c);
					if( SDL_JoystickInstanceID(joystick) != -1 ) {
						const char *instance_name = SDL_JoystickName(joystick);
						size_t namelen = strnlen(instance_name, 64);
						devices.device_guid[i] = SDL_JoystickGetGUID(joystick);
						devices.device_name[i] = (char*)malloc(sizeof(char*) * namelen + 1);
						memcpy(devices.device_name[i], instance_name, namelen + 1);
					}
				}
				SDL_GameControllerClose(c);
			}
		}
	}

	devices.ref_count++;
}

Joystick::~Joystick()
{
	Unaquire();

	if( --devices.ref_count == 0 ) {
		for( int i = 0; i < devices.num_devices; i++ ) {
			free(devices.device_name[i]);
		}
	}
}

int Joystick::NumDevices() const
{
	return devices.num_devices;
}

const char * const Joystick::DeviceName(size_t i) const
{
	if( i >= devices.num_devices )
		return NULL;

	return devices.device_name[i];
}

int Joystick::Select(size_t i)
{
	if( i >= devices.num_devices )
		return JOYSTICK_ERROR;

	if( controller ) {
		Unaquire();
	}

	controller = SDL_GameControllerOpen(i);
	if( !controller ) {
		return JOYSTICK_ERROR;
	}

	joystick = SDL_GameControllerGetJoystick(controller);

	num_buttons = (uint8_t)SDL_JoystickNumButtons(joystick);

	LOGI("Joystick Axes: %d", SDL_JoystickNumAxes(joystick));
	LOGI("Joystick Buttons: %d", num_buttons);
	LOGI("Joystick POVs: %d", SDL_JoystickNumHats(joystick));

	return JOYSTICK_OK;
}

int Joystick::Aquire()
{
	return JOYSTICK_OK;
}

int Joystick::Unaquire()
{
	if( controller ) {
		SDL_GameControllerClose(controller);
		controller = NULL;
		joystick = NULL;
	}

	return JOYSTICK_OK;
}

int Joystick::Poll()
{
	return JOYSTICK_OK;
}

int32_t Joystick::Axis(size_t a)
{
	if( joystick ) {
		return SDL_JoystickGetAxis(joystick, a);
	}

	return 0;
}

int Joystick::NumButtons() const
{
	return num_buttons;
}

int Joystick::operator[](size_t b)
{
	if( joystick ) {
		return SDL_JoystickGetButton(joystick, b);
	}

	return 0;
}

const int Joystick::operator[](size_t b) const
{
	if( joystick ) {
		return SDL_JoystickGetButton(joystick, b);
	}
	
	return 0;
}

#endif

} /* namespace Input */
