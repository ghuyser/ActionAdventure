/*
 * Copyright (c) 2014 Gary Huyser <gary.huyser@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#include <stdlib.h>
#include <string.h>

#include <memory>
#include <string>
#include <vector>
#include <unordered_map>
#include <cstring>
#include <exception>

#include "jsonobject.h"

using std::shared_ptr;

typedef std::unordered_map<std::string,JSONObject> JSONMap;
typedef std::unordered_map<std::string,JSONObject>::iterator JSONMapIterator;
typedef std::vector<JSONObject> JSONArray;
typedef std::vector<JSONObject>::iterator JSONArrayIterator;

class JSONIteratorException : public std::exception
{
	virtual const char * what() const throw()
	{
		return "JSONObject: Type not supported with iterator.";
	}
};

class JSONObject::JObj_impl {
public:
	JObj_impl();
	~JObj_impl();

	int beginParse(JSONObject *base, const char *s, int slen);
	int parseObject(JSONObject *base, const char *s, int slen);
	int parseArray(JSONObject *base, const char *s, int slen);
	int parseString(JSONObject *base, const char *s, int slen);
	int parseNumber(JSONObject *base, const char *s, int slen);
	int parseBoolean(JSONObject *base, const char *s, int slen);
	int parseNull(JSONObject *base, const char *s, int slen);

	int findString(const char *s, int slen, int& begin, int& length);

	inline bool isValueStart(const char c);
	int checkUTF8(const char *s, int slen);

	JSONType type;
	JSONNumberType number_type;

	shared_ptr<void> value;
};

class JSONObject::Iterator::JIter_impl {
public:
	JIter_impl();
	~JIter_impl();

	void find(void *value, JSONType type, const char *key);
	void begin(void *value, JSONType type);
	void end(void *value, JSONType type);
	void *iterator;
	JSONType type;
};

JSONObject::JObj_impl::JObj_impl()
	: type(JSONType::Null), number_type(JSONNumberType::Integer)
{
}

JSONObject::JObj_impl::~JObj_impl()
{
}

JSONObject::JSONObject() : pimpl(new JObj_impl())
{
}

JSONObject::JSONObject(const JSONObject& obj) : pimpl(new JObj_impl())
{
	pimpl->type = obj.pimpl->type;
	pimpl->number_type = obj.pimpl->number_type;
	pimpl->value = obj.pimpl->value;
}

JSONObject::JSONObject(const char *json) : pimpl(new JObj_impl())
{
	Parse(json);
}

JSONObject::JSONObject(const char *json, const int json_length) : pimpl(new JObj_impl())
{
	Parse(json, json_length);
}

JSONObject::JSONObject(const std::string& json) : pimpl(new JObj_impl())
{
	Parse(json);
}


JSONObject::~JSONObject()
{
	delete pimpl;
}


std::string JSONObject::GetString() const
{
	if( pimpl->type != JSONType::String )
		return std::string();

	return *(std::string*)(pimpl->value.get());
}

const char* JSONObject::GetCString() const
{
	if( pimpl->type != JSONType::String )
		return NULL;

	return (*(std::string*)(pimpl->value.get())).c_str();
}

int JSONObject::GetInteger() const
{
	if( pimpl->type != JSONType::Number )
		return 0;

	switch( pimpl->number_type ) {
	case JSONNumberType::Integer:
		return *(int*)pimpl->value.get();
	case JSONNumberType::Float:
		return (int)*(float*)pimpl->value.get();
	case JSONNumberType::Double:
		return (int)*(double*)pimpl->value.get();
	}
	return 0;
}

float JSONObject::GetFloat() const
{
	if( pimpl->type != JSONType::Number )
		return 0;

	switch( pimpl->number_type ) {
	case JSONNumberType::Integer:
		return (float)*(int*)pimpl->value.get();
	case JSONNumberType::Float:
		return *(float*)pimpl->value.get();
	case JSONNumberType::Double:
		return (float)*(double*)pimpl->value.get();
	}
	return 0;
}

double JSONObject::GetDouble() const
{
	if( pimpl->type != JSONType::Number )
		return 0;

	switch( pimpl->number_type ) {
	case JSONNumberType::Integer:
		return (double)*(int*)pimpl->value.get();
	case JSONNumberType::Float:
		return (double)*(float*)pimpl->value.get();
	case JSONNumberType::Double:
		return *(double*)pimpl->value.get();
	}
	return 0;
}

bool JSONObject::GetBoolean() const
{
	if( pimpl->type != JSONType::Boolean )
		return false;

	return *(bool*)pimpl->value.get();
}

JSONObject& JSONObject::Null()
{
	static JSONObject ret;
	return ret;
}

void JSONObject::MakeNull()
{
	pimpl->value.reset();
	pimpl->type = JSONType::Null;
}

void JSONObject::MakeEmptyArray()
{
	pimpl->value.reset(new JSONArray);
	pimpl->type = JSONType::Array;
}

void JSONObject::MakeEmptyObject()
{
	pimpl->value.reset(new JSONMap);
	pimpl->type = JSONType::Object;
}

bool JSONObject::IsEmpty() const
{
	if( !pimpl->value )
		return true;

	if( pimpl->type == JSONType::Object ) {
		return ((JSONMap*)pimpl->value.get())->size() == 0;
	} else if( pimpl->type == JSONType::Array ) {
		return ((JSONMap*)pimpl->value.get())->size() == 0;
	}

	return false;
}

int JSONObject::Count() const
{
	if( !pimpl->value || (pimpl->type != JSONType::Array && pimpl->type != JSONType::Object) )
		return 0;

	if( pimpl->type == JSONType::Object ) {
		return ((JSONMap*)pimpl->value.get())->size();
	}
	return ((JSONArray*)pimpl->value.get())->size();
}

JSONObject& JSONObject::operator=(const JSONObject& obj)
{
	pimpl->type = obj.pimpl->type;
	pimpl->number_type = obj.pimpl->number_type;
	pimpl->value = obj.pimpl->value;

	return *this;
}

JSONObject& JSONObject::operator=(const char *string)
{
	if( *this != string ) {
		pimpl->value.reset(new std::string);
		*((std::string*)pimpl->value.get()) = string;
		pimpl->type = JSONType::String;
	}

	return *this;
}

JSONObject& JSONObject::operator=(const std::string& string)
{ 
	if( *this != string ) {
		pimpl->value.reset(new std::string);
		*((std::string*)pimpl->value.get()) = string;
		pimpl->type = JSONType::String;
	}

	return *this;
}

JSONObject& JSONObject::operator=(const int& number)
{
	if( *this != number ) {
		pimpl->value.reset(new int);
		*((int*)pimpl->value.get()) = number;
		pimpl->type = JSONType::Number;
		pimpl->number_type = JSONNumberType::Integer;
	}

	return *this;
}

JSONObject& JSONObject::operator=(const float& number)
{
	if( *this != number ) {
		pimpl->value.reset(new float);
		*((float*)pimpl->value.get()) = number;
		pimpl->type = JSONType::Number;
		pimpl->number_type = JSONNumberType::Float;
	}

	return *this;
}

JSONObject& JSONObject::operator=(const double& number)
{
	if( *this != number ) {
		pimpl->value.reset(new double);
		*((double*)pimpl->value.get()) = number;
		pimpl->type = JSONType::Number;
		pimpl->number_type = JSONNumberType::Double;
	}

	return *this;
}

JSONObject& JSONObject::operator=(const bool& boolean)
{
	if( *this != boolean ) {
		pimpl->value.reset(new bool);
		*((bool*)pimpl->value.get()) = boolean;
		pimpl->type = JSONType::Boolean;
	}

	return *this;
}

bool JSONObject::operator==(const JSONObject& obj) const
{
	if( pimpl->type != obj.pimpl->type ) {
		return false;
	}

	switch( pimpl->type ) {
	case JSONType::Null:
	default:
		return true;

	case JSONType::Object:
		{
			JSONMap *m1 = (JSONMap*)pimpl->value.get(), *m2 = (JSONMap*)obj.pimpl->value.get();
			for( JSONMapIterator im = m1->begin(); im != m1->end(); ++im ) {
				if( m2->count(im->first) == 0 )
					return false;

				if( (*m2)[im->first] != im->second )
					return false;
			}
		}
		return false;

	case JSONType::Array:
		{
			JSONArray *v1 = (JSONArray*)pimpl->value.get(), *v2 = (JSONArray*)obj.pimpl->value.get();
			if( v1->size() != v2->size() )
				return false;

			for( int i = 0; i < (int)v1->size(); i++ ) {
				if( (*v1)[i] != (*v2)[i] )
					return false;
			}
		}
		return true;

	case JSONType::String:
		return obj == *(std::string*)pimpl->value.get();

	case JSONType::Number:
		switch( pimpl->number_type ) {
		case JSONNumberType::Integer:
			return obj == *(int*)pimpl->value.get();
		case JSONNumberType::Float:
			return obj == *(float*)pimpl->value.get();
		case JSONNumberType::Double:
			return obj == *(double*)pimpl->value.get();
		}
		return false;

	case JSONType::Boolean:
		return obj == *(bool*)pimpl->value.get();
	}
}

bool JSONObject::operator==(const char *string) const
{
	if( pimpl->type != JSONType::String )
		return false;

	return strcmp(GetCString(), string) == 0;
}

bool JSONObject::operator==(const std::string& string) const
{
	if( pimpl->type != JSONType::String )
		return false;

	return GetString().compare(string) == 0;
}

bool JSONObject::operator==(const int& number) const
{
	if( pimpl->type != JSONType::Number )
		return false;

	if( pimpl->number_type == JSONNumberType::Integer )
		return *(int*)pimpl->value.get() == number;

	if( pimpl->number_type == JSONNumberType::Float )
		return *(float*)pimpl->value.get() == (float)number;

	return *(double*)pimpl->value.get() == (double)number;
}

bool JSONObject::operator==(const float& number) const
{
	if( pimpl->type != JSONType::Number )
		return false;

	if( pimpl->number_type == JSONNumberType::Float )
		return *(float*)pimpl->value.get() == (float)number;

	if( pimpl->number_type == JSONNumberType::Double )
		return *(double*)pimpl->value.get() == (double)number;

	return (float)(*(int*)pimpl->value.get()) == number;
}

bool JSONObject::operator==(const double& number) const
{
	if( pimpl->type != JSONType::Number )
		return false;

	if( pimpl->number_type == JSONNumberType::Double )
		return *(double*)pimpl->value.get() == number;

	if( pimpl->number_type == JSONNumberType::Float )
		return (double)(*(float*)pimpl->value.get()) == number;

	return (double)(*(int*)pimpl->value.get()) == number;
}

bool JSONObject::operator==(const bool& boolean) const
{
	if( pimpl->type != JSONType::Boolean )
		return false;

	return *(bool*)pimpl->value.get() == boolean;
}

bool JSONObject::operator< (const JSONObject& obj) const
{
	if( pimpl->type != obj.pimpl->type ) {
		return false;
	}

	switch( pimpl->type ) {
	case JSONType::Null:
	case JSONType::Boolean:
	case JSONType::Object:
	case JSONType::Array:
	default:
		return false;

	case JSONType::String:
		return obj > *(std::string*)pimpl->value.get();

	case JSONType::Number:
		switch( pimpl->number_type ) {
		case JSONNumberType::Integer:
			return obj > *(int*)pimpl->value.get();
		case JSONNumberType::Float:
			return obj > *(float*)pimpl->value.get();
		case JSONNumberType::Double:
			return obj > *(double*)pimpl->value.get();
		}
		return false;
	}
}

bool JSONObject::operator< (const char *string) const
{
	if( pimpl->type != JSONType::String )
		return false;

	return strcmp(GetCString(), string) < 0;
}

bool JSONObject::operator< (const std::string& str) const
{
	if( pimpl->type != JSONType::String )
		return false;

	return GetString().compare(str) < 0;
}

bool JSONObject::operator< (const int& number) const
{
	if( pimpl->type != JSONType::Number )
		return false;

	if( pimpl->number_type == JSONNumberType::Integer )
		return *(int*)pimpl->value.get() < number;

	if( pimpl->number_type == JSONNumberType::Float )
		return *(float*)pimpl->value.get() < (float)number;

	return *(double*)pimpl->value.get() < (double)number;
}

bool JSONObject::operator< (const float& number) const
{
	if( pimpl->type != JSONType::Number )
		return false;

	if( pimpl->number_type == JSONNumberType::Float )
		return *(float*)pimpl->value.get() < number;

	if( pimpl->number_type == JSONNumberType::Double )
		return *(double*)pimpl->value.get() < (double)number;

	return (float)(*(int*)pimpl->value.get()) < number;
}

bool JSONObject::operator< (const double& number) const
{
	if( pimpl->type != JSONType::Number )
		return false;

	if( pimpl->number_type == JSONNumberType::Double )
		return *(double*)pimpl->value.get() < number;

	if( pimpl->number_type == JSONNumberType::Float )
		return (double)(*(float*)pimpl->value.get()) < number;

	return (double)(*(int*)pimpl->value.get()) < number;
}

bool JSONObject::operator> (const char *string) const
{
	if( pimpl->type != JSONType::String )
		return false;

	return strcmp(GetCString(), string) > 0;
}

bool JSONObject::operator> (const std::string& str) const
{
	if( pimpl->type != JSONType::String )
		return false;

	return GetString().compare(str) > 0;
}

bool JSONObject::operator> (const int& number) const
{
	return number < *(int*)pimpl->value.get();
}

bool JSONObject::operator> (const float& number) const
{
	return number < *(float*)pimpl->value.get();
}

bool JSONObject::operator> (const double& number) const
{
	return number < *(double*)pimpl->value.get();
}

JSONObject& JSONObject::operator[](const JSONObject& keyidx)
{
	return *this;
}

const JSONObject& JSONObject::operator[](const JSONObject& keyidx) const
{
	return *this;
}

JSONObject& JSONObject::operator[](const char *key)
{
	if( pimpl->type != JSONType::Object ) {
		pimpl->value.reset(new JSONMap);
		pimpl->type = JSONType::Object;
	}
	return (*(JSONMap*)pimpl->value.get())[std::string(key)];
}

const JSONObject& JSONObject::operator[](const char *key) const
{
	return (*(JSONMap*)pimpl->value.get())[std::string(key)];
}

JSONObject& JSONObject::operator[](const std::string& key)
{
	if( pimpl->type != JSONType::Object ) {
		pimpl->value.reset(new JSONMap);
		pimpl->type = JSONType::Object;
	}
	return (*(JSONMap*)pimpl->value.get())[key];
}

const JSONObject& JSONObject::operator[](const std::string& key) const
{
	return (*(JSONMap*)pimpl->value.get())[key];
}

JSONObject& JSONObject::operator[](const int& index)
{
	if( index < 0 ) {
		throw "Index out of bounds.";
		return JSONObject::Null();
	}

	if( pimpl->type != JSONType::Array ) {
		pimpl->value.reset(new JSONArray);
		pimpl->type = JSONType::Array;
	}
	JSONArray *v = (JSONArray*)pimpl->value.get();
	if( index >= (int)v->size() ) {
		v->resize(index+1);
	}
	return (*v)[index];
}

const JSONObject& JSONObject::operator[](const int& index) const
{
	JSONArray *v = (JSONArray*)pimpl->value.get();
	if( index < 0 || index >= (int)v->size() ) {
		throw "Index out of bounds.";
		return JSONObject::Null();
	}

	return (*v)[index];
}

bool JSONObject::hasKey(const char *key) const
{
	return this->hasKey(std::string(key));
}

bool JSONObject::hasKey(const std::string& key) const
{
	if( pimpl->type != JSONType::Object )
		return false;

	return ((JSONMap*)pimpl->value.get())->count(key) != 0;
}

void* JSONObject::Raw() const
{
	return pimpl->value.get();
}

const char* JSONObject::CSerialize() const
{
	return (Serialize()).c_str();
}

std::string JSONObject::Serialize() const
{
	std::string ret;
	switch( pimpl->type ) {
	case JSONType::Object:
		ret = "{";
		{
			JSONMap *m = (JSONMap*)pimpl->value.get();
			bool first = true;
			for( JSONMapIterator im = m->begin(); im != m->end(); ++im ) {
				if( !first )
					ret.append(",");
				ret.append("\"");
				ret.append(im->first);
				ret.append("\":");
				ret.append(im->second.Serialize());
				first = false;
			}
		}
		ret.append("}");
		return ret;

	case JSONType::Array:
		ret = "[";
		{
			JSONArray *v = (JSONArray*)pimpl->value.get();
			for( int i = 0; i < (int)v->size(); i++ ) {
				if( i > 0 )
					ret.append(",");
				ret.append((*v)[i].Serialize());
			}
		}
		ret.append("]");
		return ret;

	case JSONType::String:
		ret = "\"";
		ret.append(GetString());
		ret.append("\"");
		return ret;

	case JSONType::Number:
		switch( pimpl->number_type ) {
		case JSONNumberType::Integer:
			return std::to_string( GetInteger() );
			
		case JSONNumberType::Float:
			return std::to_string( GetFloat() );

		case JSONNumberType::Double:
			return std::to_string( GetDouble() );
		}
		return ret;

	case JSONType::Boolean:
		return (*(bool*)pimpl->value.get() ? "true" : "false");

	case JSONType::Null:
		return "null";
	}

	return "";
}

JSONType JSONObject::Type() const
{
	return pimpl->type;
}

JSONNumberType JSONObject::NumberType() const
{
	return pimpl->number_type;
}


void JSONObject::Parse(const char *json)
{
	if( json == NULL ) return;
	pimpl->beginParse(this, json, strlen(json));
}

void JSONObject::Parse(const char *json, const int json_length)
{
	if( json == NULL || json_length <= 0 ) return;
	pimpl->beginParse(this, json, json_length);
}

void JSONObject::Parse(const std::string& json)
{
	if( json.length() == 0 ) return;
	pimpl->beginParse(this, json.c_str(), json.length());
}


int JSONObject::JObj_impl::beginParse(JSONObject *base, const char *s, int slen)
{
	int offset = 0;

	while( offset < slen && isspace(s[offset]) )
		offset++;

	if( offset >= slen )
		return -1;

	if( s[offset] == '{' ) {
		// Object follows
		offset++;
		offset += parseObject(base, s + offset, slen - offset);
	} else if( s[offset] == '[' ) {
		// Array follows
		offset++;
		offset += parseArray(base, s + offset, slen - offset);
	}

	return offset;
}

int JSONObject::JObj_impl::parseObject(JSONObject *base, const char *s, int slen)
{
	int offset = 0;
	
	while( offset < slen ) {
		int kbegin, klen, result;
		
		// if we found a }, this object is done
		if( s[offset] == '}' )
			break;

		// find key
		result = findString(s + offset, slen - offset, kbegin, klen);
		if( result < 0 )
			return -1;

		// create key
		kbegin += offset;
		char *key = (char*)malloc(klen + 1);
		if( key == NULL ) {
			return -2;
		}
		memset(key, 0, klen + 1);
		memcpy(key, s + kbegin, klen);

		// find :
		offset += result;
		while( offset < slen && s[offset] != ':' ) {
			offset++;
		}

		if( offset >= slen )
			return -1;

		// find the value
		while( offset < slen && !isValueStart(s[offset]) ) {
			offset++;
		}

		if( offset >= slen )
			return -1;

		// determine the type
		JSONObject n;
		if( s[offset] == '{' ) {
			offset++;
			n.MakeEmptyObject();
			result = n.pimpl->parseObject(&n, s + offset, slen - offset);
		} else if( s[offset] == '[' ) {
			offset++;
			n.MakeEmptyArray();
			result = n.pimpl->parseArray(&n, s + offset, slen - offset);
		} else if( s[offset] == '"' ) {
			result = n.pimpl->parseString(&n, s + offset, slen - offset);
		} else if( s[offset] == '-' || isdigit(s[offset]) ) {
			result = n.pimpl->parseNumber(&n, s + offset, slen - offset);
		} else if( s[offset] == 't' || s[offset] == 'f' ) {
			result = n.pimpl->parseBoolean(&n, s + offset, slen - offset);
		} else if( s[offset] == 'n' ) {
			result = n.pimpl->parseNull(&n, s + offset, slen - offset);
		}

		if( result < 0 )
			return -1;

		// store the value into this object
		(*base)[key] = n;
		free(key);

		// find a , or a }
		offset += result;
		while( offset < slen && s[offset] != ',' && s[offset] != '}' ) {
			offset++;
		}

		if( offset >= slen )
			return -1;
		
		// if we found a }, this object is done
		if( s[offset] == '}' )
			break;

		// otherwise a , was found and the process is repeated
		offset++;
	}

	return offset + 1;
}

int JSONObject::JObj_impl::parseArray(JSONObject *base, const char *s, int slen)
{
	int offset = 0, idx = 0;

	while( offset < slen ) {
		int result = -1;
		
		// if we found a ], this array is done
		if( s[offset] == ']' )
			break;

		// find the value
		while( offset < slen && !isValueStart(s[offset]) ) {
			offset++;
		}

		if( offset >= slen )
			return -1;

		// determine the type
		JSONObject n;
		if( s[offset] == '{' ) {
			offset++;
			n.MakeEmptyObject();
			result = n.pimpl->parseObject(&n, s + offset, slen - offset);
		} else if( s[offset] == '[' ) {
			offset++;
			n.MakeEmptyArray();
			result = n.pimpl->parseArray(&n, s + offset, slen - offset);
		} else if( s[offset] == '"' ) {
			result = n.pimpl->parseString(&n, s + offset, slen - offset);
		} else if( s[offset] == '-' || isdigit(s[offset]) ) {
			result = n.pimpl->parseNumber(&n, s + offset, slen - offset);
		} else if( s[offset] == 't' || s[offset] == 'f' ) {
			result = n.pimpl->parseBoolean(&n, s + offset, slen - offset);
		} else if( s[offset] == 'n' ) {
			result = n.pimpl->parseNull(&n, s + offset, slen - offset);
		}

		if( result < 0 )
			return -1;

		(*base)[idx++] = n;

		// find a , or a ]
		offset += result;
		while( offset < slen && s[offset] != ',' && s[offset] != ']' ) {
			offset++;
		}

		if( offset >= slen )
			return -1;
		
		// if we found a ], this array is done
		if( s[offset] == ']' )
			break;

		// otherwise a , was found and the process is repeated
		offset++;
	}

	return offset + 1;
}

int JSONObject::JObj_impl::parseString(JSONObject *base, const char *s, int slen)
{
	int begin, length, result;

	result = findString(s, slen, begin, length);
	if( result < 0 )
		return -1;

	(*base) = std::string(s + begin, length);

	return result;
}

int JSONObject::JObj_impl::parseNumber(JSONObject *base, const char *s, int slen)
{
	int offset = 1, dec = 0;

	while( offset < slen && (isdigit(s[offset]) || s[offset] == '.') ) {
		if( s[offset] == '.' ) {
			if( dec > 0 )
				return -1;
			dec++;
		}
		offset++;
	}

	// if dec > 0, make a double - otherwise make an integer
	if( dec ) {
		*base = strtod(s, NULL);
	} else {
		std::string i(s, offset);
		*base = atoi(i.c_str());
	}

	return offset;
}

int JSONObject::JObj_impl::parseBoolean(JSONObject *base, const char *s, int slen)
{
	if( slen < 4 )
		return -1;

	if( !strncmp(s, "true", 4) ) {
		*base = true;
		return 4;
	}

	if( slen < 5 )
		return -1;

	if( !strncmp(s, "false", 5) ) {
		*base = false;
		return 5;
	}

	return -1;
}

int JSONObject::JObj_impl::parseNull(JSONObject *base, const char *s, int slen)
{
	if( slen < 4 )
		return -1;

	if( !strncmp(s, "null", 4) ) {
		type = JSONType::Null;
		value.reset();
		return 4;
	}

	return -1;
}


int JSONObject::JObj_impl::findString(const char *s, int slen, int& begin, int& length)
{
	int offset = 0;

	while( offset < slen && isspace(s[offset]) )
		offset++;

	if( offset >= slen || s[offset] != '"' ) {
		return -1;
	}

	begin = ++offset;
	while( offset < slen && s[offset] != '"' ) {
		if( s[offset] == '\\' ) {
			offset++;

			if( offset >= slen )
				return -1;

			char c = s[offset];
			if( c == '"' || c == '\\' || c == '/' || c == 'b' || c == 'f' || c == 'n' || c == 'r' || c == 't' ) {
				offset++;
				continue;
			}
			if( c == 'u' ) {
				if( offset + 4 > slen )
					return -1;

				if( isxdigit(s[offset+1]) && isxdigit(s[offset+2]) &&
					isxdigit(s[offset+3]) && isxdigit(s[offset+4])
				) {
					offset += 4;
					continue;
				}
			}
			return offset;
		}
		
		int num = checkUTF8(s + offset, slen - offset);
		if( !num )
			return -1;
		offset += num;
	}

	if( offset >= slen ) {
		return -1;
	}

	length = offset - begin;

	return offset + 1;
}

bool JSONObject::JObj_impl::isValueStart(const char c)
{
	return c == '{' || c == '[' || c == '"' || c == '-' || isdigit(c) || c == 't' || c == 'f' || c == 'n';
}

int JSONObject::JObj_impl::checkUTF8(const char *s, int slen)
{
	if( s == NULL || slen <= 0 )
		return 0;

	if( s[0] >= 0x20 && s[0] < 0x80 )
		return 1;

	unsigned char i, j, b = 0xe0, c = 0xc0;
	for( i = 2; i < 7; i++, b |= b >> 1, c |= c >> 1 ) {
		if( (s[0] & b) == c && slen >= i ) {
			for( j = 1; j < i; j++ ) {
				if( (s[j] & 0xc0) != 0x80 )
					return 0;
			}
			return i;
		}
	}
	return 0;
}


JSONObject::Iterator JSONObject::find(const char *key) const
{
	Iterator ret;

	if( pimpl->type == JSONType::Object ) {
		ret.pimpl->find(pimpl->value.get(), pimpl->type, key);
	}

	return ret;
}

JSONObject::Iterator JSONObject::begin() const
{
	Iterator ret;

	if( pimpl->type == JSONType::Object || pimpl->type == JSONType::Array ) {
		ret.pimpl->begin(pimpl->value.get(), pimpl->type);
	}

	return ret;
}

JSONObject::Iterator JSONObject::end() const
{
	Iterator ret;

	if( pimpl->type == JSONType::Object || pimpl->type == JSONType::Array ) {
		ret.pimpl->end(pimpl->value.get(), pimpl->type);
	}

	return ret;
}

JSONObject::Iterator::JIter_impl::JIter_impl() : iterator(NULL)
{
}

JSONObject::Iterator::Iterator() : pimpl(new JIter_impl)
{
}

JSONObject::Iterator::JIter_impl::~JIter_impl()
{
	if( iterator ) {
		operator delete(iterator);
	}
}

JSONObject::Iterator::~Iterator()
{
	delete pimpl;
}

JSONObject::Iterator::Iterator(const Iterator& obj) : pimpl(new JIter_impl)
{
	if( obj.pimpl->iterator != NULL ) {
		pimpl->type = obj.pimpl->type;
		if( obj.pimpl->type == JSONType::Object ) {
			JSONMapIterator i = *((JSONMapIterator*)obj.pimpl->iterator);
			pimpl->iterator = operator new(sizeof(JSONMapIterator));
			memcpy(pimpl->iterator, &i, sizeof(JSONMapIterator));
		} else if( obj.pimpl->type == JSONType::Array ) {
			JSONArrayIterator i = *((JSONArrayIterator*)obj.pimpl->iterator);
			pimpl->iterator = operator new(sizeof(JSONArrayIterator));
			memcpy(pimpl->iterator, &i, sizeof(JSONArrayIterator));
		}
	}
}

void JSONObject::Iterator::JIter_impl::find(void *value, JSONType type, const char *key)
{
	if( iterator ) {
		operator delete(iterator);
	}

	this->type = type;
	if( type == JSONType::Object ) {
		JSONMapIterator i = ((JSONMap*)value)->find(key);
		iterator = operator new(sizeof(JSONMapIterator));
		memcpy(iterator, &i, sizeof(JSONMapIterator));
	}
}

void JSONObject::Iterator::JIter_impl::begin(void *value, JSONType type)
{
	if( iterator ) {
		operator delete(iterator);
	}

	this->type = type;
	if( type == JSONType::Object ) {
		JSONMapIterator i = ((JSONMap*)value)->begin();
		iterator = operator new(sizeof(JSONMapIterator));
		memcpy(iterator, &i, sizeof(JSONMapIterator));
	} else if( type == JSONType::Array ) {
		JSONArrayIterator i = ((JSONArray*)value)->begin();
		iterator = operator new(sizeof(JSONArrayIterator));
		memcpy(iterator, &i, sizeof(JSONArrayIterator));
	}
}

void JSONObject::Iterator::JIter_impl::end(void *value, JSONType type)
{
	if( iterator ) {
		operator delete(iterator);
	}

	this->type = type;
	if( type == JSONType::Object ) {
		JSONMapIterator i = ((JSONMap*)value)->end();
		iterator = operator new(sizeof(JSONMapIterator));
		memcpy(iterator, &i, sizeof(JSONMapIterator));
	} else if( type == JSONType::Array ) {
		JSONArrayIterator i = ((JSONArray*)value)->end();
		iterator = operator new(sizeof(JSONArrayIterator));
		memcpy(iterator, &i, sizeof(JSONArrayIterator));
	}
}

JSONObject::Iterator& JSONObject::Iterator::operator++()
{
	if( pimpl->type == JSONType::Object ) {
		JSONMapIterator i = *((JSONMapIterator*)pimpl->iterator);
		++i;
		memcpy(pimpl->iterator, &i, sizeof(JSONMapIterator));
	} else if( pimpl->type == JSONType::Array ) {
		JSONArrayIterator i = *((JSONArrayIterator*)pimpl->iterator);
		++i;
		memcpy(pimpl->iterator, &i, sizeof(JSONArrayIterator));
	}
	return *this;
}

JSONObject::Iterator JSONObject::Iterator::operator++(int)
{
	JSONObject::Iterator ret;
	if( pimpl->type == JSONType::Object ) {
		JSONMapIterator i = *((JSONMapIterator*)pimpl->iterator);
		ret.pimpl->iterator = operator new(sizeof(JSONMapIterator));
		memcpy(ret.pimpl->iterator, &i, sizeof(JSONMapIterator));
		++i;
		memcpy(pimpl->iterator, &i, sizeof(JSONMapIterator));
	} else if( pimpl->type == JSONType::Array ) {
		JSONArrayIterator i = *((JSONArrayIterator*)pimpl->iterator);
		ret.pimpl->iterator = operator new(sizeof(JSONArrayIterator));
		memcpy(ret.pimpl->iterator, &i, sizeof(JSONArrayIterator));
		++i;
		memcpy(pimpl->iterator, &i, sizeof(JSONArrayIterator));
	}
	return ret;
}

const JSONObject* JSONObject::Iterator::operator->() const
{
	if( pimpl->type == JSONType::Object ) {
		return &((*((JSONMapIterator*)pimpl->iterator))->second);
	} else if( pimpl->type == JSONType::Array ) {
		return &(*(*((JSONArrayIterator*)pimpl->iterator)));
	}
	return nullptr;
}

const JSONObject& JSONObject::Iterator::operator*() const
{
	if( pimpl->type == JSONType::Object ) {
		return (*((JSONMapIterator*)pimpl->iterator))->second;
	} else if( pimpl->type == JSONType::Array ) {
		return *(*((JSONArrayIterator*)pimpl->iterator));
	}
	throw JSONIteratorException();
}

bool JSONObject::Iterator::operator==(const Iterator& obj) const
{
	if( pimpl->iterator == obj.pimpl->iterator ) {
		return true;
	}

	if( pimpl->type == JSONType::Object ) {
		JSONMapIterator a = *((JSONMapIterator*)pimpl->iterator);
		JSONMapIterator b = *((JSONMapIterator*)obj.pimpl->iterator);
		return a == b;
	} else if(pimpl->type == JSONType::Array ) {
		JSONArrayIterator a = *((JSONArrayIterator*)pimpl->iterator);
		JSONArrayIterator b = *((JSONArrayIterator*)obj.pimpl->iterator);
		return a == b;
	}

	return false;
}

const std::string JSONObject::Iterator::Key() const
{
	if( pimpl->type == JSONType::Object ) {
		JSONMapIterator i = *((JSONMapIterator*)pimpl->iterator);
		return i->first;
	}

	return "";
}

const JSONObject JSONObject::Iterator::Value() const
{
	if( pimpl->type == JSONType::Object ) {
		JSONMapIterator i = *((JSONMapIterator*)pimpl->iterator);
		return i->second;
	} else if( pimpl->type == JSONType::Array ) {
		JSONArrayIterator i = *((JSONArrayIterator*)pimpl->iterator);
		return *i;
	}

	return JSONObject();
}
