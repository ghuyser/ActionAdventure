#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <string.h>
#include <ctype.h>

#include "gl_fabric.h"

#include "common.h"
#include "logging.h"
#include "util.h"

#ifndef MAX_PATH
#define MAX_PATH		256
#endif

#define UNIFORM			0
#define ATTRIBUTE		1

#define VERTEX_SHADER_HEADER	"[vertex shader]"
#define FRAGMENT_SHADER_HEADER	"[fragment shader]"

int shader_varlen(char *s);

GLShader::GLShader() :
	script(0), script_length(0), type(0), handle(0)
{
}

GLShader::GLShader(const char *file_name, GLenum shader_type) :
	script(0), script_length(0), type(0), handle(0)
{
	loadShaderFile(file_name, shader_type);
}


GLShader::GLShader(const char *buffer, int buffer_size, GLenum shader_type) :
	script(0), script_length(0), type(0), handle(0)
{
	loadShaderBuffer(buffer, buffer_size, shader_type);
}

GLShader::~GLShader() {
	freeResources();
}

bool GLShader::loadShaderFile(const char *file_name, GLenum shader_type) {
	if( !file_name || strnlen(file_name, MAX_PATH) == MAX_PATH ) {
		LOGE("Cannot load shader: Invalid parameters.");
		return false;
	}

	FILE *fp = fopen(file_name, "r");
	if( !fp ) {
		LOGE("Cannot load shader: %s does not exist.", file_name);
		return false;
	}

	fseek(fp, 0, SEEK_END);
	int buffer_length = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	char *script_buffer = (char*)malloc(buffer_length + 1);
	if( !script ) {
		LOGE("Cannot load shader: Out of memory.");
		fclose(fp);
		freeResources();
		return false;
	}

	int num = fread(script, 1, buffer_length, fp);
	fclose(fp);
	if( num != buffer_length ) {
		LOGE("Cannot load shader: Error during read.");
		freeResources();
		return false;
	}

	bool result = loadShaderBuffer(script_buffer, buffer_length, shader_type);
	free(script_buffer);
	return result;
}

bool GLShader::loadShaderBuffer(const char *buffer, int buffer_size, GLenum shader_type) {
	if( !buffer || buffer_size <= 0 ) {
		LOGE("Cannot load shader: Invalid parameters.");
		return false;
	}

	freeResources();

	script_length = buffer_size;
	script = (char*)malloc(script_length + 1);
	if( !script ) {
		LOGE("Cannot load shader: Out of memory.");
		freeResources();
		return false;
	}
	script[script_length] = '\0';
	memcpy(script, buffer, buffer_size);

	handle = glCreateShader(shader_type);
	if( handle == 0 ) {
		LOGE("Cannot load shader: Error creating handle.");
		freeResources();
		return false;
	}

	const char *sbuf = (const char*)script;
	glShaderSource(handle, 1, &sbuf, &script_length);
	glCompileShader(handle);
	
	int compile_status;
	glGetShaderiv(handle, GL_COMPILE_STATUS, &compile_status);
	if( compile_status == 0 ) {
		char tmp[1024];
		int len;
		glGetShaderInfoLog(handle, 1024, &len, tmp);
		LOGE("Cannot load shader:  %s", tmp);		
		freeResources();
		return false;
	}

	type = shader_type;
	return true;
}

void GLShader::freeResources() {
	if( handle ) {
		glDeleteShader(handle);
		handle = 0;
	}
	SAFE_FREE(script);
	script_length = 0;
}







GLProgram::GLProgram() :
	vertex(0), fragment(0)
{
	m_attribs.count = 0;
	m_attribs.names = 0;
	m_attribs.ids = 0;
	m_uniforms.count = 0;
	m_uniforms.names = 0;
	m_uniforms.ids = 0;
}

GLProgram::~GLProgram() {
	freeResources();
}

void GLProgram::freeResources() {
	if( handle ) {
		glDeleteProgram(handle);
		handle = 0;
	}

	for(int i = 0; i < m_attribs.count; i++) {
		SAFE_FREE(m_attribs.names[i]);
	}
	SAFE_FREE(m_attribs.names);
	SAFE_FREE(m_attribs.ids);
	m_attribs.count = 0;

	for(int i = 0; i < m_uniforms.count; i++) {
		SAFE_FREE(m_uniforms.names[i]);
	}
	SAFE_FREE(m_uniforms.names);
	SAFE_FREE(m_uniforms.ids);
	m_uniforms.count = 0;

	SAFE_DELETE(vertex);
	SAFE_DELETE(fragment);
}

bool GLProgram::init(const char *vertex_file, const char *fragment_file) {
	if( !vertex_file || !fragment_file ) {
		LOGE("Cannot create program: Invalid parameters.");
		return false;
	}

	FILE *vf, *ff;
	vf = fopen(vertex_file, "rb");
	ff = fopen(fragment_file, "rb");
	if( !vf || !ff ) {
		LOGE("Cannot create program: File not found. (%s or %s)", vertex_file, fragment_file);
		if( vf ) fclose(vf);
		if( ff ) fclose(ff);
		return false;
	}

	int vsize = 0, fsize = 0;
	fseek(vf, 0, SEEK_END);
	fseek(ff, 0, SEEK_END);
	vsize = ftell(vf);
	fsize = ftell(ff);
	fseek(vf, 0, SEEK_SET);
	fseek(ff, 0, SEEK_SET);

	char *vbuf, *fbuf;
	vbuf = (char*)malloc(vsize);
	fbuf = (char*)malloc(fsize);
	if( !vbuf || !fbuf ) {
		LOGE("Cannot create program: Out of memory.");
		SAFE_FREE(vbuf);
		SAFE_FREE(fbuf);
		fclose(vf);
		fclose(ff);
		return false;
	}

	memset(vbuf, 0, vsize);
	memset(fbuf, 0, fsize);
	int vrs = fread(vbuf, 1, vsize, vf);
	int frs = fread(fbuf, 1, fsize, ff);
	fclose(vf);
	fclose(ff);

	bool result = false;
	if( vrs != vsize || frs != fsize ) {
		LOGE("Cannot create program: Could not read entire file.");
	} else {
		result = init(vbuf, vsize, fbuf, fsize);
	}

	free(vbuf);
	free(fbuf);

	return result;
}

bool GLProgram::init(const char *shader_file) {
	if( !shader_file ) {
		LOGE("Cannot create program: Invalid parameters.");
		return false;
	}

	FILE *fp = fopen(shader_file, "rb");
	if( !fp ) {
		LOGE("Cannot create program: File not found. (%s)", shader_file);
		return false;
	}

	int size = 0;
	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	char *buf = (char*)malloc(size);
	if( !buf ) {
		LOGE("Cannot create program: Out of memory.");
		fclose(fp);
		return false;
	}

	memset(buf, 0, size);
	int num_read = fread(buf, 1, size, fp);
	fclose(fp);

	if( num_read != size ) {
		LOGE("Cannot create program: Could not read entire file.");
		free(buf);
		return false;
	}

	int vpos = find_string(buf, size, VERTEX_SHADER_HEADER) + strlen(VERTEX_SHADER_HEADER);
	if( vpos < 0 ) {
		LOGE("Cannot create program: %s not found in %s.", VERTEX_SHADER_HEADER, shader_file);
		free(buf);
		return false;
	}

	int fpos = find_string(buf, size, FRAGMENT_SHADER_HEADER) + strlen(FRAGMENT_SHADER_HEADER);
	if( fpos < 0 ) {
		LOGE("Cannot create program: %s not found in %s.", FRAGMENT_SHADER_HEADER, shader_file);
		free(buf);
		return false;
	}

	int vlen = vpos < fpos ? fpos -	strlen(FRAGMENT_SHADER_HEADER) - vpos : size - vpos;
	int flen = fpos < vpos ? vpos -	strlen(VERTEX_SHADER_HEADER) - fpos : size - fpos;

	bool result = init(buf + vpos, vlen, buf + fpos, flen);

	free(buf);

	return result;
}

bool GLProgram::init(const char *buffer, int vbuf_start, int vbuf_sz, int fbuf_start, int fbuf_sz) {
	return init(buffer + vbuf_start, vbuf_sz, buffer + fbuf_start, fbuf_sz);
}

bool GLProgram::init(const char *vbuf, int vbuf_sz, const char *fbuf, int fbuf_sz) {
	if( !vbuf || vbuf_sz <= 0 || !fbuf || fbuf_sz <= 0 ) {
		LOGE("Cannot create program: Invalid parameters.");
		return false;
	}

	freeResources();

	vertex = new GLShader(vbuf, vbuf_sz, GL_VERTEX_SHADER);
	fragment = new GLShader(fbuf, fbuf_sz, GL_FRAGMENT_SHADER);
	if( vertex->handle == 0 || fragment->handle == 0 ) {
		LOGE("Cannot create program: Cannot create shader.");
		freeResources();
		return false;
	}

	handle = glCreateProgram();
	if( handle == 0 ) {
		LOGE("Cannot create program: Error creating handle.");
		freeResources();
		return false;
	}

	glAttachShader(handle, vertex->handle);
	glAttachShader(handle, fragment->handle);
	glLinkProgram(handle);

	int link_status;
	glGetProgramiv(handle, GL_LINK_STATUS, &link_status);
	if( link_status == GL_FALSE ) {
		char tmp[1024];
		int len;
		glGetProgramInfoLog(handle, 1024, &len, tmp);
		LOGE("Cannot create program:  %s", tmp);
		freeResources();
		return false;
	}

	buildNames(m_attribs, "attribute", vbuf, vbuf_sz);
	buildNames(m_attribs, "attribute", fbuf, fbuf_sz);
	buildNames(m_uniforms, "uniform", vbuf, vbuf_sz);
	buildNames(m_uniforms, "uniform", fbuf, fbuf_sz);
	generateResourceIDs(m_attribs, ATTRIBUTE);
	generateResourceIDs(m_uniforms, UNIFORM);

	return true;
}

void GLProgram::enableAttribs() {
	for(int i = 0; i < m_attribs.count; i++)
		glEnableVertexAttribArray(m_attribs.ids[i]);
}

void GLProgram::disableAttribs() {
	for(int i = 0; i < m_attribs.count; i++)
		glDisableVertexAttribArray(m_attribs.ids[i]);
}

int GLProgram::attribs(const char *attrib_name) {
	for(int i = 0; i < m_attribs.count; i++) {
		if( !strcmp(attrib_name, m_attribs.names[i]) )
			return m_attribs.ids[i];
	}

	return 0;
}

int GLProgram::uniforms(const char *uniform_name) {
	for(int i = 0; i < m_uniforms.count; i++) {
		if( !strcmp(uniform_name, m_uniforms.names[i]) )
			return m_uniforms.ids[i];
	}

	return 0;
}

void GLProgram::buildNames(Resource &resource, const char *match, const char *buffer, int buf_sz) {
	char **lines;
	int num_lines;

	split_string((char*)buffer, buf_sz, '\n', &lines, &num_lines);
	if( lines == NULL ) {
		LOGE("GLProgram::buildNames: Error building name list.");
		return;
	}

	for(int i = 0; i < num_lines; i++) {
		char *trimmed = trim(lines[i]);
		if( !trimmed ) {
			LOGE("GLProgram::buildNames: Error trimming line. (%s)", lines[i]);
			continue;
		}

		char **line;
		int num_line;
		split_string(trimmed, strlen(trimmed), ' ', &line, &num_line);
		if( line == NULL ) {
			LOGE("GLProgram::buildNames: Error splitting line.");
			continue;
		}

		if( num_line >= 3 && !strcmp(line[0], match) ) {
			resource.count++;
			char **tmp = (char**)realloc(resource.names, sizeof(char*) * resource.count);
			if( !tmp ) {
				LOGE("GLProgram::buildNames: Out of memory.");
				free_split(line, num_line);
				free(trimmed);
				break;
			}
			resource.names = tmp;

			int cpylen = shader_varlen(line[2]);
			char *cpy = (char*)malloc(cpylen + 1);
			if( !cpy ) {
				LOGE("GLProgram::buildNames: Out of memory.");
				free_split(line, num_line);
				free(trimmed);
				break;
			}
			memset(cpy, 0, cpylen + 1);
			memcpy(cpy, line[2], cpylen);
			resource.names[resource.count-1] = cpy;
		}
		free_split(line, num_line);
		free(trimmed);
	}

	free_split(lines, num_lines);
}

void GLProgram::generateResourceIDs(Resource &resource, int type) {
	int *tmp = (int*)realloc(resource.ids, sizeof(int) * resource.count);
	if( !tmp ) {
		LOGE("GLProgram::generateResourceIDs: Out of memory.");
		return;
	}

	resource.ids = tmp;
	for(int i = 0; i < resource.count; i++) {
		switch(type) {
		case ATTRIBUTE:
			resource.ids[i] = glGetAttribLocation(handle, resource.names[i]);
			break;

		case UNIFORM:
			resource.ids[i] = glGetUniformLocation(handle, resource.names[i]);
			break;

		default:
			break;
		}
		//LOGI("Resource: %s (%d)", resource.names[i], resource.ids[i]);
	}
}


/*
 * String helper function
 */
int shader_varlen(char *s) {
	char *c = s;
	while( isalnum(*c) || *c == '_' ) {
		c++;
	}
	return c - s;
}
