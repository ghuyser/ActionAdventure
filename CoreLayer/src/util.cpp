/*
* Copyright (c) 2013 Gary Huyser <gary.huyser@gmail.com>
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*
*/

#include <stdlib.h>
#include <memory.h>
#include <string.h>
#include <ctype.h>

#include "util.h"


/*
 * String utility functions
 */

int find_string(const char *src, int srclen, const char *find)
{
	const char *c = src;
	int findlen = strlen(find);
	while( c <= src + srclen - findlen && strncmp(c, find, findlen) )
		c++;

	if( c > src + srclen - findlen )
		return -1;

	return c - src;
}

void split_string(char *src, int srclen, char split, char ***outarray, int *outcount)
{
	if( src == NULL || outarray == NULL || outcount == NULL ) {
		return;
	}

	// count the number of splits to be made
	int count = 1, prvc = 0;
	char *c = src;
	while( c != src + srclen && *c ) {
		if( *c == split && prvc == 0 ) {
			count++;
			prvc = 1;
		} else if( *c != split && prvc == 1 ) {
			prvc = 0;
		}
		c++;
	}

	char **a = (char**)malloc(sizeof(char*) * count);
	if( a == NULL ) {
		*outarray = NULL;
		*outcount = 0;
		return;
	}

	c = src;
	char *prev = c;
	for( int i = 0; i < count; i++ ) {
		while( c != src + srclen && *c && *c != split )
			c++;

		int size = c - prev;
		a[i] = (char*)malloc(size+1);
		if( a[i] == NULL ) {
			break;
		}
		memcpy(a[i], prev, size);
		a[i][size] = 0;

		while( c != src + srclen && *c && *c == split )
			c++;

		prev = c;
	}

	*outarray = a;
	*outcount = count;
}

void free_split(char **splitarray, int count)
{
	if( splitarray == NULL )
		return;

	for( int i = 0; i < count; i++ ) {
		free( splitarray[i] );
	}
	free( splitarray );
}

char* trim(const char *s)
{
	if( s == 0 )
		return 0;

	int o = 0;
	int b = 0;
	int l = strlen(s);
	int e = l - 1;

	while( o < l ) {
		if( !isspace(s[o]) ) {
			b = o;
			break;
		}
		o++;
	}

	o = l - 1;
	while( o > b ) {
		if( !isspace(s[o]) ) {
			e = o + 1;
			break;
		}
		o--;
	}

	int z = e - b;
	if( z < 0 ) z = 0;
	char *ret = (char*)malloc(z + 1);
	if( !ret ) return ret;
	memset(ret, 0, z + 1);
	memcpy(ret, s + b, z);
	return ret;
}

char* get_line(const char *src, int srclen, int *nextline)
{
	if( !src || srclen <= 0 )
		return NULL;

	char *c = (char*)src;
	while( c - src < srclen && *c && *c != '\n' && *c != '\r' )
		c++;

	int len = c - src;
	while( c - src < srclen && (*c == '\n' || *c == '\r') )
		c++;

	*nextline = c - src;
	char *out = (char*)malloc(len + 1);
	if( out ) {
		memset(out, 0, len + 1);
		memcpy(out, src, len);
	}

	return out;
}



char* string_duplicate(const char *src)
{
	return string_duplicate(src, strlen(src));
}

char* string_duplicate(const char *src, int srclen)
{
	char *ret = (char*)malloc(srclen + 1);
	if( !ret )
		return NULL;

	memcpy(ret, src, srclen);
	ret[srclen] = '\0';
	return ret;
}

char* string_replace(const char *src, const char *search, const char *replace)
{
	int i = 0, c = 0, p = 0, o = 0;
	int srclen = strlen(src);
	int ls = strlen(search);
	int lr = strlen(replace);
	
	// first, find how many replacements need to be done
	while( i < srclen - ls ) {
		if( !strncmp(src + i, search, ls) )
			c++;
		i++;
	}

	// calculate the final size of the string
	int fsz = srclen - (c * ls) + (c * lr);

	// create the string
	char *ret = (char*)malloc(fsz + 1);
	if( !ret )
		return NULL;

	// build the final string
	memset(ret, 0, fsz + 1);
	i = 0;
	while( i < srclen - ls ) {
		if( !strncmp(src + i, search, ls) ) {
			memcpy(ret + o, src + p, i - p);
			o += i - p;
			p += i + ls;
			memcpy(ret + o, replace, lr);
			o += lr;
			i += ls;
		} else {
			i++;
		}
	}
	if( p < srclen ) {
		memcpy(ret + o, src + p, srclen - p);
	}

	return ret;
}

char* encode_uri_string(const char *src)
{
	int i = 0, c = 0, o = 0;
	int srclen = strlen(src);

	// count how many characters need encoding
	while( i < srclen ) {
		if( is_uri_reserved(src[i]) )
			c++;
		i++;
	}

	int fsz = srclen + (2 * c);
	char *ret = (char*)malloc(fsz + 1);
	if( !ret )
		return NULL;

	memset(ret, 0, fsz + 1);
	i = 0;
	while( i < srclen ) {
		char a = src[i];
		if( is_uri_reserved(a) ) {
			ret[o++] = '%';
			ret[o++] = (a >> 4) + '0';
			ret[o++] = (a & 0xf) + '0';

			if( ret[o - 2] > '9' )
				ret[o - 2] += 7;
			if( ret[o - 1] > '9' )
				ret[o - 1] += 7;
		} else {
			ret[o++] = a;
		}
		i++;
	}

	return ret;
}

int is_uri_reserved(char c)
{
	return !(isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~');
}
