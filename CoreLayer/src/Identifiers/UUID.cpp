#include "uuid_c.h"

#include "Identifiers/UUID.h"

#include <cstring>

#ifdef _WIN32
#define m_uuid (*((::UUID*)mUUID))
#else
#define m_uuid (*(uuid_t*)mUUID)
#endif

namespace Identifiers
{

Identifiers::UUID::UUID(const std::string uuid)
{
	uuid_parse(uuid.c_str(), m_uuid);
}

Identifiers::UUID::UUID(const char *uuid)
{
	uuid_parse(uuid, m_uuid);
}

bool Identifiers::UUID::operator==(const UUID &u) const
{
	return !memcmp(mUUID, u.mUUID, 16);
}

bool Identifiers::UUID::operator!=(const UUID &u) const
{
	return !(*this == u);
}

void Identifiers::UUID::clear()
{
	uuid_clear(m_uuid);
}

void Identifiers::UUID::generate()
{
	uuid_generate(m_uuid);
}

std::string Identifiers::UUID::getString() const
{
	uuid_str_create(uuidstr);
	uuid_unparse(m_uuid, uuidstr);
	std::string result((const char *)uuidstr);
	uuid_str_free(uuidstr);
	return result;
}

bool Identifiers::UUID::isNull() const
{
	return uuid_is_null(m_uuid);
}

UUID Identifiers::UUID::hton() const
{
	uuid_t netid;
	uuid_copy(netid, hton_uuid(m_uuid));
	
	UUID *netuuid = reinterpret_cast<UUID*>(&netid);
	return *netuuid;
}

UUID Identifiers::UUID::Null()
{
	UUID nil;
	nil.clear();
	return nil;
}

}

namespace std {
	size_t hash<Identifiers::UUID>::operator()(const Identifiers::UUID& uuid) const noexcept {
		const std::uint64_t* p = reinterpret_cast<const std::uint64_t*>(&uuid);
		std::hash<std::uint64_t> hash;
		return hash(p[0]) ^ hash(p[1]);
	}
}
