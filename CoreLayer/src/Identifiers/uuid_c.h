#pragma once

#ifdef _WIN32
#pragma comment(lib, "rpcrt4.lib")
#include <windows.h>

#ifdef __cplusplus
#undef uuid_t
#define uuid_t                          ::UUID
#endif
#define uuid_generate(out)              UuidCreate(&out)
#define uuid_clear(uu)                  UuidCreateNil(&uu)
#define uuid_copy(dst, src)             { (dst) = (src); }
#define uuid_init(unew, ufrom)          uuid_t unew = ufrom
#define uuid_parse(in, uu)              UuidFromStringA((RPC_CSTR)in, &uu)
#ifdef __cplusplus
#define uuid_unparse(uu, out)           UuidToStringA((const ::UUID*)(&uu), (RPC_CSTR*)(&out))
#else
#define uuid_unparse(uu, out)           UuidToStringA((const UUID*)(&uu), (RPC_CSTR*)(&out))
#endif
#define uuid_str_create(str)            RPC_CSTR str
#define uuid_str_free(str)              RpcStringFree((RPC_CSTR*)(&str))

inline int uuid_compare(uuid_t &uu1, uuid_t &uu2)
{
	RPC_STATUS status;
	return UuidCompare(&uu1, &uu2, &status);
}

inline int uuid_is_null(uuid_t &uu)
{
	RPC_STATUS status;
	return UuidIsNil(&uu, &status);
}

#ifdef __cplusplus
inline ::UUID hton_uuid(::UUID &u)
#else
inline UUID hton_uuid(UUID &u)
#endif
{
#ifdef __cplusplus
	::UUID res {
#else
	UUID res {
#endif
		htonl(u.Data1),
		htons(u.Data2),
		htons(u.Data3),
	};
	memcpy(res.Data4, u.Data4, 8);
	return res;
}

#elif defined __linux && !defined ANDROID
#include <arpa/inet.h>
#include <uuid/uuid.h>
#include <string.h>

#define uuid_init(unew, ufrom)  uuid_t unew; uuid_copy(unew, ufrom)
#define uuid_str_create(str)    char str[37]
#define uuid_str_free(str)

inline const unsigned char* hton_uuid(const unsigned char *u)
{
	static uuid_t res;
	*(unsigned int*)(&res) = htonl(*(unsigned int*)u);
	*(unsigned short*)(((char*)res) + 4) = htons(*(unsigned short*)(((char*)u) + 4));
	*(unsigned short*)(((char*)res) + 6) = htons(*(unsigned short*)(((char*)u) + 6));
	memcpy(((char*)res) + 8, ((char*)u) + 8, 8);
	return res;
}
#endif
