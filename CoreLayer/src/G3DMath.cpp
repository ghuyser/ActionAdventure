#define _USE_MATH_DEFINES
#include <math.h>
#include <memory.h>
#include <float.h>
#include "G3DMath.h"

using namespace G3DMath;

bool G3DMath::unProjectf(float winx, float winy, float winz, const Matrix4f& modelview, const Matrix4f& projection, const int viewport[], Vec3f& objectCoordinate)
{
	//Calculation for inverting a matrix, compute projection x modelview
	//and store in A[16]
	Matrix4f A = projection * modelview;

	//Now compute the inverse of matrix A
	if( !A.invert() )
		return false;

	//Transformation of normalized coordinates between -1 and 1
	Vec4f in( (winx - (float)viewport[0]) / (float)viewport[2] * 2.0f - 1.0f,
	          (winy - (float)viewport[1]) / (float)viewport[3] * 2.0f - 1.0f,
	          2.0f * winz - 1.0f, 1.0f );

	//Objects coordinates
	Vec4f out = A * in;
	if( out.w == 0.0f )
		return false;

	objectCoordinate.x = out.x / out.w;
	objectCoordinate.y = out.y / out.w;
	objectCoordinate.z = out.z / out.w;

	return true;
}

bool G3DMath::unProjectf(float winx, float winy, float winz, const Matrix4f& mvpMatrix, const int viewport[], Vec3f& objectCoordinate)
{
	//Compute the inverse of mvpMatrix
	Matrix4f A = mvpMatrix.inverse();

	//Transformation of normalized coordinates between -1 and 1
	Vec4f in( (winx - (float)viewport[0]) / (float)viewport[2] * 2.0f - 1.0f,
	          (winy - (float)viewport[1]) / (float)viewport[3] * 2.0f - 1.0f,
	          2.0f * winz - 1.0f, 1.0f );

	//Objects coordinates
	Vec4f out = A * in;
	if( out.w == 0.0f )
		return false;

	objectCoordinate.x = out.x / out.w;
	objectCoordinate.y = out.y / out.w;
	objectCoordinate.z = out.z / out.w;

	return true;
}



Camera::Camera() : mUpdated(true)
{
}

void Camera::setOrtho(int width, int height, float view_width, float zoom, float near_, float far_)
{
	float left, right, bottom, top;
	float ratio = (float) height / width;
	mZoom = zoom;
	
	if( ratio > 1 ) {
		left = -view_width/mZoom;
		right = view_width/mZoom;
		bottom = -view_width*ratio/mZoom;
		top = view_width*ratio/mZoom;
	} else {
		left = -view_width/ratio/mZoom;
		right = view_width/ratio/mZoom;
		bottom = -view_width/mZoom;
		top = view_width/mZoom;
	}
	
	initMatrices();
	
	float div = 1.0f;
	if( mZoom >= 0.99f ) div = mZoom * 2.0f;
	float z = -((far_-near_)/2.0f)/div;
	if( z > -(near_*4) ) z = -(near_*4);
	mProjection.ortho(left, right, bottom, top, near_, far_);
	mProjection.translateSelf(0, 0, z);
	mProjection.rotateSelf(30, 1, 0, 0);
}

void Camera::setPerspective(int width, int height, float size, float near_, float far_)
{
	float ratio = (float) width / height;

	initMatrices();
	
	if( ratio > 1 ) {
		mProjection.frustum(-size * ratio, size * ratio, -size, size, near_, far_);
	} else {
		mProjection.frustum(-size, size, -size / ratio, size / ratio, near_, far_);
	}

	mUpdated = true;
}

void Camera::setPerspective(float fovyInDegrees, float aspectRatio, float znear, float zfar)
{
	float ymax, xmax;
	ymax = znear * tanf(fovyInDegrees * 3.14159265f / 360.0f);
	xmax = ymax * aspectRatio;
	mProjection.frustum(-xmax, xmax, -ymax, ymax, znear, zfar);

	mUpdated = true;
}

void Camera::setLookAt(float eyeX, float eyeY, float eyeZ, float lookX, float lookY, float lookZ, float upX, float upY, float upZ)
{
	mView.setLookAt(eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);

	mUpdated = true;
}

void Camera::setLookAt(const Point3f& eye, const Point3f& look, const Vec3f& up)
{
	mView.setLookAt(eye, look, up);

	mUpdated = true;
}

void Camera::setViewMatrix(const Matrix4f &view)
{
	mView = view;
}

void Camera::setProjectionMatrix(const Matrix4f &projection)
{
	mProjection = projection;
}

void Camera::setModelMatrix(const Matrix4f &model)
{
	mModel = model;
}

const Matrix4f& Camera::getModelMatrix() const
{
	return mModel;
}

const Matrix4f& Camera::getViewMatrix() const
{
	return mView;
}

const Matrix4f& Camera::getProjectionMatrix() const
{
	return mProjection;
}

const Matrix4f& Camera::getModelViewMatrix()
{
	mModelView = mView * mModel;
	return mModelView;
}

const Matrix4f& Camera::getMVPMatrix()
{
	mModelViewProjection = mProjection * getModelViewMatrix();
	return mModelViewProjection;
}

float Camera::getFrustumWidth() const
{
	return (1.0f/mProjection[0]);
}

float Camera::getFrustumHeight() const
{
	return (1.0f/mProjection[5]);
}

Vec3f Camera::getEyeVector() const
{
	Vec4f up(0, 0, 0, 1);
	Vec4f eye = mView.inverse() * up;
	return Vec3f(eye.x/eye.w, eye.y/eye.w, eye.z/eye.w);
}

Vec3f Camera::getLookAtVector() const
{
	return Vec3f(mView[2], mView[6], mView[10]);
}

Vec3f Camera::getUpVector() const
{
	return Vec3f(mView[1], mView[5], mView[9]);
}

Vec3f Camera::getForwardVector()
{
	// Back vector is 2,6,10 - so reverse it
	getModelViewMatrix();
	return Vec3f(-mModelView[2], -mModelView[6], -mModelView[10]);
}

Vec3f Camera::getRightVector() const
{
	return Vec3f(mView[0], mView[4], mView[8]);
}

void Camera::setViewCenter(float x, float y, float z)
{
	mView.setIdentity();
	mView.translateSelf(-x, -y, -z);
	mUpdated = true;
}

void Camera::setViewCenter(const Point3f& p)
{
	mView.setIdentity();
	mView.translateSelf(p);
	mUpdated = true;
}

void Camera::moveView(float x, float y, float z)
{
	mView.translateSelf(-x, -y, -z);
	mUpdated = true;
}

void Camera::moveView(const Vec3f& v)
{
	mView.translateSelf(v);
	mUpdated = true;
}

void Camera::moveViewTo(float x, float y, float z)
{
	float w = mView[15];
	mView[12] = -x * w;
	mView[13] = -y * w;
	mView[14] = -z * w;
	mUpdated = true;
}

void Camera::moveViewTo(const Point3f& p)
{
	float w = mView[15];
	mView[12] = -p.x * w;
	mView[13] = -p.y * w;
	mView[14] = -p.z * w;
	mUpdated = true;
}

void Camera::moveModel(float x, float y, float z)
{
	mModel.translateSelf(x, y, z);
	mUpdated = true;
}

void Camera::moveModel(const Vec3f& v)
{
	mModel.translateSelf(v);
	mUpdated = true;
}

void Camera::moveModelTo(float x, float y, float z)
{
	float w = mModel[15];
	mModel[12] = x * w;
	mModel[13] = y * w;
	mModel[14] = z * w;
	mUpdated = true;
}

void Camera::moveModelTo(const Point3f& p)
{
	float w = mModel[15];
	mModel[12] = p.x * w;
	mModel[13] = p.y * w;
	mModel[14] = p.z * w;
	mUpdated = true;
}

void Camera::rotateView(float angle, float x, float y, float z)
{
	mView.rotateSelf(angle, x, y, z);
	mUpdated = true;
}

void Camera::rotateView(float angle, const Vec3f& v)
{
	mView.rotateSelf(angle, v);
	mUpdated = true;
}

void Camera::rotateViewTo(float angle, float x, float y, float z)
{
	mView[0] = 1; mView[1] = 0; mView[2] = 0; mView[3] = 0;
	mView[4] = 0; mView[5] = 1; mView[6] = 0; mView[7] = 0;
	mView[8] = 0; mView[9] = 0; mView[10] = 1; mView[11] = 0;
	mView.rotateSelf(angle, x, y, z);
	mUpdated = true;
}

void Camera::rotateViewTo(float angle, const Vec3f& v)
{
	mView[0] = 1; mView[1] = 0; mView[2] = 0; mView[3] = 0;
	mView[4] = 0; mView[5] = 1; mView[6] = 0; mView[7] = 0;
	mView[8] = 0; mView[9] = 0; mView[10] = 1; mView[11] = 0;
	mView.rotateSelf(angle, v);
	mUpdated = true;
}

void Camera::moveRight(float delta)
{
	Vec3f v(mView[0], mView[4], mView[8]);
	v = v.unit() * -delta;
	mView.translateSelf(v.x, v.y, v.z);
	mUpdated = true;
}

void Camera::moveForward(float delta)
{
	Vec3f v = getForwardVector().unit() * delta;
	mView.translateSelf(-v.x, -v.y, -v.z);
	mUpdated = true;
}

void Camera::moveLookAt(float delta)
{
	Vec3f v = getLookAtVector().unit() * delta;
	mView.translateSelf(-v.x, -v.y, -v.z);
	mUpdated = true;
}

void Camera::moveUp(float delta)
{
	Vec3f v = getUpVector().unit() * delta;
	mView.translateSelf(-v.x, -v.y, -v.z);
	mUpdated = true;
}

bool Camera::isUpdated()
{
	bool ret = mUpdated;
	mUpdated = false;
	return ret;
}

void Camera::initMatrices()
{
	mView.setIdentity();
	mModel.setIdentity();
	mProjection.setIdentity();

	mUpdated = true;
}

void Camera::setScale(float x, float y, float z)
{
	Matrix4<float> scale;
	scale[0] = x;
	scale[5] = y;
	scale[10] = z;
	mProjection *= scale;
}

void Camera::setScale(const Point3f& p)
{
	Matrix4<float> scale;
	scale[0] = p.x;
	scale[5] = p.y;
	scale[10] = p.z;
	mProjection *= scale;
}
